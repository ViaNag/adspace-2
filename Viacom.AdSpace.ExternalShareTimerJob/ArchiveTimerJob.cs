﻿using Microsoft.SharePoint.Administration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using Viacom.AdSpace.ExternalSharing;
using Viacom.AdSpace.ExternalSharing.Entities;


namespace Viacom.AdSpace.ExternalShareTimerJob
{
    public class ArchiveTimerJob : SPJobDefinition
    {
        # region Constants
        public const string TimerJobTitle = "ArchiveTimerJob";
        # endregion

        #region Timer job Constructors

        public ArchiveTimerJob()
            : base()
        {
        }

        public ArchiveTimerJob(string jobName, SPService service) :
            base(jobName, service, null, SPJobLockType.Job)
        {
            this.Title = TimerJobTitle;
        }

        public ArchiveTimerJob(string jobName, SPWebApplication webapp) :
            base(jobName, webapp, null, SPJobLockType.Job)
        {
            this.Title = TimerJobTitle;
        }

        #endregion

        #region
        /// <summary>
        /// overriden method of timer service to execute
        /// </summary>
        /// <param name="targetInstanceId"></param>
        public override void Execute(Guid targetInstanceId)
        {
            Configuration config = null;
            try
            {
                SPWebApplication webApp = this.Parent as SPWebApplication;
                if (webApp != null)
                {
                    config = WebConfigurationManager.OpenWebConfiguration("/", webApp.Name);
                }
                if (config != null)
                {
                    // Get appsetting section of web config 
                    AppSettingsSection appSettings = config.AppSettings;

                    // Initialize the param object
                    ReadItemCollectionParams param = new ReadItemCollectionParams();

                    // get site url from AppSettings
                    param.Siteurl = appSettings.CurrentConfiguration.AppSettings.Settings[Constants.InternalSite].Value;

                    ExternalSharingFacade extSharingFacade = new ExternalSharingFacade();
                    extSharingFacade.BulkArchival(param);
                }
            }
            catch (Exception ex)
            {
                Logging.LogException(Constants.ExceptionLocation, ex);
            }
        }
        #endregion
    }
}
