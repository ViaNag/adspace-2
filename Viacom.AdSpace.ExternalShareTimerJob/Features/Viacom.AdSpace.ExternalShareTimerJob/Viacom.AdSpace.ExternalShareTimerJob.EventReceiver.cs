using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Viacom;
using Viacom.AdSpace.ExternalShareTimerJob;

namespace Viacom.AdSpace.ExternalShareTimerJob.Features.Feature1
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("d7ca4bac-694f-4330-8267-814f9d450955")]
    public class Feature1EventReceiver : SPFeatureReceiver
    {
        // Uncomment the method below to handle the event raised after a feature has been activated.
        String JobName = "ExternalSharingTimerJob"
;
        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {

            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    SPWebApplication parentWebApp = (SPWebApplication)properties.Feature.Parent;
                    SPSite site = properties.Feature.Parent as SPSite;
                    DeleteExistingJob(JobName, parentWebApp);
                    CreateJob(parentWebApp);
                });
            }
            catch (Exception ex)
            {
                throw ex;
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("ExternalSharingTimerJob", TraceSeverity.Medium, EventSeverity.Information), TraceSeverity.Medium, ex.ToString());
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="site"></param>
        /// <returns></returns>
        private bool CreateJob(SPWebApplication site)
        {
            bool jobCreated = false;
            try
            {
                ExternalSharingTimerJob job = new ExternalSharingTimerJob(JobName, site);
                SPMinuteSchedule schedule = new SPMinuteSchedule();
                schedule.BeginSecond = 0;
                schedule.EndSecond = 59;
                schedule.Interval = 15;
                job.Schedule = schedule;

                job.Update();
            }
            catch (Exception ex)
            {
                return jobCreated;
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("ExternalSharingTimerJob", TraceSeverity.Medium, EventSeverity.Information), TraceSeverity.Medium, ex.ToString());
            }
            return jobCreated;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="jobName"></param>
        /// <param name="site"></param>
        /// <returns></returns>
        public bool DeleteExistingJob(string jobName, SPWebApplication site)
        {
            bool jobDeleted = false;
            try
            {
                foreach (SPJobDefinition job in site.JobDefinitions)
                {
                    if (job.Name == jobName)
                    {
                        job.Delete();
                        jobDeleted = true;
                    }
                }
            }
            catch (Exception)
            {
                return jobDeleted;
            }
            return jobDeleted;
        }
        // Uncomment the method below to handle the event raised before a feature is deactivated.

        public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        {


            lock (this)
            {
                try
                {
                    SPSecurity.RunWithElevatedPrivileges(delegate()
                    {
                        SPWebApplication parentWebApp = (SPWebApplication)properties.Feature.Parent;
                        DeleteExistingJob(this.JobName, parentWebApp);
                    });
                }
                catch (Exception ex)
                {
                    throw ex;
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("ExternalSharingTimerJob", TraceSeverity.Medium, EventSeverity.Information), TraceSeverity.Medium, ex.ToString());
                }
            }
        }


        // Uncomment the method below to handle the event raised after a feature has been installed.

        //public override void FeatureInstalled(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised before a feature is uninstalled.

        //public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
        //{
        //}

        // Uncomment the method below to handle the event raised when a feature is upgrading.

        //public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
        //{
        //}
    }
}
