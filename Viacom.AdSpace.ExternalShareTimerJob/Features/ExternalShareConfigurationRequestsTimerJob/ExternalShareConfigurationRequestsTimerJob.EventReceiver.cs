using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Viacom.AdSpace.ExternalShareTimerJob;

namespace ExternalShareTimerJob.Features.Feature2
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("adace84f-62f1-4fce-9cd5-4758856d0e06")]
    public class Feature2EventReceiver : SPFeatureReceiver
    {// Uncomment the method below to handle the event raised after a feature has been activated.
        String JobName = "ExternalShareConfigurationRequestsTimerJob";
        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {

            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    SPWebApplication parentWebApp = (SPWebApplication)properties.Feature.Parent;
                    SPSite site = properties.Feature.Parent as SPSite;
                    DeleteExistingJob(JobName, parentWebApp);
                    CreateJob(parentWebApp);
                });
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("ExternalShareConfigurationRequestsTimerJob", TraceSeverity.Medium, EventSeverity.Information), TraceSeverity.Medium, ex.ToString());
                throw ex;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="site"></param>
        /// <returns></returns>
        private bool CreateJob(SPWebApplication site)
        {
            bool jobCreated = false;
            try
            {
                ExternalShareConfigurationRequestsTimerJob job = new ExternalShareConfigurationRequestsTimerJob(JobName, site);
                SPDailySchedule schedule = new SPDailySchedule();
                schedule.BeginHour = 1;
                schedule.EndHour = 5;
                job.Schedule = schedule;

                job.Update();
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("ArchiveTimerJob", TraceSeverity.Medium, EventSeverity.Information), TraceSeverity.Medium, ex.ToString());
                return jobCreated;
            }
            return jobCreated;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="jobName"></param>
        /// <param name="site"></param>
        /// <returns></returns>
        public bool DeleteExistingJob(string jobName, SPWebApplication site)
        {
            bool jobDeleted = false;
            try
            {
                foreach (SPJobDefinition job in site.JobDefinitions)
                {
                    if (job.Name == jobName)
                    {
                        job.Delete();
                        jobDeleted = true;
                    }
                }
            }
            catch (Exception)
            {
                return jobDeleted;
            }
            return jobDeleted;
        }
        // Uncomment the method below to handle the event raised before a feature is deactivated.

        public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        {


            lock (this)
            {
                try
                {
                    SPSecurity.RunWithElevatedPrivileges(delegate()
                    {
                        SPWebApplication parentWebApp = (SPWebApplication)properties.Feature.Parent;
                        DeleteExistingJob(this.JobName, parentWebApp);
                    });
                }
                catch (Exception ex)
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("ArchiveTimerJob", TraceSeverity.Medium, EventSeverity.Information), TraceSeverity.Medium, ex.ToString());
                    throw ex;
                }
            }
        }

        // Uncomment the method below to handle the event raised after a feature has been installed.

        //public override void FeatureInstalled(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised before a feature is uninstalled.

        //public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
        //{
        //}

        // Uncomment the method below to handle the event raised when a feature is upgrading.

        //public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
        //{
        //}
    }
}
