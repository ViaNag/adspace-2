﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Configuration;

namespace Viacom.AdSpace.ExternalShareTimerJob
{

    public class LockInActiveUserTimerJob : SPJobDefinition
    {
        # region Constants

        public const string CONSTANT_COLUMN_EMAIL = "Email";
        public const string CONSTANT_LAST_LOGIN = "LastLogin";
        public const string CONSTANT_CREATED = "Created";
        public const string LOCK_USER_SP = "aspnet_Membership_LockInactiveUsers";
        public const string TimerJobName = "LockInActiveUserTimerJob";
        public const string TimerJobTitle = "LockInActiveUserTimerJob";
        public const string SP_PARM = "@MONTHS";
        public const string CONNECTION_STRING_NAME = "extuserdbstrcon";

        # endregion

        # region Global Variables

        Configuration config = null;
        int months = 0;

        # endregion

        # region Timer job Constructors
        public LockInActiveUserTimerJob()
            : base()
        {
        }

        public LockInActiveUserTimerJob(string jobName, SPService service) :
            base(jobName, service, null, SPJobLockType.Job)
        {
            this.Title = TimerJobTitle;
        }

        public LockInActiveUserTimerJob(string jobName, SPWebApplication webapp) :
            base(jobName, webapp, null, SPJobLockType.Job)
        {
            this.Title = TimerJobTitle;
        }
        # endregion

        # region Public Methods

        /// <summary>
        /// overriden method of timer service to execute
        /// </summary>
        /// <param name="targetInstanceId"></param>
        public override void Execute(Guid targetInstanceId)
        {
            try
            {
                SPWebApplication webApp = this.Parent as SPWebApplication;
                if (webApp != null) config = WebConfigurationManager.OpenWebConfiguration("/", webApp.Name);
                if (config != null)
                {
                    AppSettingsSection appSettings = config.AppSettings;
                    ReadConfigurationSettings(appSettings.CurrentConfiguration.AppSettings.Settings);
                    LockUsers();
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory(TimerJobName, TraceSeverity.Medium, EventSeverity.Information), TraceSeverity.Medium, ex.ToString());
            }
        }

        # endregion

        # region Private Methods


        /// <summary>
        /// Method to lock inactive users
        /// </summary>
        /// <param name="dtUsers"></param>
        private void LockUsers()
        {
            int numberofuserlocked;
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    String connectionString = config.ConnectionStrings.ConnectionStrings[CONNECTION_STRING_NAME].ConnectionString;
                    using (SqlConnection sqlConnection = new SqlConnection(connectionString))
                    {
                        SqlCommand command = new SqlCommand();
                        command.CommandText = LOCK_USER_SP;
                        command.CommandType = CommandType.StoredProcedure;
                        command.Connection = sqlConnection;
                        SqlParameter param = new SqlParameter(SP_PARM, months);
                        param.Direction = ParameterDirection.Input;
                        param.DbType = DbType.Int32;
                        command.Parameters.Add(param);
                        sqlConnection.Open();
                        numberofuserlocked = command.ExecuteNonQuery();
                        sqlConnection.Close();
                    }
                });
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory(TimerJobName, TraceSeverity.Medium, EventSeverity.Information), TraceSeverity.Medium, ex.ToString());
            }
        }

        /// <summary>
        /// Read configuration from app settings
        /// </summary>
        /// <param name="keyValueConfigurationCollection"></param>
        private void ReadConfigurationSettings(KeyValueConfigurationCollection keyValueConfigurationCollection)
        {
            if (keyValueConfigurationCollection != null)
            {
                months = Convert.ToInt32(keyValueConfigurationCollection["NumberOfMonths"].Value);
            }
        }

        # endregion
    }
}
