﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="c4d72d51-6d43-43f7-9051-d50bdfd4f668" description="Feature contains viacom adspace list instances" featureId="c4d72d51-6d43-43f7-9051-d50bdfd4f668" imageUrl="" solutionId="00000000-0000-0000-0000-000000000000" title="Viacom.AdSpace.CommonListInstances.RootSite" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <projectItems>
    <projectItemReference itemId="37c4d290-d49b-496f-aa7b-1dc05db80e43" />
    <projectItemReference itemId="77b2d98c-880c-4b77-8782-caa07f56b949" />
    <projectItemReference itemId="63cf9724-e700-458f-92c9-524cfbd96168" />
    <projectItemReference itemId="3b157fbd-4791-4b90-95e1-11c3657b5e6e" />
    <projectItemReference itemId="c8d60121-9e73-4e14-bf29-c6e4fa6b4d58" />
    <projectItemReference itemId="96bf3045-b2d0-4c2c-920c-a5c927e2d53d" />
    <projectItemReference itemId="f6fd8d9d-5eb1-464a-8833-a4f17fa51f73" />
    <projectItemReference itemId="2afadafc-4891-4f64-a91a-3d84c70f9a95" />
  </projectItems>
</feature>