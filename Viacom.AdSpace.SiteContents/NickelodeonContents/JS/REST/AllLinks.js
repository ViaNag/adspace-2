﻿
var Linkselected="0",getselectedLink="0";
var userid;
var siteUrl;
var linkCategory;
var linkCategoryTitle;
var LinkTitle="";
var URL=""
var LinkGroup;
var structure="";
var noItemsflag=1;
var preferenceId;
var thisSite ;
var index;

/**********Method to insert Unique items in array*************/
var LinkGroupArr = new Array(); //array that holds unique link categories
function searchStringInArray (str, strArray) 
{
	    for (var j=0; j<strArray.length; j++) {
	        if (strArray[j].match(str)) return 'found';
	    }
	    return -1;
}

/*********Document.Ready*****************/
$(document).ready(function()
{

	userid = _spPageContextInfo.userId;
	siteUrl= _spPageContextInfo.siteAbsoluteUrl;
	thisSite = $().SPServices.SPGetCurrentSite();
	index = thisSite .lastIndexOf('/');
	thisSite = thisSite.substring(index + 1).toLowerCase();

	displayLinks();
	/**getExpandedLink();
	/**$("#accordion > li").click(function()
	{
		Linkselected=$(this).index().toString();
		updateUserPreference(Linkselected);
	
	});**/
	/*******Accordion Pattern Code***************/
	$(function() 
	{
		$("#accordion > li > div").click(function(){
	 
	    if(false == $(this).next().is(':visible')) {
	        $('#accordion ul').slideUp(300);
	    }
	    $(this).next().slideToggle(300);
		}); 
		
		$('#accordion ul:eq('+getselectedLink+')').show();
	 });
/****************************************************/
 	/*below code to apply css to fetured links section on home page*/
	//$("li[class^='linkTitle']").css("background","none repeat scroll 0% 0% rgb(241, 242, 245)");
	$("li[class^='linkTitle'] > a").css("color","black");	
	$("#LeftFeaturedLinks").css("background","none repeat scroll 0% 0% rgb(241, 242, 245)");
	/*********************************************************************/
});//doc.ready end


/*Below function will be used to display featured links*/
function displayLinks()
{ 
  /****get link categories using REST******************/
  		
	/*******************AJAX (REST) to get Group link categories*************************************/
	$.ajax({ 
		
		/****query gets all global and site specific links where Display On = Featured Link Widget ********/
	    url: _spPageContextInfo.siteAbsoluteUrl + "/_api/Web/Lists/getbytitle('Links')/Items"
	   										+ "?$select=ContentType,DisplayOn,GroupLinkBy/LinkCategory"
	   										+"&$expand=GroupLinkBy/LinkCategory&$filter=(ContentType eq 'Link Category' or ContentType eq 'Site Link')",									 
	   									   
	   type: "GET", 
	   async:false,
	   headers: {"accept": "application/json;odata=verbose"}, 
	   success: function (data) 
	   { 
	      if (data.d.results) 
	      { 
		      for(var i=0;i<data.d.results.length;i++)
			  { 
		         	linkCategory= data.d.results[i].GroupLinkBy.LinkCategory;
		         	linkCategoryTitle=linkCategory.replace(" ","");;
					var ExistOrNot=  searchStringInArray(linkCategory,LinkGroupArr);
					if(ExistOrNot==-1)
					{
						LinkGroupArr.push(linkCategory);
						$("#accordion").append("<li id='"+linkCategoryTitle+"'><div id='"+linkCategoryTitle+"' style='padding-top: 5px'><img style='margin-right: 3px;' src='/images/arrowTrans.jpg' complete='complete'/>"+linkCategory+"</div><ul style='margin-left:10px'></ul></li>");
					} 
			        //alert('handle the data'+LinkGroupArr); 
		      }//for loop
	      }// if(data.d.results) 
	   },//success 
	   error: function (xhr) { 
	      alert(xhr.status + ': ' + xhr.responseText); 
	   } 
	}); 
  /****************REST to get CATEGORIES ebds here************/
  /****************************************************************************/
  /*********Below SPservice will get all the links whose display On value is "Featured Link Widget" to display in featured link section*********/
  $().SPServices
  ({
	    operation: "GetListItems",
	    webURL : siteUrl,
	    async: false,
	    listName: "Links",
	    viewName: "All Links",//All links view
	    CAMLQuery: "<Query>"
		    		  +"<Where>"
			    		  +"<Or>"
				    		  +"<Membership Type='CurrentUserGroups'> "
				    		  +"<FieldRef Name='AccessTo' />"
				    		  +"</Membership>"
			      			  +"<Eq>"
			         		  +"<FieldRef Name='AccessTo' />"
			         		  +"<Value Type='Integer'><UserID /></Value>"
			      			  +"</Eq>"
			    		  +"</Or>"
	    		  +"</Where><OrderBy><FieldRef Name='GroupLinkBy' Ascending='True' /></OrderBy></Query>",
 
	    completefunc: function (xData, Status) 
	    {
			 $(xData.responseXML).SPFilterNode("z:row").each(function() 
			 {
			     var title =$(this).attr("ows_Title") ;   
			     var itemUrl=$(this).attr("ows_Url");
			     var itemID =$(this).attr("ows_ID") ; 
			     var ContactPerson =$(this).attr("ows_ContactPerson").split(";#")[1] ; 
			     var PurposeOfApplication =$(this).attr("ows_Purpose_x0020_Of_x0020_Applicati") ; 
			     
			     var siteName=$(this).attr("ows_SiteName");
				 var showOnHome =$(this).attr("ows_ShowOnHomePage") ;  
				 var groupLinkCategory=$(this).attr("ows_GroupLinkBy").split(";#")[1];
				 groupLinkCategory=groupLinkCategory.replace(" ","");
		         var contenType=$(this).attr("ows_ContentType");
		        
		    	 var displayOn=$(this).attr("ows_DisplayOn");
				
					if(contenType == "Site Link")
					{
							//var sitename=$(this).attr("ows_SiteName").toLowerCase();
							if(PurposeOfApplication!=undefined)
							{
$("li#"+groupLinkCategory+" > ul").append("<li class='AllLinksLi' id='linkTitleLI"+groupLinkCategory+"'><a href='"+itemUrl+"'>"+title +"</a><span style='padding:3px;font-size:11px;'>"+ContactPerson+"<hr style='border=1px grey dotted'/>"+PurposeOfApplication +"</span></li>");
							}
							else
							{
								$("li#"+groupLinkCategory+" > ul").append("<li class='AllLinksLi' id='linkTitleLI"+groupLinkCategory+"'><a href='"+itemUrl+"'>"+title +"</a><span style='padding:3px;font-size:11px;'>"+ContactPerson+"</span></li>");												
							}
							
						    /**if(thisSite == "adspaceprototype" && showOnHome == '1')
							{											
								$("li#"+groupLinkCategory+" > ul").append("<li class='linkTitleLI"+groupLinkCategory+"'><a href='"+itemUrl+"'>"+title +"</a></li>");												
							}
							if(thisSite == sitename)
							{										
								$("li#"+groupLinkCategory+" > ul").append("<li class='linkTitleLI"+groupLinkCategory+"'><a href='"+itemUrl+"'>"+title +"</a></li>");	
							}**/
					}
					else
					{									
						if(PurposeOfApplication!=undefined)
							{
							$("li#"+groupLinkCategory+" > ul").append("<li class='AllLinksLi' id='linkTitleLI"+groupLinkCategory+"'><a href='"+itemUrl+"'>"+title +"</a><span style='padding:3px;font-size:11px;'>"+ContactPerson+"<hr style='border=1px grey dotted'/>"+PurposeOfApplication +"</span></li>");
							}
							else
							{
								$("li#"+groupLinkCategory+" > ul").append("<li class='AllLinksLi' id='linkTitleLI"+groupLinkCategory+"'><a href='"+itemUrl+"'>"+title +"</a><span style='padding:3px;font-size:11px;'>"+ContactPerson+"</span></li>");												
							}
					}
      		});
    	}
  	});

}//Display Links Ends Here 
/******************************************Display Links Ends Here***************************************************/

/***Below function will be used to update the user preference list based on the last selected link of the user***/
function updateUserPreference(Linkselected,curUserId)
{
	/*******Check if user Preferences list is empty, set noItemsflag=0 if empty ****/
	$.ajax({ 
	   url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('UserPreferences')", 
	   type: "GET", 
	   headers: {"accept": "application/json;odata=verbose"}, 
	      async: false,
	   success: function (data) {               
	                 if(data.d.ItemCount==0)
	                 	noItemsflag=0;
	                 	   }, 
	   error: function (xhr) { 
	      alert(xhr.status + ': ' + xhr.statusText); 
	   } 
	}); 
	
	/*****if user preferences list is not empty*****/
	if(noItemsflag==1)
	{
			$.ajax({ 
			   url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('UserPreferences')/items"
			   										 + "?$select=ID,AdSpaceUserName/ID" + "&$expand=AdSpaceUserName/ID&$filter=(AdSpaceUserName/ID eq " + userid + ")",
			   type: "GET", 
			   headers: {"accept": "application/json;odata=verbose"}, 
			   async: false,
			   success: function (data) { 
			
			                 if(data.d.results.length==0)
									//noItemsCurrentUserflag=0;//create new item
									CreateItem();
			                 else
			                 {
			                 
			                 preferenceId=data.d.results[0].ID;
			                 UpdateItem(preferenceId);

			                 }
			                    }, 
			   error: function (xhr) { 
			      alert(xhr.status + ': ' + xhr.statusText); 
			   } 
			});

	}//noOfItems Check ends here

}//updateUserPreference
/*******************************updateUserPreference ends here*******************************************************/


/*************Retrieve  the last expanded link of the user using REST*****************/
function getExpandedLink()
{	
	/*********REST to get last selected link******/
	$.ajax({ 
			   url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('UserPreferences')/items"
			   										 + "?$select=ID,AdSpaceUserName/ID,FeaturedLinks" + "&$expand=AdSpaceUserName/ID&$filter=(AdSpaceUserName/ID eq " + userid + ")",
			   type: "GET", 
			   headers: {"accept": "application/json;odata=verbose"}, 
			   async: false,
			   success: function (data) { 
			
			                 if(data.d.results.length==0)
			                 {
									CreateItem();
									getselectedLink="0";
							}
			                 else
			                 {
			                 	getselectedLink=data.d.results[0].FeaturedLinks;
			                 }
			                    }, 
			   error: function (xhr) { 
			      alert(xhr.status + ': ' + xhr.statusText); 
			   } 
			});

	}
	
/*****************************METHOD to create preference for last selected link**************************************************/
function CreateItem()
{
	
	$.ajax({ 
		url:_spPageContextInfo.webAbsoluteUrl+"/_api/web/lists/getbytitle('UserPreferences')/items",
		 type: "POST",
		    async: false,
	        data:  JSON.stringify({ '__metadata': { 'type': 'SP.Data.UserPreferencesListItem' },'AdSpaceUserNameId': userid,'FeaturedLinks':Linkselected}),
	        headers: 
	        { 
	            "accept": "application/json;odata=verbose",
	            "content-type":"application/json;odata=verbose",
	            "X-RequestDigest": $("#__REQUESTDIGEST").val()
	        },
	        success: function(data) {},
	       error: function (xhr) { 
	      alert(xhr.status + ': ' + xhr.statusText); 
	   }
	}); 

}

/*****************************METHOD to update last selected link*********************************************************/
function UpdateItem(itemId)
{
	
	$.ajax
	({ 
		url:_spPageContextInfo.webAbsoluteUrl+"/_api/web/lists/getbytitle('UserPreferences')/items("+itemId+")",
		 type: "POST",
	     async: false,
		 data:  JSON.stringify({ '__metadata': { 'type': 'SP.Data.UserPreferencesListItem' },'FeaturedLinks':Linkselected}),
	     headers: 
	     {           	
	        "X-RequestDigest": $("#__REQUESTDIGEST").val(), 
	        	"IF-MATCH":"*",
	        	"X-HTTP-Method":"MERGE",
	            "accept": "application/json;odata=verbose",
	            "content-type":"application/json;odata=verbose",
	            
	       },
	      success: function(data) {},
	      error: function (xhr) { 
	      alert(xhr.status + ': ' + xhr.responseText); 
	   }
	}); 
}
/**************************************************************************************/