﻿if (!window.console) {
    console = { log: function () { } };
}

var Viacom = Viacom || {};
Viacom.AdSpace = Viacom.AdSpace || {};
Viacom.AdSpace.ManageBookMark = Viacom.AdSpace.ManageBookMark || {};

Viacom.AdSpace.ManageBookMark.Constants = function () {
    return {
        listNm: null,
        grpCTName: null,
        addBookmarkToGrp: null,
        listContentTypes: null,
        bookmarkLstColNm: null,
        addBookmarkToGrpId: null,
        bookmarkLstCTName: null
    }
};

$(document).ready(function () {
    SP.SOD.executeFunc('sp.js', 'SP.ClientContext',Viacom.AdSpace.ManageBookMark.Initialize);
});

Viacom.AdSpace.ManageBookMark.Initialize = function () {
    Viacom.AdSpace.ManageBookMark.Constants.listNm = "Bookmark List";
    Viacom.AdSpace.ManageBookMark.Constants.grpCTName = "BookmarkGroups";
    Viacom.AdSpace.ManageBookMark.Constants.bookmarkLstCTName = "Bookmark List";
    Viacom.AdSpace.ManageBookMark.Constants.addBookmarkToGrp = null;
    Viacom.AdSpace.ManageBookMark.Constants.listContentTypes = null;
    Viacom.AdSpace.ManageBookMark.Constants.addBookmarkToGrpId = null;
    Viacom.AdSpace.ManageBookMark.Constants.bookmarkLstColNm = {};
    Viacom.AdSpace.ManageBookMark.Constants.bookmarkLstColNm.UserGrp = "UserGroup";
    Viacom.AdSpace.ManageBookMark.Constants.bookmarkLstColNm.CTId = "ContentTypeId";
    Viacom.AdSpace.ManageBookMark.Constants.bookmarkLstColNm.Title = "Title";
    Viacom.AdSpace.ManageBookMark.Constants.bookmarkLstColNm.Url = "Url";
    Viacom.AdSpace.ManageBookMark.Constants.bookmarkLstColNm.GrpNm = "GroupName";
    $("#ddlBookmarkLnk").unbind('change').change(Viacom.AdSpace.ManageBookMark.EditBookmarkLnkGrpOnChange);
};

Viacom.AdSpace.ManageBookMark.EditBookmarkLnkGrpOnChange = function () {
    try {
        var select_group_val = $("#ddlBookmarkLnk").find("option:selected").attr("value");
        if (select_group_val == 'AddNewGroup') {
            $("#ddlBookmarkLnk").parents('.edit-bookmark-form').children('.add-group-field').show();
        }
        else {
            $('.add-group-field').hide();
        }
    }
    catch (e) {
        $("#dialog").dialog("close");
        //alert("Exception while onchange event of Group Dropdown");
        console.log("Exception while onchange event of Group Dropdown");
    }
};

Viacom.AdSpace.ManageBookMark.EditBookmarkGrpAddBtn = function (grpTitle) {
    try {
        var grpValue = $("#txtEditBookmarkGrp").val();
        if (grpValue == "" || grpValue === null || grpValue === undefined) {
            //alert("Group name cannot be empty.");
            console.log("Group name cannot be empty.");
            $("#editGroupValidationMsg").empty();
            $("#editGroupValidationMsg").append("<br/>Group name cannot be empty.");
            //$("#edit-group").dialog("close");
        }
        else if (grpValue !== null || grpValue !== undefined) {
            if (grpValue.toLowerCase() === grpTitle.toLowerCase()) {
                //alert("Editing group name required different name as comapared to existing one");
                console.log("Editing group name required different name as comapared to existing one.");
                $("#editGroupValidationMsg").empty();
                $("#editGroupValidationMsg").append("<br/>Editing group name required different name as comapared to existing one.");
                //$("#edit-group").dialog("close");
            }
            else {
                var editGrpCtx = SP.ClientContext.get_current();
                var rootWeb = editGrpCtx.get_site().get_rootWeb();
                var oList = rootWeb.get_lists().getByTitle(Viacom.AdSpace.ManageBookMark.Constants.listNm);
                var camlQuery = new SP.CamlQuery();
                var query = "<View><Query><Where><And><Eq><FieldRef Name='Author' LookupId='True'/><Value Type='Lookup'>" + _spPageContextInfo.userId + "</Value></Eq><And><Eq><FieldRef Name='ContentType'/><Value Type='Computed'>" + Viacom.AdSpace.ManageBookMark.Constants.grpCTName + "</Value></Eq><Eq><FieldRef Name='UserGroup'/><Value Type='Text'>" + grpTitle + "</Value></Eq></And></And></Where></Query></View>";
                camlQuery.set_viewXml(query);
                var collListItem = oList.getItems(camlQuery);
                editGrpCtx.load(collListItem);

                var camlQueryNew = new SP.CamlQuery();
                var queryNew = "<View><Query><Where><And><Eq><FieldRef Name='Author' LookupId='True'/><Value Type='Lookup'>" + _spPageContextInfo.userId + "</Value></Eq><And><Eq><FieldRef Name='ContentType'/><Value Type='Computed'>" + Viacom.AdSpace.ManageBookMark.Constants.grpCTName + "</Value></Eq><Eq><FieldRef Name='UserGroup'/><Value Type='Text'>" + grpValue + "</Value></Eq></And></And></Where></Query></View>";
                camlQueryNew.set_viewXml(queryNew);
                var collListItemNew = oList.getItems(camlQueryNew);
                editGrpCtx.load(collListItemNew);

                editGrpCtx.executeQueryAsync(
                      function (sender, args) {
                          var itemsCount = collListItem.get_count();
                          var itemsCountNew = collListItemNew.get_count();
                          if (itemsCountNew > 0) {
                              //alert("New group name already exist.");
                              console.log("New group name already exist.");
                              $("#editGroupValidationMsg").empty();
                              $("#editGroupValidationMsg").append("<br/>New group name already exist.");
                              //$("#edit-group").dialog("close");
                          }
                          else {
                              if (itemsCount == 1) {
                                  var listItemEnumerator = collListItem.getEnumerator();
                                  while (listItemEnumerator.moveNext()) {
                                      var oListItem = listItemEnumerator.get_current();
                                      oListItem.set_item("UserGroup", $("#txtEditBookmarkGrp").val());
                                      oListItem.update();
                                      editGrpCtx.executeQueryAsync(function () {
                                          $("#edit-group").dialog("close");
                                          //alert("Bookmark group name edited successfully");
                                          Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Bookmark group name edited successfully");
                                          AllBookmarks.loadBookMarkSection();
                                          $('.add-group-field').hide();
                                          $("#txtLnkBookmarkUrl").val("");
                                      }, function (sender, args) {
                                          $("#edit-group").dialog("close");
                                          //alert("Error in while updating list item");
                                          console.log('Error in while updating list item');
                                          $('.add-group-field').hide();
                                          $("#txtLnkBookmarkUrl").val("");
                                          console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                                          Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Failed to edit Bookmark group.");
                                      });
                                  }
                              }
                              else {
                                  //alert("More than one group exist with this title in Bookmark list");
                                  console.log("More than one group exist with this title in Bookmark list");
                                  $("#edit-group").dialog("close");
                                  $('.add-group-field').hide();
                                  $("#txtLnkBookmarkUrl").val("");
                              }
                          }
                      }, function (sender, args) {
                          $("#edit-group").dialog("close");
                          //alert("Error while fetching data from Bookmark list");
                          console.log("Error while fetching data from Bookmark list");
                          $('.add-group-field').hide();
                          $("#txtLnkBookmarkUrl").val("");
                          console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                          Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Failed to edit Bookmark group.");
                      });
            }
        }
    }
    catch (e) {
        $("#edit-group").dialog("close");
        //alert("Error: Exception while renaming group for current user");
        console.log("Error: Exception while renaming group for current user");
        $('.add-group-field').hide();
        $("#txtLnkBookmarkUrl").val("");
        Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Failed to edit Bookmark group.");
    }
};

Viacom.AdSpace.ManageBookMark.EditBookmarkGrp = function (grpTitle) {
    try {
        $("#editGroupValidationMsg").empty();
        $("#txtEditBookmarkGrp").val(grpTitle);
        $("#edit-group").dialog({
            width: 450,
            autoOpen: true,
            modal: true,
            draggable: false,
            buttons: {
                Add: function () {
                    Viacom.AdSpace.ManageBookMark.EditBookmarkGrpAddBtn(grpTitle);
                },
                Cancel: function () {
                    $(this).dialog("close");
                    $('.add-group-field').hide();
                    $("#txtLnkBookmarkUrl").val("");
                }
            }
        });
    }
    catch (e) {
        $("#edit-group").dialog("close");
        //alert("Error: Exception while opening dialog.");
        console.log("Error: Exception while opening dialog.");
        $('.add-group-field').hide();
        $("#txtLnkBookmarkUrl").val("");
    }
};

Viacom.AdSpace.ManageBookMark.DeleteBookmarkGrpOKBtn = function (grpTitle) {
    try {
        var delGrpCtx = SP.ClientContext.get_current();
        var rootWeb = delGrpCtx.get_site().get_rootWeb();
        var oList = rootWeb.get_lists().getByTitle(Viacom.AdSpace.ManageBookMark.Constants.listNm);
        var camlQuery = new SP.CamlQuery();
        var query = "<View><Query><Where><And><Eq><FieldRef Name='Author' LookupId='True'/><Value Type='Lookup'>" + _spPageContextInfo.userId + "</Value></Eq><And><Eq><FieldRef Name='ContentType'/><Value Type='Computed'>" + Viacom.AdSpace.ManageBookMark.Constants.grpCTName + "</Value></Eq><Eq><FieldRef Name='UserGroup'/><Value Type='Text'>" + grpTitle + "</Value></Eq></And></And></Where></Query></View>";
        camlQuery.set_viewXml(query);
        var collListItem = oList.getItems(camlQuery);
        delGrpCtx.load(collListItem);
        delGrpCtx.executeQueryAsync(
              function (sender, args) {
                  var itemsCount = collListItem.get_count();
                  if (itemsCount == 1) {
                      var listItemEnumerator = collListItem.getEnumerator();
                      var oListItem = null;
                      while (listItemEnumerator.moveNext()) {
                          oListItem = listItemEnumerator.get_current();
                      }
                      oListItem.deleteObject();
                      delGrpCtx.executeQueryAsync(
                          function (sender, args) {
                              //alert("Group deleted successfully");
                              $("#group-del").dialog("close");
                              Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Bookmark group deleted successfully");
                              AllBookmarks.loadBookMarkSection();
                              $('.add-group-field').hide();
                              $("#txtLnkBookmarkUrl").val("");
                          }, function (sender, args) {
                              $("#group-del").dialog("close");
                              //alert("Error while deleting group from Bookmark list");
                              console.log("Error while deleting group from Bookmark list");
                              $('.add-group-field').hide();
                              $("#txtLnkBookmarkUrl").val("");
                              console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                              Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Failed to delete Bookmark group.");
                          });
                  }
                  else {
                      //alert("More than one group exist for current user in Bookmark list");
                      console.log("More than one group exist for current user in Bookmark list");
                      $("#group-del").dialog("close");
                      $('.add-group-field').hide();
                      $("#txtLnkBookmarkUrl").val("");
                  }
              }, function (sender, args) {
                  $("#group-del").dialog("close");
                  //alert("Error while fetching data from Bookmark list");
                  console.log("Error while fetching data from Bookmark list");
                  $('.add-group-field').hide();
                  $("#txtLnkBookmarkUrl").val("");
                  console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                  Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Failed to delete Bookmark group.");
              });
    }
    catch (e) {
        $("#group-del").dialog("close");
        $('.add-group-field').hide();
        $("#txtLnkBookmarkUrl").val("");
        //alert("Error: Exception while deleting group for current user.");
        console.log("Error: Exception while deleting group for current user.");
        Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Failed to delete Bookmark group.");
    }
};

Viacom.AdSpace.ManageBookMark.DeleteBookmarkGrp = function (grpTitle) {
    try {
        $("#group-del").dialog({
            width: 450,
            autoOpen: true,
            modal: true,
            draggable: false,
            buttons: {
                Ok: function () {
                    Viacom.AdSpace.ManageBookMark.DeleteBookmarkGrpOKBtn(grpTitle);
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });
    }
    catch (e) {
        $("#group-del").dialog("close");
        //alert("Error: Exception while opening dialog.");
        console.log("Error: Exception while opening dialog.");
        $('.add-group-field').hide();
        $("#txtLnkBookmarkUrl").val("");
    }
};

Viacom.AdSpace.ManageBookMark.EditBookmarkLinkAddBtnReAdd = function (id, linkTitle, linkUrl, grpNm, bookMarkType, newGrpName, grpItemid) {
    try {
        if ((bookMarkType === "NoGroup" && newGrpName === null && grpItemid === null)) {
            var newlinkTitle = $("#txtLnkBookmarkTitle").val();
            var newlinkUrl = $("#txtLnkBookmarkUrl").val();
            if (!(newlinkTitle === "" || newlinkUrl === "" || newlinkTitle === null || newlinkUrl === null || newlinkTitle === undefined || newlinkUrl === undefined)) {
                var delLinkCtx = SP.ClientContext.get_current();
                var rootWeb = delLinkCtx.get_site().get_rootWeb();
                var oList = rootWeb.get_lists().getByTitle(Viacom.AdSpace.ManageBookMark.Constants.listNm);
                var oListItem = oList.getItemById(id);
                oListItem.deleteObject();
                delLinkCtx.load(oList);
                delLinkCtx.executeQueryAsync(
                    function (sender, args) {
                        var newlinkTitle = $("#txtLnkBookmarkTitle").val();
                        var newlinkUrl = $("#txtLnkBookmarkUrl").val();
                        Viacom.AdSpace.ManageBookMark.Constants.listContentTypes = oList.get_contentTypes();
                        delLinkCtx.load(Viacom.AdSpace.ManageBookMark.Constants.listContentTypes);
                        var item = oList.addItem();
                        item.set_item(Viacom.AdSpace.ManageBookMark.Constants.bookmarkLstColNm.Title, newlinkTitle);
                        var bookmarkUrl = new SP.FieldUrlValue();
                        bookmarkUrl.set_url(newlinkUrl);
                        bookmarkUrl.set_description(newlinkUrl);
                        item.set_item(Viacom.AdSpace.ManageBookMark.Constants.bookmarkLstColNm.Url, bookmarkUrl);
                        item.update();
                        delLinkCtx.load(item);
                        delLinkCtx.executeQueryAsync(function () {
                            var ct_enumerator = Viacom.AdSpace.ManageBookMark.Constants.listContentTypes.getEnumerator();
                            while (ct_enumerator.moveNext()) {
                                var ct = ct_enumerator.get_current();
                                if (ct.get_name().toString() === Viacom.AdSpace.ManageBookMark.Constants.bookmarkLstCTName) {
                                    item.set_item(Viacom.AdSpace.ManageBookMark.Constants.bookmarkLstColNm.CTId, ct.get_id().toString());
                                    item.update();
                                    delLinkCtx.executeQueryAsync(function () {
                                        $("#dialog").dialog("close");
                                        //alert("Bookmark link updated successfully");
                                        Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Bookmark link updated successfully");
                                        AllBookmarks.loadBookMarkSection();
                                        $('.add-group-field').hide();
                                        $("#bookmarkNewGrp").val("");
                                    }, function (sender, args) {
                                        $("#dialog").dialog("close");
                                        //alert("Error in while updating list item");
                                        console.log("Error in while updating list item");
                                        $('.add-group-field').hide();
                                        $("#bookmarkNewGrp").val("");
                                        console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                                    });
                                }
                            }
                        }, function (sender, args) {
                            $("#dialog").dialog("close");
                            //alert("Error while making call to funtion to make entry in BookMark list");
                            console.log("Error while making call to funtion to make entry in BookMark list");
                            $('.add-group-field').hide();
                            $("#bookmarkNewGrp").val("");
                            console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                            Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Failed to edit Bookmark link.");
                        });
                    }, function (sender, args) {
                        $("#dialog").dialog("close");
                        //alert("Error while re-adding bookmark link to Bookmark list");
                        console.log("Error while re-adding bookmark link to Bookmark list");
                        $('.add-group-field').hide();
                        $("#txtLnkBookmarkUrl").val("");
                        console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                        Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Failed to edit Bookmark link.");
                    });
            }
            else {
                //$("#dialog").dialog("close");
                //alert("Bookmark field cannot be empty.");
                console.log("Bookmark field cannot be empty.");
                $("#editBookmarkValidationMsg").empty();
                $("#editBookmarkValidationMsg").append("<br/>Bookmark field cannot be empty.");
                //$('.add-group-field').hide();
                //$("#txtLnkBookmarkUrl").val("");
                AllBookmarks.loadBookMarkSection();
            }
        }
    }
    catch (e) {
        $("#dialog").dialog("close");
        //alert("Error: Exception while re-adding bookmark link.");
        console.log("Error: Exception while re-adding bookmark link.");
        $('.add-group-field').hide();
        $("#txtLnkBookmarkUrl").val("");
        Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Failed to edit Bookmark link.");
    }
}

Viacom.AdSpace.ManageBookMark.EditBookmarkLinkAddBtn = function (id, linkTitle, linkUrl, grpNm, bookMarkType, newGrpName, grpItemid) {
    try {
        if (!((newGrpName === "" || newGrpName === null || newGrpName === undefined) && (bookMarkType !== "NoGroup"))) {
            var newlinkTitle = $("#txtLnkBookmarkTitle").val();
            var newlinkUrl = $("#txtLnkBookmarkUrl").val();
            if (!(newlinkTitle === "" || newlinkUrl === "" || newlinkTitle === null || newlinkUrl === null || newlinkTitle === undefined || newlinkUrl === undefined)) {
                var clientContext = SP.ClientContext.get_current();
                var rootWeb = clientContext.get_site().get_rootWeb();
                var oList = rootWeb.get_lists().getByTitle(Viacom.AdSpace.ManageBookMark.Constants.listNm);
                var oListItem = oList.getItemById(id);
                var bookmarkUrl = new SP.FieldUrlValue();
                bookmarkUrl.set_url(newlinkUrl);
                bookmarkUrl.set_description(newlinkUrl);
                oListItem.set_item(Viacom.AdSpace.ManageBookMark.Constants.bookmarkLstColNm.Url, bookmarkUrl);
                oListItem.set_item(Viacom.AdSpace.ManageBookMark.Constants.bookmarkLstColNm.Title, newlinkTitle);
                switch (bookMarkType) {
                    case "NewGroup": {
                        var grpLookupId = new SP.FieldLookupValue();
                        grpLookupId.set_lookupId(grpItemid);
                        oListItem.set_item(Viacom.AdSpace.ManageBookMark.Constants.bookmarkLstColNm.GrpNm, grpLookupId);
                        break;
                    }
                    case "NoGroup": {
                        break;
                    }
                    case "WithGroup": {
                        var grpLookupId = new SP.FieldLookupValue();
                        grpLookupId.set_lookupId(grpItemid);
                        oListItem.set_item(Viacom.AdSpace.ManageBookMark.Constants.bookmarkLstColNm.GrpNm, grpLookupId);
                        break;
                    }
                }
                oListItem.update();
                clientContext.load(oListItem);
                clientContext.executeQueryAsync(function () {
                    $("#dialog").dialog("close");
                    //alert("Bookmark link updated successfully");
                    Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Bookmark link updated successfully");
                    AllBookmarks.loadBookMarkSection();
                    $('.add-group-field').hide();
                    $("#txtLnkBookmarkUrl").val("");
                }, function (sender, args) {
                    $("#dialog").dialog("close");
                    //alert("Error while making call to funtion to update entry in BookMark list");
                    console.log("Error while making call to funtion to update entry in BookMark list");
                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                    Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Failed to edit Bookmark link.");
                });
            }
            else {
                //$("#dialog").dialog("close");
                //alert("Bookmark field cannot be empty.");
                console.log("Bookmark field cannot be empty.");
                $("#editBookmarkValidationMsg").empty();
                $("#editBookmarkValidationMsg").append("<br/>Bookmark field cannot be empty.");
                //$('.add-group-field').hide();
                //$("#txtLnkBookmarkUrl").val("");
                AllBookmarks.loadBookMarkSection();
            }
        }
        else {
            //$("#dialog").dialog("close");
            //alert("Group name cannot be empty.");
            console.log("Group name cannot be empty.");
            $("#editBookmarkValidationMsg").empty();
            $("#editBookmarkValidationMsg").append("<br/>Group name cannot be empty.");
            //$('.add-group-field').hide();
            //$("#txtLnkBookmarkUrl").val("");
            AllBookmarks.loadBookMarkSection();
        }
    }
    catch (e) {
        $("#dialog").dialog("close");
        //alert("Error: Exception while editing bookmark link.");
        console.log("Error: Exception while editing bookmark link.");
        $('.add-group-field').hide();
        $("#txtLnkBookmarkUrl").val("");
        Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Failed to edit Bookmark link.");
    }
};

Viacom.AdSpace.ManageBookMark.EditBookmarkLinkCreateGrpAddBtn = function (id, linkTitle, linkUrl, grpNm) {
    try {
        var ddlSelVal = $("#ddlBookmarkLnk").find("option:selected").attr("value");
        switch (ddlSelVal) {
            case "AddNewGroup": {
                var bookMarkNewGroup = $("#txtLnkBookmarkNewGrp").val();
                if (!(bookMarkNewGroup === "" || bookMarkNewGroup === null || bookMarkNewGroup === undefined)) {
                    Viacom.AdSpace.ManageBookMark.Constants.addBookmarkToGrp = bookMarkNewGroup;
                    var groupCtx = SP.ClientContext.get_current();
                    var rootWeb = groupCtx.get_site().get_rootWeb();
                    var oList = rootWeb.get_lists().getByTitle(Viacom.AdSpace.ManageBookMark.Constants.listNm);
                    Viacom.AdSpace.ManageBookMark.Constants.listContentTypes = oList.get_contentTypes();
                    groupCtx.load(Viacom.AdSpace.ManageBookMark.Constants.listContentTypes);
                    var item = oList.addItem();
                    item.set_item(Viacom.AdSpace.ManageBookMark.Constants.bookmarkLstColNm.UserGrp, bookMarkNewGroup);
                    item.update();
                    groupCtx.load(item);
                    groupCtx.executeQueryAsync(function () {
                        var ct_enumerator = Viacom.AdSpace.ManageBookMark.Constants.listContentTypes.getEnumerator();
                        while (ct_enumerator.moveNext()) {
                            var ct = ct_enumerator.get_current();
                            if (ct.get_name().toString() === Viacom.AdSpace.ManageBookMark.Constants.grpCTName) {
                                item.set_item(Viacom.AdSpace.ManageBookMark.Constants.bookmarkLstColNm.CTId, ct.get_id().toString());
                                item.update();
                                groupCtx.executeQueryAsync(function () {
                                    console.log("New group created");
                                    Viacom.AdSpace.ManageBookMark.Constants.addBookmarkToGrpId = item.get_id();
                                    Viacom.AdSpace.ManageBookMark.EditBookmarkLinkAddBtn(id, linkTitle, linkUrl, grpNm, "NewGroup", Viacom.AdSpace.ManageBookMark.Constants.addBookmarkToGrp, Viacom.AdSpace.ManageBookMark.Constants.addBookmarkToGrpId);
                                }, function (sender, args) {
                                    $("#dialog").dialog("close");
                                    //alert("Error in while updating list item");
                                    console.log("Error in while updating list item");
                                    $('.add-group-field').hide();
                                    $("#txtLnkBookmarkUrl").val("");
                                    console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                                    Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Failed to edit Bookmark link.");
                                });
                            }
                        }
                    }, function (sender, args) {
                        $("#dialog").dialog("close");
                        //alert("Error while making call to funtion to make entry in BookMark list for new group");
                        console.log("Error while making call to funtion to make entry in BookMark list for new group");
                        $('.add-group-field').hide();
                        $("#txtLnkBookmarkUrl").val("");
                        console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                        Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Failed to edit Bookmark link.");
                    });
                }
                else {
                    //$("#dialog").dialog("close");
                    console.log("Group name cannot be empty.");
                    $("#editBookmarkValidationMsg").empty();
                    $("#editBookmarkValidationMsg").append("<br/>Group name cannot be empty.");
                    //alert("Group name cannot be empty.");
                    //$('.add-group-field').hide();
                    //$("#txtLnkBookmarkUrl").val("");
                }
                break;
            }
            case "UniqueGroup": {
                Viacom.AdSpace.ManageBookMark.EditBookmarkLinkAddBtnReAdd(id, linkTitle, linkUrl, grpNm, "NoGroup", null, null);
                break;
            }
            default: {
                Viacom.AdSpace.ManageBookMark.Constants.addBookmarkToGrp = $("#ddlBookmarkLnk option:selected").text();
                Viacom.AdSpace.ManageBookMark.Constants.addBookmarkToGrpId = ddlSelVal;
                Viacom.AdSpace.ManageBookMark.EditBookmarkLinkAddBtn(id, linkTitle, linkUrl, grpNm, "WithGroup", Viacom.AdSpace.ManageBookMark.Constants.addBookmarkToGrp, ddlSelVal);
            }
        }
    }
    catch (e) {
        $("#dialog").dialog("close");
        //alert("Error: Exception while editing bookmark link.");
        console.log("Error: Exception while editing bookmark link.");
        $('.add-group-field').hide();
        $("#txtLnkBookmarkUrl").val("");
        Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Failed to edit Bookmark link.");
    }
};

Viacom.AdSpace.ManageBookMark.EditBookmarkLinkValidateAddButton = function (id, linkTitle, linkUrl, grpNm) {
    $("#editBookmarkValidationMsg").empty();
    var newBookmarklinkTitle = $("#txtLnkBookmarkTitle").val();
    var newBookmarklinkUrl = $("#txtLnkBookmarkUrl").val();
    var newGroupName = $("#txtLnkBookmarkNewGrp").val();
    var ddlSelectedValue = $("#ddlBookmarkLnk").find("option:selected").attr("value");
    if (newBookmarklinkTitle === "" || newBookmarklinkTitle === null || newBookmarklinkTitle === undefined) {
        $("#editBookmarkValidationMsg").append("<br/>Please provide a Bookmark display name.");
    }
    if (newBookmarklinkUrl === "" || newBookmarklinkUrl === null || newBookmarklinkUrl === undefined || newBookmarklinkUrl == "http://" || newBookmarklinkUrl == "https://") {
        $("#editBookmarkValidationMsg").append("<br/>The URL field cannot be left blank.");
    }
    if (ddlSelectedValue === "AddNewGroup" && (newGroupName === "" || newGroupName === null || newGroupName === undefined)) {
        $("#editBookmarkValidationMsg").append("<br/>Group name cannot be empty.");
    }
    if ($("#editBookmarkValidationMsg").html() === "" || $("#editBookmarkValidationMsg").html() === null || $("#editBookmarkValidationMsg").html() === undefined) {
        Viacom.AdSpace.ManageBookMark.EditBookmarkLinkCreateGrpAddBtn(id, linkTitle, linkUrl, grpNm);
    }
};

Viacom.AdSpace.ManageBookMark.ddlPopulateGrp = function (dropdown, id, linkTitle, linkUrl, grpNm) {
    try {
        $(dropdown).length = 0;
        $(dropdown).append("<option value='UniqueGroup'>Add unique bookmark (no group)</option><option value='AddNewGroup'>Add New Group</option>");
        $.ajax({
            type: "GET",
            url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('" + Viacom.AdSpace.CustomAction.Constants.bookmarkLst + "')/items?" +
                "$select ContentTypeId,Author/ID,UserGroup&$expand=ContentType&$filter=((Author/ID eq " + _spPageContextInfo.userId + ") and (ContentType eq 'BookmarkGroups'))",
            headers: {
                accept: "application/json;odata=verbose"
            },
            async: false,
            success: function (e) {
                if (e.d.results.length > 0) {
                    $(dropdown).html("<option value='UniqueGroup'>Add unique bookmark (no group)</option>"
                    + "<option value='AddNewGroup'>Add New Group</option>");
                    for (i = 0; i < e.d.results.length; i++) {
                        $(dropdown).append($('<option>', {
                            value: e.d.results[i].ID,
                            text: e.d.results[i].UserGroup
                        }));
                    }
                    Viacom.AdSpace.ManageBookMark.EditBookmarkLinkOpenDialog(id, linkTitle, linkUrl, grpNm);
                }
                else {
                    Viacom.AdSpace.ManageBookMark.EditBookmarkLinkOpenDialog(id, linkTitle, linkUrl, grpNm);
                }
            },
            error: function (e) {
                //alert("Error while fetching groups from Bookmark List");
                console.log("Error while fetching groups from Bookmark List");
                $('.add-group-field').hide();
                $("#txtLnkBookmarkUrl").val("");
                Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Failed to edit Bookmark link.");
            }
        })
    }
    catch (ex) {
        //alert("Error: Exception while fetching groups from Bookmark List");
        console.log("Error: Exception while fetching groups from Bookmark List");
        $('.add-group-field').hide();
        $("#txtLnkBookmarkUrl").val("");
        Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Failed to edit Bookmark link.");
    }
};

Viacom.AdSpace.ManageBookMark.EditBookmarkLinkOpenDialog = function (id, linkTitle, linkUrl, grpNm) {
    try {
        var ddlBookmark = document.getElementById('ddlBookmarkLnk');
        for (var k = 0; k < ddlBookmark.options.length; k++) {
            var optionText = ddlBookmark.options[k].text;
            if (optionText.toLowerCase() === grpNm.toLowerCase()) {
                $("#ddlBookmarkLnk").val(ddlBookmark.options[k].value);
            }
        }
        $("#txtLnkBookmarkTitle").val(linkTitle);
        $("#txtLnkBookmarkUrl").val(linkUrl);
        $("#dialog").dialog({
            width: 450,
            autoOpen: true,
            modal: true,
            draggable: false,
            buttons: {
                Ok: function () {
                    Viacom.AdSpace.ManageBookMark.EditBookmarkLinkValidateAddButton(id, linkTitle, linkUrl, grpNm);
                },
                Cancel: function () {
                    $(this).dialog("close");
                    $('.add-group-field').hide();
                    $("#txtLnkBookmarkUrl").val("");
                }
            }
        });
    }
    catch (e) {
        $("#dialog").dialog("close");
        //alert("Error: Exception while opening dialog.");
        console.log("Error: Exception while opening dialog.");
        $('.add-group-field').hide();
        $("#txtLnkBookmarkUrl").val("");
        Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Failed to edit Bookmark link.");
    }
};

Viacom.AdSpace.ManageBookMark.EditBookmarkLink = function (id, linkTitle, linkUrl, grpNm) {
    try {
        $("#editBookmarkValidationMsg").empty();
        $('.add-group-field').hide();
        $("#txtLnkBookmarkUrl").val("");
        Viacom.AdSpace.ManageBookMark.ddlPopulateGrp("#ddlBookmarkLnk", id, linkTitle, linkUrl, grpNm);
    }
    catch (e) {
        $("#dialog").dialog("close");
        //alert("Error: Exception while opening dialog.");
        console.log("Error: Exception while opening dialog.");
        $('.add-group-field').hide();
        $("#txtLnkBookmarkUrl").val("");
        Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Failed to edit Bookmark link.");
    }
};

Viacom.AdSpace.ManageBookMark.DeleteBookmarkLinkOKBtn = function (linkTitle, id) {
    try {
        var delLinkCtx = SP.ClientContext.get_current();
        var rootWeb = delLinkCtx.get_site().get_rootWeb();
        var oList = rootWeb.get_lists().getByTitle(Viacom.AdSpace.ManageBookMark.Constants.listNm);
        var oListItem = oList.getItemById(id);
        oListItem.deleteObject();
        delLinkCtx.executeQueryAsync(
            function (sender, args) {
                //alert("Bookmark link deleted successfully");
                Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Bookmark link deleted successfully");
                $("#dialog-del").dialog("close");
                AllBookmarks.loadBookMarkSection();
                $('.add-group-field').hide();
                $("#txtLnkBookmarkUrl").val("");
            }, function (sender, args) {
                $("#dialog-del").dialog("close");
                //alert("Error while deleting bookmark link from Bookmark list");
                console.log("Error while deleting bookmark link from Bookmark list");
                $('.add-group-field').hide();
                $("#txtLnkBookmarkUrl").val("");
                console.log('Request failed. ' + args.get_message() + '\n' + args.get_stackTrace());
                Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Failed to delete Bookmark link.");
            });
    }
    catch (e) {
        $("#dialog-del").dialog("close");
        $('.add-group-field').hide();
        $("#txtLnkBookmarkUrl").val("");
        //alert("Error: Exception while deleting bookmark link for current user.");
        console.log("Error: Exception while deleting bookmark link for current user.");
        Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Failed to delete Bookmark link.");
    }
}

Viacom.AdSpace.ManageBookMark.DeleteBookmarkLink = function (id, linkTitle, linkUrl, grpNm) {
    try {
        $("#dialog-del").dialog({
            width: 450,
            autoOpen: true,
            modal: true,
            draggable: false,
            buttons: {
                Ok: function () {
                    Viacom.AdSpace.ManageBookMark.DeleteBookmarkLinkOKBtn(linkTitle, id);
                },
                Cancel: function () {
                    $(this).dialog("close");
                    $('.add-group-field').hide();
                    $("#txtLnkBookmarkUrl").val("");
                }
            }
        });
    }
    catch (e) {
        $("#dialog-del").dialog("close");
        //alert("Error: Exception while opening dialog.");
        Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox("Failed to delete Bookmark link.");
        console.log("Error: Exception while opening dialog.");
        $('.add-group-field').hide();
        $("#txtLnkBookmarkUrl").val("");
    }
};

Viacom.AdSpace.ManageBookMark.CommonDialogAlertBox = function (msg) {
    try {
        $("#commonDlgAlertBox").html(msg);
        $(".dialog-alert").dialog({
            width: 450,
            autoOpen: true,
            modal: true,
            draggable: false,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            }
        });
    }
    catch (e) {
        $(".dialog-alert").dialog("close");
        console.log("Error: Exception while opening dialog.");
    }
};
