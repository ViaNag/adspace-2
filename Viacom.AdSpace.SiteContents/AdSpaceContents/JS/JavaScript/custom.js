var ascommon = {
    topContentTab: function () {
        /* Top Content Tab */
        $('.top-content-tab:first').show();

    },
    CommonDialogAlertBox: function (msg) {
        try {
            $("#commonDlgAlertBox").html(msg);
            $(".dialog-alert").dialog({
                width: 450,
                autoOpen: true,
                modal: true,
                draggable: false,
                buttons: {
                    Ok: function () {
                        $(this).dialog("close");
                    }
                }
            });
        }
        catch (e) {
            $(".dialog-alert").dialog("close");
            console.log("Error: Exception while opening dialog.");
        }
    },
    refinerAccordion: function () {
        /* Refiner Accordion */
        $('.refiner-block h4:first').addClass('active');
        $('.refiner-block ul:first').show();
        $('.refiner-block h4').click(function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $(this).next('.refiner-block ul').hide();
                ascommon.leftRightHeight();
            }
            else {
                $(this).addClass('active');
                $(this).next('.refiner-block ul').show();
                ascommon.leftRightHeight();
                return false;
            }
        });
    },
    featuredLinkAccordion: function () {
        /* Featured Links Accordion */
        $('.featured-links h4').click(function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $(this).parents('.featured-head').next('.featured-links ul').stop().slideUp(function () {
                    ascommon.leftRightHeight();
                });
            }
            else {
                $('.featured-links h4').removeClass('active');
                $(this).addClass('active');
                $('.featured-links ul').slideUp();
                $(this).parents('.featured-head').next('.featured-links ul').stop().slideDown(function () {
                    ascommon.leftRightHeight();
                });
                return false;
            }
        });
    },
    leftRightHeight: function () {
        $(".left-sidebar,.mid-container").css('min-height', 0);
        var leftHeight = $(".left-sidebar").outerHeight();
        var rightHeight = $(".mid-container").outerHeight();
        if (leftHeight > rightHeight) { $(".left-sidebar,.mid-container").css('min-height', leftHeight) }
        else { $(".left-sidebar,.mid-container").css('min-height', rightHeight) };
    },
    externalRefinerAccordion: function () {
        /* External News Refiner Accordion */
        $('.external-news-refiner h4').click(function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $(this).next('.external-news-refiner ul').stop().slideUp();
            }
            else {
                $('.external-news-refiner h4').removeClass('active');
                $(this).addClass('active');
                $('.external-news-refiner ul').slideUp();
                $(this).next('.external-news-refiner ul').stop().slideDown();
                return false;
            }
        });
    },
    sharedContentAccordiaon: function () {
        /* Shared Content With User Accordion */
        $('.content-shared-head h4').click(function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $(this).parents('.content-shared-head').next('.content-shared-body').stop().slideUp();
            }
            else {
                $('.content-shared-head h4').removeClass('active');
                $(this).addClass('active');
                $('.content-shared-body').slideUp();
                $(this).parents('.content-shared-head').next('.content-shared-body').stop().slideDown();
                return false;
            }
        });
    },
    searchOverlayShowHide: function () {
        $('.content-result').mouseover(function () {
            $(this).find('.content-overlay').fadeIn();
        });
        $('.content-result').mouseleave(function () {
            $('.content-overlay').stop().hide();
        });
        $('.close-overlay').click(function () {
            $('.content-overlay').hide();
        });
    },
    openSubMenu: function () {

        $('.mob-menu-level1 > a').click(function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $(this).parents('.mob-menu-level1').find('.mob-submenu-level1').stop().slideUp();
                return false;
            }
            else {
                $('.mob-menu-level1 > a').removeClass('active');
                $(this).addClass('active');
                $('.mob-submenu-level1').slideUp();
                $(this).parents('.mob-menu-level1').find('.mob-submenu-level1').stop().slideDown();
                return false;
            }
        });
    },
    openSubSubMenu: function () {
        $('.mob-menu-level2 > a').click(function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $(this).parents('.mob-menu-level2').find('.mob-submenu-level2').stop().slideUp();
                return false;
            }
            else {
                $('.mob-menu-level2 > a').removeClass('active');
                $(this).addClass('active');
                $('.mob-submenu-level2').slideUp();
                $(this).parents('.mob-menu-level2').find('.mob-submenu-level2').stop().slideDown();
                return false;
            }
        });
    },
    showHideRefiner: function () {
        $('.refiner-btn').click(function (e) {
            $(this).parents('.refiner-mobile').toggleClass('active');
            $('.refiner-mobile-container').toggle();
            $('.search-mobile-container').hide();
            $('.search-mobile').removeClass('active');
            e.preventDefault();
        });
    },
    showHideSearch: function () {
        $('.search-mobile-btn').click(function (e) {
            $(this).parents('.search-mobile').toggleClass('active');
            $('.search-mobile-container').toggle();
            $('.refiner-mobile-container').hide();
            $('.refiner-mobile').removeClass('active');
            e.preventDefault();
        });
    },
    bookmarkAccordion: function () {
        /* Bookmark Accordion */
        $('.bookmark-box h4').click(function () {
            $(this).toggleClass('active');
            $(this).parents('.bookmark-box').children('.bookmark-box ul').slideToggle(function () {
                ascommon.leftRightHeight();
            });
        });
    },
    bookmarkAddDialog: function (e) {
        /* Bookmark Add Dialog */
        $("#dialog-add").dialog({
            width: 450,
            autoOpen: false,
            modal: true,
            draggable: false,
            buttons: {
                Add: function (e) {
                    AllBookmarks.AddBookMark(e);

                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });
        $(".add-bookmark").click(function (e) {
            ascommon.openAddBookMark(e);
        });
    },
    openAddBookMark: function (e) {
        AllBookmarks.loadBookMarkSection();
        AllBookmarks.targetclass = ($(e.target).parent().parent().attr("class"));
        $("#BookMarkTitle input").val('');
        $("#BookMarkUrl input").val('http://');
        $("#BookMarkGroup").val(0);
        $('#validationMsg').html('');
        $('.add-group-field').hide();
        $("#BookMarkNewGroup input").val('');
        AllBookmarks.GetContentTypeIdbyName("BookmarkGroups");
        AllBookmarks.GetContentTypeId('.select-group');
        if ($("#BookMarkGroupCount").attr("Count") < 30) {
            $("#dialog-add").dialog("open");
        }
        else {
            ascommon.CommonDialogAlertBox(AllBookmarks.maxBookmarkmsg);
        }
        e.preventDefault();
    },
    openAddBookmarkCustomAction:function(e)
    {
        AllBookmarks.loadBookMarkSection();        
        AllBookmarks.targetclass = 'submenu-level1 bookmark-container';
        $("#BookMarkTitle input").val('');
        $("#BookMarkUrl input").val('http://');
        $("#BookMarkGroup").val(0);
        $('#validationMsg').html('');
        $('.add-group-field').hide();
        $("#BookMarkNewGroup input").val('');
        AllBookmarks.GetContentTypeIdbyName("BookmarkGroups");
        AllBookmarks.GetContentTypeId('.select-group');
        if ($(".bookmark-container>#BookMarkGroupCount").attr("Count") < 30) {
            $("#dialog-add").dialog("open");
        }
        else {
            ascommon.CommonDialogAlertBox(AllBookmarks.maxBookmarkmsg);
        }
        e.preventDefault();
    },
    groupAddSelect: function () {
        /* Show Add Group on Select Change*/
        $(".select-group").change(function () {
            var select_group_val = $(this).find("option:selected").attr("value");
            if (select_group_val == '1') {
                $(this).parents('.edit-bookmark-form').children('.add-group-field').show();
            }
            else {
                $('.add-group-field').hide();
            }
        });
    },
    bookmarkAddDocLibrary: function () {
        /* Bookmark Add Document library */

        $("#dialog-add-doc").dialog({
            width: 450,
            autoOpen: true,
            modal: true,
            draggable: false,
            buttons: {
                Add: function () {
                    $(this).dialog("close");
                },
                Cancel: function () {
                    $(this).dialog("close");
                }
            }
        });
        $(".add-bookmark-doc").click(function (e) {
            $("#dialog-add-doc").dialog("open");
            e.preventDefault();
        });
    },
    /* Functions for top content widget */
    loadLatest: function () {
        $('#fragment2').hide();
        $('#tab2').css('border-bottom', 'none');
        $('#fragment3').hide();
        $('#tab3').css('border-bottom', 'none');
        $('#fragment4').hide();
        $('#tab4').css('border-bottom', 'none');
        $('#fragment1').show();
        $('#tab1').css('border-bottom', '5px solid #F8820E');
        ascommon.createCookie("selectedTab", "#fragment1", 365);

    },
    loadLastAccesed: function () {
        $('#tab1').css('border-bottom', 'none');
        $('#fragment1').hide();
        $('#tab2').css('border-bottom', 'none');
        $('#fragment3').hide();
        $('#fragment4').hide();
        $('#tab4').css('border-bottom', 'none');
        $('#tab3').css('border-bottom', '5px solid #F8820E');
        $('#fragment2').show();
        ascommon.createCookie("selectedTab", "#fragment2", 365);

    },
    loadMostViwed: function () {
        $('#fragment1').hide();
        $('#tab1').css('border-bottom', 'none');
        $('#fragment2').hide();
        $('#tab3').css('border-bottom', 'none');
        $('#fragment4').hide();
        $('#tab4').css('border-bottom', 'none');
        $('#tab2').css('border-bottom', '5px solid #F8820E');
        $('#fragment3').show();
        ascommon.createCookie("selectedTab", "#fragment3", 365);
    },
    loadPopular: function () {
        $('#fragment1').hide();
        $('#tab1').css('border-bottom', 'none');
        $('#fragment2').hide();
        $('#tab3').css('border-bottom', 'none');
        $('#fragment4').hide();
        $('#tab4').css('border-bottom', 'none');
        $('#fragment3').hide();
        $('#tab2').css('border-bottom', 'none');

        $('#tab4').css('border-bottom', '5px solid #F8820E');
        $('#fragment4').show();
        ascommon.createCookie("selectedTab", "#fragment4", 365);

    },
    createCookie: function (name, value, days) {
        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            var expires = "; expires=" + date.toGMTString();
        }
        else var expires = "";
        document.cookie = name + "=" + value + expires + "; path=/";
    },

    readCookie: function (name) {
        var nameEQ = name + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
        }
        return null;
    },

    eraseCookie: function (name) {
        createCookie(name, "", -1);
    }
}


$(document).ready(function () {
    ascommon.topContentTab();
    ascommon.refinerAccordion();
    ascommon.featuredLinkAccordion();
    ascommon.externalRefinerAccordion();
    ascommon.sharedContentAccordiaon();
    ascommon.searchOverlayShowHide();
    $(window).bind("load", function () {
        ascommon.leftRightHeight();
    });
    ascommon.openSubMenu();
    ascommon.openSubSubMenu();
    $('.mobile-toggle').bigSlide();
    ascommon.showHideRefiner();
    ascommon.showHideSearch();
    AllBookmarks.loadBookMark("BookmarkListing");
    ascommon.bookmarkAccordion();
    ascommon.bookmarkAddDialog();
    ascommon.groupAddSelect();

    /* Check the cookie for top content widget and list*/
    //$("#tabs").tabs();
    var showtab = ascommon.readCookie("selectedTab");
    if (showtab === undefined || showtab === null) {
        showtab = "#fragment1";
    }

    $('#fragment1').hide();
    $('#fragment2').hide();
    $('#fragment3').hide();
    $('#fragment4').hide();
    var lastChar = showtab.substr(showtab.length - 1);
    var tabvalue = "#tab" + lastChar;
    $(tabvalue).css('border-bottom', '6px solid #F8820E');
    $(showtab).show();

});
