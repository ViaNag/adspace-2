﻿var AsTopContent =
    {
        CurrentUrl: '',
        collListItem: '',
        listItemInfo: new Array(),
        executed: false,
        /* gets all the items from the configuration list */
        retrieveListItems: function () {
            
            $.ajax({
                url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('ConfigurationList')/items?$select=Title,Key,Value1&$filter=Title eq 'TopContentWidget'",
                method: "GET",
                async: false,
                headers: { "Accept": "application/json; odata=verbose" },
                success: function (data) {
                    
                    for (i = 0; i < data.d.results.length; i++)//data.d.results.length
                    {
                        
                        var key = data.d.results[i].Key
                        var value = _spPageContextInfo.siteAbsoluteUrl+"/"+data.d.results[i].Value1;
                        AsTopContent.listItemInfo[key] = value;
                    }

                    AsTopContent.executed = true;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    
                }
            });
        }
    }