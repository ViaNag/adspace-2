﻿var AdSpaceLinks =
    {
        featureLinks: 'Featured',
        bannerLinks: 'Banner',
        footerLinks: 'Footer',
        category: [],
        structure: '',
        retrieveLinks: function (complete, failure, SectionType) {

            $.ajax({
                url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('Master Link')/items?$select=Title,Url,GroupLinkBy/LinkCategory&$expand=GroupLinkBy&$filter=(DisplayOn eq '" + SectionType + "')",
                method: "GET",
                async: false,
                headers: { "Accept": "application/json; odata=verbose" },
                success: function (data) {

                    try
                    {
                        complete(data.d);
                        AdSpaceLinks.loadUI(data, SectionType);
                        AdSpaceLinks.structure = '';
                        AdSpaceLinks.category = [];
                    }
                    catch(ex)
                    {
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    
                }
            });
        },
        loadUI: function (data, SectionType) {
            if (AdSpaceLinks.category.length > 0) {

                for (index = 0; index < AdSpaceLinks.category.length; index++) {
                    if (AdSpaceLinks.category[index] != '' || AdSpaceLinks.category[index] != undefined) {
                        switch (SectionType) {
                            case AdSpaceLinks.featureLinks:
                                {
                                    AdSpaceLinks.structure = AdSpaceLinks.structure + "<div class='featured-head'>" +
                                        "<h4><span class='toggle-arrow'></span>" + AdSpaceLinks.category[index] + "</h4></div><ul>"
                                        + AdSpaceLinks.retrieveLinksByCategory(AdSpaceLinks.category[index], data, SectionType) + "</ul>";
                                   
                                }
                                break;
                            case AdSpaceLinks.bannerLinks:
                                {
                                    AdSpaceLinks.structure = AdSpaceLinks.structure + " <ul><li><h4>" + AdSpaceLinks.category[index] +
                                        "</h4></li>" + AdSpaceLinks.retrieveLinksByCategory(AdSpaceLinks.category[index], data, SectionType)
                                    + "</ul>";
                                }
                                break;
                            case AdSpaceLinks.footerLinks:
                                {
                                    AdSpaceLinks.structure = AdSpaceLinks.structure + "<div class='footer-links'><h3>" +
                                       AdSpaceLinks.category[index] + "</h3><ul>" +
                                       AdSpaceLinks.retrieveLinksByCategory(AdSpaceLinks.category[index], data, SectionType) +
                                       "</ul></div>";

                                }
                                break;

                        }
                    }
                }
                switch (SectionType) {
                    case AdSpaceLinks.featureLinks:
                        {
                            AdSpaceLinks.structure = AdSpaceLinks.structure + " <a href='/sites/adspace/Pages/AllLinks.aspx' class='more'>more</a> ";
                            $('#featuredLinks').append(AdSpaceLinks.structure);
                        }
                        break;
                    case AdSpaceLinks.bannerLinks:
                        {
                            AdSpaceLinks.structure = AdSpaceLinks.structure + " <div class='bannerLink'><a  href='/sites/adspace/Pages/AllLinks.aspx' class='more'>more</a> </div>";
                            $('.quicklink-container').append(AdSpaceLinks.structure);
                        }
                        break;
                    case AdSpaceLinks.footerLinks:
                        {
                            $('#footerLinks').append(AdSpaceLinks.structure);

                        }
                        break;
                }
            }
        },
        retrieveLinksByCategory: function (category, data, SectionType) {
            var LinkHtml = "";
            for (i = 0; i < data.d.results.length; i++) {
                if (data.d.results[i].GroupLinkBy.LinkCategory == category) {

                    LinkHtml = LinkHtml + "<li><a href='" + data.d.results[i].Url.Url + "' target='_blank'>" + data.d.results[i].Title + "</a></li>";

                }
            }
            return LinkHtml;

        },
        groupBy: function (items, propertyName1, propertyName2) {

            for (i = 0; i < items.results.length; i++) {

                if ($.inArray(items.results[i][propertyName1][propertyName2], AdSpaceLinks.category) == -1) {
                    AdSpaceLinks.category.push(items.results[i][propertyName1][propertyName2]);
                }

            }

        }
    }
$(document).ready(function () {
    AdSpaceLinks.retrieveLinks(
function (items) {
    var taskNames = AdSpaceLinks.groupBy(items, 'GroupLinkBy', 'LinkCategory');
    console.log(taskNames);
},
function (error) {
    console.log(JSON.stringify(error));
},
AdSpaceLinks.featureLinks
);

    AdSpaceLinks.retrieveLinks(
function (items) {
    var taskNames = AdSpaceLinks.groupBy(items, 'GroupLinkBy', 'LinkCategory');
    console.log(taskNames);
},
function (error) {
    console.log(JSON.stringify(error));
},
AdSpaceLinks.bannerLinks
);


    AdSpaceLinks.retrieveLinks(
function (items) {
    var taskNames = AdSpaceLinks.groupBy(items, 'GroupLinkBy', 'LinkCategory');
    console.log(taskNames);
},
function (error) {
    console.log(JSON.stringify(error));
},
AdSpaceLinks.footerLinks
);

})