﻿var UrlUtility = {
    GetQueryStringColl: function (url) {
        var queryString = url.substring(url.indexOf('?') + 1);
        if (queryString != '') {
            queryStringColl = new Array();
            var keyPairs = queryString.split('&');
            for (var i = 0; i < keyPairs.length; i++) {
                var keyPair = keyPairs[i].split('=');
                queryStringColl[keyPair[0]] = keyPair[1];
            }
        }
        return queryStringColl;
    }
};

window.onload = function () {
    // We are here overwritting the OOB Search function for our purpose-

    if ($find('ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr') != null) {

        $find('ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr').search = function (a) {
            var b = new SP.CScope("SearchBox.Search");
            b.start();
            if (Srch.U.n(a))
                a = "";
            else
                a = a.trim();
            if (a === this.get_currentPrompt())
                a = "";

            var d = this.get_events().getHandler(Srch.EventType.toString(1));
            if (!this.$3I_3 || Srch.U.n(d)) {
                var c = Srch.U.getResultsPageUrl(this.$3A_3, a);
                if (!Srch.U.e(c)) {
                    this.$3_2("Search", "Redirecting to url '{0}'...", c); /*SP.Utilities.HttpUtility.navigateTo(c);*/
                    var queryStringColl = UrlUtility.GetQueryStringColl(c);
                    var key = queryStringColl["k"];
                    var type = queryStringColl["LSType"];
                    // redirect on respective page
                    if (type != null && (key != '' || type == 'Create')) {
                        switch (type) {
                            case "Company":
                                var url = "https://www.nexis.com/api/accessGW?gw=FC&suppressTab=ID&ui=HOWARDBRATHWAITE4&pw=QY9FTZ&hgn=t&nm=" + key;
                                window.open(url, '_blank');
                                break;
                            case "Ticker":
                                var url = "https://www.nexis.com/api/accessGW?gw=FC&suppressTab=ID&ui=HOWARDBRATHWAITE4&pw=QY9FTZ&hgn=t&tr=" + key;
                                window.open(url, '_blank');
                                break;
                            case "Create":
                                var url = "https://www.nexis.com/api/accessGW?gw=CL&suppressTab=ID&ui=HOWARDBRATHWAITE4&pw=QY9FTZ";
                                window.open(url, '_blank');
                            default:
                                break;
                        }
                    }
                    else {
                        if (Srch.U.e(a) && !this.$2Z_3) {
                            b.stop(); return
                        }
                        SP.Utilities.HttpUtility.navigateTo(c);
                    }
                    b.stop();
                    return
                }
            }
            this.$S_3 = a;
            EnsureScript("sp.search.js", TypeofFullName("Microsoft.SharePoint.Client.Search.Query.KeywordQuery"), this.$$d_$5U_3);
            Srch.ScriptApplicationManager.get_current().$3L_1()
        };
    }
};
