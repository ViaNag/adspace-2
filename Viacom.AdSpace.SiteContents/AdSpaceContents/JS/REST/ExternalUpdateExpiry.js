﻿/***get user name and item count from query string********************************************************/		
$(document).ready(function () 
 {
    ExternalUpdateExpiry.EditExpiryID=ExternalUpdateExpiry.GetQueryString('EditExpiryID');
    ExternalUpdateExpiry.EditExpiryStatus=ExternalUpdateExpiry.GetQueryString('EditExpiryStatus');
 });


var ExternalUpdateExpiry ={
EditExpiryID: null,
EditExpiryStatus: null,

/*****method is called if the status is pending***/
UpdateExpiryDate: function (itemId,updatedSharedWith)
{
	var UpdatedExpiryDate=$("#dateSpan input").val();
	if(UpdatedExpiryDate!="")
	{
		if(ExternalUpdateExpiry.EditExpiryStatus=="Pending")
		{	
			ExternalUpdateExpiry.UpdateInternalListItem(ExternalUpdateExpiry.EditExpiryID,UpdatedExpiryDate);
		}
		else
		{
			ExternalUpdateExpiry.UpdateInternalListItem(ExternalUpdateExpiry.EditExpiryID,UpdatedExpiryDate);
			ExternalUpdateExpiry.createUpdationRequest(ExternalUpdateExpiry.EditExpiryID,UpdatedExpiryDate);
		}
	}
	else
	{
		$("#RequiredDate").css("color","red");

	}
	
},

/*Update Expiry date***/
UpdateInternalListItem: function (ExternalUpdateExpiry.EditExpiryID,UpdatedExpiryDate)
{

	UpdatedExpiryDate=UpdatedExpiryDate.toString();
	$.ajax
	({ 
		url:_spPageContextInfo.webAbsoluteUrl+"/_api/web/lists/getbytitle('Externally shared Content')/items("+EditExpiryID+")",
		 type: "POST",
	     async: false,
		 data:  JSON.stringify({ '__metadata': { 'type': 'SP.Data.Externally_x0020_shared_x0020_ContentListItem'},'ExpiresOn':UpdatedExpiryDate}),
	     headers: 
	     {           	
	        "X-RequestDigest": $("#__REQUESTDIGEST").val(), 
	        	"IF-MATCH":"*",
	        	"X-HTTP-Method":"MERGE",
	            "accept": "application/json;odata=verbose",
	            "content-type":"application/json;odata=verbose", 
	       },
	      success: function(data) 
	      {
	        $("#ExpiryDateUpdation").hide();
	      	$("#UpdatedMessage").show();
	      	alert('hi');
	      },
	      error: function (xhr) { 
	      alert(xhr.status + ': ' + xhr.responseText); 
	   }
	}); 
},
/*********createAccessRemovalRequest****/
createUpdationRequest: function (ExternalUpdateExpiry.EditExpiryID,UpdatedExpiryDate)
{
		//content type id: for updation request CT
	EditExpiryID=EditExpiryID.toString();
	UpdatedExpiryDate=UpdatedExpiryDate.toString();
	$.ajax({ 
	url:_spPageContextInfo.webAbsoluteUrl+"/_api/web/lists/getbytitle('External Sharing Processing Pool')/items",
	 type: "POST",
	    async: false,
        data:  JSON.stringify({ '__metadata': { 'type': 'SP.Data.External_x0020_Sharing_x0020_Processing_x0020_PoolListItem' }, 
  		'ItemID': EditExpiryID,
  		'ContentTypeId':"0x01006986E1FC40E144199D5F621549614C5C",
  		 'UpdateFor':"Expiry Date",
  		 'Request_x0020_Status':"Pending",
  		 'UpdatedExpiryDate':UpdatedExpiryDate}
		),

        headers: { 
            "accept": "application/json;odata=verbose",
            "content-type":"application/json;odata=verbose",
            "X-RequestDigest": $("#__REQUESTDIGEST").val()
        },
        success: function(data) {
 $("#ExpiryDateUpdation").hide();
	      	$("#UpdatedMessage").show();

        },
       error: function (jqXHR, textStatus, errorThrown) 
	        {
				alert(jqXHR.responseText );
	        }
});
		
},

/******************************/
/****get query string*********************************************************************************************/
GetQueryString: function (qs)
 {
 	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');
	for (var i = 0; i < sURLVariables.length; i++)
	{
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == qs)
			{
			return sParameterName[1];
			}
	}

 },
}

