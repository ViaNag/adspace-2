﻿
// common name space 
var AdSpaceCommon = {
    // Method to check current user group
    IsCurrentUserMemberOfGroup: function (groupName, OnComplete) {

        var currentContext = new SP.ClientContext.get_current();
        var currentWeb = currentContext.get_web();

        var currentUser = currentContext.get_web().get_currentUser();
        currentContext.load(currentUser);

        var groups = currentUser.get_groups();

        currentContext.load(groups);

        currentContext.executeQueryAsync(OnSuccess, OnFailure);

        function OnSuccess(sender, args) {
            var userInGroup = false;
            var groupUserEnumerator = groups.getEnumerator();
            while (groupUserEnumerator.moveNext()) {
                var group = groupUserEnumerator.get_current();
                // if the user has mentioned group.
                if (group.get_title() == groupName) {
                    userInGroup = true;
                    break;
                }
            }
            OnComplete(userInGroup);
        }

        function OnFailure(sender, args) {
            OnComplete(false);
        }
    },
    // If the current user is super admin
    IsCurrentUserSuperAdmin: function () {
        // verify and add call back method to hide share options
        AdSpaceCommon.IsCurrentUserMemberOfGroup("AdSpace Super Admin", function (isCurrentUserInGroup) {
            if (!isCurrentUserInGroup) {
                // the 
                var str = '.js-callout-footerArea .js-callout-actions .js-callout-action:nth-child(2) {display: none;} #Ribbon\\.Documents\\.Manage #Ribbon\\.Documents\\.Manage-LargeMedium-1-1 {display: none;} #Ribbon\\.Documents\\.Share #Ribbon\\.Documents\\.Share\\.ShareItem-Large {display: none;} a[title="Shared With"].ms-core-menu-link:link {display : none !important;}  a[title="Shared With"].ms-core-menu-link:visited {display : none !important;} a[title="Shared With"].ms-core-menu-link:hover {display : none !important;}    a[title="Shared With"].ms-core-menu-link:focus {display : none !important;}    a[title="Shared With"].ms-core-menu-link:active {display : none !important;}     .js-callout-sharedWithInfo > .js-callout-bodySection > div[class="js-callout-sharedWithList"]{display : none !important;} #s4-ribbonrow #Ribbon\\.Library\\.Settings #Ribbon\\.Library\\.Settings\\.LibraryPermissions-Large {display: none;}';
                // hide the share and share with options.
                $("<style>" + str + "</style>").appendTo("head");
            }
        });
    }
}

var ExceptionHandler =
{
    LogError : function(message,pageName,fileName,methodName)
    {
        $.ajax({
            type: "POST",
            url: _spPageContextInfo.siteAbsoluteUrl + "/Pages/Operations.aspx/LogException",
            data: JSON.stringify({
                msg : message,
                page: pageName,
                file: fileName,
                method:methodName
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (n) {
            },
            error: function (request, status, error) {
            }
        });
    }
};

// after sp.js get loaded, call method to check the super admin group
ExecuteOrDelayUntilScriptLoaded(AdSpaceCommon.IsCurrentUserSuperAdmin, 'SP.js');



