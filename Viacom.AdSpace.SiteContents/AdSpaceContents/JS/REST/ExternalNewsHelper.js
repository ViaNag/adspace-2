
var ViacomExternalNewsHelper = { 
    AllExternalNewsItems: null,
	ExtNewsItemsByCategory:null,
	AllCategoryChoices:null,
    NewsCategory: 'NewsCategory',
    RSSNewsTitle: 'RSSNewsTitle',
    LastExtViacomNews: 'LastExtViacomNews',
	ViacomExternalNews:'ViacomExternalNews',
    HomePageFeedCount: 5,
    ListingPageFeedCount: 10, 
	IsHomePage:true,
	RssFeedDefaultImage:"/_catalogs/masterpage/AdSpaceMasterPageAndLayouts/images/viacom.jpg",
	ShowLoadingImage : function ()
    {
        $("#imgRssFeedLoading").show();
    },
    HideLoadingImage : function ()
    {
        $("#imgRssFeedLoading").hide();
    },
    LoadConfigData : function ()
    {
        try
        {
            $.ajax({
                url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('ConfigurationList')/items?$select=Title,Key,Value1&$filter=(Title eq '"+ViacomExternalNewsHelper.ViacomExternalNews+"')",
                method: "GET",				
                async: false,
                headers: { "Accept": "application/json; odata=verbose" },
                success: function (data) {

                    for (i = 0; i < data.d.results.length; i++)//data.d.results.length
                    {

                        var key = data.d.results[i].Key

                        if (key == 'HomePage') {
                            ViacomExternalNewsHelper.HomePageFeedCount = data.d.results[i].Value1;
                        }
                        else if (key == 'ListingPage') {
                            ViacomExternalNewsHelper.ListingPageFeedCount = data.d.results[i].Value1;
                        }
						else if(Key=='RssFeedDefaultImage')
						{
							ViacomExternalNewsHelper.RssFeedDefaultImage = data.d.results[i].Value1;
						}                        
                    }
                },
                error: function (jqXHR, textStatus, error) {
                    ExceptionHandler.LogError(error.message, "External Viacom News", "ExternalNewsHelper.js", "LoadConfigData");
                }
            });
        }
        catch (error)
        {
            ExceptionHandler.LogError(error.message, "External Viacom News", "ExternalNewsHelper.js", "LoadConfigData");
        }
    },    
	GetAllExternalNewsItems: function () {
		try
        {			
            ViacomExternalNewsHelper.LoadConfigData();
			$.ajax({
				url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('News')/items?$select=ID,RSSNewsTitle,RSSNewsSource,NewsCategory&$filter=(IsViacomNews eq 0)&$orderby=RSSNewsTitle asc",
				method: "GET",				
				async: false,
				headers: { "Accept": "application/json; odata=verbose" },
				success: function (data) {
					AllExternalNewsItems = data.d;
				},
				error: function (jqXHR, textStatus, error) {
					ExceptionHandler.LogError(error.message, "External Viacom News", "ExternalNewsHelper.js", "GetAllExternalNewsItems");					
				}
			});
		}
        catch(error)
        {
            ExceptionHandler.LogError(error.message,"External Viacom News", "ExternalNewsHelper.js", "GetAllExternalNewsItems");
        }
    },
	FillExternalNewsArrayObject: function ()
    {
		try
        {
			ExtNewsItemArray=[];
			$.each(AllExternalNewsItems.results, function (val, item) {				 					
				var externalNewsItem = {newsCategory:'',newsSourseUrl: '',id:0,subExternalNewsItems: []};
				var newsCategory=item['NewsCategory'];
				if(newsCategory=='None')
				{
					externalNewsItem.newsCategory=item['RSSNewsTitle'];
					externalNewsItem.newsSourseUrl=item['RSSNewsSource']['Url'];
					externalNewsItem.id=item['ID'];
					ExtNewsItemArray.push(externalNewsItem);
				}
				else
				{
					var subExternalNewsItem = { newsTitle:'', newsSourseUrl: '',id:0};
					var isNewsCategoryExist=false;
					var categoryIndex=-1;
					for(var i=0;i<newsCategory.length;i++)
					{
						if(newsCategory==newsCategory[i].newsCategory)
						{
							isNewsCategoryExist=true;
							categoryIndex=i;
							break;
						}
					}
					if(isNewsCategoryExist)
					{
						subExternalNewsItem.newsTitle=item['RSSNewsTitle'];
						subExternalNewsItem.newsSourseUrl=item['RSSNewsSource']['Url'];
						subExternalNewsItem.id=item['ID'];
						ExtNewsItemArray[categoryIndex].subExternalNewsItems.push(subExternalNewsItem);
					}
					else
					{
						externalNewsItem.newsCategory=newsCategory;
						subExternalNewsItem.newsTitle=item['RSSNewsTitle'];
						subExternalNewsItem.newsSourseUrl=item['RSSNewsSource']['Url'];
						subExternalNewsItem.id=item['ID'];
						externalNewsItem.subExternalNewsItems.push(subExternalNewsItem);
						ExtNewsItemArray.push(externalNewsItem);
					}
				}						
			});	
		}
        catch(error)
        {
            ExceptionHandler.LogError(error.message,"External Viacom News", "ExternalNewsHelper.js", "FillExternalNewsObject");
        }
    },
    CreateCategoryFilter: function ()
    {
		try{
			for(var i=0;i<ExtNewsItemArray.length;i++)
			{	
				var categoryHtml='';		
				if(ExtNewsItemArray[i].newsSourseUrl!='' && ExtNewsItemArray[i].subExternalNewsItems.length==0)
				{					
					categoryHtml='<h4 class="individual-news"><a id="anchorFeed'+ExtNewsItemArray[i].id+'" href="#" feedUrl="'+ExtNewsItemArray[i].newsSourseUrl+'">'+ExtNewsItemArray[i].newsCategory+'</a></h4>';
				}
				else
				{
					categoryHtml='<h4 class="groupNews"><span class="toggle-arrow"></span><strong>'+ExtNewsItemArray[i].newsCategory+'</strong></h4><ul>';
					for(var j=0;j<ExtNewsItemArray[i].subExternalNewsItems.length;j++)
					{
						categoryHtml=categoryHtml+'<li><a id="anchorFeed'+ExtNewsItemArray[i].subExternalNewsItems[j].id+'" href="#" feedUrl="'+ExtNewsItemArray[i].subExternalNewsItems[j].newsSourseUrl+'">'+ExtNewsItemArray[i].subExternalNewsItems[j].newsTitle+'</a></li>';
					}
					categoryHtml=categoryHtml+'</ul>';				
				}				
				$('.external-news-refiner.external-news-section').append( categoryHtml);				
			}
			
			if (ViacomExternalNewsHelper.IsHomePage) 
			{
				$('.external-news-refiner.external-news-section').append('<div class="more-news"> <a href="/Pages/AllExternalNews.aspx" class="more">more</a> </div>')
			}
			
			if(ExtNewsItemArray.length>0)
			{				
				ViacomExternalNewsHelper.ShowLoadingImage();
				var selectedFeedElementId = ascommon.readCookie(ViacomExternalNewsHelper.LastExtViacomNews);
				if(selectedFeedElementId && $('#'+selectedFeedElementId).length > 0)
				{
					if($('#'+selectedFeedElementId).closest('ul').length > 0)
					{
						$('#'+selectedFeedElementId).closest('ul').prev('h4').addClass('selected');
					}
					else
					{
						$('#'+selectedFeedElementId).closest('h4').addClass('selected');
					}					
				}
				else
				{
					$('.external-news-refiner.external-news-section').find('h4:first').addClass('selected');
					selectedFeedElementId=$('.external-news-refiner.external-news-section').find('h4:first').find('a').attr('id');
				}
				ViacomExternalNewsHelper.ShowExternalRSSFeed(selectedFeedElementId);
			}
		}
		catch(error)
        {
            ExceptionHandler.LogError(error.message,"External Viacom News", "ExternalNewsHelper.js", "CreateCategoryFilter");
        }
    },
    ShowExternalRSSFeed : function (selectedFeedElementId)
    {  		
		var rssFeedUrl=$('#'+selectedFeedElementId).attr('feedUrl');
		if (selectedFeedElementId.length > 0 && rssFeedUrl.length>0) {	
		
			ascommon.createCookie(ViacomExternalNewsHelper.LastExtViacomNews, selectedFeedElementId, 356); 

            if (ViacomExternalNewsHelper.IsHomePage) {
                ViacomExternalNewsHelper.GetRSSFeed(rssFeedUrl, this.HomePageFeedCount, 0)
            }
            else {
                ViacomExternalNewsHelper.GetRSSFeed(rssFeedUrl, this.ListingPageFeedCount, 0)
            }
        }
    },
    GetRSSFeed: function (Url,PageSize,PageCount)
    {
        $.ajax({
            type: "POST",
            url: _spPageContextInfo.siteAbsoluteUrl + "/Pages/Operations.aspx/GetNewsFeed",
            data: JSON.stringify({
                url: Url,
                pageSize:PageSize,
                pageCount: PageCount
            }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (n) {
                try {
                    if (n.d) {
						
                        // parse json string
                        var ExternalNewsFeed =n.d.Items;
						
                        $(".external-news-results").empty();

                        var ItemTemplate = '';
                        $(ExternalNewsFeed).each(function (val,item) {
							var summaryText=item['Summary']['Text'];
							var feedImageSrc=ViacomExternalNewsHelper.RssFeedDefaultImage;
							var div = document.createElement('div');
							div.innerHTML = summaryText;
							var firstImage = div.getElementsByTagName('img')[0];
							if(firstImage)
							{
								feedImageSrc=firstImage.getAttribute("src");
							}							
                            ItemTemplate += '<div class="external-news-box">' +
                                  '<h3><a target="_blank" href="' + item['Links'][0]['Uri'] + '">' + item['Title']['Text'] + '</a></h3>' +
                                  '<div class="external-news-img"><img src="'+feedImageSrc+'" alt="" /></div><div class="external-news-text">'+
								  '<span class="news-date">' +ViacomExternalNewsHelper.FormatDate(item['PublishDate']) + '</span>' +
                                  '<p>' + summaryText + '</p></div></div>';                          
                            
                        });
						$(".external-news-results").append(ItemTemplate);
                    }
					ViacomExternalNewsHelper.HideLoadingImage();
                }
                catch (error) {
                    ExceptionHandler.LogError(error.message, "External Viacom News", "ExternalNewsHelper.js", "GetRSSFeed");
                    ViacomExternalNewsHelper.HideLoadingImage();
                }
            },
            error: function (request, status, error) {
                ExceptionHandler.LogError(error.message,  "External Viacom News", "ExternalNewsHelper.js", "GetRSSFeed");
                ViacomExternalNewsHelper.HideLoadingImage();
            }
        });

    },
    FormatDate : function (date)
    {
        var newDate = eval(date.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));

        var mL = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

        var dateStr = mL[newDate.getMonth()] + " " + newDate.getDay() + ", " + newDate.getFullYear();

        return dateStr;
    },
	externalRefinerAccordion : function(){
		/* External News Refiner Accordion */	
		$('.external-news-section h4.groupNews').click(function(){
			if($(this).hasClass('active')){
				$(this).removeClass('active');
				$(this).next('.external-news-section ul').stop().slideUp();
			}
			else
			{
				$('.external-news-section h4').removeClass('active');
				$(this).addClass('active');
				$('.external-news-section ul').slideUp();
				$(this).next('.external-news-section ul').stop().slideDown();
				return false;
			}
		});
	}
}

