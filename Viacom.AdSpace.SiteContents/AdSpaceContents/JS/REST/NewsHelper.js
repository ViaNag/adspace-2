﻿
// 
var ViacomNewsHelper = {
    ViacomNews:1,
    ExternalNews: 0,
    AllNewsItems: null,
    NewsCategory: 'NewsCategory',
    RSSNewsTitle: 'RSSNewsTitle',
    LastViacomNews: 'LastViacomNews',
    Home: 1,
    Listing: 0,
    HomeNewsCount: 5,
    ListingNewsCount: 10,
    GetNewsFromRSS: function () {

    },
    ShowLoadingImage : function ()
    {
        $("#NewsLoading").show();
    },
    HideLoadingImage : function ()
    {
        $("#NewsLoading").hide();
    },
    LoadConfigData : function ()
    {
        try
        {
            $.ajax({
                url: _spPageContextInfo.siteAbsoluteUrl + "/_api/web/lists/getbytitle('ConfigurationList')/items?$select=Title,Key,Value1&$filter=Title eq 'ViacomNews'",
                method: "GET",
                async: false,
                headers: { "Accept": "application/json; odata=verbose" },
                success: function (data) {

                    for (i = 0; i < data.d.results.length; i++)//data.d.results.length
                    {

                        var key = data.d.results[i].Key

                        if (key == 'HomePage') {
                            ViacomNewsHelper.HomeNewsCount = data.d.results[i].Value1;
                        }
                        else if (key == 'ListingPage') {
                            ViacomNewsHelper.ListingNewsCount = data.d.results[i].Value1;
                        }
                        
                    }
                },
                error: function (jqXHR, textStatus, error) {
                    ExceptionHandler.LogError(error.message, "Viacom News", "NewsHelper.js", "LoadConfigData");
                    ViacomNewsHelper.HideLoadingImage();
                }
            });
        }
        catch (error)
        {
            ExceptionHandler.LogError(error.message, "Viacom News", "NewsHelper.js", "LoadConfigData");
            ViacomNewsHelper.HideLoadingImage();
        }
    },
    GetItemsWithoutParent: function (type) {
        try
        {
            ViacomNewsHelper.ShowLoadingImage();

            ViacomNewsHelper.LoadConfigData();

            $.ajax({
                url: _spPageContextInfo.webAbsoluteUrl + "/_api/web/lists/getbytitle('News')/items?$select=RSSNewsTitle,RSSNewsSource,NewsCategory/RSSNewsTitle,Keyword,ID&$expand=NewsCategory&$filter=(IsViacomNews eq " + type + ")",
                method: "GET",
                async: false,
                headers: { "Accept": "application/json; odata=verbose" },
                success: function (data) {
                    AllNewsItems = data.d;
                },
                error: function (jqXHR, textStatus, error) {
                    ExceptionHandler.LogError(error.message, "Viacom News", "NewsHelper.js", "GetItemsWithoutParent");
                    ViacomNewsHelper.HideLoadingImage();
                }
            });
        }
        catch(error)
        {
            ExceptionHandler.LogError(error.message, "Viacom News", "NewsHelper.js", "GetItemsWithoutParent");
            ViacomNewsHelper.HideLoadingImage();
        }
    },
    BindDropDown: function (where)
    {
        try
        {
            // insert new data
            $.each(AllNewsItems.results, function (val, item) {
                $('#DropDownViacomNews').append(
                    $('<option></option>').val(item['RSSNewsSource']['Url']).html(item['RSSNewsTitle'])
                );
            })

            var lastViewedUrl = ascommon.readCookie(ViacomNewsHelper.LastViacomNews);

            if (lastViewedUrl != null && lastViewedUrl != '')
            {
                $('#DropDownViacomNews option:selected').attr("selected", null);

                $('#DropDownViacomNews option[value="' + lastViewedUrl + '"]').attr("selected", "selected");
            }

            $("#DropDownViacomNews").change(ViacomNewsHelper.ShowViacomRSSFeed(where));
        }
        catch (error)
        {
            ExceptionHandler.LogError(error.message, "Viacom News", "NewsHelper.js", "BindDropDown");
            ViacomNewsHelper.HideLoadingImage();
        }
    },
    ShowViacomRSSFeed : function (where)
    {
        try
        {
            ViacomNewsHelper.ShowLoadingImage();

            var RSSFeedUrl = ''

            if ($("#DropDownViacomNews option:selected")) {
                RSSFeedUrl = $("#DropDownViacomNews option:selected").val();

                ascommon.createCookie(ViacomNewsHelper.LastViacomNews, RSSFeedUrl, 356);
            }

            if (RSSFeedUrl.length > 0) {

                if (where == this.Home) {
                    ViacomNewsHelper.GetRSSFeed(RSSFeedUrl, this.HomeNewsCount, 0)
                }
                else {
                    ViacomNewsHelper.GetRSSFeed(RSSFeedUrl, this.ListingNewsCount, 0)
                }
            }
        }
        catch (error)
        {
            ExceptionHandler.LogError(error.message, "Viacom News", "NewsHelper.js", "ShowViacomRSSFeed");
            ViacomNewsHelper.HideLoadingImage();
        }
    },
    GetRSSFeed: function (Url,PageSize,PageCount)
    {
        try
        {
            $.ajax({
                type: "POST",
                url: _spPageContextInfo.siteAbsoluteUrl + "/Pages/Operations.aspx/GetNewsFeed",
                data: JSON.stringify({
                    url: Url,
                    pageSize:PageSize,
                    pageCount: PageCount
                }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (n) {

                    try {
                        if (n.d) {

                            // parse json string
                            var NewsFeed = n.d.Items;

                            var source = n.d.Source;

                            $("#ViacomNewsRSSFeed").empty();

                            var ItemCounter = 0;
                            var ItemTemplate = '';
                            $(NewsFeed).each(function (val,item) {

                                if (ItemCounter % 2 == 0) {
                                    ItemTemplate = '<tr>';
                                }

                                if (item.Source) {
                                    source = item.Source;
                                }

                                ItemTemplate += '<td><div class="news-box">' +
                                      '<h3><a target="_blank" href="' + item['Links'][0]['Uri'] + '">' + item['Title']['Text'] + '</a></h3>' +
                                      '<span class="news-date">' + source + ', ' + ViacomNewsHelper.FormatDate(item['PublishDate']) + '</span>' +
                                      '<div class="news-content">' + item['Summary']['Text'] + '</div>' +
                                      '</div></td>';

                                if (ItemCounter % 2 == 1) {
                                    ItemTemplate += '</tr>';
                                    $("#ViacomNewsRSSFeed").append(ItemTemplate);
                                    ItemTemplate = '';
                                }

                                ItemCounter++;
                            
                            });

                            if (ItemCounter % 2 == 1 && ItemTemplate != '') {
                                ItemTemplate += '<td></td></tr>';
                                $("#ViacomNewsRSSFeed").append(ItemTemplate);
                            }
                        }

                        ViacomNewsHelper.HideLoadingImage();
                    }
                    catch (error) {
                        ExceptionHandler.LogError(error.message, "Viacom News", "NewsHelper.js", "GetRSSFeed");
                        ViacomNewsHelper.HideLoadingImage();
                    }
                },
                error: function (request, status, error) {
                    ExceptionHandler.LogError(error.message, "Viacom News", "NewsHelper.js", "GetRSSFeed");
                    ViacomNewsHelper.HideLoadingImage();
                }
            });
        }
        catch (error)
        {
            ExceptionHandler.LogError(error.message, "Viacom News", "NewsHelper.js", "GetRSSFeed");
            ViacomNewsHelper.HideLoadingImage();
        }
    },
    FormatDate : function (date)
    {
        try
        {
            var newDate = eval(date.replace(/\/Date\((\d+)\)\//gi, "new Date($1)"));

            var mL = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

            var dateStr = mL[newDate.getMonth()] + " " + newDate.getDate() + ", " + newDate.getFullYear();

            return dateStr;
        }
        catch (error)
        {
            ExceptionHandler.LogError(error.message, "Viacom News", "NewsHelper.js", "FormatDate");
            ViacomNewsHelper.HideLoadingImage();
        }
    },
    GetNavigationNewsData: function (type) // method to get data for the tree navigation or drop down.
    {
        try
        {
            if (type == this.ViacomNews) {

                var ViacomNewsSource = []

                // loop all items
                for (i = 0; i < AllNewsItems.results.length; i++) {

                    // check  for null  and empty.
                    if (AllNewsItems.results[i][ViacomNewsHelper.NewsCategory][ViacomNewsHelper.RSSNewsTitle] != '') {

                        // if the rss feed source name not added add it.
                        if ($.inArray(AllNewsItems.results[i][ViacomNewsHelper.NewsCategory][ViacomNewsHelper.RSSNewsTitle], ViacomNewsSource) == -1) {
                            ViacomNewsSource.push(AllNewsItems.results[i][ViacomNewsHelper.NewsCategory][ViacomNewsHelper.RSSNewsTitle]);

                            alert(AllNewsItems.results[i][ViacomNewsHelper.NewsCategory][ViacomNewsHelper.RSSNewsTitle])
                        }
                    }
                }

                return ViacomNewsSource;
            }
            else if (type == this.ExternalNews) {

            }
        }
        catch (error)
        {
            ExceptionHandler.LogError(error.message, "Viacom News", "NewsHelper.js", "GetNavigationNewsData");
            ViacomNewsHelper.HideLoadingImage();
        }
    }
}

