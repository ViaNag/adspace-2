$(document).ready(function () {
    PerformOnLoad_Tablet.loadfunction()
});
var PerformOnLoad_Tablet = {
    GetCurrentUser: function () {
        var e = _spPageContextInfo.webAbsoluteUrl + "/_api/SP.UserProfiles.PeopleManager/GetMyProperties?$select=PictureUrl,DisplayName";
        var t = {
            accept: "application/json;odata=verbose"
        };
        $.ajax({
            url: e,
            contentType: "application/json;odata=verbose",
            headers: t,
            success: PerformOnLoad_Tablet.onSuccess,
            error: PerformOnLoad_Tablet.onError
        })
    },
    onSuccess: function (e, t) {
        var n = e.d.DisplayName;
        var r = e.d.PictureUrl;
        if (r == null) {
            r = _spPageContextInfo.siteAbsoluteUrl + "/SiteAssets/images/ProfilePicture.png"
        }
        $("#ProfilePicture").attr("src", r);
        $("#DisplayName").append(n)
    },
    onError: function (e) {
        alert(e)
    },
    CreateLeftSiteLinks: function () {
        var e = _spPageContextInfo.siteAbsoluteUrl;
        var t = _spPageContextInfo.webAbsoluteUrl;
        currentSiteIndex = t.lastIndexOf("/");
        t = t.substring(currentSiteIndex + 1).toLowerCase();
        if (t == "musicandentertainment") {
            $("#SiteHomeDiv").before("<div class='divHeadNav'><a id=AdSpaceHome href=" + _spPageContextInfo.siteAbsoluteUrl + "/Pages/AdSpace.aspx?devicechannel=tablet class=aStaticHeaderName>AdSpace</a></div>");
            $("#SiteHome").attr("href", _spPageContextInfo.siteAbsoluteUrl + "/musicandentertainment/Pages/Home.aspx?devicechannel=tablet");
            $("#SiteTopContent").parent().css("display", "none");
            $("#SiteAllLinks").attr("href", _spPageContextInfo.siteAbsoluteUrl + "/musicandentertainment/Pages/AllLinks.aspx?devicechannel=tablet");
            $("#SiteAllAnnouncements").attr("href", _spPageContextInfo.siteAbsoluteUrl + "/musicandentertainment/Pages/AllAnnouncements.aspx?devicechannel=tablet");
            $("#SiteBookmarks").parent().css("display", "none");
            $("#topContent").css("display", "none")
        }
        if (t == "international") {
            $("#SiteHomeDiv").before("<div class='divHeadNav'><a id=AdSpaceHome href=" + _spPageContextInfo.siteAbsoluteUrl + "/Pages/AdSpace.aspx?devicechannel=tablet class=aStaticHeaderName>AdSpace</a></div>");
            $("#SiteHome").attr("href", _spPageContextInfo.siteAbsoluteUrl + "/international/Pages/Home.aspx?devicechannel=tablet");
            $("#SiteTopContent").parent().css("display", "none");
            $("#SiteAllLinks").attr("href", _spPageContextInfo.siteAbsoluteUrl + "/international/Pages/AllLinks.aspx?devicechannel=tablet");
            $("#SiteAllAnnouncements").attr("href", _spPageContextInfo.siteAbsoluteUrl + "/international/Pages/AllAnnouncements.aspx?devicechannel=tablet");
            $("#SiteBookmarks").parent().css("display", "none");
            $("#topContent").css("display", "none")
        }
        if (t == "nickelodeon") {
            $("#SiteHomeDiv").before("<div class='divHeadNav'><a id=AdSpaceHome href=" + _spPageContextInfo.siteAbsoluteUrl + "/Pages/AdSpace.aspx?devicechannel=tablet class=aStaticHeaderName>AdSpace</a></div>");
            $("#SiteHome").attr("href", _spPageContextInfo.siteAbsoluteUrl + "/nickelodeon/Pages/Home.aspx?devicechannel=tablet");
            $("#SiteTopContent").parent().css("display", "none");
            $("#SiteAllLinks").attr("href", _spPageContextInfo.siteAbsoluteUrl + "/nickelodeon/Pages/AllLinks.aspx?devicechannel=tablet");
            $("#SiteAllAnnouncements").attr("href", _spPageContextInfo.siteAbsoluteUrl + "/nickelodeon/Pages/AllAnnouncements.aspx?devicechannel=tablet");
            $("#SiteBookmarks").parent().css("display", "none");
            $("#topContent").css("display", "none")
        }
        if (t == "corporatereports") {
            $("#SiteHomeDiv").before("<div class='divHeadNav'><a id=AdSpaceHome href=" + _spPageContextInfo.siteAbsoluteUrl + "/Pages/AdSpace.aspx?devicechannel=tablet class=aStaticHeaderName>AdSpace</a></div>");
            $("#SiteHome").attr("href", _spPageContextInfo.siteAbsoluteUrl + "/corporatereports/Pages/Home.aspx?devicechannel=tablet");
            $("#SiteTopContent").parent().css("display", "none");
            $("#SiteAllLinks").attr("href", _spPageContextInfo.siteAbsoluteUrl + "/corporatereports/Pages/AllLinks.aspx?devicechannel=tablet");
            $("#SiteAllAnnouncements").attr("href", _spPageContextInfo.siteAbsoluteUrl + "/corporatereports/Pages/AllAnnouncements.aspx?devicechannel=tablet");
            $("#SiteBookmarks").parent().css("display", "none");
            $("#topContent").css("display", "none")
        }
        if (t == "searchcenter") {
            if ($(".ms-searchCenter-refinement").length > 0) {
                $("#s4-titlerow").attr("style", "display:block !important");
                $("#searchIcon").hide();
                $(".ms-searchCenter-refinement").css("margin-top", "0");
                $("body").append("<style>#s4-titlerow { display: block !important; }</style>")
            }
            $("#s4-titlerow").removeAttr("class");
            $("#sideNavBox").css("display", "block");
            $("#middleConetntDiv").css("width", "auto");
            $("#SiteHomeDiv").before("<div class='divHeadNav'><a id=AdSpaceHome href=" + _spPagseContextInfo.siteAbsoluteUrl + "/Pages/AdSpace.aspx?devicechannel=tablet class=aStaticHeaderName>AdSpace</a></div>");
            $("#SiteHome").parent().css("display", "none");
            $("#SiteTopContent").parent().css("display", "none");
            $("#SiteAllLinks").parent().css("display", "none");
            $("#SiteAllAnnouncements").parent().css("display", "none");
            $("#SiteBookmarks").parent().css("display", "none");
            $("#topContent").css("display", "none")
        }
    },
    getSideNavStructre: function () {
        var e = $("#sideNavBox");
        var t = e.html();
        $("#TopNavigationMenu").append(t)
    },
    GetNavigationStructure: function () {
        var e = $("#TopNavigation");
        e.css({
            position: "absolute",
            visibility: "hidden",
            display: "block"
        });
        var t = e.html();
        e.css({
            position: "",
            visibility: "",
            display: ""
        });
        $("#TopNavigationMenu").append(t);
        $("#drawerNavBotton").bigSlide();
        $(".aHeaderName").attr("href", "#");
        $(".aHeaderName").removeAttr("target");
        $("ul.ulCategoryNav").each(function () {
            if ($(this).children().length <= 1) {
                $(this).remove()
            }
        })
    },
    loadfunction: function () {
        var e = _spPageContextInfo.userId;
        PerformOnLoad_Tablet.GetCurrentUser();
        PerformOnLoad_Tablet.CreateLeftSiteLinks();
        PerformOnLoad_Tablet.GetNavigationStructure();
    }
}