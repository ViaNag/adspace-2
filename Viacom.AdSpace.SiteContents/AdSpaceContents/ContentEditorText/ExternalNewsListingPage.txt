<script type="text/javascript" src="/_catalogs/masterpage/AdSpaceMasterPageAndLayouts/JS/REST/ExternalNewsHelper.js"></script>
<script type="text/javascript">
$(document).ready(function() {
	ViacomExternalNewsHelper.IsHomePage=false;
    ViacomExternalNewsHelper.GetAllExternalNewsItems(); 
	ViacomExternalNewsHelper.FillExternalNewsArrayObject();
	ViacomExternalNewsHelper.CreateCategoryFilter();
	ViacomExternalNewsHelper.externalRefinerAccordion();
	$(".external-news-refiner.external-news-section a").click(function (event) {
		ViacomExternalNewsHelper.ShowLoadingImage();
		$('.external-news-refiner.external-news-section > .selected').removeClass('selected');
		if($(this).closest('ul').length > 0)
		{
			$(this).closest('ul').prev('h4').addClass('selected');
		}
		else
		{
			$(this).closest('h4').addClass('selected');
		}
		var selectedFeedElementId=$(this).attr('id');	
        ViacomExternalNewsHelper.ShowExternalRSSFeed(selectedFeedElementId);
    });
	
});
</script>
<style type="text/css">
.welcome-content{display:none;}
</style>
<div class="heading-bg hide-mobile">Viacom External News</div>
<div class="content-padding">
	<div class="external-news">
		<img id="imgRssFeedLoading" src="/_catalogs/masterpage/AdSpaceMasterPageAndLayouts/images/Loading.gif" style="display:none;" width="40" height="40"/>
		<div class="content-padding">
			<div class="external-news-refiner external-news-section">
			</div>
			<div class="external-news-results">				
			</div>
		</div>
	</div>
</div>
