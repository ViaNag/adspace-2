//var PerformOnLoad={lexis:"disabled",getQuerystring:function(e){e=e.replace(/[\[]/,"\\[").replace(/[\]]/,"\\]");var t=new RegExp("[\\?&]"+e+"=([^&#]*)");var n=t.exec(window.location.href);if(n==null)return null;else return n[1]},HighlightCurrentSiteNav:function(){var e=_spPageContextInfo.siteAbsoluteUrl;var t=_spPageContextInfo.webAbsoluteUrl;currentSiteIndex=t.lastIndexOf("/");t=t.substring(currentSiteIndex+1).toLowerCase();$(".aHomeLink").attr("href",e);if(t=="adspaceviacom")$(".aHomeLink").css("background-color","#EA8525");if(t=="musicandentertainment")$("a[id*='music']").css("background-color","#EA8525");if(t=="international")$("a[id*='international']").css("background-color","#EA8525");if(t=="nickelodeon")$("a[id*='nickelodeon']").css("background-color","#EA8525");if(t=="corporatereports")$("a[id*='corporatereports']").css("background-color","#EA8525");$("ul.ulCategoryNav").each(function(){if($(this).children().length<=1){$(this).remove()}})},LexixNexixSearch:function(e){var t=document.getElementById("lxselect").selectedIndex;try{if(e!=""){if(t==0){window.open("https://w3.nexis.com/new/api/accessGW?gw=FC&suppressTab=ID&ui=HOWARDBRATHWAITE4&pw=QY9FTZ&hgn=t&nm="+e)}else if(t==1){window.open("https://w3.nexis.com/new/api/accessGW?gw=FC&suppressTab=ID&ui=HOWARDBRATHWAITE4&pw=QY9FTZ&hgn=t&tr="+e)}}else{alert("Please enter a search key")}}catch(n){alert(n)}},iframeResize:function(){iFrameResize({autoResize:true,enablePublicMethods:true,checkOrigin:false,sizeWidth:false})}};$(document).ready(function(){$(".styled-select").hide();$(".ms-webpart-chrome.ms-webpart-chrome-vertical ").removeAttr("style");PerformOnLoad.HighlightCurrentSiteNav();$("#ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr_sboxdiv").append('<a id="searchMe" style="display:none"><img id="searchImg"  src="'+_spPageContextInfo.siteAbsoluteUrl+'/SiteAssets/images/searchIcon.png" /></a>');$("div#verticalSearch ul.ms-srchnav-list").append('<li class="ms-srchnav-item ms-verticalAlignTop"><h2 class="ms-displayInline"><a id="lexisNexis" href="javascript:{}" onclick="" class="ms-srchnav-link" title="Search Lexis Nexis">Lexis Nexis</a></h2></li>');$("a#lexisNexis").click(function(e){if($(".styled-select").is(":hidden")){$("a#lexisNexis").css("font-weight","bold");PerformOnLoad.lexis="enabled";$("input#ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr_sbox").attr("title","Search Lexis Nexis");$(".styled-select").show();$("#ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr_sboxdiv a.ms-srch-sb-searchLink").css("display","none");$("#searchMe").css("display","inline-block");if($("input[title='Search Lexis Nexis']").val()!=""){$("a#searchMe").click(function(){var e=$("input[title='Search Lexis Nexis']").val();PerformOnLoad.LexixNexixSearch(e)})}}else{PerformOnLoad.lexis="disabled";$(".styled-select").hide();$(this).css("font-weight","normal");$("#ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr_sboxdiv a.ms-srch-sb-searchLink").css("display","inline-block");$("#searchMe").css("display","none")}});if($('.ms-core-listMenu-verticalBox:contains("Site Contents")')){}iFrameResize({autoResize:true,enablePublicMethods:true,checkOrigin:false,sizeWidth:false})})

var PerformOnLoad = {
	lexis: "disabled",
	getQuerystring: function(e) {
		e = e.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var t = new RegExp("[\\?&]" + e + "=([^&#]*)");
		var n = t.exec(window.location.href);
		if (n == null) return null;
		else return n[1]
	},
	HighlightCurrentSiteNav: function() {
		var e = _spPageContextInfo.siteAbsoluteUrl;
		var t = _spPageContextInfo.webAbsoluteUrl;
		currentSiteIndex = t.lastIndexOf("/");
		t = t.substring(currentSiteIndex + 1).toLowerCase();
		$(".aHomeLink").attr("href", e);
		if (t == "adspaceviacom") $(".aHomeLink").css("background-color", "#EA8525");
		if (t == "musicandentertainment") $("a[id*='music']").css("background-color", "#EA8525");
		if (t == "international") $("a[id*='international']").css("background-color", "#EA8525");
		if (t == "nickelodeon") $("a[id*='nickelodeon']").css("background-color", "#EA8525");
		if (t == "corporatereports") $("a[id*='corporatereports']").css("background-color", "#EA8525");
		$("ul.ulCategoryNav").each(function() {
			if ($(this).children().length <= 1) {
				$(this).remove()
			}
		})
	},
	LexixNexixSearch: function(e) {
		var t = document.getElementById("lxselect").selectedIndex;
		try {
			if (e != "") {
				if (t == 0) {
					window.open("https://w3.nexis.com/new/api/accessGW?gw=FC&suppressTab=ID&ui=HOWARDBRATHWAITE4&pw=QY9FTZ&hgn=t&nm=" + e)
				} else if (t == 1) {
					window.open("https://w3.nexis.com/new/api/accessGW?gw=FC&suppressTab=ID&ui=HOWARDBRATHWAITE4&pw=QY9FTZ&hgn=t&tr=" + e)
				}
			} else {
				alert("Please enter a search key")
			}
		} catch (n) {
			alert(n)
		}
	},
	iframeResize: function() {
		iFrameResize({
			autoResize: true,
			enablePublicMethods: true,
			checkOrigin: false,
			sizeWidth: false
		})
	}
};
$(document).ready(function() {
    $(".ms-cui-ctl-large").each(function(index,alg)
	{ 
	    if(index==3)
	    {
	       
	       $(this).hide();
	    } 
	})
	$(".styled-select").hide();
	$(".ms-webpart-chrome.ms-webpart-chrome-vertical ").removeAttr("style");
	PerformOnLoad.HighlightCurrentSiteNav();
	$("#ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr_sboxdiv").append('<a id="searchMe" style="display:none"><img id="searchImg"  src="' + _spPageContextInfo.siteAbsoluteUrl + '/SiteAssets/images/searchIcon.png" /></a>');
	$("div#verticalSearch ul.ms-srchnav-list").append('<li class="ms-srchnav-item ms-verticalAlignTop"><h2 class="ms-displayInline"><a id="lexisNexis" href="javascript:{}" onclick="" class="ms-srchnav-link" title="Search Lexis Nexis">Lexis Nexis</a></h2></li>');
	$("a#lexisNexis").click(function(e) {
		if ($(".styled-select").is(":hidden")) {
			$("a#lexisNexis").css("font-weight", "bold");
			PerformOnLoad.lexis = "enabled";
			$("input#ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr_sbox").attr("title", "Search Lexis Nexis");
			$(".styled-select").show();
			$("#ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr_sboxdiv a.ms-srch-sb-searchLink").css("display", "none");
			$("#searchMe").css("display", "inline-block");
			if ($("input[title='Search Lexis Nexis']").val() != "") {
				$("a#searchMe").click(function() {
					var e = $("input[title='Search Lexis Nexis']").val();
					PerformOnLoad.LexixNexixSearch(e)
				})
			}
		} else {
			PerformOnLoad.lexis = "disabled";
			$(".styled-select").hide();
			$(this).css("font-weight", "normal");
			$("#ctl00_PlaceHolderSearchArea_SmallSearchInputBox1_csr_sboxdiv a.ms-srch-sb-searchLink").css("display", "inline-block");
			$("#searchMe").css("display", "none")
		}
	});
	if ($('.ms-core-listMenu-verticalBox:contains("Site Contents")')) {}
	iFrameResize({
		autoResize: true,
		enablePublicMethods: true,
		checkOrigin: false,
		sizeWidth: false
	})
	
	

})