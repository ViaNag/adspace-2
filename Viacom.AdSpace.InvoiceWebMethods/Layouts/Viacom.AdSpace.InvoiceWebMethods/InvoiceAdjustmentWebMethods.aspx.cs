﻿using System;
using System.Web.Services;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using Viacom.AdSpace.Invoice.BAL;
using Viacom.AdSpace.Invoice.BAL.InvoiceAdjustment;
using Viacom.AdSpace.Invoice.BAL.Entities;
using Viacom.AdSpace.Invoice.BAL.Email;

namespace Viacom.AdSpace.InvoiceWebMethods.Layouts.Viacom.AdSpace.InvoiceWebMethods
{
    public partial class InvoiceAdjustmentWebMethods : LayoutsPageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        static InvoiceAdjustmentRequest InvoiceOps
        {
            get { return new InvoiceAdjustmentRequest(); }
        }

        static BillingEmail BEmail
        {
            get { return new BillingEmail(); }
        }

        static RateAdjustmentRequest RateAdjustOps
        {
            get { return new RateAdjustmentRequest(); }
        }

        #region WebMethods
        /// <summary>
        /// 
        /// </summary>
        /// <param name=""></param>
        [WebMethod]
        public static string FetchNetworksForCurrentUser()
        {
            string result = null;
            try
            {
                IInvoiceMethodResponse<List<Network>> res=InvoiceOps.GetNetworksList();
                if(res.ActionSuccessful)
                {
                    result = ObjecttoJSON(res.Data);
                }
                else 
                {
                    if (res.ExceptionIfAny != null) throw res.ExceptionIfAny;
                    else throw new Exception("InvoiceOps method Action was not succcessful");
                }
            }
            catch(Exception ex){
                throw new Exception(ex.Message, ex); 
            }
            return result;
        }
        [WebMethod]
        public static string FetchBrandForNetwork(string network)
        {
            string result = null;
            try
            {
                IInvoiceMethodResponse<List<Brand>> res = InvoiceOps.GetBrandsForNetwork(network);
                if (res.ActionSuccessful)
                {
                    result = ObjecttoJSON(res.Data);
                }
                else
                {
                    if (res.ExceptionIfAny != null) throw res.ExceptionIfAny;
                    else throw new Exception("InvoiceOps method Action was not succcessful");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return result;
        }

        [WebMethod]
        public static string FetchInvoicesForBrand(string network ,string brand)
        {
            string result = null;
            try
            {
                IInvoiceMethodResponse<List<string>> res = InvoiceOps.GetInvoicesForBrand(network, brand);
                if (res.ActionSuccessful)
                {
                    result = ObjecttoJSON(res.Data);
                }
                else
                {
                    if (res.ExceptionIfAny != null) throw res.ExceptionIfAny;
                    else throw new Exception("InvoiceOps method Action was not succcessful");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return result;
        }

        [WebMethod]
        public static string FetchInvoiceDetails(string invoiceID)
        {
            string result = null;
            try
            {
                IInvoiceMethodResponse<InvoiceDetails> res = InvoiceOps.GetInvoiceDetails(invoiceID);
                if (res.ActionSuccessful)
                {
                    result = ObjecttoJSON(res.Data);
                }
                else
                {
                    if (res.ExceptionIfAny != null) throw res.ExceptionIfAny;
                    else throw new Exception("InvoiceOps method Action was not succcessful");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return result;
        }

        [WebMethod]
        public static bool IsRequestor(string network)
        {
            bool result = false;
            try
            {
                IInvoiceMethodResponse<bool> res = InvoiceOps.IsRequestor(network);
                if (res.ActionSuccessful)
                {
                    result = res.Data;
                }
                else
                {
                    if (res.ExceptionIfAny != null) throw res.ExceptionIfAny;
                    else throw new Exception("InvoiceOps method Action was not succcessful");
                }
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return result;
        }
        
        [WebMethod]
        public static bool IsApprover(string itemID)
        {
            bool result = false;
            try
            {
                IInvoiceMethodResponse<bool> res = InvoiceOps.IsApprover(itemID);
                if (res.ActionSuccessful)
                {
                    result = res.Data;
                }
                else
                {
                    if (res.ExceptionIfAny != null) throw res.ExceptionIfAny;
                    else throw new Exception("InvoiceOps method Action was not succcessful");
                }
            }
            catch(Exception ex) 
            {
                throw new Exception(ex.Message, ex);
            }
            return result;
        }

        [WebMethod]
        public static string IsFormEditable(string itemID)
        {
            string result = null;
            try
            {
                IInvoiceMethodResponse<Dictionary<string, bool>> res = InvoiceOps.IsFormEditable(itemID);
                if (res.ActionSuccessful)
                {
                    result = ObjecttoJSON(res.Data);
                }
                else
                {
                    if (res.ExceptionIfAny != null) throw res.ExceptionIfAny;
                    else throw new Exception("InvoiceOps method Action was not succcessful");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return result;
        }

        [WebMethod]
        public static bool IsBillingVisible(string itemID)
        {
            bool result = false;
            try
            {
                IInvoiceMethodResponse<bool> res = InvoiceOps.IsBillingVisible(itemID);
                if (res.ActionSuccessful)
                {
                    result = res.Data;
                }
                else
                {
                    if (res.ExceptionIfAny != null) throw res.ExceptionIfAny;
                    else throw new Exception("InvoiceOps method Action was not succcessful");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return result;
        }

        [WebMethod]
        public static CurrentStatus GetCurrentStatus(string itemID)
        {
            CurrentStatus result = null;
            try
            {
                IInvoiceMethodResponse<CurrentStatus> res = InvoiceOps.GetCurrentRequestStatus(itemID);
                if (res.ActionSuccessful)
                {
                    result = res.Data;
                }
                else
                {
                    if (res.ExceptionIfAny != null) throw res.ExceptionIfAny;
                    else throw new Exception("InvoiceOps method Action was not succcessful");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return result;
        }

        [WebMethod]
        public static bool SaveApproverComment(string json)
        {
            bool result = false;
            try
            {
                result = _saveApproverComment(json);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return result;
        }

        [WebMethod]
        public static bool SaveInvoiceChanges(string json)
        {
            bool result = false;
            try
            {
                result = _saveInvoiceChanges(json);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return result;
        }

        [WebMethod]
        public static string CurrentRoles(string network)
        {
            string result = string.Empty;
            try
            {
                IInvoiceMethodResponse<List<IRole>> res = InvoiceOps.GetCurrentUserRoles(network);
                if (res.ActionSuccessful)
                {
                    result = ObjecttoJSON(res.Data);
                }
                else
                {
                    if (res.ExceptionIfAny != null) throw res.ExceptionIfAny;
                    else throw new Exception("InvoiceOps method Action was not succcessful");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return result;
        }

        [WebMethod]
        public static string GetCurrentStageMessageByStageName(string stageName, string adjAmount)
        {
            string result = string.Empty;
            try
            {
                IInvoiceMethodResponse<string> res = InvoiceOps.GetCurrentStatusMessageByStage(stageName, adjAmount);
                if (res.ActionSuccessful)
                {
                    result = res.Data;
                }
                else
                {
                    if (res.ExceptionIfAny != null) throw res.ExceptionIfAny;
                    else throw new Exception("InvoiceOps method Action was not succcessful");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return result;
        }

        [WebMethod]
        public static string SendBillingEmail(string itemID)
        {
            string result = null;
            try
            {
                IInvoiceMethodResponse<BillingEmail> res = BEmail.SendBillingEmail(itemID);
                if (res.ActionSuccessful)
                {
                    result = res.Data.EmailSentResponse;
                }
                else
                {
                    if (res.ExceptionIfAny != null) throw res.ExceptionIfAny;
                    else throw new Exception("Error while sending Billing Email");
                }  
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return result;
        }

        [WebMethod]
        public static string GetTrafficDealByNetwork(string network)
        {
            string result = null;
            try
            {
                IInvoiceMethodResponse<List<TrafficDeal>> res = RateAdjustOps.GetTrafficDealByNetwork(network);
                if (res.ActionSuccessful)
                {
                    result = ObjecttoJSON(res.Data);
                }
                else
                {
                    if (res.ExceptionIfAny != null) throw res.ExceptionIfAny;
                    else throw new Exception("GetTrafficDealByNetwork - RateAdjustOps method Action was not succcessful");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return result;
        }

        [WebMethod]
        public static string GetDealDetailsByNetworkNDeal(string network, string dealno)
        {
            string result = null;
            try
            {
                IInvoiceMethodResponse<List<TrafficDealDetails>> res = RateAdjustOps.GetDealDetailsByNetworkNDeal(network, dealno);
                if (res.ActionSuccessful)
                {
                    result = ObjecttoJSON(res.Data);
                }
                else
                {
                    if (res.ExceptionIfAny != null) throw res.ExceptionIfAny;
                    else throw new Exception("GetDealDetailsByNetworkNDeal - RateAdjustOps method Action was not succcessful");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return result;
        }

        [WebMethod]
        public static string GetBrandByNetworkNDeal(string network, string dealno)
        {
            string result = null;
            try
            {
                IInvoiceMethodResponse<List<Brand>> res = RateAdjustOps.GetBrandByNetworkNDeal(network, dealno);
                if (res.ActionSuccessful)
                {
                    result = ObjecttoJSON(res.Data);
                }
                else
                {
                    if (res.ExceptionIfAny != null) throw res.ExceptionIfAny;
                    else throw new Exception("GetBrandByNetworkNDeal - RateAdjustOps method Action was not succcessful");
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return result;
        }

        [WebMethod]
        public static bool SaveRateAdjChanges(string json)
        {
            bool result = false;
            try
            {
                result = _saveRateAdjChanges(json);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return result;
        }

        [WebMethod]
        public static string SendFinalEmail(string itemID, string dispFormPath)
        {
            string result = null;
            try
            {
                IInvoiceMethodResponse<BillingEmail> res = BEmail.GetFinallyApprovedEmail(itemID, dispFormPath);
                if (res.ActionSuccessful)
                {
                    result = res.Data.EmailSentResponse;
                }
                else
                {
                    if (res.ExceptionIfAny != null) throw res.ExceptionIfAny;
                    else throw new Exception("Error while sending Finally Approved Email");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        #endregion 

        #region Private Methods

        static string ObjecttoJSON(object obj)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Serialize(obj); 
        }

        private static bool _saveApproverComment(string json){
            JObject dataObj = null;
            bool result = false;
            try
            {
                if (!string.IsNullOrEmpty(json))
                {
                    dataObj = JObject.Parse(json);
                    
                    string itemID = Convert.ToString(dataObj[Constants.ITEMIDKEY]);
                    string approvalStatus = Convert.ToString(dataObj[Constants.APPROVALSTATUSKEY]);
                    string comment = Convert.ToString(dataObj[Constants.COMMENTKEY]);
                    bool isBillingSelected = Convert.ToBoolean(dataObj[Constants.BILLINGSELECTEDKEY]);
                    if (!String.IsNullOrEmpty(itemID) && !String.IsNullOrEmpty(approvalStatus) && !String.IsNullOrEmpty(comment)) 
                    {
                        IInvoiceMethodResponse<bool> res = InvoiceOps.SaveApproverComment(itemID, approvalStatus, comment, isBillingSelected);
                        if (res.ActionSuccessful)
                        {
                            result = res.Data;
                        }
                        else
                        {
                            if (res.ExceptionIfAny != null) throw res.ExceptionIfAny;
                            else throw new Exception("InvoiceOps method Action was not succcessful");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return result;
        }
        private static bool _saveInvoiceChanges(string json)
        {
            JObject dataObj = null;
            bool result = false;
            string itemID = null;
            List<KeyValuePair<string, string>> kvpList = new List<KeyValuePair<string,string>>();
            try
            {
                if (!string.IsNullOrEmpty(json))
                {
                    dataObj = JObject.Parse(json);
                    itemID = Convert.ToString(dataObj[Constants.ITEMIDKEY]);
                    var resultObjects = AllChildren(dataObj)
                    .First(c => c.Type == JTokenType.Array && c.Path.Contains(Constants.INVOICEDATAKEY))
                    .Children<JObject>();

                    foreach (JObject childItem in resultObjects)
                    {
                        foreach (JProperty property in childItem.Properties())
                        {
                            kvpList.Add(new KeyValuePair<string, string>(property.Name, property.Value.ToString()));
                        }
                    }
                    if (itemID != null && kvpList != null)
                    {
                        IInvoiceMethodResponse<bool> res = InvoiceOps.SaveInvoiceChanges(itemID, kvpList);
                        if (res.ActionSuccessful)
                        {
                            result = res.Data;
                        }
                        else
                        {
                            if (res.ExceptionIfAny != null) throw res.ExceptionIfAny;
                            else throw new Exception("InvoiceOps method Action was not succcessful");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return result;
        }
        private static IEnumerable<JToken> AllChildren(JToken json)
        {
            foreach (var c in json.Children())
            {
                yield return c;
                foreach (var cc in AllChildren(c))
                {
                    yield return cc;
                }
            }
        }

        private static bool _saveRateAdjChanges(string json)
        {
            JObject dataObj = null;
            bool result = false;
            string itemID = null;
            List<KeyValuePair<string, string>> kvpList = new List<KeyValuePair<string, string>>();
            List<KeyValuePair<string, string>> childKvpList = null;
            try
            {
                if (!string.IsNullOrEmpty(json))
                {
                    dataObj = JObject.Parse(json);
                    itemID = Convert.ToString(dataObj[Constants.ITEMIDKEY]);
                    var resultObjects = AllChildren(dataObj)
                    .First(c => c.Type == JTokenType.Array && c.Path.Contains(Constants.INVOICEDATAKEY))
                    .Children<JObject>();

                    foreach (JObject childItem in resultObjects)
                    {
                        foreach (JProperty property in childItem.Properties())
                        {
                            kvpList.Add(new KeyValuePair<string, string>(property.Name, property.Value.ToString()));
                        }
                    }

                    var resultChildObjects = AllChildren(dataObj)
                    .First(c => c.Type == JTokenType.Array && c.Path.Contains(Constants.INVOICECHILDITEMS))
                    .Children<JObject>();

                    ArrayList childList = new ArrayList();

                    foreach (JObject childItem in resultChildObjects)
                    {
                        childKvpList = new List<KeyValuePair<string, string>>();
                        foreach (JProperty property in childItem.Properties())
                        {
                            childKvpList.Add(new KeyValuePair<string, string>(property.Name, property.Value.ToString()));
                        }
                        childList.Add(childKvpList);
                    }

                    if (itemID != null && kvpList != null && childList.Count > 0)
                    {
                        IInvoiceMethodResponse<bool> res = RateAdjustOps.SaveRateAdjChanges(itemID, kvpList, childList);
                        if (res.ActionSuccessful)
                        {
                            result = res.Data;
                        }
                        else
                        {
                            if (res.ExceptionIfAny != null) throw res.ExceptionIfAny;
                            else throw new Exception("_saveRateAdjChanges - InvoiceOps method Action was not succcessful");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
            return result;
        }

        #endregion
    }
}
