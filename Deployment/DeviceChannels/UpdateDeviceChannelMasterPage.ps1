[Reflection.Assembly]::LoadFrom("C:\Program Files\Common Files\microsoft shared\Web Server Extensions\15\ISAPI\Microsoft.SharePoint.Publishing.dll")

[Reflection.Assembly]::LoadFrom("C:\Program Files\Common Files\microsoft shared\Web Server Extensions\15\ISAPI\Microsoft.SharePoint.dll")

if ((Get-PSSnapin "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue) -eq $null) 
{
    Add-PSSnapin "Microsoft.SharePoint.PowerShell"
}

Write-host "Enter Site Collection URL"
$siteUrl =  Read-Host


$typeWeb = [Microsoft.SharePoint.SPWeb]
$typeBool = [System.Boolean]

$typeMappingFile = [System.Type]::GetType("Microsoft.SharePoint.Publishing.Mobile.MasterPageMappingsFile, Microsoft.SharePoint.Publishing,Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c")

$consMappingFileParams = ($typeWeb, $typeBool, $typeWeb)

$consMappingFile = $typeMappingFile.GetConstructor($consMappingFileParams)

$mappingFileParams = [System.Array]::CreateInstance([System.Object], 3)
$mappingFileParams[0] = (Get-SPSite $siteUrl).RootWeb
$mappingFileParams[1] = $false
$mappingFileParams[2] = $null

$mappingFileTablet = $consMappingFile.Invoke($mappingFileParams)

$mappingFileTablet["Tablet"].MasterPageUrl = "/_catalogs/masterpage/AdSpaceIPadMaster.master"

$mappingFileTablet.UpdateSingleChannel("Tablet")


