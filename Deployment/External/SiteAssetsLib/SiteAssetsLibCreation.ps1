#add sharepoint cmdlets
if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null )
{    
      Add-PsSnapin Microsoft.SharePoint.PowerShell
}

Write-Host "Enter the site collection url"
$spWebUrl = Read-Host

$spWeb = Get-SPWeb -Identity $spWebUrl 
$listTemplate = [Microsoft.SharePoint.SPListTemplateType]::DocumentLibrary 
$spWeb.Lists.Add("SiteAssets","Site Assets Library",$listTemplate)
$spWeb.Update()
$spDocumentLibrary = $spWeb.Lists["SiteAssets"]
$spDocumentLibrary.Title = "Site Assets"
$spDocumentLibrary.Update()