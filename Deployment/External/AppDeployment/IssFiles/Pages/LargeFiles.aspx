﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LargeFiles.aspx.cs" Inherits="Viacom.AdSpace.AppsWeb.LaregeFiles" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        
    </style>
    <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table border="0" class="fullWidth">
                <tr>
                    <td colspan="2">
                        <hr class="fullWidth" style="height: 2px" />
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label runat="server" ID="SubTitle" Text="Choose a file :"></asp:Label>
                        &nbsp;</td>
                    <td>
                        <asp:FileUpload ID="FileUpload1" BorderStyle="Ridge" Width="200px" runat="server" AllowMultiple="True" /></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Label runat="server" ID="Label3" Font-Size="Smaller" Text="Browse to the document you intend to submit"></asp:Label>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox ID="chkOverride" runat="server" Checked="true" Text="Override Existing File" />
                    </td>
                </tr>
                <tr>

                    <td>
                        <asp:CheckBox ID="chkEmail" runat="server" Checked="false" Text="Send Email Notification" />
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Button CssClass="button" ID="btnUpload" runat="server" OnClick="btnUpload_Click" Text="Upload" Width="80px" /></td>
                    <td>
                        <asp:Button CssClass="button" ID="btnCancel" runat="server" OnClientClick="javascript:window.parent.postMessage('CloseCustomActionDialogNoRefresh', '*');" Text="Cancel" Width="80px" /></td>
                </tr>
            </table>
            <asp:Label ID="Label1" runat="server" Text="File Names:" Visible="true"></asp:Label>
            <br />
            <asp:Label ID="lblFileName" runat="server" Text="Label" Visible="False"></asp:Label>
            <br />
            <asp:Label ID="lbl" runat="server"></asp:Label>
        </div>
    </form>
</body>
</html>
