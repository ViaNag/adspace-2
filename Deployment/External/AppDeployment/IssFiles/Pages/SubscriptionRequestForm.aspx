﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SubscriptionRequestForm.aspx.cs" Inherits="Viacom.AdSpace.AppsWeb.Pages.SubscriptionRequestForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">

    <head runat="server">
       <title></title>
       <style type="text/css">
           #cancel
           {
               float:none!important;
           }
       </style>
        <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="../styles/CSS/Subscriptions.css" type="text/css"/>
         <script type="text/javascript" src="../Scripts/js/jquery.js"></script>
        <script src="../Scripts/js/iframeResizer.contentWindow.min.js"></script>
        <script src="../Scripts/js/jquery.js" type="text/javascript"></script>
        <script src="../Scripts/js/jquery-ui.js" type="text/javascript"></script>
       
        <script type="text/javascript">
            var spHostUrl;
            $(document).ready(function ()
            {
                $('table[id$="grdSubscriptions"]').find('textarea[id*="_txtReasonForAccess"]').attr('disabled', "disabled");
                spHostUrl = decodeURIComponent(getQueryStringParameter('SPHostUrl'));
                
            });

            function getQueryStringParameter(urlParameterKey) {
                var params = document.URL.split('?')[1].split('&');
                var strParams = '';
                for (var i = 0; i < params.length; i = i + 1) {
                    var singleParam = params[i].split('=');
                    if (singleParam[0] == urlParameterKey)
                        return decodeURIComponent(singleParam[1]);
                }
            }

            function CheckAllEmp(Checkbox)
            {
               var GridVwHeaderChckbox = document.getElementById("grdSubscriptions");
            
               for (i = 1; i < GridVwHeaderChckbox.rows.length; i++)
               {
                   //alert(Checkbox.checked);
                   GridVwHeaderChckbox.rows[i].cells[0].getElementsByTagName("INPUT")[0].checked = Checkbox.checked;
                   
                   if (Checkbox.checked) {
                       GridVwHeaderChckbox.rows[i].style.backgroundColor = "#E6F2FB";
                       GridVwHeaderChckbox.rows[i].style.border = "5px solid white";
                       GridVwHeaderChckbox.rows[i].cells[2].getElementsByTagName("TEXTAREA")[0].disabled = false;
 
                   }
                   else
                   {
                       GridVwHeaderChckbox.rows[i].style.backgroundColor = "white";
                       GridVwHeaderChckbox.rows[i].style.border = "5px solid white";
                       GridVwHeaderChckbox.rows[i].cells[2].getElementsByTagName("TEXTAREA")[0].disabled = true;
                       GridVwHeaderChckbox.rows[i].cells[2].getElementsByTagName("TEXTAREA")[0].value = "";
                   }
                }
            }

            function GetSelectedRow(Checkbox)
            {
                var GridVwHeaderChckbox = document.getElementById("grdSubscriptions");
                var row = Checkbox.parentNode.parentNode;
                var rowIndex = row.rowIndex ;
                if (Checkbox.checked)
                {

                    GridVwHeaderChckbox.rows[rowIndex].style.backgroundColor = "#E6F2FB";
                    GridVwHeaderChckbox.rows[rowIndex].style.border = "5px solid white";
                    GridVwHeaderChckbox.rows[rowIndex].cells[2].getElementsByTagName("TEXTAREA")[0].disabled= false;
                }
                else
                {
                    var SelectAllChkBox = document.getElementById("chkboxSelectAll");
                    if (SelectAllChkBox.checked)
                    {
                        SelectAllChkBox.checked = false;
                    }
                    GridVwHeaderChckbox.rows[rowIndex].style.backgroundColor = "white";
                    GridVwHeaderChckbox.rows[rowIndex].style.border = "5px solid white";
                    GridVwHeaderChckbox.rows[rowIndex].cells[2].getElementsByTagName("TEXTAREA")[0].disabled = true;
                    GridVwHeaderChckbox.rows[rowIndex].cells[2].getElementsByTagName("TEXTAREA")[0].value = "";
                }
                    
                //alert("RowIndex: " + rowIndex);
                return false;
            }
            function redirect() {

                window.parent.location.href = spHostUrl+"/Pages/AllSubscriptions.aspx";
            }
        </script>

    </head>

    <body>
        <form runat="server" id="frmSubscriptionReqForm">
            <asp:HiddenField ID="lblSubscriptionReqSenderID" runat="server"></asp:HiddenField> 
            <div id="buttons" runat="server" class="dvRow" style="text-align:right;padding-top:5px">
		        <asp:button ID="save" runat="server" class="requestButton"   OnClick="save_Click" Text="Save"></asp:button>
		        <asp:button ID="cancel" runat="server"  class="requestButton"   OnClick="cancel_Click" Text="Cancel">  </asp:button>
		    </div>
        <div id="dvStatus" runat="server"></div>
        <div id="DvSubscription" runat="server">
            <div class="dvRow">
                <div  class="leftsection">Requestor&#39;s Name :</div>
                <div class="rightsection"><asp:label runat="server" id="lblrequestor"></asp:label></div>
            </div>
            <div class="dvRow" runat="server" id="DvPendingReq" visible="true">
                <div  class="leftsection">Pending Requests :</div>
                <div class="rightsection"><asp:label runat="server" id="lblPendingRequest"></asp:label></div>
            </div>
            <div class="dvRow">
                <div class="leftsection" > Subscriptions :</div>
                <div class="rightsection" id="NoSubscriptionstoReq" visible="false"  runat="server">No subscriptions to be requested. Please access <a  href="#"  onclick="javascript:redirect();">Subscriptions</a> page to view your subscriptions.</div>
		        <div class="rightsection" id="dvSubscriptionsGrid" runat="server">
                
                   
                     <asp:GridView ID="grdSubscriptions" runat="server" AutoGenerateColumns="False"
                                        Width="100%" GridLines="None" BorderWidth="5px" CellPadding="5" BorderStyle="None"
                                        AlternatingRowStyle-CssClass="GridLine" RowStyle-CssClass="GridLine"
                                       >
                                        <AlternatingRowStyle CssClass="GridLine" />

                                        <Columns>
                                            <asp:TemplateField ItemStyle-Width="5px" HeaderStyle-Width="5px" HeaderStyle-CssClass="GridHeaderLeft"
                                                ItemStyle-CssClass="GridRowLeft">
                                                <HeaderTemplate>
                                                    <asp:CheckBox Text="" ID="chkboxSelectAll" ClientIDMode="Static" runat="server" onclick="CheckAllEmp(this);" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox runat="server" ID="chkEmp" Text="" ClientIDMode="Static" onclick="GetSelectedRow(this);"></asp:CheckBox>
                                                </ItemTemplate>
                                            
                                                <HeaderStyle CssClass="GridHeaderLeft" Width="5px"></HeaderStyle>
                                                <ItemStyle VerticalAlign="Top" CssClass="GridRowLeft" Width="5px"></ItemStyle>
                                            
                                            </asp:TemplateField>
                                        
                                            <asp:TemplateField ItemStyle-VerticalAlign="Top" 
                                                HeaderText="Subscription Name" HeaderStyle-CssClass="GridHeaderLeft" ItemStyle-CssClass="GridRowLeft">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTitle" runat="server" Text='<%# Bind("SubscriptionName") %>'></asp:Label>
                                                </ItemTemplate>

                                            <HeaderStyle CssClass="GridHeaderLeft"></HeaderStyle>

                                            <ItemStyle VerticalAlign="Top" CssClass="GridRowLeft" ></ItemStyle>
                                            </asp:TemplateField>

                                            <asp:TemplateField ItemStyle-VerticalAlign="Top" 
                                                HeaderText="Reason For Access" HeaderStyle-CssClass="GridHeaderLeft" ItemStyle-CssClass="GridRowLeft">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtReasonForAccess" runat="server" rows="2" TextMode="MultiLine" ></asp:TextBox>
                                                </ItemTemplate>
                                            <HeaderStyle CssClass="GridHeaderLeft" ></HeaderStyle>
                                            <ItemStyle VerticalAlign="Top" CssClass="GridRowLeft" ></ItemStyle>
                                            </asp:TemplateField>

                                            <asp:TemplateField Visible="false" HeaderText="Subscription ID">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblID" runat="server" Text='<%# Bind("ID_Type") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>

                                        <RowStyle CssClass="GridLine"></RowStyle>
                                    </asp:GridView>

		        </div>
            </div>
        
        </div>
    
  <asp:Label class="dvStatus" ID="errorlbl" runat="server"></asp:Label>
  </form>
</body>
</html>
