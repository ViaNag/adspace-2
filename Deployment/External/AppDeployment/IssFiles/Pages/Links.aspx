﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Links.aspx.cs" Inherits="Viacom.AdSpace.AppsWeb.Pages.Links" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <script type="text/javascript" src="../Scripts/js/jquery.js"></script>
    <script src="../Scripts/js/CustomJs/Links.js" type="text/javascript"></script>
     <link rel="stylesheet" href="../styles/CSS/Links.css" />
    <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css" />
    <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css" />
    <script src="../Scripts/js/iframeResizer.contentWindow.min.js"></script>
 
</head>
<body>
  <form id="frmLinks" runat="server" class="fullWidth">
      <asp:HiddenField ID="lblSender" runat="server"></asp:HiddenField>
        <div id="mainDv" runat="server"  class="fullWidth">
    <div class="visible">
    <asp:Label ID="lbl" runat="server" Text="" ></asp:Label>
    </div>
        <div id="dvLinks" runat="server" class="fullWidth"></div></div>
    </form>
</body>
</html>
