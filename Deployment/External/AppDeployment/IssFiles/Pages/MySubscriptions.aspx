﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MySubscriptions.aspx.cs" Inherits="Viacom.AdSpace.AppsWeb.Pages.MySubscriptions" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">

       
    </style>
    <script type="text/javascript" src="../Scripts/js/jquery.js"></script>
    <script src="../Scripts/js/iframeResizer.contentWindow.min.js"></script>
    <link href="../styles/CSS/Subscriptions.css" rel="stylesheet" />
    <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        // Set the style of the client web part page to be consistent with the host web.
        function setStyleSheet() {
            var hostUrl = ""
            if (document.URL.indexOf("?") != -1) {
                var params = document.URL.split("?")[1].split("&");
                for (var i = 0; i < params.length; i++) {
                    p = decodeURIComponent(params[i]);
                    if (/^SPHostUrl=/i.test(p)) {
                        hostUrl = p.split("=")[1];
                        document.write("<link rel=\"stylesheet\" href=\"" + hostUrl + "/_layouts/15/defaultcss.ashx\" />");
                        break;
                    }
                }
            }
            if (hostUrl == "") {
                document.write("<link rel=\"stylesheet\" href=\"/_layouts/15/1033/styles/themable/corev15.css\" />");
            }
        }
        setStyleSheet();
    </script>

     <script type="text/javascript">

        </script>
</head>
<body id="mySubBody" runat="server">
    <form runat="server" id="frmMySubscriptions" >
        <!--Hidden field to get senderID for app part resizing--->
        <asp:HiddenField ID="lblMySubscriptionSenderID" runat="server"></asp:HiddenField>
        
        <div id="dvSubscriptions">
            <!--Error Message Label--->
            <asp:Label runat="server" ID="errorlbl"></asp:Label>
            <!--Literal to which the dynamic subscription structure is appended--->
            <asp:Literal runat="server" ID="ltMainStructure"></asp:Literal>
        </div>
    </form>
</body>
</html>