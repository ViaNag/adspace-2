﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AccessRequestManager.aspx.cs" Inherits="Viacom.AdSpace.AppsWeb.Pages.AccessRequestManager" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <title>:: AdSpace Permission Management Module ::</title>
    <link href="../styles/CSS/Access_request_style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../styles/CSS/jquery-ui.css" />
    <link rel="stylesheet" href="../styles/CSS/jquery-ui-1.10.4.custom.css" />
    <link rel="stylesheet" href="../styles/CSS/jquery.dataTables_themeroller.css" />
    <link rel="stylesheet" href="../styles/CSS/ManagePermissions.css" />
    <script type="text/javascript" src="../Scripts/js/jquery.js"></script>
    <script type="text/javascript" src="../Scripts/js/jquery-ui.js"></script>
    <script type="text/javascript" src="../Scripts/js/jquery.dataTables.js"></script>
    <script src="../Scripts/js/jquery-datatables-grouping.js"></script>
   <script src="../Scripts/js/microsoftajax.js"></script>
    <script src="../Scripts/js/uxscript.js"></script>
    <script src="../Scripts/js/CustomJs/AccessRequestManager.js"></script>
    <link href="../styles/CSS/jquery.dataTables.css" rel="stylesheet" />
    <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css" />
    <%--<link href="../styles/CSS/jquery-ui.css" rel="stylesheet" />--%>
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="hdnRequestID" runat="server" />
        <asp:HiddenField ID="hdnAccessRequestStatus" runat="server" />

        <div id="mainwrapper" class="div_mainwrapper">
            <!--Top menu starts-->
            <div id="menu"  class="div_menu">
                <ul>
                    
                       <li ><a href="" id="AccessRequest">Access Request</a></li>
                    <li ><a href="" id="ManageByUser">Manage Permissions by User</a></li>
                    <li><a href="" id="ManageByContent">Manage Permission by Content</a></li>
                    <li><a href="" id="RequestApproval">Request Approval</a></li>
                    <li class="active_menu"><a href="" id="MyRequest">My Request</a></li>
                </ul>
            </div>
            <!--Top menu ends-->
            <asp:HiddenField ID="hidRoleDefs" runat="server" />
            <asp:Label ID="lblError" runat="server"></asp:Label>
            <br />
            <img id="loadingAccessApprovalAdminView" style="display: none" src="../images/ajax-loader2.gif" />
            <span id="ErrorMessage" style="display: none; color: red">Required Field</span>
            <div id="divAllAccessRequests">
            </div>
            <div id="divAllPendingApprovals" class="hide">
                <table id="tbApproverItemsList" class="dataTable dataTables_scroll">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th>Request ID</th>
                            <th>Request Type</th>
                            <th>Requested By</th>
                            <%--<th>User Comments</th>--%>
                            <th>No of Operations</th>
                        </tr>
                    </thead>
                </table>
            </div>

            <div id="AllPendingApprovalLoading" class="div_AllPendingApprovalLoading">
                <img id="imgAllPendingApprovals" src="../images/ajaxloader2.gif" />
            </div>
            <br />
            <div id="divRequestSummary" class="hide">
                <asp:Label ID="lblRequestId" runat="server" Text="Request Id :" /><asp:Label ID="lblRequestIDResult" runat="server"></asp:Label>
                <br />
                <br />
                <asp:Label ID="lblRequestType1" runat="server" Text="Request Type :" /><asp:Label ID="lblRequestTypeResult" runat="server"></asp:Label>
                <br />
                <br />
                <asp:Label ID="lblRequestStatus" runat="server" Text="Request Status :" /><asp:Label ID="lblRequestStatusResult" runat="server"></asp:Label>
                <br />
                <br />
                
          <div class="div_User_Details">
                    <table id="tblUserDetails" style="float:left;display:table;width:49%";>
                        <tr>
                            <th colspan="2">Requested for</th>
                        </tr>
                        <tr>
                            <td>Name</td>
                            <td>
                                <label id="reqForUserName" runat="server" text="Label"></label>
                            </td>
                        </tr>
                        <tr>
                            <td>Location</td>
                            <td>
                                <label id="reqForUserLocation" runat="server" text="Label"></label>
                            </td>
                        </tr>
                          <tr>
                              <td>
                                  Manager
                              </td>
                            <td>
                                <label id="reqForUserManager" runat="server" text="Label"></label>
                            </td>
                        </tr>
                    </table>

                    <table id="tblUserDetails" style="float:left;display:table;width:50%;margin-left:8px;">
                        <tr>
                            <th colspan="2">Requested by</th>
                        </tr>
                        <tr>
                            <td>Name</td>
                            <td>
                                <label id="reqFromUserName" runat="server" text="Label"></label>
                            </td>
                        </tr>
                        <tr>
                            <td>Location</td>
                            <td>
                                <label id="reqFromUserLocation" runat="server" text="Label"></label>
                            </td>
                        </tr>
                        <tr>
                             <td>
                                  Manager
                              </td>
                            <td>
                                <label id="reqFromUserManager" runat="server" text="Label"></label>
                            </td>
                        </tr>
                    </table>

                </div>
                <br />
                <br />
                <br />
            </div>

            <div id="divRequestDetails" class="hide">
                <asp:Label ID="lblActionDetails1" runat="server" Text="Request Details :"></asp:Label>
                <div class="div_RequestDetails" id="RequestDetails">
                    <table id="tbrequestDetails">
                        <thead>
                            <tr>
                                <th>Site</th>
                                <th>List</th>
                                <th>Item</th>
                                <th>Target User</th>
                                <th>Status</th>
                                <th>Permission Level</th>
                                <th>User Comments</th>
                                <th>Approver Comments</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                
                <div id="CopyRequestDetails">
                    <table id="tblCopyRequestDetails">
                        <tbody>
                            <tr>
                                <td>
                                    <label id="lblRequestBy" runat="server">Requested By</label></td>
                                <td>
                                    <label id="lblRequestedByResult" runat="server"></label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label id="lblRequestedFor" runat="server">Requested For</label></td>
                                <td>
                                    <label id="lblRequestedforResult" runat="server"></label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label id="lblCopyFrom" runat="server">Copy From</label></td>
                                <td>
                                    <label id="lblCopyFromResult" runat="server"></label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label id="lblStatus" runat="server">Status</label></td>
                                <td>
                                    <select id="ddlStatus" name='ApproverStatus' class="hide">
                                        <option value='--Select--'>--Select--</option>
                                        <option value='Approved'>Approved</option>
                                        <option value='Rejected'>Rejected</option>
                                    </select>
                                    <label id="lblStatusResult" runat="server" class="hide"></label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label id="lblApproverComment" runat="server">Approver Comment</label></td>
                                <td>
                                    <textarea rows='2' cols='20' id="txtCommentBox" name='ApproverComment' class="hide"></textarea>
                                    <label id="lblApproverCommentResult" runat="server" class="hide"></label>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>

            <div class="flt_left align_center" id="PendingApprovalButtons">
                <%--<input type="button" id="ARUVSubmitBtn" class="button_primary" runat="server" value="Submit" />--%>
                <input type="button" id="ARUVCancelbtn" class="button_primary" runat="server" value="Cancel" />
            </div>

            <div id="loadingUnderlay" class="MyRequestUnderlay">
                <div class="MyRequestLightBox" id="loadingDiv">
                    <img id="imgLoader" src="../images/ajaxloader2.gif" /><br />
                </div>
            </div>
        </div>
    </form>

</body>
</html>
