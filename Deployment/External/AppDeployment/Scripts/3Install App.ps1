[string] $currentLocation = Get-Location

[string] $ConfigFileName = $currentLocation + "\Settings.xml"

Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
[string]$xmlpath = $ConfigFileName
	 
$settingsXML =  [xml](Get-Content ($xmlpath))
if( $? -eq $false ) 
{
	LogError "Could not read config file. Exiting ..."
	Stop-Transcript
	Stop-SPAssignment -Global
	Exit 0
}


$appFilePath = $settingsXML.AppSettings.appFilePath
$targetWeb = $settingsXML.AppSettings.targetWebToInstallApp



Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green


$spapp = Import-SPAppPackage -Path $appFilePath -Site $targetWeb �Source ObjectModel

$instance = Install-SPApp -Web $targetWeb -Identity $spapp