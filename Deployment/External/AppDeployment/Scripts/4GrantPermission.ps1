[string] $currentLocation = Get-Location

[string] $ConfigFileName = $currentLocation + "\Settings.xml"

Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
[string]$xmlpath = $ConfigFileName
	 
$settingsXML =  [xml](Get-Content ($xmlpath))
if( $? -eq $false ) 
{
	LogError "Could not read config file. Exiting ..."
	Stop-Transcript
	Stop-SPAssignment -Global
	Exit 0
}


$clientID = $settingsXML.AppSettings.clientID
$targetWeb = $settingsXML.AppSettings.targetWebToInstallApp

$authRealm = Get-SPAuthenticationRealm -ServiceContext $targetWeb 
$AppIdentifier = $clientID + "@" + $authRealm 
$appPrincipal = Get-SPAppPrincipal -Site $targetWeb.RootWeb -NameIdentifier $AppIdentifier 

Set-SPAppPrincipalPermission -Site $targetWeb.RootWeb -AppPrincipal $appPrincipal -Scope SiteCollection -Right FullControl