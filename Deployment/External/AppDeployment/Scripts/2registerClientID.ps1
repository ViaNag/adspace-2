[string] $currentLocation = Get-Location

[string] $ConfigFileName = $currentLocation + "\Settings.xml"

Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
[string]$xmlpath = $ConfigFileName
	 
$settingsXML =  [xml](Get-Content ($xmlpath))
if( $? -eq $false ) 
{
	LogError "Could not read config file. Exiting ..."
	Stop-Transcript
	Stop-SPAssignment -Global
	Exit 0
}


$appDisplayName = $settingsXML.AppSettings.appDisplayName
$clientID = $settingsXML.AppSettings.clientID
$targetWeb = $settingsXML.AppSettings.targetWebToInstallApp



Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green



$authRealm = Get-SPAuthenticationRealm -ServiceContext $targetWeb
$AppIdentifier = $clientID + "@" + $authRealm

Write-Host "Creating the new app principal registration..." -ForegroundColor Green

Register-SPAppPrincipal -NameIdentifier $AppIdentifier -Site $targetWeb -DisplayName $appDisplayName

$appPrincipal = Get-SPAppPrincipal -Site $targetWeb -NameIdentifier $AppIdentifier

write-host "Display Name:" $appPrincipal.DisplayName -ForegroundColor Green
write-host "Name Identifier:" $appPrincipal.NameIdentifier -ForegroundColor Green