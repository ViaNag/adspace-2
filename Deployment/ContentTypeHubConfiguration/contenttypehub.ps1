﻿
function Publish-ContentTypeHub {
    param
    (
        [parameter(mandatory=$true)][string]$CTHUrl,
        [parameter(mandatory=$true)][string]$Name
    )
 
    $site = Get-SPSite $CTHUrl
    if(!($site -eq $null))
    {
        $contentTypePublisher = New-Object Microsoft.SharePoint.Taxonomy.ContentTypeSync.ContentTypePublisher ($site)
        $site.RootWeb.ContentTypes | ? {$_.Name -match $Name} | % {
            $contentTypePublisher.Publish($_)
            write-host "Content type" $_.Name "has been republished" -foregroundcolor Green
        }
    }
}
if ((Get-PSSnapin "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue) -eq $null) 
{
    Add-PSSnapin "Microsoft.SharePoint.PowerShell"
}
$MMDName = "Managed Metadata Service"

Write-host "Enter Site Collection URL"
$SiteCol =  Read-Host
#$SiteCol = "http://devviacom-sp:9811/ContentTypeHub"

Set-SPMetadataServiceApplication -Identity $MMDName -HubURI $SiteCol
# Consumes content types from the Content Type Gallery 
$metadataserviceapplicationname = $MMDName 
$metadataserviceapplicationproxy = get-spmetadataserviceapplicationproxy $metadataserviceapplicationname 
$metadataserviceapplicationproxy.Properties["IsNPContentTypeSyndicationEnabled"] = $true 
$metadataserviceapplicationproxy.Update() 
Write-Host "Property Updated successfully"
Publish-ContentTypeHub $SiteCol "Viacom Document"
