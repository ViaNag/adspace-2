
# ===================================================================================
# FUNC: Break inheritence
# DESC: Breaks inheritence
# ===================================================================================
function BreakInheritence([String]$ConfigFileName = "")
{

	# Check that the config file exists.
	if (-not $(Test-Path -Path $configFileName -Type Leaf))
	{
		Write-Error -message ("Configuration file '" + $configFileName + "' does not exist.") -Red
	}

	$configXml = [xml]$(get-content $configFileName)
	if( $? -eq $false ) 
	{
		Write-Host "Could not read config file. Exiting ..." -ForegroundColor Red`
	}

	if ($configXml.BreakRoleInheritence)
	{
		foreach ($list in $configXml.BreakRoleInheritence.List)
		{
			try
			{
				Write-Host "Breaking the inheritence for the list " $list.Name " on " $list.WebUrl -ForegroundColor Yellow

				if($list.Name)
				{
					# Break role inheritance of the list
					$web = Get-SPWeb $list.WebUrl
					
					$ListToBreak = $web.Lists[$list.Name]
					
					if($ListToBreak)
					{
					    if($list.InheritFromParent -eq "true")
						{
                            Write-Host "Inheritance true" -ForegroundColor Green
						}
						else
						{
                            Write-Host "Inheritance false on the list :" $list.Name " So we are reseting and breaking it again" -ForegroundColor Green
                            $ListToBreak.ResetRoleInheritance()
                            $ListToBreak.Update();
						    $BreakFromParent=$false
						}
                        
						$ListToBreak.BreakRoleInheritance($false)
					
                        foreach ($listGroup in $list.Group)
		                {
                            # Give custom permissions on the list
						    

						    # Fetching the group
						    $groupName = $list.RootWebName + " " + $listGroup.GroupName

						    foreach ($grp in $web.SiteGroups) 
						    {
							    if($grp.name -eq $groupName)
							    {
								    $group = $grp
								    break
							    }
						    }

						    $roleAssigment = new-object Microsoft.SharePoint.SPRoleAssignment($group)					
						    
                            $listGroup.PermissionLevel.Split(",") | ForEach {

                            $roleDefinition = $web.Site.RootWeb.RoleDefinitions[$_]
						  
						    $roleAssigment.RoleDefinitionBindings.Add($roleDefinition)

                          }

						    $ListToBreak.RoleAssignments.Add($roleAssigment) 
						    $ListToBreak.Update()

						    Write-Host "New permissions applied on the site-" $web.Title", list-" $list.Name". Group-" $groupName", given" $listGroup.PermissionLevel "access." -ForegroundColor Green
                        }
					}
					else
					{
						write-host "List-" $list.Name ", not found on " $web.Title -ForegroundColor Yellow
					}

					<#if($list.AllSubWeb -eq "true")
					{
						foreach($subWeb in $web.Webs)
						{
							$ListToBreakInSub = $subWeb.Lists[$list.Name]

							if($ListToBreakInSub)
							{
								$ListToBreakInSub.BreakRoleInheritance($BreakFromParent)
					
								$roleAssigmentInSub = new-object Microsoft.SharePoint.SPRoleAssignment($group)					
								$roleAssigmentInSub.RoleDefinitionBindings.Add($roleDefinition)
								
								$ListToBreakInSub.RoleAssignments.Add($roleAssigmentInSub) 
								$ListToBreakInSub.Update()

								Write-Host "New permissions applied on the site-" $subWeb.Title", list-" $list.Name". Group-" $groupName", given" $list.PermissionLevel "access." -ForegroundColor Green
							}
							else
							{
								write-host "List-" $list.Name ", not found on " $subWeb.Title -ForegroundColor Yellow
							}
						}
					}#>
				}

				if($Error.Count -gt 0)
				{
					Write-Host "Error Breaking Inheritence. Cause : " $Error -ForegroundColor Red
					$Error.Clear()	
				}
				else
				{
					Write-Host "Process Complete. New permissions applied." -ForegroundColor Green
				}
			}
			catch
			{
				Write-Host "Exception while Breaking Inheritence." $Error -ForegroundColor Red
			}
		}
	}
}

[string] $currentLocation = Get-Location

[string] $parameterfile = $currentLocation + "\BreakInheritence.xml"





BreakInheritence $parameterfile
