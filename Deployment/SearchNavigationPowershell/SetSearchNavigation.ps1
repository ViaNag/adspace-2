function Update-SearchNav([string]$Identity)
{
 Write-Host -ForegroundColor Red "============================================="
 Write-Host -ForegroundColor Green "Updating Search Navigation at URL " -NoNewline;
 Write-Host -ForegroundColor Green $Identity

 $s = Get-SPSite $Identity
 $w = $s.RootWeb

 foreach ($w in $s.AllWebs) { 
  Write-Host -ForegroundColor Red "============================================="
  Write-Host -ForegroundColor Green "Updating Search Navigation at URL " -NoNewline;
  Write-Host -ForegroundColor Green $w.Url
  
  $SearchNav = $w.Navigation.SearchNav
  
  IF ($SearchNav -ne $NULL)
  {
   Write-Host -ForegroundColor Red "This Site Search Navigation Already containing values";
   $SearchNav
    Write-Host -ForegroundColor Green "Deleting Navigation nodes";
    $cnt= $SearchNav.Count
    for($i=0; $i-lt $cnt; $i++)  
       {  
       write-host  -ForegroundColor Green  "Deleting Node title : "$SearchNav[0].Title 
           $SearchNav[0].Delete() 
        } 
  }
 
  
   
   Write-Host -ForegroundColor Green "Content Only";
   $Title = "Documents & Media"
   $RelativeUrl = "/sites/adspace/SearchCenter/Pages/ContentOnly.aspx?ud={ContextUrl}"
   $node = new-object -TypeName "Microsoft.SharePoint.Navigation.SPNavigationNode" -ArgumentList $Title, $RelativeUrl, $true
   $w.Navigation.SearchNav.AddAsLast($node)

   Write-Host -ForegroundColor Green "All AdSpace";
   $Title = "All AdSpace"
   $RelativeUrl = "/sites/adspace/SearchCenter/Pages/AllAdSpace.aspx"
   $node = new-object -TypeName "Microsoft.SharePoint.Navigation.SPNavigationNode" -ArgumentList $Title, $RelativeUrl, $true
   $w.Navigation.SearchNav.AddAsLast($node)
   
    Write-Host -ForegroundColor Green "This Site";
   $Title = "This Site"
   $RelativeUrl = "/sites/adspace/SearchCenter/Pages/AdSpaceSubsite.aspx?ud={ContextUrl}"
   $node = new-object -TypeName "Microsoft.SharePoint.Navigation.SPNavigationNode" -ArgumentList $Title, $RelativeUrl, $true
   $w.Navigation.SearchNav.AddAsLast($node)

   Write-Host -ForegroundColor Green "People"
   $Title = "People"
   $RelativeUrl = "/sites/adspace/SearchCenter/Pages/peopleresults.aspx"
   $node = new-object -TypeName "Microsoft.SharePoint.Navigation.SPNavigationNode" -ArgumentList $Title, $RelativeUrl, $true
   $w.Navigation.SearchNav.AddAsLast($node)

   Write-Host -ForegroundColor Green "Company Lookup by Name"
   $Title = "Company Lookup by Name"
   $RelativeUrl = "/sites/adspace/SearchCenter/Pages/LexisNexis.aspx?LSType=Company"
   $node = new-object -TypeName "Microsoft.SharePoint.Navigation.SPNavigationNode" -ArgumentList $Title, $RelativeUrl, $true
   $w.Navigation.SearchNav.AddAsLast($node)

   Write-Host -ForegroundColor Green "Company Lookup by Ticker"
   $Title = "Company Lookup by Ticker"
   $RelativeUrl = "/sites/adspace/SearchCenter/Pages/LexisNexis.aspx?LSType=Ticker"
   $node = new-object -TypeName "Microsoft.SharePoint.Navigation.SPNavigationNode" -ArgumentList $Title, $RelativeUrl, $true
   $w.Navigation.SearchNav.AddAsLast($node)

   Write-Host -ForegroundColor Green "Create a Company List"
   $Title = "Create a Company List"
   $RelativeUrl = "/sites/adspace/SearchCenter/Pages/LexisNexis.aspx?LSType=Create"
   $node = new-object -TypeName "Microsoft.SharePoint.Navigation.SPNavigationNode" -ArgumentList $Title, $RelativeUrl, $true
   $w.Navigation.SearchNav.AddAsLast($node)
    
  Write-Host -ForegroundColor Red "============================================="
    } 
 
 $w.Dispose()
 $s.Dispose()
 Write-Host -ForegroundColor Red "============================================="
}

#TODO Add Your Site Collection URL


 Update-SearchNav("http://sp2013-dev-6:7000/sites/adspace/")
 Write-Host "Press any key to continue ..."

 $x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")


