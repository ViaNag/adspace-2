﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExternalSharingInterface.aspx.cs" Inherits="Viacom.AdSpace.AppsWeb.Pages.ExternalSharingInterface" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="../styles/CSS/ExternalSharing.css" rel="stylesheet" />
    <style type="text/css">
       
    </style>
    <title></title>
    <link type="text/css" href="../styles/CSS/ExternalSharing.css" rel="stylesheet" />
    <link href="../styles/CSS/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/js/jquery.js" type="text/javascript"></script>
    <script src="../Scripts/js/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript">
        function dia() {
            $("#termscond").dialog({
                modal: true,
                minHeight: 'auto',
                maxHeight: 'auto',
                width: 400,
                resizable: false,
                draggable: false,
                buttons: {
                    Submit: function (event, n) {
                        if (!$("#field_terms").prop("checked")) {
                            var myCheckboxMsg = $('#lblerrormsg').text()
                            alert(myCheckboxMsg);
                            event.preventDefault();
                            $("#lblcheck").css("color", "red");
                        } else {
                            $(this).dialog("close");
                            $("#termsbtn").click();
                        }
                    },
                    Cancel: function () {
                        $(this).dialog('destroy');
                    }
                },
            });
            $(".ui-dialog-buttonpane button:contains('Cancel')").attr("disabled", false)
            $(".ui-dialog-buttonpane button:contains('Submit')").attr("disabled", false);
            $(".ui-dialog-titlebar").hide();
        }
        $(function () {
            $('#field_terms').change(function () {
                //$('#terms').toggle(this.checked);
                $('#lblerrormsg').hide();
                $("#lblcheck").css("color", "black");
            }).change(); //ensure visible state matches initially
        });
        var linkBtnAddMoreClk = '';
        var dtCh = "/";
        var minYear = 1900;
        var maxYear = 2100;

        function isInteger(s) {
            var i;
            for (i = 0; i < s.length; i++) {
                // Check that current character is number.
                var c = s.charAt(i);
                if (((c < "0") || (c > "9"))) return false;
            }
            // All characters are numbers.
            return true;
        }

        function stripCharsInBag(s, bag) {
            var i;
            var returnString = "";
            // Search through string's characters one by one.
            // If character is not in bag, append to returnString.
            for (i = 0; i < s.length; i++) {
                var c = s.charAt(i);
                if (bag.indexOf(c) == -1) returnString += c;
            }
            return returnString;
        }

        function daysInFebruary(year) {
            // February has 29 days in any year evenly divisible by four,
            // EXCEPT for centurial years which are not also divisible by 400.
            return (((year % 4 == 0) && ((!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28);
        }

        function DaysArray(n) {
            for (var i = 1; i <= n; i++) {
                this[i] = 31
                if (i == 4 || i == 6 || i == 9 || i == 11) {
                    this[i] = 30
                }
                if (i == 2) {
                    this[i] = 29
                }
            }
            return this
        }

        function isDate(dtStr) {
            var daysInMonth = DaysArray(12)
            var pos1 = dtStr.indexOf(dtCh)
            var pos2 = dtStr.indexOf(dtCh, pos1 + 1)
            var strMonth = dtStr.substring(0, pos1)
            var strDay = dtStr.substring(pos1 + 1, pos2)
            var strYear = dtStr.substring(pos2 + 1)
            strYr = strYear
            if (strDay.charAt(0) == "0" && strDay.length > 1) strDay = strDay.substring(1)
            if (strMonth.charAt(0) == "0" && strMonth.length > 1) strMonth = strMonth.substring(1)
            for (var i = 1; i <= 3; i++) {
                if (strYr.charAt(0) == "0" && strYr.length > 1) strYr = strYr.substring(1)
            }
            month = parseInt(strMonth)
            day = parseInt(strDay)
            year = parseInt(strYr)
            if (pos1 == -1 || pos2 == -1) {
                alert("The date format should be : mm/dd/yyyy")
                return false
            }
            if (strMonth.length < 1 || month < 1 || month > 12) {
                alert("Please enter a valid month")
                return false
            }
            if (strDay.length < 1 || day < 1 || day > 31 || (month == 2 && day > daysInFebruary(year)) || day > daysInMonth[month]) {
                alert("Please enter a valid day")
                return false
            }
            if (strYear.length != 4 || year == 0 || year < minYear || year > maxYear) {
                alert("Please enter a valid 4 digit year between " + minYear + " and " + maxYear)
                return false
            }
            if (dtStr.indexOf(dtCh, pos2 + 1) != -1 || isInteger(stripCharsInBag(dtStr, dtCh)) == false) {
                alert("Please enter a valid date")
                return false
            }
            return true
        }

        function CheckUnCheckChildern(id) {
            var grid;
            var cell;
            var cellIndex = 0;
            if (grid.rows.length > 0) {
                for (i = 1; i < grid.rows.length; i++) {
                    cell = grid.rows[i].cells[cellIndex];
                    for (j = 0; j < cell.childNodes.length; j++) {
                        if (cell.childNodes[j].type == "checkbox") {
                            cell.childNodes[j].checked = document.getElementById(id).checked;
                        }
                    }
                }
            }
        }

        function checkall(bx) {
            $('#grdSharingItems').find('input:checkbox').each(function () {
                alert('hi');
                cbs[i].checked = bx.checked;
            });
        }

        function echeck(str) {
            var at = "@"
            var dot = "."
            var lat = str.indexOf(at)
            var lstr = str.length
            var ldot = str.indexOf(dot)
            if (str.indexOf(at) == -1) {
                alert("Invalid E-mail ID : " + str)
                return false
            }
            if (str.indexOf(at) == -1 || str.indexOf(at) == 0 || str.indexOf(at) == lstr) {
                alert("Invalid E-mail ID : '" + str + "'")
                return false
            }
            if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0 || str.indexOf(dot) == lstr) {
                alert("Invalid E-mail ID : '" + str + "'")
                return false
            }
            if (str.indexOf(at, (lat + 1)) != -1) {
                alert("Invalid E-mail ID : '" + str + "'")
                return false
            }
            if (str.substring(lat - 1, lat) == dot || str.substring(lat + 1, lat + 2) == dot) {
                alert("Invalid E-mail ID : '" + str + "'")
                return false
            }
            if (str.indexOf(dot, (lat + 2)) == -1) {
                alert("Invalid E-mail ID : '" + str + "'")
                return false
            }
            if (str.indexOf(" ") != -1) {
                alert("Invalid E-mail ID : '" + str + "'")
                return false
            }
            return true
        }
        $(document).ready(function () {
            SearchText();
            $('#hdrcheckboxDownload').change(function () {
                $('#grdSharingItems').find('input:checkbox').each(function (i) {
                    console.log(i);
                    if (i != 0) {
                        if ($('#hdrcheckboxDownload').is(":checked") == true) {
                            $(this).attr('checked', 'checked');
                            $(this).attr('checked', true);
                        } else {
                            $(this).removeAttr('checked');
                        }
                    }
                });
            });

            function SearchText() {
                $("#PeoplePicker").autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            type: "POST",
                            contentType: "application/json; charset=utf-8",
                            url: "ExternalSharingInterface.aspx/GetAutoCompleteData",
                            data: "{'email':'" + extractLast(request.term) + "'}",
                            dataType: "json",
                            success: function (data) {
                                response(data.d);
                            },
                            error: function (result) {
                                alert("Error");
                            }
                        });
                    },
                    focus: function () {
                        // prevent value inserted on focus
                        return false;
                    },
                    select: function (event, ui) {
                        var terms = split(this.value);
                        // remove the current input
                        terms.pop();
                        // add the selected item
                        terms.push(ui.item.value);
                        // add placeholder to get the comma-and-space at the end
                        terms.push("");
                        this.value = terms.join(", ");
                        return false;
                    }
                });
                $("#PeoplePicker").bind("keydown", function (event) {
                    if (event.keyCode === $.ui.keyCode.TAB && $(this).data("autocomplete").menu.active) {
                        event.preventDefault();
                    }
                })

                function split(val) {
                    return val.split(/,\s*/);
                }

                function extractLast(term) {
                    return split(term).pop();
                }
            }
        });

        function validate_field() {
            //var ch = $("#termscond").dialog("isOpen");
            var recepient = $("#PeoplePicker").val();
            var recepientclear = recepient.replace(/ /g, '');
            var recepientcheck = [];
            recepientcheck = recepientclear.split(',');
            var set_title = $("#ExSsetTitleTxt").val();
            var con_cat = $("#ExSContentGrpDrpDwn option:selected").text();
            var expdate = $("#ExpiryDate").val();
            var grdshares = $("#grdSharingItems").val();
            if (isDate(expdate) == false) {
                return false;
            }
            if (recepient == "") {
                alert("Please enter the recepients");
                return false;
            }
            if (set_title == "" && con_cat == "select") {
                alert("Please Enter A Title or Select a Content Category");
                return false;
            }
            for (i = 0; i <= recepientcheck.length; i++) {
                if (recepientcheck[i] != "" && recepientcheck[i] != null) {
                    if (echeck(recepientcheck[i]) == false) {
                        return false
                    }
                }
            }
            dia();
            var isOpen = $("#termscond").dialog("isOpen");
            if (isOpen) {
                return false;
            }
        }

        function closeParentDialog(refresh) {
            var target = parent.postMessage ? parent : (parent.document.postMessage ? parent.document : undefined);
            if (refresh) target.postMessage('CloseCustomActionDialogRefresh', '*');
            else target.postMessage('CloseCustomActionDialogNoRefresh', '*');
        }

        function concatval() {
            alert("Content Group already exists , Please select from dropdown or else enter a new group name.");
            return false;
        }

        function CheckAllEmp(Checkbox) {
            var GridVwHeaderChckbox = document.getElementById("grdSharingItems");
            for (i = 1; i < GridVwHeaderChckbox.rows.length; i++) {
                GridVwHeaderChckbox.rows[i].cells[1].getElementsByTagName("INPUT")[0].checked = Checkbox.checked;
            }
        }
</script>
</head>
<body>
    <form id="ExternalSharingForm" runat="server">

        <ajax:ToolkitScriptManager runat="server"></ajax:ToolkitScriptManager>
        <div id="mainDiv" runat="server" style="width: 98%; text-align: left" align="center">
            <asp:Label runat="server" ID="ContentSharedlbl" Style="color: green; font-size: 16px; font-weight: bold"></asp:Label>
            <div id="ExSTitleDiv">
                <asp:Label runat="server" ID="ErrorLblAddList" Style="color: green; font-size: 14px;"></asp:Label>
                <asp:Label runat="server" ID="ErrorLbl" Style="color: green; font-size: 14px;"></asp:Label>
            </div>
            <div id="messDiv">
                <asp:Label runat="server" ID="Messagelbl" Style="color: green; font-size: 14px;"></asp:Label>
            </div>
            <div id="ExternalSharingDiv" runat="server" visible="false" style="width: 100%">

                <div id="ExternalSharingLeftSideDiv" style="width: 34%">
                    <div class="ExternalSharingConetnt" style="width: 100%">
                        <div class="ExSLabel">
                            <asp:Label runat="server" Text="Label" ID="sharedWithTxt">To:</asp:Label>
                            <br />
                            <br />
                        </div>
                        <div class="ExSInputContent">
                            <div class="demo">
                                <div class="ui-widget">
                                    <asp:TextBox runat="server" ID="PeoplePicker" Width="90%" class="autosuggest" TextMode="MultiLine"></asp:TextBox>
                                    <br />
                                    <br />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ExternalSharingConetnt">
                        <div class="ExSLabel">
                            <asp:Label runat="server" Text="Label">Message</asp:Label>
                        </div>
                        <div class="ExSInputContent">
                            <textarea name="ExternalSharingMessageTxt" id="MessageTA" style="width: 90%; border: #ccc 1px solid" cols="23" rows="10" runat="server"></textarea>
                        </div>
                    </div>
                    <div class="ExternalSharingConetnt">
                        <div class="ExSLabel">
                            <asp:Label runat="server" Text="Label">Expiry Date</asp:Label>
                        </div>
                        <div class="ExSInputContent">
                            <asp:TextBox runat="server" ID="ExpiryDate" Width="90%"></asp:TextBox>
                            <ajax:CalendarExtender TargetControlID="ExpiryDate" ID="CalendarExtender1" runat="server"></ajax:CalendarExtender>
                        </div>
                    </div>
                    <div class="ExternalSharingConetnt">

                        <div class="ExSLabel" style="width:100%;text-align:left;margin-bottom:10px;">
                            <asp:Label runat="server" Text="Label">Shared With :</asp:Label>
                        </div>
                        <div class="sharedwith" style="float:left;" runat="server" id="divsharedwith">
                            <asp:GridView ShowHeader="false" ID="grddocviewers" runat="server" AutoGenerateColumns="false" GridLines="None" OnRowDeleting="grddocviewers_RowDeleting"
                                DataKeyNames="UName" Font-Bold="false">
                                <Columns>
                                    <asp:BoundField DataField="UName" />
                                    <asp:CommandField ShowDeleteButton="True" ButtonType="Link" DeleteText="X" ControlStyle-ForeColor="Gray" ControlStyle-Font-Bold="true" ControlStyle-Font-Underline="false" />
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
                <div id="ExternalSharingRightSideDiv" style="width: 64%">
                    <div id="ExSRightTop">
                        <div id="ExSlblDiv">
                            <asp:Label class="ExSlbl" runat="server" Text="Label">Shared Content</asp:Label>

                        </div>

                        <div class="leftContent">
                            <div class="ExSLabel">
                                <asp:Label runat="server" Text="Label">Set Title</asp:Label>
                            </div>
                            <div class="ExSInputContent">
                                <asp:TextBox runat="server" ID="ExSsetTitleTxt" AutoPostBack="True" OnTextChanged="ExSsetTitleTxt_TextChanged"></asp:TextBox>
                            </div>
                        </div>


                        <div>
                            <div class="ExSLabel" style="padding: 5px">
                                <asp:Label runat="server" ID="ExSorLbl">- or -</asp:Label>
                            </div>

                        </div>

                        <div class="ExternalSharingConetnt">
                            <div class="ExSLabel">
                                <asp:Label runat="server">Select Existing Content Group</asp:Label>
                            </div>
                            <div class="ExSInputContent">
                                <asp:DropDownList runat="server" ID="ExSContentGrpDrpDwn" OnSelectedIndexChanged="ExSContentGrpDrpDwn_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="add-more" style="color: grey; font-size: 12px; cursor: pointer; text-decoration: none">
                            <asp:LinkButton ID="hladdmore" runat="server" ClientIDMode="Static" OnClick="hladdmore_Click" OnClientClick="return validate_field()" Enabled="false" Font-Underline="false" ForeColor="gray">Add More Links</asp:LinkButton>
                            <br />
                            <br />
                            <asp:Label ID="lblItemSelected" runat="server"></asp:Label>
                        </div>

                    </div>
                    <div id="ExSRightBottom">
                        <div class="ExSSelectedDetails">
                            <div id="SharedContentDocsDiv">

                                <asp:GridView ID="grdSharingItems" runat="server" AutoGenerateColumns="False"
                                    Width="100%" GridLines="None" BorderWidth="0px" CellPadding="5" BorderStyle="None"
                                    AlternatingRowStyle-CssClass="GridLine" RowStyle-CssClass="GridLine"
                                    OnRowDeleting="grdSharingItems_RowDeleting">
                                    <AlternatingRowStyle CssClass="GridLine" />

                                    <Columns>

                                        <asp:TemplateField ItemStyle-VerticalAlign="Top" ItemStyle-Width="500px" HeaderStyle-Width="500px"
                                            HeaderText="No. Of Items Selected" HeaderStyle-CssClass="GridHeaderLeft" ItemStyle-CssClass="GridRowLeft">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTitle" runat="server" Text='<%# Bind("FileName") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle CssClass="GridHeaderLeft" Width="250px"></HeaderStyle>
                                            <ItemStyle VerticalAlign="Top" CssClass="GridRowLeft" Width="500px"></ItemStyle>
                                        </asp:TemplateField>


                                        <asp:TemplateField ItemStyle-Width="280px" HeaderStyle-Width="280px" HeaderStyle-CssClass="GridHeaderLeft"
                                            ItemStyle-CssClass="GridRowLeft">
                                            <HeaderTemplate>
                                                <asp:CheckBox Text="Select All" ID="chkboxSelectAll" ClientIDMode="Static" runat="server" onclick="CheckAllEmp(this);" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox runat="server" ID="chkEmp" Text="Allow Download"></asp:CheckBox>
                                            </ItemTemplate>

                                            <HeaderStyle CssClass="GridHeaderLeft" Width="280px"></HeaderStyle>

                                            <ItemStyle CssClass="GridRowLeft" Width="280px"></ItemStyle>

                                        </asp:TemplateField>

                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lblhr" runat="server" Text='<%#Eval("fName")%>' Width="30px" Style="display: none"></asp:Label>
                                            </ItemTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lblfname" runat="server" Text='<%#Eval("fName")%>' Width="30px" Style="display: none"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="lblfPath" runat="server" Text='<%#Eval("fPath")%>' Width="30px" Style="display: none"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:CommandField ShowDeleteButton="True" ButtonType="Link" DeleteText="X" ControlStyle-ForeColor="Gray" ControlStyle-Font-Bold="true" ControlStyle-Font-Underline="false">
                                            <ControlStyle Font-Bold="True" Font-Underline="False" ForeColor="Gray"></ControlStyle>
                                        </asp:CommandField>
                                    </Columns>

                                    <RowStyle CssClass="GridLine"></RowStyle>
                                </asp:GridView>
                            </div>

                        </div>

                    </div>

                </div>

            </div>
            
            
                
            <div id="ExSButtons" align="right">
                
                

                <asp:Button CssClass="button" runat="server" Text="Cancel" ID="cancelBtn" OnClientClick="javascript:window.parent.postMessage('CloseCustomActionDialogNoRefresh', '*');" ></asp:Button>
                <asp:Button CssClass="button" runat="server" Text="Share" ID="shareBtn" OnClientClick="return validate_field()" ></asp:Button>
                <asp:Button CssClass="button" runat="server" Text="Test" ID="termsbtn" OnClick="shareBtn_Click"></asp:Button>

            </div>
            
            <div id="termscond" style="display:none">
                <div id="condn">
                    <asp:Label ID="lblterms" runat="server"></asp:Label> 
                    </div>
                    
                    <div id="terms" >
                        <p><input id="field_terms" type="checkbox" required name="terms" runat="server"/><asp:Label ID="lblcheck" runat="server">I accept the Terms and Conditions</asp:Label> </p>
                        <asp:Label ID="lblerrormsg" runat="server"></asp:Label>
                      </div>
                        
                </div>
               
        </div>
    </form>
</body>
</html>
