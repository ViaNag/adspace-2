﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BookmarkActivity.aspx.cs" Inherits="Viacom.AdSpace.AppsWeb.Pages.BookmarkActivity" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <script type="text/javascript" src="../Scripts/js/jquery.js"></script>
    <script src="../Scripts/js/CustomJs/BookmarkActivity.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../styles/CSS/Bookmark.css" />
    <script src="../Scripts/js/iframeResizer.contentWindow.min.js"></script>
    <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="frmBookmarkActivity" runat="server">

        <div id="dvMessage"></div>

        <div id="dvEditBookmark" >

 <div id="dvBookmarkTitle">   
     <div class="leftsection" style="width:50%;float:left">Title:</div>
     <div class="rightsection" style="width:50%;float:left"> <asp:TextBox ID="title" runat="server"></asp:TextBox></div>
 </div>

<div  id="dvBookmarkGroup">   
    <div class="leftsection" style="width:50%;float:left">Group Name: </div>
    <div  class="rightsection" style="width:50%;float:left">  <span style="float:left" title="Group Name: Choose Option">
                                    <input id="chkGrpName" name="chkGroupName" value="DropDownButton" checked="checked"  type="radio"/>
                                    </span>
                                    <select style="float:left"  name="ddlGrpNAme"  class="rightsection" id="ddlGrpNAme" title="Group Name: Choice Drop Down" >					
                                    </select><br />
                                    <span style="float:left"  onclick="" class="rightsection"  title="Group Name: Specify your own value:">
                                        <input id="chkOther" style="float:left" name="chkGroupName" value="FillInButton" type="radio"/>
                                        <label  class="rightsection" >Specify your own value:</label>
                                    </span><br /><input style="margin-left:24px;"  class="rightsection" onclick="" name="txtGroupName" maxlength="255" id="txtOther" title="Group Name : Specify your own value:" type="text"/>
                          </div>     
</div>

<div style="float:left"><button style="width:70px" class="button" onclick="javascript:BookmarkActivity.SaveEditBookmark();return false;"> Save</button> </div>
    <div id="dvError"  style="display:none;color:red;" class="ErrorMessage"></div>
</div>
 
       
  
    </form>
</body>
</html>
