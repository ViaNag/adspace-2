﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SubscriptionApprovalForm.aspx.cs" Inherits="Viacom.AdSpace.AppsWeb.Pages.SubscriptionApprovalForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css"/>
    <style type="text/css">

  
        </style>
    <link rel="stylesheet" href="../styles/CSS/Subscriptions.css" type="text/css"/>
    <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css" />
        <script src="../Scripts/js/jquery.js" type="text/javascript"></script>
        <script src="../Scripts/js/jquery-ui.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/js/jquery.js"></script>
    <script src="../Scripts/js/iframeResizer.contentWindow.min.js"></script>
     
    <script type="text/javascript">
         

         $(document).ready(function () {

                $('table[id$="grdUserSubscriptions"]').find('textarea[id*="_txtLicenseeDetails"]').attr('disabled', "true");
 
            });

            function CheckAllUserSub(Checkbox)
            {
                //alert("in check");

                var GridVwHeaderChckbox = document.getElementById("grdUserSubscriptions");

                for (i = 1; i < GridVwHeaderChckbox.rows.length; i++) {
                    //alert(Checkbox.checked);
                    GridVwHeaderChckbox.rows[i].cells[0].getElementsByTagName("INPUT")[0].checked = Checkbox.checked;

                    if (Checkbox.checked)
                    {
                        GridVwHeaderChckbox.rows[i].style.backgroundColor = "#E6F2FB";
                        GridVwHeaderChckbox.rows[i].style.border = "5px solid white";
                        //GridVwHeaderChckbox.rows[i].cells[4].getElementsByTagName("TEXTAREA")[0].style.display = "block";
                        GridVwHeaderChckbox.rows[i].cells[3].getElementsByTagName("TEXTAREA")[0].disabled = false;

                    }
                    else
                    {
                        GridVwHeaderChckbox.rows[i].style.backgroundColor = "white";
                        GridVwHeaderChckbox.rows[i].style.border = "5px solid white";
                        //GridVwHeaderChckbox.rows[i].cells[4].getElementsByTagName("TEXTAREA")[0].style.display = "none";
                        GridVwHeaderChckbox.rows[i].cells[3].getElementsByTagName("TEXTAREA")[0].disabled = true;
                        GridVwHeaderChckbox.rows[i].cells[3].getElementsByTagName("TEXTAREA")[0].value = "";

                    }
                }
            }

            function GetSelectedUserRow(Checkbox) {
                var GridVwHeaderChckbox = document.getElementById("grdUserSubscriptions");
                var row = Checkbox.parentNode.parentNode;
                var rowIndex = row.rowIndex;
                if (Checkbox.checked) {

                    GridVwHeaderChckbox.rows[rowIndex].style.backgroundColor = "#E6F2FB";
                    GridVwHeaderChckbox.rows[rowIndex].style.border = "5px solid white";
                    //GridVwHeaderChckbox.rows[rowIndex].cells[4].getElementsByTagName("TEXTAREA")[0].style.display = "block";
                    GridVwHeaderChckbox.rows[rowIndex].cells[3].getElementsByTagName("TEXTAREA")[0].disabled = false;
                    
                }
                else {
                    var SelectAllChkBox = document.getElementById("chkboxSelectAllUser");
                    if (SelectAllChkBox.checked) {
                        SelectAllChkBox.checked = false;
                    }
                    GridVwHeaderChckbox.rows[rowIndex].style.backgroundColor = "white";
                    GridVwHeaderChckbox.rows[rowIndex].style.border = "5px solid white";
                    //GridVwHeaderChckbox.rows[rowIndex].cells[4].getElementsByTagName("TEXTAREA")[0].style.display = "none";
                    GridVwHeaderChckbox.rows[rowIndex].cells[3].getElementsByTagName("TEXTAREA")[0].disabled = true;
                    GridVwHeaderChckbox.rows[rowIndex].cells[3].getElementsByTagName("TEXTAREA")[0].value = "";
                }

                //alert("RowIndex: " + rowIndex);
                return false;
            }

            function CheckAllCompanySub(Checkbox) {
                //alert("in check");

                var GridVwHeaderChckbox = document.getElementById("grdCompanySubscriptions");

                for (i = 1; i < GridVwHeaderChckbox.rows.length; i++) {
                    //alert(Checkbox.checked);
                    GridVwHeaderChckbox.rows[i].cells[0].getElementsByTagName("INPUT")[0].checked = Checkbox.checked;

                    if (Checkbox.checked) {
                        GridVwHeaderChckbox.rows[i].style.backgroundColor = "#E6F2FB";
                        GridVwHeaderChckbox.rows[i].style.border = "5px solid white";
                        //GridVwHeaderChckbox.rows[i].cells[3].getElementsByTagName("TEXTAREA")[0].style.display = "block";

                    }
                    else {
                        GridVwHeaderChckbox.rows[i].style.backgroundColor = "white";
                        GridVwHeaderChckbox.rows[i].style.border = "5px solid white";
                        //GridVwHeaderChckbox.rows[i].cells[3].getElementsByTagName("TEXTAREA")[0].style.display = "none";


                    }
                }
            }

            function GetSelectedCompanyRow(Checkbox) {
                var GridVwHeaderChckbox = document.getElementById("grdCompanySubscriptions");
                var row = Checkbox.parentNode.parentNode;
                var rowIndex = row.rowIndex;
                if (Checkbox.checked) {

                    GridVwHeaderChckbox.rows[rowIndex].style.backgroundColor = "#E6F2FB";
                    GridVwHeaderChckbox.rows[rowIndex].style.border = "5px solid white";
                    //GridVwHeaderChckbox.rows[rowIndex].cells[3].getElementsByTagName("TEXTAREA")[0].style.display = "block";
                }
                else {
                    var SelectAllChkBox = document.getElementById("chkboxSelectAllCompany");
                    if (SelectAllChkBox.checked) {
                        SelectAllChkBox.checked = false;
                    }
                    GridVwHeaderChckbox.rows[rowIndex].style.backgroundColor = "white";
                    GridVwHeaderChckbox.rows[rowIndex].style.border = "5px solid white";
                    //GridVwHeaderChckbox.rows[rowIndex].cells[3].getElementsByTagName("TEXTAREA")[0].style.display = "none";
                }

                //alert("RowIndex: " + rowIndex);
                return false;
            }
        </script>
    
</head>
<body>
    <form runat="server" id="frmSubscriptionApprovalForm" class="fullWidth"> 
        <div id="dvSubscriptionContainer" class="fullWidth">
        <asp:HiddenField ID="lblSubscriptionApprovalSenderID" runat="server"></asp:HiddenField> 
        <asp:Label runat="server" ID="errorlbl"></asp:Label>  
        
    <div id="dvStatus" visible="false" runat="server">The subscription Request Has been Processed.Requestor will recieve a mail shortly. </div>
        
    <div id="DvSubscription"  runat="server" class="fullWidth">
        <div class="dvRow rightsection zeroPaddingMargin" >
            <asp:button ID="approve" runat="server" class="requestButton"  Text="Approve / Reject" OnClick="approve_Click" ></asp:button>
            <br />
            <asp:Label ID="lbl_note" runat="server" Text="**Please Note : Unmarked Items will be rejected" Font-Size="Small"></asp:Label>
        </div>
        
       
		<div class="dvRow">
		    <div class="leftsection"  >Request ID: </div>
		    <div class="rightsection leftTextAlign" >
                <asp:label  runat="server" ID="lblrequestID"></asp:label>
		    </div>
	    </div>
        <div class="dvRow">
		    <div class="leftsection"  style="">Requestor&#39;s Name : </div>
		    <div class="rightsection leftTextAlign" >
                <asp:label  runat="server" ID="lblrequestor"></asp:label>
                <asp:label Visible="false"  runat="server" ID="lblrequestorUserID"></asp:label>
		    </div>
	    </div>
        
	       <div class="dvRow">

	       </div>
	    <div class="dvRow" id="dvUserSubscriptions" runat="server">
		    <div class="leftsection" style="vertical-align:top">User Subscriptions : </div>
            <!--user subscriptions--->
		    <div class="rightsection">
                    <asp:GridView ID="grdUserSubscriptions" runat="server" AutoGenerateColumns="False"
                            Width="100%" GridLines="None" BorderWidth="5px" CellPadding="3" BorderStyle="None"
                            AlternatingRowStyle-CssClass="GridLine" RowStyle-CssClass="GridLine"
                            >
                            <AlternatingRowStyle CssClass="GridLine" />

                            <Columns>
                                <asp:TemplateField ItemStyle-Width="5px" HeaderStyle-Width="5px" HeaderStyle-CssClass="GridHeaderLeft" ItemStyle-CssClass="GridRowLeft">
                                    <HeaderTemplate>
                                        <asp:CheckBox Text="" ID="chkboxSelectAllUser" ClientIDMode="Static" runat="server" onclick="CheckAllUserSub(this);" ></asp:CheckBox>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox runat="server" ID="chkSubscriptionUser" Text="" ClientIDMode="Static" onclick="GetSelectedUserRow(this);"  ></asp:CheckBox>
                                    </ItemTemplate> 
                                    <HeaderStyle CssClass="GridHeaderLeft" Width="5px"></HeaderStyle>
                                    <ItemStyle VerticalAlign="Top" CssClass="GridRowLeft" Width="5px"></ItemStyle>        
                                </asp:TemplateField>
                                        
                                <asp:TemplateField ItemStyle-VerticalAlign="Top" ItemStyle-Width="250px" HeaderStyle-Width="250px" HeaderText="Subscription" HeaderStyle-CssClass="GridHeaderLeft" ItemStyle-CssClass="GridRowLeft">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserSubscription" runat="server" Text='<%# Bind("SubscriptionName") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridHeaderLeft" Width="250px"></HeaderStyle>
                                    <ItemStyle VerticalAlign="Top" CssClass="GridRowLeft" Width="250px"></ItemStyle>
                                </asp:TemplateField>

                                <asp:TemplateField ItemStyle-VerticalAlign="Top" ItemStyle-Width="100px" HeaderStyle-Width="100px"
                                    HeaderText="Reason" HeaderStyle-CssClass="GridHeaderLeft" ItemStyle-CssClass="GridRowLeft">
                                    <ItemTemplate>
                                        <asp:TextBox Width="90px" TextMode="MultiLine" Rows="2"  ID="UserReason" Enabled="false" runat="server" Text='<%# Bind("ReasonForAccess") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridHeaderLeft" Width="100px"></HeaderStyle>
                                    <ItemStyle VerticalAlign="Top" CssClass="GridRowLeft" Width="100px"></ItemStyle>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Licensee Details" ItemStyle-Width="100px" HeaderStyle-Width="100px" HeaderStyle-CssClass="GridHeaderLeft" ItemStyle-CssClass="GridRowLeft">
                                    <ItemTemplate>
                                        <asp:textbox Width="90px" TextMode="MultiLine" Rows="2"  id="txtLicenseeDetails" runat="server" ></asp:textbox>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridHeaderLeft" Width="100px"></HeaderStyle>
                                    <ItemStyle VerticalAlign="Top" CssClass="GridRowLeft" Width="100px"></ItemStyle>           
                                </asp:TemplateField>

                                 <asp:TemplateField ItemStyle-VerticalAlign="Top" ItemStyle-Width="100px" HeaderStyle-Width="100px"
                                    HeaderText="Comments" HeaderStyle-CssClass="GridHeaderLeft" ItemStyle-CssClass="GridRowLeft">
                                    <ItemTemplate>
                                        <asp:TextBox Width="90px" ID="txtApprovalComments" runat="server" Rows="2"  TextMode="MultiLine"></asp:TextBox>
                                    </ItemTemplate>
                                     <HeaderStyle CssClass="GridHeaderLeft" Width="100px"></HeaderStyle>
                                    <ItemStyle VerticalAlign="Top" CssClass="GridRowLeft" Width="100px"></ItemStyle>
                                </asp:TemplateField>

                                <asp:TemplateField Visible="false" HeaderText="Subscription ID" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserSubID" runat="server" Text='<%# Bind("SubID_ReqID") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 
                               
                                      
                            </Columns>

                            <RowStyle CssClass="GridLine"></RowStyle>
                   </asp:GridView>
		    </div>
        </div>

        <div class="dvRow" id="dvCompanySubscriptions" runat="server">
		     <div class="leftsection" >Copmany Subscriptions : </div>
			 <div class="rightsection">
           <!--Comapny Subscriptions Grid-->
                    <asp:GridView ID="grdCompanySubscriptions" runat="server" AutoGenerateColumns="False"
                        Width="100%" GridLines="None" BorderWidth="0px" CellPadding="3" BorderStyle="None"
                        AlternatingRowStyle-CssClass="GridLine" RowStyle-CssClass="GridLine"
                        >
                        <AlternatingRowStyle CssClass="GridLine" />

                        <Columns>
                            <asp:TemplateField ItemStyle-Width="5px" HeaderStyle-Width="5px" HeaderStyle-CssClass="GridHeaderLeft"
                                ItemStyle-CssClass="GridRowLeft">
                                <HeaderTemplate>
                                    <asp:CheckBox Text="" ID="chkboxSelectAllCompany" ClientIDMode="Static" runat="server" onclick="CheckAllCompanySub(this);" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox runat="server" ID="chkComapnySubscription" Text="" onclick="GetSelectedCompanyRow(this);" ></asp:CheckBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridHeaderLeft" Width="5px"></HeaderStyle>
                                <ItemStyle VerticalAlign="Top" CssClass="GridRowLeft" Width="5px"></ItemStyle>              
                            </asp:TemplateField>
                                        
                            <asp:TemplateField ItemStyle-VerticalAlign="Top" ItemStyle-Width="350px" HeaderStyle-Width="350px"
                                HeaderText="Subscription" HeaderStyle-CssClass="GridHeaderLeft" ItemStyle-CssClass="GridRowLeft">
                                <ItemTemplate>
                                    <asp:Label ID="lblCompanySubscription" runat="server" Text='<%# Bind("SubscriptionName") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridHeaderLeft" Width="350px"></HeaderStyle>
                                    <ItemStyle VerticalAlign="Top" CssClass="GridRowLeft" Width="350px"></ItemStyle>
                            </asp:TemplateField>
                                           
                            <asp:TemplateField ItemStyle-VerticalAlign="Top" ItemStyle-Width="100px" HeaderStyle-Width="100px"
                                HeaderText="Reason" HeaderStyle-CssClass="GridHeaderLeft" ItemStyle-CssClass="GridRowLeft">
                                <ItemTemplate>
                                    <asp:TextBox TextMode="MultiLine" Rows="2"  Width="90px" ID="UserReason" Enabled="false" runat="server" Text='<%# Bind("ReasonForAccess") %>'></asp:TextBox>
                                </ItemTemplate>
                                <HeaderStyle CssClass="GridHeaderLeft" Width="100px"></HeaderStyle>
                                <ItemStyle VerticalAlign="Top" CssClass="GridRowLeft" Width="100px"></ItemStyle>
                            </asp:TemplateField>

                             <asp:TemplateField ItemStyle-VerticalAlign="Top" ItemStyle-Width="100px" HeaderStyle-Width="100px"
                                    HeaderText="Comments" HeaderStyle-CssClass="GridHeaderLeft" ItemStyle-CssClass="GridRowLeft">
                                    <ItemTemplate>
                                        <asp:TextBox  Width="90px" ID="txtApprovalComments" runat="server" Rows="2"  TextMode="MultiLine"  ></asp:TextBox>
                                    </ItemTemplate>
                                    <HeaderStyle CssClass="GridHeaderLeft" Width="100px"></HeaderStyle>
                                    <ItemStyle VerticalAlign="Top" CssClass="GridRowLeft" Width="100px"></ItemStyle>
                                </asp:TemplateField>

                            <asp:TemplateField Visible="false" HeaderText="Subscription ID">
                                <ItemTemplate>
                                    <asp:Label ID="lblCompanySubID" runat="server" Text='<%# Bind("SubID_ReqID") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>

                           
                            

                        </Columns>


                        <RowStyle CssClass="GridLine"></RowStyle>
               </asp:GridView>
              </div>
       </div>
        
       	

    </div>
</div>
    </form>
</body>
</html>
