﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExternalUsers.aspx.cs" Inherits="Viacom.AdSpace.AppsWeb.Pages.ExternalUsers" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <script type="text/javascript" src="../Scripts/js/jquery.js"></script>
    <script src="../Scripts/js/CustomJs/ExternalUsers.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../styles/CSS/ExternalUsers.css" />
    <script src="../Scripts/js/iframeResizer.contentWindow.min.js"></script>
    <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="frmExternalUsers" runat="server">
    <div>

	<div id="RemoveSharingDiv" >
		<button type="button" style="float:right;" class="button" onclick="RemoveAccess()">
		Remove Access</button>
	</div>

<asp:Label ID="lbl" runat="server" />
<table id="ExternalUsers" class="fullWidth" cellspacing="0" cellpadding="1" border="0"  summary="External Users">
<thead id="ExternalUsersThead">
<tr class="trExternal">
	<th  class="ms-vh2 maxWidth" >
		<div  class="externalHeader">
			<a class="externalHeader"></a>
		</div>
	</th>
	<th  class="ms-vh2 maxWidth" >
		<div class="externalHeader">
			Name
		</div>
	</th>
	<th  class="ms-vh2 maxWidth" >
		<div class="externalHeader">
			Email ID
		</div>
	</th>
	<th  class="ms-vh2 maxWidth" >
		<div  class="externalHeader">
			Shared Items
		</div>
	</th>
</tr>

</thead>

<tbody id="ExternalUsersTbody">
	
</tbody>
</table>

    </div>
    </form>
</body>
</html>
