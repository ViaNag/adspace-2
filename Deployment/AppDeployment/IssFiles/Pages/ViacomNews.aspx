﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViacomNews.aspx.cs" Inherits="Viacom.AdSpace.AppsWeb.Pages.ViacomNews" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <link href="../styles/CSS/News.css" rel="stylesheet" />
    <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet" type="text/css" />
     <script src="../Scripts/js/iframeResizer.contentWindow.min.js"></script>
    <title></title>
    <script type="text/javascript">
        // Set the style of the client web part page to be consistent with the host web.
        function setStyleSheet() {
            var hostUrl = ""
            if (document.URL.indexOf("?") != -1) {
                var params = document.URL.split("?")[1].split("&");
                for (var i = 0; i < params.length; i++) {
                    p = decodeURIComponent(params[i]);
                    if (/^SPHostUrl=/i.test(p)) {
                        hostUrl = p.split("=")[1];
                        document.write("<link rel=\"stylesheet\" href=\"" + hostUrl + "/_layouts/15/defaultcss.ashx\" />");
                        document.write("<link rel=\"stylesheet\" href=\"" + hostUrl + "/CSS/AdSalesUI.css\" />");
                    
                        break;
                    }
                }
            }
            if (hostUrl == "") {
                document.write("<link rel=\"stylesheet\" href=\"/_layouts/15/1033/styles/themable/corev15.css\" />");
            }
        }
        setStyleSheet();
    </script>    
     
</head>
<body>
    <form id="frmViacomNews" runat="server">
  
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
  <asp:HiddenField ID="lblSenderViacom" runat="server"></asp:HiddenField>
    <asp:Label ID="lbl" CssClass="span" runat="server"></asp:Label>
        
    </div>

        <asp:UpdatePanel ID="upViacomNews" runat="server" UpdateMode="Conditional">
     <Triggers> 
        <asp:AsyncPostBackTrigger ControlID="drpdwnlst" EventName="SelectedIndexChanged" /> 
    </Triggers>
    <ContentTemplate> 
         <div class="drpDownContainer">
    
        <asp:DropDownList ID="drpdwnlst" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpdwnlst_SelectedIndexChanged" style="height: 22px">
        </asp:DropDownList>
    
    </div>
       <div class="dvSeperator" ></div>
        <div id="dvNews" runat="server">          
    
          
        </div>
         </ContentTemplate>
</asp:UpdatePanel>

    </form>
</body>
</html>