﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="90d180b6-1bb8-468d-8484-e6405e362a04" alwaysForceInstall="true" description="Feature contains viacom adspace common content types." featureId="90d180b6-1bb8-468d-8484-e6405e362a04" imageUrl="" scope="Site" solutionId="00000000-0000-0000-0000-000000000000" title="Viacom.AdSpace.CommonContentTypes.ContentTypesFeature" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <projectItems>
    <projectItemReference itemId="2fc1f31f-87a2-40f8-a011-1a784dd88f96" />
    <projectItemReference itemId="a1cf8a52-2f41-4082-8cce-17b6c8d9c4e8" />
    <projectItemReference itemId="b03bdcf2-7c6d-4781-9639-4160347dd8e2" />
    <projectItemReference itemId="873d320f-f1d5-400e-aff0-a23aba59ebdc" />
    <projectItemReference itemId="07a8f6f6-3595-4ba2-8975-a9f1f1c25502" />
    <projectItemReference itemId="bd114508-1e9e-4f57-a99d-51bda7ad46e1" />
  </projectItems>
</feature>