﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.ExternalSharing.Entities
{
    public class Constants
    {
        public const string JobTitle = "External Sharing TimerJob";
        public const string InternalSite = "IntSiteUrl";
        public const string ExternalSite = "ExtSiteUrl";
        public const string ResetPassword = "ResetPasswordUrl";
        public const string TemplateWebUrl = "SPRegisterURL";
        public const string ExternalSharedTitle = "Externally shared Content";
        public const string Query = @"<Where><Eq><FieldRef Name='{0}' /><Value Type='{1}'>{2}</Value></Eq></Where>";
        public const string FileDeleteErrorMsg = "File '{0}': failed to delete from external web library '{1}'. ";
        public const string FolderDeleteErrorMsg = "Folder '{0}': failed to delete from external web library '{1}'. ";
        public const string ItemMoveErrorMsg = "Item '{0}': failed to move into archived folder. ";
        public const string FolderRelativeUrl = "{0}/{1}";
        public const string FileRelativeUrl = "{0}/{1}/{2}";
        public const string SharedContentListTitle = "Shared Content";
        public const string DownloadableContentListTitle = "DownloadableContent";
        public const string FileNameField = "Name";
        public const string IDField = "ID";
        public const string FileTitleField = "LinkFilename";
        public const string ConfigKeyField = "Key";
        public const string ContentGroupField = "ContentGroup";
        public const string AllowDownloadField = "AllowDownload";
        public const string ExternallySharedContentStatusField = "ExternallySharedContentStatus";
        public const string DeletedStatus = "Deleted";
        public const string PendingStatus = "Pending";
        public const string ToBeDeletedStatus = "ToBeDeleted";
        public const string ItemUrlToMove = "{0}_.000";
        public const string ItemUrlAftreMove = "{0}/{1}_.000";
        public const string ArchiveFolderFormat = "MMM_yyyy";
        public const string TitleField = "Title";
        public const string ChoiceType = "Choice";
        public const string NumberType = "Number";
        public const string FileType = "File_x0020_Type";
        public const string ExceptionLocation = "External Sharing Timer Job";
        public const string KeyNotFound = "Corresponding Key not Found in Dictionary";
        public const string ExternalSharingItemCollectionFailed = "Failed to get list item from external shared list.";
        public const string ExternalSharingConfigRequestItemCollectionFailed = "Failed to get list item from external Sharing Config Request list.";
        public const string DeleteExpiredItemsActionFailed = "Failed to delete file(s) from external web.";
        public const string MoveExecutedItemsToArchiveFolderActionFailed = "Failed to move external share item into archive folder.";
        public const string CreateMonthlyFolderActionFailed = "Failed to create archived folder in external shared list.";
        public const string FailedToSendMail = "Failed to send mail to user(s).";
        public const string FailedToGetMailTemplate = "Failed to get mail template from list.";
        public const string FailedToCreateMailStrcture = "Failed to create mail strcture.";

        public const string FailedToCreateUser = "Failed in creating user.";
        public const string FailedToCreateFolder = "Failed in creating folder on external site.";
        public const string FailedToUpdateItemStatus = "Failed in updating item status.";
        public const string FailedToGivePermissions = "Failed in giving permissions to user on external sharing site folder.";
        public const string FailedToCopyFiles = "Failed in Copy Files to external site.";
        public const string ExternalShareConfigurationRequestsTimerJobLocation = "ExternalShare  Configuration Requests TimerJob";
        public const string LoadListItemIntoCollectionJobLocation = "Load List Item Into Collection";
        public const string GetFoldersFromDictionaryLocation = "Get Folders From Dictionary";
        public const string UpdateExternalSharingMetadataLocation = "Update External Sharing Metadata";
        public const string UpdateColumnLocation = "Update Column";
        public const string UpdateExtConfigListItemLocation = "Update ExtConfig List Item";
        public const string CreatedField = "Created";

        public const string JobMessageText = "Item Processing completed";

        public const string StartULtag = "<ul>";
        public const string EndULtag = "</ul>";
        public const string fileListing = "<li type='square'>{0}</li>";
        public const string SharedWith = "SharedWith";
        public const string AdSpaceName = "AdSpaceName";
        public const string itemId = "ItemID";
        public const string EmailSubjectField = "EmailSubject";
        public const string EmailBodyField = "EmailBody";
        public const string EmailTitleField = "EmailTitle";
        public const string EmailTemplateListTitle = "EmailTemplate";
        public const string TextTypeField = "Text";
        public const string ExistingUserMailKey = "ExistingUser";
        public const string MailHtmlExtSiteurl = "<a href='{0}'>here</a>";
        public const string MailHtmlResetPassword = "<a href='{0}'>here</a>";
        public const string ChangePasswordMailHtml = "&lt;Change Password&gt;";
        public const string UserNameMailHtml = "&lt;User Name&gt;";
        public const string LibraryNameMailHtml = "&lt;Library Name&gt;";
        public const string UserComment = "&lt;Comment from the user&gt;";
        public const string PortalUrlMailHtml = "&lt;Portal Url&gt;";
        public const string MessageField = "Message";
        public const string QueryRecursiveScope = "Scope=\"Recursive\"";
        // Media silo properties
        public const string IsVideo = "IsVideo";
        public const string DownloadServer = "DownloadServer";
        public const string SourceVideoURL = "SourceVideoURL";
        public const string ProxyVideoURL = "ProxyVideoURL";
        public const string DownloadProxy = "DownloadProxy";
        public const string DownloadableContent = "DownloadableContent";
        public const string AssetID = "AssetID";

        // Configuration list
        public const string ConfigurationList = "ConfigurationList";
        public const string MideaSiloColumns = "MideaSiloColumns";
        public const string DocumentFetchingLimit = "DocumentFetchingLimit";
        public const string ArchivalBatchLimit = "ArchivalBatchLimit";
        public const string Value1 = "Value1";
        public const string ExtShareConfigurationListName = "External Share Configuration Requests";
        public const string ArchivalRequestsListName = "Archival Requests";
       
        // Externally Shared List Columns
        public const string ExpiresOn = "ExpiresOn";
        public const string PermissionStatus = "PermissionStatus";
        public const string ExpiryDate = "ExpiryDate";
        public const string ListName = "ListName";
        public const string SiteURL = "SiteURL";
        public const string InProgressStatus = "In Progress";
        public const string Read = "Read";
        public const string ViewOnly = "View Only";
        public const string SharedStatus = "Shared";
        public const string CompletedStatus = "Completed";
        public const string InProgressJobMessage = "Items processing is in progress";
        public const string GetItemCollectionOnStatusLocation = "Get Item Collection On Status";
        public const string UserExistsMsg = "User exists in system";
        public const string FbaUserId = "Fbauserid";

        // Externally Shared Config List Columns
        public const string SharedItemType = "SharedItemType";
        public const string LibraryName = "LibraryName";
        public const string ItemURL = "ItemURL";
        public const string SharingEnabled = "SharingEnabled";
        public const string SharingDuration = "SharingDuration";
        public const string JobStatus = "JobStatus";
        public const string JobMessage = "JobMessage";
        public const string ArchivalEnabled = "ArchivalEnabled";
        public const string ExternalSharingupdateDateTime = "ExternalSharingupdateDateTime";
        public const string ArchivalSettingUpdateDateTime = "ArchivalSettingupdateDateTime";

        public const string NewJobStatus = "New";
        public const string InProgressJobStatus = "In Progress";

        public const string ShareableField = "Shareable";
        public const string MaxShareDurationField = "MaxShareDuration";
        public const string ListItemItType = "List";
        public const string FolderItemType = "Folder";
        public const string ArchievedField = "IsArchived";
        public const string isArchievedField = "Is Archived";

        //Query 
        public const string ExtShareConfigListQuery = @"<Where><Or>
                                                        <Eq><FieldRef Name='{0}' /><Value Type='{1}'>{2}</Value></Eq>
                                                        <Eq><FieldRef Name='{0}' /><Value Type='{1}'>{3}</Value></Eq>
                                                        </Or></Where><OrderBy><FieldRef Name='{4}' Ascending='FALSE'/></OrderBy>";

        public const string ExtShareMetadataQuery = @"<Where>
                                                  <And>
                                                    <Or>
                                                       <IsNull>
                                                         <FieldRef Name='ExternalSharingupdateDateTime' />
                                                      </IsNull>
                                                       <Lt>
                                                          <FieldRef Name='ExternalSharingupdateDateTime' />
                                                          <Value IncludeTimeValue='TRUE' Type='DateTime'>{0}</Value>
                                                       </Lt>
                                                    </Or>
                                                    <Or>
                                                       <Neq>
                                                          <FieldRef Name='Shareable' />
                                                          <Value Type='Boolean'>{1}</Value>
                                                       </Neq>
                                                       <Neq>
                                                          <FieldRef Name='MaxShareDuration' />
                                                          <Value Type='Number'>{2}</Value>
                                                       </Neq>
                                                    </Or>
                                                   </And>
                                                   </Where>";

        public const string ArchiveMetadataQuery = @"<Where>
                                                      <And>
                                                         <Or>
                                                            <IsNull>
                                                               <FieldRef Name='ArchivalSettingupdateDateTime' />
                                                            </IsNull>
                                                            <Lt>
                                                               <FieldRef Name='ArchivalSettingupdateDateTime' />
                                                               <Value IncludeTimeValue='TRUE' Type='DateTime'>{0}</Value>
                                                            </Lt>
                                                         </Or>
                                                         <Neq>
                                                            <FieldRef Name='IsArchived' />
                                                            <Value Type='Integer'>{1}</Value>
                                                         </Neq>
                                                      </And>
                                                   </Where>";
    }
}