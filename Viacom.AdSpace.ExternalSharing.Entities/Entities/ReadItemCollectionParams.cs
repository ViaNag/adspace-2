﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.ExternalSharing.Entities
{
    /// <summary>
    /// This class contains the entites for the Read item collection
    /// </summary>
    public class ReadItemCollectionParams : IOperationParam, IOutcome
    {
        /// <summary>
        ///  Implementing IOperationParam property
        /// </summary>
        public string Query { get; set; }

        /// <summary>
        ///  Implementing IOperationParam property
        /// </summary>
        public string Siteurl { get; set; }

        /// <summary>
        /// Implementing IOperationParam property
        /// </summary>
        public string ListTitle { get; set; }

        /// <summary>
        /// Implementing Ioutcome property
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// Implementing Ioutcome property
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets current item
        /// </summary>
        public string ExternalSiteUrl { get; set; }

        /// <summary>
        /// Gets or sets tempalte web url
        /// </summary>
        public string TemplateWebUrl { get; set; }

        /// <summary>
        /// Gets or sets reset pass word url
        /// </summary>
        public string ResetPasswordUrl { get; set; }

        /// <summary>
        /// Gets or sets current item
        /// </summary>
        public string FbaUserId { get; set; }

        /// <summary>
        /// Gets or sets Dictionary
        /// </summary>
        public Dictionary<string, IOperationParam> folderSettingDict { get; set; }
    }
}
