﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;

namespace Viacom.AdSpace.ExternalSharing.Entities
{
    /// <summary>
    /// This class contains the entities that is needed for mail
    /// </summary>
    public class MailDetails : IOutcome, IOperationParam
    {
        /// <summary>
        /// Gets or sets mail subject
        /// </summary>
        public string MailSubject { get; set; }

        /// <summary>
        /// Gets or sets mail tempalte
        /// </summary>
        public string MailTemplate { get; set; }

        /// <summary>
        /// Gets or sets mail body
        /// </summary>
        public string MailBody { get; set; }

        /// <summary>
        /// Gets or sets mail message
        /// </summary>
        public string MailMessage { get; set; }
        /// <summary>
        /// Gets or sets list of users whom mail need to sent
        /// </summary>
        public List<string> MailToUsers { get; set; }

        /// <summary>
        /// Gets or sets reset password url
        /// </summary>
        public string ResetPasswordUrl { get; set; }

        /// <summary>
        /// Gets or sets reset content group
        /// </summary>
        public string ContentGroup { get; set; }

        /// <summary>
        /// Gets or sets content group item
        /// </summary>
        public List<SPListItem> ContentGroupItems { get; set; }
        /// <summary>
        /// Implementing IOperationParam property
        /// </summary>
        public string Query { get; set; }

        /// <summary>
        /// Implementing IOperationParam property
        /// </summary>
        public string ListTitle { get; set; }

        /// <summary>
        /// Implementing IOperationParam property
        /// </summary>
        public string Siteurl { get; set; }


        /// <summary>
        /// Implementing Ioutcome property
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// Implementing Ioutcome property
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets Dictionary
        /// </summary>
        public Dictionary<string, IOperationParam> folderSettingDict { get; set; }
    }
}
