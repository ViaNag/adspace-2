﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.ExternalSharing.Entities
{
    /// <summary>
    /// This class defines the Account action outcomes.
    /// </summary>
    public class ActionOutcome : IOutcome
    {
        /// <summary>
        /// Gets or sets a value indicating whether the action was a success.
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// Gets or sets a value for the message of the action outcome.
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets Dictionary
        /// </summary>
        public Dictionary<string, IOperationParam> folderSettingDict { get; set; }
    }
}
