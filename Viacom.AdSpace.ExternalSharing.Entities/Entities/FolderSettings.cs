﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.ExternalSharing.Entities.Interface;

namespace Viacom.AdSpace.ExternalSharing.Entities.Entities
{
    public class FolderSettings : IExtShareConfigRequest, IArchivalRequests, IOutcome, IOperationParam
    {
        // <summary>
        /// Gets or sets Site Url
        /// </summary>
        public string SiteURL { get; set; }

        /// <summary>
        /// Gets or sets name of library
        /// </summary>
        public string LibraryName { get; set; }

        /// <summary>
        /// Gets or sets folder url
        /// </summary>
        public string FolderURL { get; set; }

        /// <summary>
        /// Gets or sets folder url
        /// </summary>
        public DateTime SettingDateTime { get; set; }

        /// <summary>
        /// Gets or sets folder url
        /// </summary>
        public IOperationParam ParentRequest { get; set; }

        /// <summary>
        /// Implementing IOperationParam property
        /// </summary>
        public string Query { get; set; }

        /// <summary>
        /// Implementing IOperationParam property
        /// </summary>
        public string ListTitle { get; set; }

        /// <summary>
        /// Implementing IOperationParam property
        /// </summary>
        public string Siteurl { get; set; }

        /// <summary>
        /// Implementing Ioutcome property
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// Implementing Ioutcome property
        /// </summary>
        public string Message { get; set; }


        /// <summary>
        /// Gets or sets Dictionary
        /// </summary>
        public Dictionary<string, IOperationParam> folderSettingDict { get; set; }
    }
}
