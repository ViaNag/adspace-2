﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.ExternalSharing.Entities
{
    /// <summary>
    /// This class contains the entities which are the outcome of the read operations on the list items
    /// </summary>
    public class ExternalSharingItemCollection : IOutcome, IOperationParam
    {
        /// <summary>
        /// Gets or sets the value for the distinct Content group collection names
        /// </summary>
        public List<string> DistinctContentGroupCollection { get; set; }

        /// <summary>
        /// Gets or set the value for the current group
        /// </summary>
        public string CurrentContentGroup { get; set; }

        /// <summary>
        /// Gets or sets name of library
        /// </summary>
        public string CurrentLibrary { get; set; }

        /// <summary>
        /// Gets or sets name of library
        /// </summary>
        public string FolderPath { get; set; }

        /// <summary>
        /// Gets or sets the sharepoint list item collection
        /// </summary>
        public SPListItemCollection ItemCollection { get; set; }

        /// <summary>
        /// Gets or sets current item
        /// </summary>
        public SPListItem CurentItem { get; set; }

        /// <summary>
        /// Implementing IOperationParam property
        /// </summary>
        public string Siteurl { get; set; }

        /// <summary>
        /// Gets or sets current item
        /// </summary>
        public string ExternalSiteUrl { get; set; }

        /// <summary>
        /// Gets or sets tempalte web url
        /// </summary>
        public string TemplateWebUrl { get; set; }

       /// <summary>
       /// Gets or sets content group items
       /// </summary>
        public List<SPListItem> ContentGroupItems { get; set; }

        /// <summary>
        /// Implementing IOperationParam property
        /// </summary>
        public string Query { get; set; }

        /// <summary>
        /// Implementing IOperationParam property
        /// </summary>
        public string ListTitle { get; set; }


        /// <summary>
        /// Implementing Ioutcome property
        /// </summary>
        public bool IsSuccess { get; set; }

        /// <summary>
        /// Implementing Ioutcome property
        /// </summary>
        public string Message { get; set; }


        /// <summary>
        /// Gets or sets column name
        /// </summary>
        public string ColumnName { get; set; }

        /// <summary>
        /// Gets or sets status of current item
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets users
        /// </summary>
        public string User { get; set; }

        /// <summary>
        /// Gets or sets current permission level
        /// </summary>
        public string permissionLevel { get; set; }

         /// <summary>
        /// Gets or sets current permission level
        /// </summary>
        public string FbaUserId { get; set; }


        /// <summary>
        /// Gets or sets Dictionary
        /// </summary>
        public Dictionary<string, IOperationParam> folderSettingDict { get; set; }
    }
}
