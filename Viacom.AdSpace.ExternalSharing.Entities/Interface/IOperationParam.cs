﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.ExternalSharing.Entities
{
    /// <summary>
    /// Interface for operation params
    /// </summary>
    public interface IOperationParam
    {
        /// <summary>
        /// Gets or sets a value for the query to be executed
        /// </summary>
        string Query { get; set; }

        /// <summary>
        /// Gets or sets a value for the web name
        /// </summary>
        string Siteurl { get; set; }

        /// <summary>
        /// Gets or sets a value for the list title
        /// </summary>
        string ListTitle { get; set; }
    }
}
