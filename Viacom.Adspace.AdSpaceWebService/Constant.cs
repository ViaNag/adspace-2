﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.AdSpaceWebService
{
    public class Constant
    {
        internal const string CONFIGLISTNAME = "Configurations";
        internal static readonly string[] OOTB_LIB_NAME = { "Master Page Gallery", "Converted Forms", "Form Templates", "List Template Gallery", "Site Assets", "Site Collection Documents", "Pages", "Images", "Events", "Solution Gallery", "Style Library", "Theme Gallery", "Web Part Gallery" };
        internal static readonly string[] OOTB_FOLDER_NAME = { "Forms" };
       // internal static readonly string[] SHARED_COLUMN = { "ViacomDepartment", "Division", "DocContentType", "DocCreated", "DocCreatedBy", "DocModified", "DocModifiedBy", "DocSourceLibrary", "LifecycleState", "RecordDeclarationDate", "UncategorizedReason" };
        internal static readonly string CONFIG_LIST_URL = "/FieldsConfigList";
        internal static readonly string QueryForTitleEqualValue = "<Where><Eq><FieldRef Name='Key'/><Value Type='Text'>{0}</Value></Eq></Where>";
        internal static readonly string CONFIG_LIST_FIELD_NAME = "Value1";
        internal static readonly string SHARED_COLUMN_KEY = "SharedColumn";
    }
}
     
 
