﻿using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Data;


namespace Viacom.AdSpace.Shared
{
    public class ComonOperations
    {
        public static string hostURL = string.Empty;
        private static bool exceuteOnce = true;


        public static void CreateItemsinOperationTable(ClientContext clientContext, int reqID, string actionLevel, string roldef, string apprvRoleDefination, string webSiteUrl, string listName, string itemID)
        {
            List oList = clientContext.Web.Lists.GetByTitle("RequestsTasksList");
            clientContext.Load(oList);
            ListItemCreationInformation itemCreateInfo = new ListItemCreationInformation();
            Microsoft.SharePoint.Client.ListItem oLitm = oList.AddItem(itemCreateInfo);

            oLitm["RequestID"] = reqID;
            oLitm["Scope"] = actionLevel;
            oLitm["PermissionLevel"] = roldef;
            oLitm["WebSiteUrl"] = webSiteUrl;
            oLitm["ListName"] = listName;

            oLitm.Update();
            clientContext.ExecuteQuery();

        }

        /// <summary>
        /// This method convert datatable into JSON string
        /// </summary>
        /// <param name="dTUsersTable"></param>
        /// <returns></returns>
        public static string GetJson(DataTable dTUsersTable)
        {
            try
            {
                //Declaration for serializer
                System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();

                try
                {
                    Dictionary<string, object> row = null;
                    //Iterating the Each row in the ADLDS user table
                    foreach (DataRow dr in dTUsersTable.Rows)
                    {
                        row = new Dictionary<string, object>();
                        foreach (DataColumn col in dTUsersTable.Columns)
                        {
                            //Adding a JSON string
                            row.Add(col.ColumnName.Trim(), dr[col]);
                        }
                        rows.Add(row);
                    }
                }
                catch (Exception ex)
                {
                    //stores the error
                    #region Error Log

                    #endregion
                }
                return serializer.Serialize(rows);
            }
            catch (Exception ex)
            {
                return null;

            }
        }
    }

}