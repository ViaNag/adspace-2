﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint.Taxonomy;

namespace Viacom.AdSpace.TeamSiteSharedContent
{
    public class Extensions
    {

        public static Group GetByName(this GroupCollection groupCollection, string groupName)
        {
            if (String.IsNullOrEmpty(groupName))
            {
                throw new ArgumentException("Taxonomy group name cannot be empty", "name");
            }
            foreach (var group in groupCollection)
            {
                if (group.Name == groupName)
                {
                    return group;
                }
            }
            throw new ArgumentOutOfRangeException("Group Name", groupName, " Could not find the taxonomy group");
        }

        public static TermSet GetByName(this TermSetCollection termSets, string termSetName)
        {
            if (String.IsNullOrEmpty(termSetName))
            {
                throw new ArgumentException("Term set name cannot be empty", "name");
            }
            foreach (var termSet in termSets)
            {
                if (termSet.Name == termSetName)
                {
                    return termSet;
                }
            }
            throw new ArgumentOutOfRangeException("TermSetName", termSetName, " Could not find the term set");
        }
    }
}