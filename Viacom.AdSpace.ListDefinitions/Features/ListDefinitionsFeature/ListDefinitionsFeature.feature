﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="8d8ce174-2b15-43ba-8994-d03e059cac34" description="Feature Contains viacom adspace list definitions." featureId="8d8ce174-2b15-43ba-8994-d03e059cac34" imageUrl="" solutionId="00000000-0000-0000-0000-000000000000" title="Viacom.AdSpace.ListDefinitions.ListDefinitionsFeature" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <projectItems>
    <projectItemReference itemId="5c0bf87b-eb72-4159-9fb1-2e3af5c7f9f3" />
    <projectItemReference itemId="5346db70-e163-4370-bc4f-b2f31bb32387" />
    <projectItemReference itemId="198c7536-9b7b-4463-9dd0-62b8aa0d261e" />
    <projectItemReference itemId="e0b309b0-079a-4ae1-9374-17c8bda00d25" />
    <projectItemReference itemId="8ce6e874-2d73-4c6d-a41b-1d6e404e64aa" />
    <projectItemReference itemId="9f282158-f759-4507-9793-277b1b00cc9f" />
    <projectItemReference itemId="1c2fd235-5bdf-42f4-b72e-56c2ee6cfc4f" />
    <projectItemReference itemId="f451876f-295d-4d96-9297-d143ffb02e92" />
    <projectItemReference itemId="a93e0df0-eb51-4bb1-be98-70e9181bbf17" />
  </projectItems>
</feature>