﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;

namespace Viacom.AdSpace.SiteProvisioning.BLL
{
    /// <summary>
    /// Description			: Interface for Site Provisioning Request
    /// System				: Viacom - Ad Space
    /// 
    /// </summary>
    /// <ModificationLog>
    /// Ver	|Date			|Author			|Change Desc.
    /// ---------------------------------------------------------------------------------------------------
    /// 1	|06/17/2015		|Ateet 		|Initial Version
    /// </ModificationLog>
    public interface ISiteProvisioningRequest
    {
        //Common Column
        /// <summary>
        /// Create site
        /// </summary>
        ActionTypes Action { get; set; }
        /// <summary>
        /// Type of the site: Client
        /// </summary>
        SiteTypes SiteType { get; set; }

        //Client Column
        string ClientName { get; set; }
        string ClientCategory { get; set; }
        string ClientSegment { get; set; }
        string RDMClientID { get; set; }
        string GabrielClientID { get; set; }
        string WideOrbitClientID { get; set; }
        string AdFrontClientID { get; set; }
        string SiteURL { get; set; }
        string RequestStatus { get; set; }
        string Comments { get; set; }
        string TeamSiteURL { get; set; }
        string ClientSiteURL { get; set; }

        #region "Create Action"
        string CheckSiteExists(string clientSiteCollectionURL, ISiteProvisioningRequest request);
        string InsertMasterData(string rootSiteCollectionURL, ISiteProvisioningRequest request);
        string GetWebTemplate(string clientSiteCollectionURL, ISiteProvisioningRequest request);
        string CreateSite(string clientSiteCollectionURL, ISiteProvisioningRequest request, string templateName);
        string ActivateFeature(string webUrl);
        #endregion

        #region "Common method to use for Create/Edit Action
        string ReactivateFeature(string siteURL, ISiteProvisioningRequest request);
        string DeleteListItems(string siteURL, ISiteProvisioningRequest request);
        string UpdateSiteName(string siteURL, ISiteProvisioningRequest request);
        #endregion

        #region "Common method to use for Create/Edit Action
        string UpdatedListItems(string clientSiteCollectionURL, ISiteProvisioningRequest request, string status, string comments);
        #endregion
    }
}
