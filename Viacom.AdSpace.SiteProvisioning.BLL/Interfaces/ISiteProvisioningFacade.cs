﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.SiteProvisioning.BLL;

namespace Viacom.AdSpace.SiteProvisioning.BLL
{
    /// <summary>
    /// Description			: Interface for Site Provisioning Facade
    /// System				: Viacom - AdSpace
    /// 
    /// </summary>
    /// <ModificationLog>
    /// Ver	|Date			|Author			|Change Desc.
    /// ---------------------------------------------------------------------------------------------------
    /// 1	|06/17/2015		|Ateet 		|Initial Version
    /// </ModificationLog>
    public interface ISiteProvisioningFacade
    {
        void ExecuteSiteProvisioningRequest(ISiteProvisioningAction siteAction);
        void ExecuteSiteProvisioningRequest(ISiteProvisioningAction siteAction,string rootSiteCollectionURL);
    }
}
