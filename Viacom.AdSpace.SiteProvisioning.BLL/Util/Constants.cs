﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.SiteProvisioning.BLL
{
    /// <summary>
    /// Description			: class for defining Constants
    /// System				: Viacom - Ad Space
    /// 
    /// </summary>
    /// <ModificationLog>
    /// Ver	|Date			|Author			|Change Desc.
    /// ---------------------------------------------------------------------------------------------------
    /// 1	|06/17/2015		|Ateet 		|Initial Version
    /// </ModificationLog>
    public class Constants
    {
        public const string SITEPROVISIONING_TIMERJOBNAME = "AdSpace Site Provisioning";
        public const string CLIENT_PROVISION_LIST_NAME = "ClientConfiguration";
        public const string CLIENT_SITE_POSTFIX = "/sites/ClientsSpace/";
        public const string CLIENT_RELATIVE_URL = "sites/ClientsSpace/";

public const string FAIL_NULL_EX_MSG = "Term is Null";


        //Createlist categories
        public const string CREATE_ITEM_URL = "URL";
        public const string CREATE_ITEM_MULTIUSER = "MultiUser";
        public const string CREATE_ITEM_USER = "User";
        public const string CREATE_ITEM_TAX = "Taxonomy";
        public const string CREATE_ITEM_SLT = "Text";

        //Site Type Constant
        public const string CLIENT_SITE_TYPE = "ClientSite";

        //Web Template Name
        public const string CLIENT_WEB_TEMPLATE = "ViacomClientSpace";

        //Dictionary object constant for web creation
        public const string DICT_PARENTWEB_KEY = "parentWebUrl";
        public const string DICT_NEWWEB_KEY = "newWebUrl";
        public const string DICT_NEWWEB_TITLE_KEY = "newWebTitle";
        public const string DICT_NEWWEB_DESC_KEY = "newWebDescription";
        public const string DICT_TEMPLATENAME_KEY = "templateName";
        public const uint DICT_LCID = 1033;

        //relative path
        public const string TEAMSITE_PATH = "/sites/clients";
        public const string CLIENTSPACE_PATH = "sites/clientsspace";
        public const string CLIENTSPACE_PATHWITHSLASH = "sites/clientsspace/";

        //CLient data list field name
        public const string CLIENT_DATA_CLIENTNAME = "ClientName";
        public const string CLIENT_DATA_SITEURL = "SiteURL";
        public const string CLIENT_DATA_RDMID = "RDMClientID";
        public const string CLIENT_DATA_GABRIELID = "GabrielClientID";
        public const string CLIENT_DATA_WIDEID = "WideOrbitClientID";
        public const string CLIENT_DATA_ADFRONTID = "AdFrontClientID";
        public const string CLIENT_DATA_CATEGORY = "ClientCategory";
        public const string CLIENT_DATA_SEGMENT = "ClientSegment";
        public const string CLIENT_DATA_REQSTATUS = "AdSpaceStatus";
        public const string CLIENT_DATA_COMMENTS = "AdSpaceMessage";
        public const string CLIENT_DATA_CLIENTSITE_URL = "ClientSiteUrl";
        public const string CLIENT_DATA_SEQUENCE_NUMBER = "SequenceNumber";
        public const string CLIENT_DATA_SEQUENCE = "ClientSequence";
        //Site Creation Action
        public const string SITE_PROVISIONING_DATA_ACTIONTYPE = "ActionType";
        public const string SITE_PROVISIONING_DELETE_SITE = "DeleteSite";        

        //CLient library name
        public const string CLIENT_LIBRARY_NAME_CLIENTPLANNING = "Client Planning";
        public const string CLIENT_LIBRARY_NAME_PRESENTATION = "Client Presentations";
        public const string CLIENT_LIBRARY_NAME_CUSTOMRESEARCH = "Custom Research";
        public const string CLIENT_LIBRARY_NAME_MKTCASESTUDIES= "Marketing Case Studies";

        //General Constants
        public const string SUCESS_MSG = "Success";
        public const string FAIL_MSG = "Operation Fail during processing of action : ";
        public const string FAIL_EX_MSG = ",Exception : ";
        public const string FLD_TYPE_TAX = "Managed Metadata";
        

        //Status Message
        public const string STATUS_INITIATED = "Initiate";
        public const string STATUS_FAILED = "Failed";
        public const string STATUS_COMPLETED = "Success";

        //Client data list update
        public const string ADSPACE_SITE_CREATED = "AdSpaceSiteCreated";
        public const string CLIENT_DATA_LIST_NAME = "Client Data";
       
        //Query to fetach item from team site list
        public const string SITEPRO_LIST_QUERY = "<View><Query><OrderBy><FieldRef Name='ActionType' Ascending='True' /></OrderBy><Where><And><And><Eq><FieldRef Name='AdSpaceStatus'/><Value Type='Choice'>" + Constants.STATUS_INITIATED + "</Value></Eq><Eq><FieldRef Name='Status'/><Value Type='Choice'>" + Constants.STATUS_COMPLETED + "</Value></Eq></And><Neq><FieldRef Name='ActionType'/><Value Type='Choice'>" + Constants.SITE_PROVISIONING_DELETE_SITE + "</Value></Neq></And></Where></Query></View>";

        //Query to fetach item from team client data list
        public const string DATA_LIST_QUERY = "<View><Query><Where><Eq><FieldRef Name='ClientName'/><Value Type='Note'>{0}</Value></Eq></Where></Query></View>";


        //Default Value feature 
        public static string[] CLIENT_DEFAULT_FEATURE_GUID = { "1e067a8f-a504-4a71-82d2-6e0f0f22b576", "48e97285-d643-41c2-bfbd-ae19432d16a5" };
        public static string[] ARRAY_CREATE_CLIENT_SITE_FEATURE_GUID = { "1e067a8f-a504-4a71-82d2-6e0f0f22b576" };


        //Constants for credential
        public const string SOURCE_DOMAIN = "SourceDomain";
        public const string SOURCE_USER_NAME = "SourceUserName";
        public const string SOURCE_PASSWORD = "SourcePassWord";

        //Constants for Teamsite
        
        public const string SITEPRO_LIST_TITLE = "Site Provisioning";

        public const string CONFIGURATION_LISTNAME = "FieldsConfigList";
        public const string TEAMSITE_QUERY = "<Where><Eq><FieldRef Name='Key'/><Value Type='Text'>" + Constants.TEAMSITE_URL_KEY + "</Value></Eq></Where>";
        public const string CONFIGLIST_FIELDKEY_NAME = "Key";
        public const string TEAMSITE_URL_KEY = "TeamSiteURL";
        public const string CONFIGLIST_FIELDKVALUE_NAME = "Value1";
        
    }

}
