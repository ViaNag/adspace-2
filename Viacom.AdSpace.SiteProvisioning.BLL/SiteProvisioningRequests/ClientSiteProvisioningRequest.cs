﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.SiteProvisioning.BLL;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Taxonomy;

namespace Viacom.AdSpace.SiteProvisioning
{
    /// <summary>
    /// Description			: class for site provisioning Client request
    /// System				: Viacom - Ad Space
    /// 
    /// </summary>
    /// <ModificationLog>
    /// Ver	|Date			|Author			|Change Desc.
    /// ---------------------------------------------------------------------------------------------------
    /// 1	|06/17/2015		|Ateet 		|Initial Version
    /// </ModificationLog>
    public class ClientSiteProvisioningRequest : ISiteProvisioningRequest
    {
        //Client Column
        public string ClientName { get; set; }
        public string ClientCategory { get; set; }
        public string ClientSegment { get; set; }
        public string RDMClientID { get; set; }
        public string GabrielClientID { get; set; }
        public string WideOrbitClientID { get; set; }
        public string AdFrontClientID { get; set; }
        public string SiteURL { get; set; }
        public string RequestStatus { get; set; }
        public string Comments { get; set; }
        public string TeamSiteURL { get; set; }
        public string ClientSiteURL { get; set; }

        ActionTypes ISiteProvisioningRequest.Action { get; set; }

        SiteTypes ISiteProvisioningRequest.SiteType { get; set; }
        private string statusMsg = string.Empty;
        Util oUtil = new Util();

        #region Create client site method

        /// <summary>
        /// Function to get veirfy is site exists or not
        /// </summary>
        /// <param name="rootSiteCollectionURL">Site collection url</param>
        /// <param name="request">Interface object</param>
        /// <returns>string</returns>
        public string CheckSiteExists(string clientSiteCollectionURL, ISiteProvisioningRequest request)
        {
            try
            {
                string sucessMsg = string.Empty;
                sucessMsg = oUtil.CheckSiteExists(clientSiteCollectionURL, request.SiteURL);
                return sucessMsg;
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space Site Provisioning-ClientSiteProvisioningrequest.cs-CheckSiteExists", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                StringBuilder sbFailMsg = new StringBuilder();
                return Convert.ToString(sbFailMsg.AppendFormat("{0}{1}{2}{3}", Constants.FAIL_MSG, "CheckSiteExists", Constants.FAIL_EX_MSG, ex.Message));
            }
        }

        /// <summary>
        /// Function to create items in CLient master list- CLient Data, Sites and Site Mapping
        /// </summary>
        /// <param name="rootSiteCollectionURL">Root site collection url</param>
        /// <param name="request">Interface object</param>
        /// <returns>Success message whether operation performed successfully or not</returns>
        public string InsertMasterData(string rootSiteCollectionURL, ISiteProvisioningRequest request)
        {
            try
            {
                string sucessMsg = string.Empty;

                //Call function for entry in Client Data List
                List<object[]> lstData = new List<object[]>();
                lstData.Add(new object[] { Constants.CLIENT_DATA_CLIENTNAME, Constants.CREATE_ITEM_TAX, request.ClientName });
                lstData.Add(new object[] { Constants.CLIENT_DATA_RDMID, Constants.CREATE_ITEM_SLT, request.RDMClientID });
                lstData.Add(new object[] { Constants.CLIENT_DATA_GABRIELID, Constants.CREATE_ITEM_SLT, request.GabrielClientID });
                lstData.Add(new object[] { Constants.CLIENT_DATA_WIDEID, Constants.CREATE_ITEM_SLT, request.WideOrbitClientID });
                lstData.Add(new object[] { Constants.CLIENT_DATA_ADFRONTID, Constants.CREATE_ITEM_SLT, request.AdFrontClientID });
                lstData.Add(new object[] { Constants.CLIENT_DATA_CATEGORY, Constants.CREATE_ITEM_TAX, request.ClientCategory });
                lstData.Add(new object[] { Constants.CLIENT_DATA_SEGMENT, Constants.CREATE_ITEM_TAX, request.ClientSegment });
                sucessMsg = oUtil.InsertMasterData(request.SiteURL, Constants.CLIENT_PROVISION_LIST_NAME, lstData);
                
                return sucessMsg;
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Sales ClientSiteProvisioningrequest.cs-InsertMasterData ", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                StringBuilder sbFailMsg = new StringBuilder();
                return Convert.ToString(sbFailMsg.AppendFormat("{0}{1}{2}{3}", Constants.FAIL_MSG, "InsertMasterData ", Constants.FAIL_EX_MSG, ex.Message));
            }
        }

        /// <summary>
        /// Function to get web template name by title
        /// </summary>
        /// <param name="rootSiteCollectionURL">Site collection url</param>
        /// <param name="request">Interface object</param>
        /// <returns>Web template name</returns>
        public string GetWebTemplate(string clientSiteCollectionURL, ISiteProvisioningRequest request)
        {
            try
            {
                string sucessMsg = string.Empty;
                string siteType = Convert.ToString(request.SiteType);
                StringBuilder clientSiteURL = new StringBuilder();
                sucessMsg = oUtil.GetWebTemplate(clientSiteCollectionURL, siteType);
                return sucessMsg;
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space Site Provisioning-ClientSiteProvisioningrequest.cs-GetWebTemplate", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                StringBuilder sbFailMsg = new StringBuilder();
                return Convert.ToString(sbFailMsg.AppendFormat("{0}{1}{2}{3}", Constants.FAIL_MSG, "GetWebTemplate", Constants.FAIL_EX_MSG, ex.Message));
            }
        }

        /// <summary>
        /// Function to create web using web template
        /// </summary>
        /// <param name="rootSiteCollectionURL">Site collection url</param>
        /// <param name="request">Interface object</param>
        /// <param name="templateName">Web template name</param>
        /// <returns>string whether operation executed succesfully or not</returns>
        public string CreateSite(string clientSiteCollectionURL, ISiteProvisioningRequest request, string templateName)
        {
            try
            {
                string sucessMsg = string.Empty;
                Dictionary<string, string> objNewWeb = new Dictionary<string, string>();
                string webUrl = request.SiteURL.ToLower();
                string newWebUrl = webUrl.Split(new string[] { Constants.CLIENT_SITE_POSTFIX.ToLower() }, StringSplitOptions.None).Last();

                objNewWeb.Add(Constants.DICT_NEWWEB_KEY, newWebUrl);
                objNewWeb.Add(Constants.DICT_NEWWEB_TITLE_KEY, request.ClientName.Split(new char[] { '|' }).First());
                objNewWeb.Add(Constants.DICT_NEWWEB_DESC_KEY, request.ClientName.Split(new char[] { '|' }).First());
                objNewWeb.Add(Constants.DICT_TEMPLATENAME_KEY, templateName);
                sucessMsg = oUtil.CreateSite(clientSiteCollectionURL, objNewWeb, Constants.DICT_LCID, false);
                return sucessMsg;
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space Site Provisioning-ClientSiteProvisioningrequest.cs-CreateSite", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                StringBuilder sbFailMsg = new StringBuilder();
                return Convert.ToString(sbFailMsg.AppendFormat("{0}{1}{2}{3}", Constants.FAIL_MSG, "CreateSite", Constants.FAIL_EX_MSG, ex.Message));
            }
        }

        /// <summary>
        /// Function to activate features for web
        /// </summary>
        /// <param name="webUrl">Web url where feature will be activated</param>
        /// <returns>string message whether operation executed succesfully or not</returns>
        public string ActivateFeature(string webUrl)
        {
            try
            {
                webUrl = webUrl.Split(new char[] { ',' }).First();
                string sucessMsg = string.Empty;
                foreach (string featureID in Constants.ARRAY_CREATE_CLIENT_SITE_FEATURE_GUID)
                {
                    sucessMsg = oUtil.ActivateFeature(webUrl, featureID);
                    if (sucessMsg.Equals(Constants.SUCESS_MSG))
                    {
                        continue;
                    }
                    else
                    {
                        throw new Exception("Error while activating feature with id - " + featureID );
                    }
                }
                return sucessMsg;
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space Site Provisioning.cs-ActivateFeature", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                StringBuilder sbFailMsg = new StringBuilder();
                return Convert.ToString(sbFailMsg.AppendFormat("{0}{1}{2}{3}", Constants.FAIL_MSG, "ActivateFeature", Constants.FAIL_EX_MSG, ex.Message));
            }
        }
        #endregion

        #region "Edit CLient"
        /// <summary>
        /// Funcation to reactivate feature
        /// </summary>
        /// <param name="siteURL">Site url where feature need ot activate</param>
        /// <param name="request">Site Request object</param>
        /// <returns>Status message</returns>
        public string ReactivateFeature(string siteURL, ISiteProvisioningRequest request)
        {
            try
            {
                string sucessMsg = string.Empty;
                string webUrl = siteURL.Split(new char[] { ',' }).First();
                foreach(string featureGuid in Constants.CLIENT_DEFAULT_FEATURE_GUID)
                {
                    sucessMsg = oUtil.DeActivateFeature(webUrl, featureGuid);
                    if (sucessMsg.Equals(Constants.SUCESS_MSG))
                    {
                        sucessMsg = oUtil.ActivateFeature(webUrl, featureGuid);
                    }
                    else
                    {
                        throw new Exception("Error while deactivating feature with id - " + Constants.CLIENT_DEFAULT_FEATURE_GUID);
                    }
                }
                
                return sucessMsg;
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space Site Provisioning.cs-ReactivateFeature", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                StringBuilder sbFailMsg = new StringBuilder();
                return Convert.ToString(sbFailMsg.AppendFormat("{0}{1}{2}{3}", Constants.FAIL_MSG, "ReactivateFeature", Constants.FAIL_EX_MSG, ex.Message));
            }
        }
        #endregion

        #region "Common method for Create/Edit client sites"
        /// <summary>
        /// Function to update list item
        /// </summary>
        /// <param name="clientSiteCollectionURL"></param>
        /// <param name="request">Interface object</param>
        /// <returns>Status message</returns>
        public string UpdatedListItems(string clientSiteCollectionURL, ISiteProvisioningRequest request, string status, string comments)
        {
            try
            {
                string sucessMsg = string.Empty;
                string clientName = request.ClientName;
                string clientSiteURL = request.ClientSiteURL;
                if (!string.IsNullOrEmpty(clientName))
                {
                    clientName = clientName.Split(new char[] { '|' }).First();
                    if (!string.IsNullOrEmpty(clientName))
                    {
                        if (clientName.Contains(";#"))
                        {
                            clientName = clientName.Split(new string[] { ";#" }, StringSplitOptions.None).Last();
                        }
                    }
                }
                if (comments.Equals(Constants.SUCESS_MSG))
                {
                    //Call function for Client data in RM
                    string[] clienDatafieldToUpdate = { Constants.ADSPACE_SITE_CREATED };
                    string[] clienDatavalueToUpdate = { "true" };

                    StringBuilder osbclientDataQuery = new StringBuilder();

                    osbclientDataQuery.AppendFormat("{0}{1}{2}{3}{4}", "<View><Query><Where><BeginsWith><FieldRef Name='", Constants.CLIENT_DATA_CLIENTNAME, "'/><Value Type='TaxonomyFieldType'>", clientName, "</Value></BeginsWith></Where></Query></View>");
                    string oClientDataQuery = Convert.ToString(osbclientDataQuery);
                    sucessMsg = oUtil.UpdateListItemCOM(request.TeamSiteURL, Constants.CLIENT_DATA_LIST_NAME, oClientDataQuery, clienDatafieldToUpdate, clienDatavalueToUpdate, Constants.CLIENT_DATA_CLIENTSITE_URL, clientSiteURL);
                }
                //Call function for Site Provisioning in RM
                string[] fieldToUpdate = { Constants.CLIENT_DATA_REQSTATUS, Constants.CLIENT_DATA_COMMENTS };
                string[] valueToUpdate = { status, comments };

                StringBuilder osbQuery = new StringBuilder();
                osbQuery.AppendFormat("{0}{1}{2}{3}{4}", "<View><Query><Where><Contains><FieldRef Name='", Constants.CLIENT_DATA_CLIENTNAME, "'/><Value Type='TaxonomyFieldType'>", clientName, "</Value></Contains></Where></View></Query>");
                string oQuery = Convert.ToString(osbQuery);
                sucessMsg = oUtil.UpdateListItemCOM(request.TeamSiteURL, Constants.SITEPRO_LIST_TITLE, oQuery, fieldToUpdate, valueToUpdate, Constants.CLIENT_DATA_CLIENTNAME, clientName);

                return sucessMsg;
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space Site Provisioning.cs-UpdatedListItems", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                StringBuilder sbFailMsg = new StringBuilder();
                return Convert.ToString(sbFailMsg.AppendFormat("{0}{1}{2}{3}", Constants.FAIL_MSG, "UpdatedListItems", Constants.FAIL_EX_MSG, ex.Message));
            }
        }

        public string DeleteListItems(string clientSiteCollectionURL, ISiteProvisioningRequest request)
        {
            try
            {
                return oUtil.DeleteMasterData(request.SiteURL, Constants.CLIENT_PROVISION_LIST_NAME);
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Sales ClientSiteProvisioningrequest.cs-DeleteListItems ", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                StringBuilder sbFailMsg = new StringBuilder();
                return Convert.ToString(sbFailMsg.AppendFormat("{0}{1}{2}{3}", Constants.FAIL_MSG, "InsertMasterData ", Constants.FAIL_EX_MSG, ex.Message));
            }            
        }

        public string UpdateSiteName(string clientSiteCollectionURL, ISiteProvisioningRequest request)
        {
            try
            {
                string sucessMsg = string.Empty;

                sucessMsg = oUtil.UpdateSiteName(request.SiteURL, request.ClientName);

                return sucessMsg;
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Sales ClientSiteProvisioningrequest.cs-DeleteListItems ", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                StringBuilder sbFailMsg = new StringBuilder();
                return Convert.ToString(sbFailMsg.AppendFormat("{0}{1}{2}{3}", Constants.FAIL_MSG, "InsertMasterData ", Constants.FAIL_EX_MSG, ex.Message));
            }
        }

        #endregion


    }
}