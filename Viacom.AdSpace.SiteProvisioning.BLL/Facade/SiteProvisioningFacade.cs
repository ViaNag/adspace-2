﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.SiteProvisioning.BLL;

namespace Viacom.AdSpace.SiteProvisioning.BLL
{
    /// <summary>
    /// Description			: Site Provisioning class to be used as entry point for Executing Site Operation
    /// System				: Viacom - Ad Space
    /// 
    /// </summary>
    /// <ModificationLog>
    /// Ver	|Date			|Author			|Change Desc.
    /// ---------------------------------------------------------------------------------------------------
    /// 1	|06/17/2015		|Ateet 		|Initial Version
    /// </ModificationLog>
    public class SiteProvisioningFacade : ISiteProvisioningFacade
    {

        #region Private Variables
        //The variable is declared to be volatile to ensure that assignment to the 
        //mInstance variable completes before the instance variable can be accessed.
        private static volatile SiteProvisioningFacade mInstance;
        private static object mSyncObject = new object();
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor definition
        /// </summary>
        private SiteProvisioningFacade()
        {
        }
        #endregion

        #region Public Properties

        #region SingleInstance
        /// <summary>
        /// Instance is private static property to return the same Instance of the class everytime.
        /// Note: Double - checked serialized initialization of class pattern is used.
        /// </summary>
        public static SiteProvisioningFacade SingleInstance
        {
            get
            {
                //Check for null before acquiring the lock.
                if (mInstance == null)
                {
                    //Use a mSyncObject to lock on, to avoid deadlocks among multiple threads.
                    lock (mSyncObject)
                    {
                        //Again check if mInstance has been initialized, 
                        //since some other thread may have acquired the lock first and constructed the object.
                        if (mInstance == null)
                        {
                            mInstance = new SiteProvisioningFacade();
                        }
                    }
                }
                return mInstance;
            }
        }
        #endregion

        #endregion
        public void ExecuteSiteProvisioningRequest(ISiteProvisioningAction siteAction)
        {
            siteAction.ExecuteAction();
        }


        public void ExecuteSiteProvisioningRequest(ISiteProvisioningAction siteAction, string rootSiteCollectionURL)
        {
            siteAction.ExecuteAction(rootSiteCollectionURL);
        }
    }
}
