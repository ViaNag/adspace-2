﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.Invoice.Base.Roles
{
    public interface IRoleSource
    {
        List<IRole> Roles { get; }        
    }
}
