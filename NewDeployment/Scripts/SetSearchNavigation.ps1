function UpdateSearchNav([String]$ConfigFileName = "")
{
	Write-Host "Reading Configuration file: Process Starting ....." -ForegroundColor Green
	[string]$xmlpath = $ConfigFileName

$searchXML =  [xml](Get-Content ($xmlpath))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
	Write-Host "Success: Reading Configuration file: Process Completed ..." -ForegroundColor Green
	
 Write-Host -ForegroundColor Red "============================================="
 Write-Host -ForegroundColor Green "Updating Search Navigation at URL " -NoNewline;
 Write-Host -ForegroundColor Green $Identity

try
		
{
$siteurl=$searchXML.SearchNavigation.SiteUrl

 $s = Get-SPSite $siteurl
 $w = $s.RootWeb

 foreach ($website in $searchXML.SearchNavigation.SearchWeb) 
 { 
  Write-Host -ForegroundColor Red "============================================="
  Write-Host -ForegroundColor Green "Updating Search Navigation at URL " -NoNewline;
  Write-Host -ForegroundColor Green $w.Url
  $weburl=$website.Weburl;
$webname=Get-SPWeb $weburl;
  $SearchNav =  $webname.Navigation.SearchNav
  
  IF ($SearchNav -ne $NULL)
  {
   Write-Host -ForegroundColor Red "This Site Search Navigation Already containing values";
   $SearchNav
    Write-Host -ForegroundColor Green "Deleting Navigation nodes";
    $cnt= $SearchNav.Count
    for($i=0; $i-lt $cnt; $i++)  
       {  
       write-host  -ForegroundColor Green  "Deleting Node title : "$SearchNav[0].Title 
           $SearchNav[0].Delete() 
        } 
  }
 
   foreach($Nav in $website.Navigation)
   {
   Write-Host -ForegroundColor Green $Nav.Title;
   $Title = $Nav.Title
   $RelativeUrl = $Nav.RelativeUrl
   $node = new-object -TypeName "Microsoft.SharePoint.Navigation.SPNavigationNode" -ArgumentList $Title, $RelativeUrl, $true
   $webname.Navigation.SearchNav.AddAsLast($node)
        
        if($Nav.OpenInNewTab -ne $NULL -and $Nav.OpenInNewTab -eq 'true')
        {
            $node.Properties["Target"] = "_blank";

            $node.Update()
        }
   }
 
 $webname.Dispose()
}
}
catch
{
}
finally 
{
}
  Write-Host -ForegroundColor Red "============================================="
 

 $s.Dispose()
 Write-Host -ForegroundColor Red "============================================="
}



