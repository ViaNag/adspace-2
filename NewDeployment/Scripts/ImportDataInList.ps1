﻿ # =================================================================================
 #
 # DESC:	Iterates each sheet in workbook and import the data in the SharePoint lists
 # Column1 and Row1 of each sheet denotes the list name
 # Row2 denotes the internal names of the columns
 # Subsequent rows denotes the list items
 # =================================================================================
function ImportData([String]$ConfigFileName = "")
{
	 
	$ConfigXML =  [xml](Get-Content ($ConfigFileName))
	if( $? -eq $false ) 
	{
		LogError "Could not read config file. Exiting ..."
		Stop-Transcript
		Stop-SPAssignment -Global
		Exit 0
	}
    $snapin = Get-PSSnapin | Where-Object {$_.Name -eq 'Microsoft.SharePoint.Powershell'}
    $ServiceContext = "string";
    if ($snapin -eq $null) 
    {
        Write-Host "Loading SharePoint Powershell Snapin"
        Add-PSSnapin "Microsoft.SharePoint.Powershell"
    }
    	Write-Host "Importing data : Process Starting ...." -ForegroundColor Green
        
        #Fetch path of ImportData.xlsx
		[string] $solutionFileName = Get-Location
        $solutionFileName = $solutionFileName + "\ImportData\" + $ConfigXML.ImportExcel.File.Name
        $FileName = Resolve-Path $solutionFileName 

         if(-not $FileName)
         {
            throw "Unable to open Excel File -  $FileName"
             exit
         }
        
        $excel = New-Object -com "Excel.Application"
        $excel.Visible = $false
        $workbook = $excel.workbooks.open($FileName)
        try
        {
           Write-Host "Executing ImportData Scripts : Process Starting ...." -ForegroundColor Green
           foreach ($webApp in $Configuration.Configuration.Farm.WebApplications.Webapplication)
			{
		          $site= Get-SPSite -Identity $webApp.SiteCollection.URL
                  
                  if($site -ne $null)
                  {
                          $sheetCount = $workbook.WorkSheets.Count 
                       for($sheetNo = 1; $sheetNo -le $sheetCount-1;$sheetNo++)
                       {
                         $sheet = $workbook.WorkSheets.Item($sheetNo)
                         $sheetName = $sheet.Name
                         $columns = $sheet.UsedRange.Columns.Count
                         $lines = $sheet.UsedRange.Rows.Count
                         $rowOfData = $lines - 3
                         Write-Warning "Worksheet $sheetName contains $columns columns and $rowOfData lines of data"
                         #Check if the list is at rootsite or subsite and open the web
                         if($sheet.Cells.Item.Invoke(2,1).Value2 -ne $null)
                         {
                         
                          $splitSite = $sheet.Cells.Item.Invoke(2,1).Value2.Split(":");
                          $subsiteURL = $webApp.SiteCollection.URL+$splitSite[1] + "/";
                          $web = Get-SPWeb -Identity $subsiteURL;
                         
               
                         }
                         else
                         {
                          $web =$site.RootWeb
                         }
                         $list = $web.Lists.get_Item($sheet.Cells.Item.Invoke(1,1).Value2) 
                         #Delete list items if exists
                         while($list.Items.Count -gt 0){$list.Items.Delete(0)}
                         
                         for ($line = 4; $line -le $lines; $line ++) {
                             $item = $list.Items.Add();
       
                         for ($column = 1; $column -le $columns; $column++) {
                         $isLookUp = $sheet.Cells.Item.Invoke(3, $column).Value2.Contains(",")
                          if($isLookUp -eq $true) 
                          {
                          if($sheet.Cells.Item.Invoke(3, $column).Value2 -ne $null)
                            {
                                # If columns is type of lookup - Value: ColumnName,LookUpList (ex ProgramName,ProgramsLookUp)
                                $SplitTitle = $sheet.Cells.Item.Invoke(3, $column).Value2.Split(",")
                                $ColumnName = $SplitTitle[0]
                                $LookUpList = $SplitTitle[1]
                                $LookUpListItem = $web.Lists.get_Item($LookUpList).Items | where {$_["Title"] -eq $sheet.Cells.Item.Invoke($line, $column).Value2}
                                if( $LookUpListItem -ne $null)
                                {
                                 $columnValue =  $LookUpListItem["ID"].ToString() + ";#" + $LookUpListItem["Title"].ToString().Trim()
                            
                                }
                            }
                          }
                          else
                          {
                            # If columns is not lookup
                            $ColumnName = $sheet.Cells.Item.Invoke(3, $column).Value2
                            $columnValue = $sheet.Cells.Item.Invoke($line, $column).Value2
                            
                           }
                           #Set value in list item
                           
                           $item[$ColumnName] = $columnValue
                         
                         }
                          $item.Update()  
  
                        }
                           
                      }
                      
                    }
                     $web.Dispose()
                  }
             Write-Host "Success: ImportData." -ForegroundColor Green
           
         }
         catch [System.Exception]
         {

            Write-Host "Exception while Installing ImportData Script. Exiting script..." -ForegroundColor Red
            write-host $_.Exception.Message -ForegroundColor Red
            Stop-SPAssignment -Global

		   Exit -1
         }
        
         $workbook.Close()
         $excel.Quit()
         [System.Runtime.Interopservices.Marshal]::ReleaseComObject($workbook)
         [System.Runtime.Interopservices.Marshal]::ReleaseComObject($excel)
        

   }

   