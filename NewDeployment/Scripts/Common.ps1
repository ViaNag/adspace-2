#Create Custom Permission Level 
function ManagePermissionLevel()
{
$web = Get-SPWeb $Global.Site.Url
 
#Create New Permission Level
foreach($p in $Global.Site.Permissions.Permission)
{
    $ContributeNoDelete =New-Object Microsoft.SharePoint.SPRoleDefinition
    $ContributeNoDelete.Name=$p.Name
    $ContributeNoDelete.Description=$p.Description
    $ContributeNoDelete.BasePermissions=$p.PermissionSet
    $web.RoleDefinitions.Add($ContributeNoDelete);
    write-host $ContributeNoDelete.Name
    write-host "Permission level created successfully"
}
}