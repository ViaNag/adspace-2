﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TopNavigationUC.ascx.cs" Inherits="Viacom.AdSpace.TopNavigation.ControlTemplates.Viacom.AdSpace.TopNavigation.TopNavigationUC" %>
<div class="header-menu">
    <div class="mobile-toggle-btn">
        <span></span><span></span><span></span>
    </div>
    <div class="mega-menu-container">
        <ul class="mega-menu">
            <li id="ulHomeMenu" runat="server"><a class="home-link" href="/">Home</a></li>
            <asp:Literal ID="SubSiteNavLiteral" runat="server"></asp:Literal>
        </ul>
        <ul class="mega-menu pull-right">
            <li class="has-dropdown"><a href="#">Resources <span></span></a>
                <div class="submenu-level1 resources-link"></div>
                <li class="has-dropdown"><a href="#">Links <span></span></a>

                    <div class="link-dropdown">

                        <div id="quicklink" class="quicklink-container">
                        </div>


                    </div>


                </li>
            <li class="has-dropdown"><a href="#">Bookmarks <span></span></a>
                <div class="submenu-level1 bookmark-container">
                </div>
            </li>
            <li><a id="aSupport" href="#">Support</a></li>
        </ul>
    </div>
    <asp:Label ID="errorLBL" runat="server"></asp:Label>
</div>


