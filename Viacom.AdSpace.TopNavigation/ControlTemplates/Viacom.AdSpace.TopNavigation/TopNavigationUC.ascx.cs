﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

using System.Web.Hosting;
using System.Web.UI;
using Viacom.AdSpace.Shared;
using System.Configuration;
using System.Web.Configuration;

namespace Viacom.AdSpace.TopNavigation.ControlTemplates.Viacom.AdSpace.TopNavigation
{
  
    public partial class TopNavigationUC : UserControl
    {
        /****DECLARATIONS******/
        List<string> LinkHeaders =new List<string>();
        List<string> LinkCategories = new List<string>();
        StringBuilder mainStructure=new StringBuilder();
        StringBuilder navigationStructure = new StringBuilder();
        StringBuilder tnavigationStructure = new StringBuilder();
        SPListItemCollection navigationItems = null;
        private const string C_SELECTED_CLASS = "header-selected-menu";
        private bool IsSelected = false;
        private string _fromUrl = string.Empty;


        /// <summary>
        /// Get From Url For search center
        /// </summary>
        private string FromUrl
        {
            get
            {
                if (this._fromUrl == string.Empty && HttpContext.Current.Request.QueryString["ud"] != null)
                {
                    _fromUrl = HttpContext.Current.Request.QueryString["ud"];
                }

                return _fromUrl;
            }
        }

        //*****Page Load************************************
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                PrepareTopNavigationUI();

                if (!this.IsSelected)
                {
                    this.ulHomeMenu.Attributes.Add("class", C_SELECTED_CLASS);
                }
            }
            
        }

        /// <summary>
        /// Gets the app key value pair
        /// </summary>
        /// <param name="key">key</param>
        /// <returns>value</returns>
        private static string GetAppKeyValue(string key)
        {
            string returnValue = string.Empty;

            String _siteName = HostingEnvironment.ApplicationHost.GetSiteName();
            Configuration config = WebConfigurationManager.OpenWebConfiguration("/", _siteName);

            if (config.AppSettings.Settings.AllKeys.Contains(key))
            {
                returnValue = config.AppSettings.Settings[key].Value;
            }

            return returnValue;

        }

        /// <summary>
        /// Prepare top navigation UI
        /// </summary>
        public void PrepareTopNavigationUI()
        {
            try
            {
                string SiteUrl = SPContext.Current.Web.Site.WebApplication.Sites[0].Url;                   //GetAppKeyValue("AdspaceSiteUrl");
                
                using (SPSite site = new SPSite(SiteUrl))  // Site Collection
                {
                    using (SPWeb web = site.RootWeb) // Site
                    {
                        SPList navigationList = web.Lists[Constants.NavigationListName];
                        SPView myNavigationView = navigationList.Views[Constants.MyNavigationListView];
                        
                        navigationItems = navigationList.GetItems(myNavigationView);
                        int cnt = navigationItems.Count;
                        PrepareSubSiteLinkUI();

                        string navStr = tnavigationStructure.ToString();
                        SubSiteNavLiteral.Text = navStr;
                    }
                }
            }
            catch (Exception ex)
            {
                ExceptionHandling.LogToSPDiagnostic(ex);
            }

        }

        /// <summary>
        /// Get Relative url from full url
        /// </summary>
        /// <param name="fullUrl"></param>
        /// <returns></returns>
        private static string GetRelativeUrl(string fullUrl)
        {
            try
            {
                Uri uri = new Uri(fullUrl);//fullUrl is absoluteUrl  
                string relativeUrl = uri.AbsolutePath;//The Uri property AbsolutePath gives the relativeUrl  

                return relativeUrl;
            }
            catch(Exception ex)
            {
                ExceptionHandling.LogToSPDiagnostic(ex);
                return fullUrl;
            }  
        }  

        /// <summary>
        /// Prepares Sub site links UI
        /// </summary>
        public void PrepareSubSiteLinkUI()
        {
            
            var headerItems = (from SPListItem navigationItem in navigationItems
                                           where navigationItem[Constants.ListColumns.Navigation.ContentType].ToString() == Constants.ContentType.Navigation.NavigationHeader
                                           select navigationItem).ToList();

            if (headerItems.Count > 0)
            {
                foreach (var headerItem in headerItems)
                {
                    string headerUrl = string.Empty;
                    string headerTitle = string.Empty;

                    //Check if Sub site title is present
                    if (headerItem[Constants.ListColumns.Navigation.HeaderTitle] != null)
                    {
                        headerTitle = headerItem[Constants.ListColumns.Navigation.HeaderTitle].ToString();

                        if (headerTitle != string.Empty)
                        {
                            if (headerItem[Constants.ListColumns.Navigation.HeaderUrl] != null)
                            {
                                SPFieldUrlValue headerUrlValue = new SPFieldUrlValue(headerItem[Constants.ListColumns.Navigation.HeaderUrl].ToString());
                                headerUrl = headerUrlValue.Url;
                            }

                            //Create Sub Site Link Strcuture
                            // check if the tab is selected or not
                            if ( HttpContext.Current.Request.RawUrl.Trim().ToLower().Contains(GetRelativeUrl( headerUrl.Trim()).ToLower()) 
                                || this.FromUrl.Trim().ToLower().Contains(GetRelativeUrl( headerUrl.Trim()).ToLower() ))
                            {
                                this.IsSelected = true;
                                tnavigationStructure.Append(string.Format("<li class='has-dropdown {2}'><a href='{0}'>{1}<span></span></a>", headerUrl, headerTitle,C_SELECTED_CLASS));
                            }
                            else
                            {
                                tnavigationStructure.Append(string.Format("<li class='has-dropdown'><a href='{0}'>{1}<span></span></a>", headerUrl, headerTitle));
                            }
                            
                            

                            //Create Library Link Structure
                            PrepareLibraryLinkUI(headerTitle);

                            tnavigationStructure.Append("</li>");
                        }
                    }
                    
                }
            }
        }

        /// <summary>
        /// Prepares Library links UI inside each sub site link
        /// </summary>
        /// <param name="headerTitle">Sub Site Link Title</param>
        public void PrepareLibraryLinkUI(string headerTitle)
        {
            var categoryItems = (from SPListItem navigationItem in navigationItems
                                 where navigationItem[Constants.ListColumns.Navigation.ContentType].ToString() == Constants.ContentType.Navigation.NavigationCategory && navigationItem[Constants.ListColumns.Navigation.LinkHeader] != null && ((new SPFieldLookupValue(navigationItem[Constants.ListColumns.Navigation.LinkHeader].ToString())).LookupValue == headerTitle)
                                                     select navigationItem).ToList();

            if (categoryItems.Count > 0)
            {
                tnavigationStructure.Append("<div class='submenu-level1'><ul>");

                foreach (var categoryItem in categoryItems)
                {
                    string categoryTitle = string.Empty;
                    string categoryUrl = string.Empty;

                    if (categoryItem[Constants.ListColumns.Navigation.CategoryTitle] != null)
                    {
                        categoryTitle = categoryItem[Constants.ListColumns.Navigation.CategoryTitle].ToString();

                        if (categoryItem[Constants.ListColumns.Navigation.LinkCategoryUrl] != null)
                        {
                            SPFieldUrlValue categoryrUrlValue = new SPFieldUrlValue(categoryItem[Constants.ListColumns.Navigation.LinkCategoryUrl].ToString());
                            categoryUrl = categoryrUrlValue.Url;
                        }

                        //Create Folders links inside Library links
                        PrepareFolderLinkUI(headerTitle, categoryTitle, categoryUrl);
                        
                    }
                }

                tnavigationStructure.Append("</ul></div>");
            }
        }

        /// <summary>
        /// Prepares top level folder links UI inside each library link for each sub site
        /// </summary>
        /// <param name="headerTitle">Sub Site Link Title</param>
        /// <param name="categoryTitle">Library link Title</param>
        public void PrepareFolderLinkUI(string headerTitle, string categoryTitle, string categoryUrl)
        {
            var linkItems = (from SPListItem navigationItem in navigationItems
                             where navigationItem[Constants.ListColumns.Navigation.ContentType].ToString() == Constants.ContentType.Navigation.NavigationLink
                             && navigationItem[Constants.ListColumns.Navigation.LinkHeader] != null && ((new SPFieldLookupValue(navigationItem[Constants.ListColumns.Navigation.LinkHeader].ToString())).LookupValue == headerTitle)
                             && navigationItem[Constants.ListColumns.Navigation.LinkCategory] != null && ((new SPFieldLookupValue(navigationItem[Constants.ListColumns.Navigation.LinkCategory].ToString())).LookupValue == categoryTitle)
                             select navigationItem).ToList();

            int noOfColumns = (linkItems.Count / 15) + 1;
            
            if (linkItems.Count > 0)
            {
                tnavigationStructure.Append(string.Format("<li><a href='{0}'>{1}</a>", categoryUrl, categoryTitle));

                tnavigationStructure.Append("<div class='submenu-level2'><ul class='split-list'>");
                int folderCounter = 1;
                foreach (var linkItem in linkItems)
                {
                    string linkTitle = string.Empty;
                    string linkUrl = string.Empty;
                    
                    if (linkItem[Constants.ListColumns.Navigation.LinkName] != null)
                    {
                        linkTitle = linkItem[Constants.ListColumns.Navigation.LinkName].ToString();

                        if (linkItem[Constants.ListColumns.Navigation.LinkUrl] != null)
                        {
                            SPFieldUrlValue linkUrlValue = new SPFieldUrlValue(linkItem[Constants.ListColumns.Navigation.LinkUrl].ToString());
                            linkUrl = linkUrlValue.Url;
                        }

                        /*if ((folderCounter - 1) % 15 == 0 && folderCounter != 1)
                        {
                            tnavigationStructure.Append("</ul><ul>");
                        }*/

                        //Create Sub Site Link Strcuture
                        tnavigationStructure.Append(string.Format("<li><a href='{0}'>{1}</a></li>",linkUrl,linkTitle));
                        
                        folderCounter++;
                    }
                }

                tnavigationStructure.Append("</ul></div>");
                tnavigationStructure.Append("</li>");
            }
            else
            {
                tnavigationStructure.Append(string.Format("<li class='has-no-dropdown'><a href='{0}'>{1}</a></li>", categoryUrl, categoryTitle));

            }
        }

    }
}
