﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ Page Language="C#" %>
<%@ Register tagprefix="SharePoint" namespace="Microsoft.SharePoint.WebControls" assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<html dir="ltr" xmlns="http://www.w3.org/1999/xhtml" xmlns:mso="urn:schemas-microsoft-com:office:office" xmlns:msdt="uuid:C2F41010-65B3-11d1-A29F-00AA00C14882">

<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled 1</title>
<meta http-equiv="X-UA-Compatible" content="IE=10" />
<SharePoint:CssRegistration Name="default" runat="server"/>
<style type="text/css">
.message
{font-family:Verdana;
 font-size:10pt;
 color:#198FAF;
 font-weight:bold;
}
#btnBack
{
    background-color:gray ;
    color:white;
    font-family:Verdana;
    font-size:11pt;
    font-weight:bold;
}
    </style>

<!--[if gte mso 9]><SharePoint:CTFieldRefs runat=server Prefix="mso:" FieldList="FileLeafRef,Comments,PublishingStartDate,PublishingExpirationDate,PublishingContactEmail,PublishingContactName,PublishingContactPicture,PublishingPageLayout,PublishingVariationGroupID,PublishingVariationRelationshipLinkFieldID,PublishingRollupImage,Audience,PublishingIsFurlPage,SeoBrowserTitle,SeoMetaDescription,SeoKeywords,RobotsNoIndex,gd55b52111464ed79bbc2d9a708ef2ba,TaxCatchAllLabel,d72115c958e9422f9e0de162abcd01fc,Content_x0020_Publishing_x0020_Frequency,Content_x0020_Purpose,CategoryDescription,k32a8eea669d4858be192e7d518c02e4,SummaryText,ThumbnailImage,TopContentExcluded,f1d9cd25d01c4ab2899fdc6176501a6d,Expiry_x0020_date,BaseLocation,ArchiveNow,DocumentDescription,Shareable,ExternalSharingExpiryDate,_dlc_Exempt,_dlc_ExpireDateSaved,_dlc_ExpireDate"><xml>
<mso:CustomDocumentProperties>
<mso:_dlc_ExpireDate msdt:dt="string">2015-05-14T00:00:00Z</mso:_dlc_ExpireDate>
<mso:ItemRetentionFormula msdt:dt="string">&lt;formula id=&quot;Microsoft.Office.RecordsManagement.PolicyFeatures.Expiration.Formula.BuiltIn&quot;&gt;&lt;number&gt;1&lt;/number&gt;&lt;property&gt;Expiry_x0020_date&lt;/property&gt;&lt;propertyId&gt;5b3357a0-af84-4655-8f1e-33340e5a329a&lt;/propertyId&gt;&lt;period&gt;days&lt;/period&gt;&lt;/formula&gt;</mso:ItemRetentionFormula>
<mso:_dlc_policyId msdt:dt="string">0x0101|1866761191</mso:_dlc_policyId>
</mso:CustomDocumentProperties>
</xml></SharePoint:CTFieldRefs><![endif]-->
</head>

<body>
	<h1 class="message">Bookmark Added Successfully !!!</h1>

<form id="form1" runat="server">
</form>

</body>

</html>
