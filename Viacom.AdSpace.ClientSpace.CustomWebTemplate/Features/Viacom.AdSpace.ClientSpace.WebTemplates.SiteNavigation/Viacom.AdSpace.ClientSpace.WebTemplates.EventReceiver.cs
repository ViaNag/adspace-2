
using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using System.Xml;
using System.IO;
using System.Linq;
using Microsoft.SharePoint.Navigation;
using Microsoft.SharePoint.Utilities;
using System.Collections.Generic;
using Microsoft.SharePoint.Publishing;
using Microsoft.SharePoint.Publishing.Navigation;
using Microsoft.SharePoint.Administration;
using Viacom.AdSpace.ClientSpace.CustomWebTemplate;


namespace Viacom.AdSpace.ClientSpace.WebTemplates.SiteNavigation
{
    [Guid("f2f5aeeb-eece-4657-9c2f-987489f125ca")]
    public class ViacomAdSpaceWebTemplatesEventReceiver : SPFeatureReceiver
    {
        //List of links for which left navigation will be created with target audience.
        List<string[]> qckLaunch = new List<string[]>();

        /// <summary>
        /// This funtion is starting Point of this class to set navigation
        /// </summary>
        /// <param name="properties"></param>
        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            SPWeb web = null;
            try
            {
                web = (SPWeb)properties.Feature.Parent;
                string webAppUrl = web.Site.WebApplication.GetResponseUri(SPUrlZone.Default).AbsoluteUri;
                
                if (PublishingWeb.IsPublishingWeb(web))
                {
                    PublishingWeb publishingWeb = PublishingWeb.GetPublishingWeb(web);
                    WebNavigationSettings webNavigationSettings = new WebNavigationSettings(web);

                    if (webNavigationSettings != null)
                    {
                        
                        publishingWeb.Navigation.CurrentIncludePages = false;
                        publishingWeb.Navigation.CurrentIncludeSubSites = false;
                       
                        webNavigationSettings.CurrentNavigation.Source = StandardNavigationSource.PortalProvider;
                        webNavigationSettings.Update();
                        publishingWeb.Update();
                        web.Update();
                        
                    }
                }
                
                CleanUpQuickLaunch(web);
                CreateNavigation(web);
                web.Update();
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space SiteNavigation-FeatureActivated", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            }
        }

        

         /// <summary>
         /// Function to clean quick launch before creating new left navigation
         /// </summary>
         /// <param name="web">SPWeb object of current site</param>
        void CleanUpQuickLaunch(SPWeb web)
        {
            try
            {
                bool allowUnsafeupdates = web.AllowUnsafeUpdates;
                web.AllowUnsafeUpdates = true;
                SPNavigationNodeCollection quickLaunch = web.Navigation.QuickLaunch;
                for (int i = quickLaunch.Count - 1; i >= 0; i--)
                {
                    quickLaunch.Delete(quickLaunch[i]);
                }
                web.AllowUnsafeUpdates = allowUnsafeupdates;
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space SiteNavigation-CleanUpQuickLaunch", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            }
        }

        /// <summary>
        /// Function to prepare qckLaunch List to create left navigation
        /// </summary>
        /// <param name="webTitle">Web object of current site</param>
       
        void quickLaunchItem(SPWeb web)
        {
            
            try
            {
                qckLaunch.Clear();
                qckLaunch.Add(new string[] { "Heading", Constant.CLIENT_SUBSITE_HOME, "#", string.Empty });
                qckLaunch.Add(new string[] { "Heading", Constant.CLIENT_SUBSITE_CLIENTPRESENTATION, web.Lists[Constant.LIBRARY_TITLE_CLIENTPRESENTATION].DefaultViewUrl, string.Empty });
                qckLaunch.Add(new string[] { "Heading", Constant.CLIENT_SUBSITE_CLIENTPLANNING, web.Lists[Constant.LIBRARY_TITLE_CLIENTPLANNING].DefaultViewUrl, string.Empty });
                qckLaunch.Add(new string[] { "Heading", Constant.CLIENT_SUBSITE_CUSTOM_RESEARCH, web.Lists[Constant.LIBRARY_TITLE_CUSTOM_RESEARCH].DefaultViewUrl, string.Empty });
                qckLaunch.Add(new string[] { "Heading", Constant.CLIENT_SUBSITE_MARKETING_CASESTUDIES, web.Lists[Constant.LIBRARY_TITLE_MARKETING_CASESTUDIES].DefaultViewUrl, string.Empty });
                
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space SiteNavigation-quickLaunchItem", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            }

        }

       

        /// <summary>
        /// Function to create left navigation
        /// </summary>
        /// <param name="web">SPWeb object of current site</param>
        void CreateNavigation(SPWeb web)
        {
            try
            {
                quickLaunchItem(web);
                List<string[]> headerLst = qckLaunch.Where(p => p[0] == "Heading").ToList();
                int i = 0;
                foreach (string[] headerArr in headerLst)
                {
                    if (i == 0)
                        AddQuickLaunchItem(web, headerArr[1], headerArr[2], headerArr[3], null);
                    else
                        AddQuickLaunchItem(web, headerArr[1], headerArr[2], headerArr[3], headerLst[i - 1][1]);
                    i++;
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("AdSpace SiteNavigation-CreateNavigation", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            }
        }

       

        /// <summary>
        /// One of overloaded method to add header in left navigation
        /// </summary>
        /// <param name="oweb">SPWeb object of current site</param>
        /// <param name="header">Name of header which will be added</param>
        /// <param name="headerURL">Url of header</param>
        /// <param name="targetAudience">target audience of header</param>
        /// <param name="previousNode">Previous node after which new header will be added</param>
        void AddQuickLaunchItem(SPWeb oweb, string header, string headerURL,string targetAudience, string previousNode)
        {
            try
            {
                SPNavigationNodeCollection quickLaunch = oweb.Navigation.QuickLaunch;
                SPNavigationNode nodeHeader = quickLaunch.Cast<SPNavigationNode>().Where(n => n.Title == header).FirstOrDefault();
                SPNavigationNode PreviousNodeHeader = null;
                if (!string.IsNullOrEmpty(previousNode))
                    PreviousNodeHeader = quickLaunch.Cast<SPNavigationNode>().Where(n => n.Title == previousNode).FirstOrDefault();
                if (nodeHeader == null && (!string.IsNullOrEmpty(previousNode)))
                {
                    nodeHeader = quickLaunch.Add(new SPNavigationNode(header, headerURL, true), PreviousNodeHeader);
                    if (!string.IsNullOrEmpty(targetAudience))
                    {
                        if (nodeHeader.Properties.Contains("Audience"))
                        {
                            nodeHeader.Properties["Audience"] = targetAudience;
                        }
                        else
                        {
                            nodeHeader.Properties.Add("Audience", targetAudience);
                        }
                    }
                }
                else
                {
                    nodeHeader = quickLaunch.AddAsFirst(new SPNavigationNode(header, headerURL, true));
                    if (!string.IsNullOrEmpty(targetAudience))
                    {
                        if (nodeHeader.Properties.Contains("Audience"))
                        {
                            nodeHeader.Properties["Audience"] = targetAudience;
                        }
                        else
                        {
                            nodeHeader.Properties.Add("Audience", targetAudience);
                        }
                    }
                }
                nodeHeader.Update();
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space3 SiteNavigation-AddQuickLaunchItem", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            }
        }

        /// <summary>
        /// Function which will call on feature deactivated event
        /// </summary>
        /// <param name="properties"></param>
        public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        {
            
        }
    }
}
