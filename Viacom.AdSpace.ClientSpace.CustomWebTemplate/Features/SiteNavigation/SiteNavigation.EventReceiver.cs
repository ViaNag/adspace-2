using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using System.Xml;
using System.IO;
using System.Linq;
using Microsoft.SharePoint.Navigation;
using Microsoft.SharePoint.Utilities;
using System.Collections.Generic;
using Microsoft.SharePoint.Publishing;
using Microsoft.SharePoint.Publishing.Navigation;
using Microsoft.SharePoint.Administration;
using Viacom.AdSpace.ClientSpace.CustomWebTemplate;
using System.Web.Configuration;
using System.Configuration;

namespace Viacom.AdSpace.ClientSpace.CustomWebTemplate.Features.SiteNavigation
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("f69fef86-8a8d-44c1-a95e-cda97bdd15b5")]
    public class SiteNavigationEventReceiver : SPFeatureReceiver
    {
        //List of links for which left navigation will be created with target audience.
        List<string[]> qckLaunch = new List<string[]>();
        Configuration config = null;
        public static string siteCollURL = string.Empty;
        public static string siteURL = string.Empty;
        public static string subSiteIRL = string.Empty;
        public static string sharedContentURL = string.Empty;

        public const string CONST_KEY_SHARE_CONTENT = "MySharedContentTargetAudience";
        public const string CONST_KEY_EXT_ADMIN_CONSOLE = "ExtAdminConsoleTargetAudience";

        /// <summary>
        /// This funtion is starting Point of this class to set navigation
        /// </summary>
        /// <param name="properties"></param>
        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            SPWeb web = null;
            try
            {
                web = (SPWeb)properties.Feature.Parent;
                siteURL = web.Url.ToString();
                SPWebApplication webApp = web.Site.WebApplication;
                if (webApp != null) config = WebConfigurationManager.OpenWebConfiguration("/", webApp.Name);
                if (config != null)
                {
                    AppSettingsSection appSettings = config.AppSettings;
                    SetConfigurationSettings(appSettings.CurrentConfiguration.AppSettings.Settings); 
                }
                subSiteIRL = siteCollURL + "/Pages/ExternalSharing.aspx?SiteURL=" + siteURL;
                sharedContentURL = siteCollURL + "/Pages/SharedContent.aspx";
                string webAppUrl = web.Site.WebApplication.GetResponseUri(SPUrlZone.Default).AbsoluteUri;

                if (PublishingWeb.IsPublishingWeb(web))
                {
                    PublishingWeb publishingWeb = PublishingWeb.GetPublishingWeb(web);
                    WebNavigationSettings webNavigationSettings = new WebNavigationSettings(web);

                    if (webNavigationSettings != null)
                    {

                        publishingWeb.Navigation.CurrentIncludePages = false;
                        publishingWeb.Navigation.CurrentIncludeSubSites = false;

                        webNavigationSettings.CurrentNavigation.Source = StandardNavigationSource.PortalProvider;
                        webNavigationSettings.Update();
                        publishingWeb.Update();
                        web.Update();

                    }
                }

                CleanUpQuickLaunch(web);
                CreateNavigation(web);
                web.Update();
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space SiteNavigation-FeatureActivated", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            }
        }

        /// <summary>
        /// /Set sttings vaue from app settings
        /// </summary>
        /// <param name="keyValueConfigurationCollection"></param>
        private void SetConfigurationSettings(KeyValueConfigurationCollection keyValueConfigurationCollection)
        {
            if (keyValueConfigurationCollection != null)
            {
                siteCollURL = keyValueConfigurationCollection["SiteUrl"].Value;
            }
        }



        /// <summary>
        /// Function to clean quick launch before creating new left navigation
        /// </summary>
        /// <param name="web">SPWeb object of current site</param>
        void CleanUpQuickLaunch(SPWeb web)
        {
            try
            {
                bool allowUnsafeupdates = web.AllowUnsafeUpdates;
                web.AllowUnsafeUpdates = true;
                SPNavigationNodeCollection quickLaunch = web.Navigation.QuickLaunch;
                for (int i = quickLaunch.Count - 1; i >= 0; i--)
                {
                    quickLaunch.Delete(quickLaunch[i]);
                }
                web.AllowUnsafeUpdates = allowUnsafeupdates;
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space SiteNavigation-CleanUpQuickLaunch", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            }
        }

        /// <summary>
        /// Function to prepare qckLaunch List to create left navigation
        /// </summary>
        /// <param name="webTitle">Web object of current site</param>

        void quickLaunchItem(SPWeb web)
        {
            string siteName = web.Title + " " + Constant.CLIENT_SUBSITE_HOME;
            string sharedConetentTargetAudience = GetValueFromKey(web,CONST_KEY_SHARE_CONTENT);// ";;;;AdSpace Super Admin,AdSpace ClientSpace Contributors";
            string adminConsoleTargetAudience = GetValueFromKey(web, CONST_KEY_EXT_ADMIN_CONSOLE);
            try
            {
                qckLaunch.Clear();
                qckLaunch.Add(new string[] { "Heading", siteName, "#", string.Empty });
                qckLaunch.Add(new string[] { "Heading", Constant.CLIENT_SUBSITE_CLIENTPRESENTATION, web.Lists[Constant.LIBRARY_TITLE_CLIENTPRESENTATION].DefaultViewUrl, string.Empty });
                qckLaunch.Add(new string[] { "Heading", Constant.CLIENT_SUBSITE_CLIENTPLANNING, web.Lists[Constant.LIBRARY_TITLE_CLIENTPLANNING].DefaultViewUrl, string.Empty });
                qckLaunch.Add(new string[] { "Heading", Constant.CLIENT_SUBSITE_CUSTOM_RESEARCH, web.Lists[Constant.LIBRARY_TITLE_CUSTOM_RESEARCH].DefaultViewUrl, string.Empty });
                qckLaunch.Add(new string[] { "Heading", Constant.CLIENT_SUBSITE_MARKETING_CASESTUDIES, web.Lists[Constant.LIBRARY_TITLE_MARKETING_CASESTUDIES].DefaultViewUrl, string.Empty });
                qckLaunch.Add(new string[] { "Heading", Constant.CLIENT_SUBSITE_EXT_ADMIN_VIEW, subSiteIRL, adminConsoleTargetAudience });
                qckLaunch.Add(new string[] { "Heading", Constant.CLIENT_SUBSITE_SHARED_CONTENT, sharedContentURL, sharedConetentTargetAudience });

            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space SiteNavigation-quickLaunchItem", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            }

        }



        /// <summary>
        /// Function to create left navigation
        /// </summary>
        /// <param name="web">SPWeb object of current site</param>
        void CreateNavigation(SPWeb web)
        {
            try
            {
                quickLaunchItem(web);
                List<string[]> headerLst = qckLaunch.Where(p => p[0] == "Heading").ToList();
                int i = 0;
                foreach (string[] headerArr in headerLst)
                {
                    if (i == 0)
                        AddQuickLaunchItem(web, headerArr[1], headerArr[2], headerArr[3], null);
                    else
                        AddQuickLaunchItem(web, headerArr[1], headerArr[2], headerArr[3], headerLst[i - 1][1]);
                    i++;
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("AdSpace SiteNavigation-CreateNavigation", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            }
        }



        /// <summary>
        /// One of overloaded method to add header in left navigation
        /// </summary>
        /// <param name="oweb">SPWeb object of current site</param>
        /// <param name="header">Name of header which will be added</param>
        /// <param name="headerURL">Url of header</param>
        /// <param name="targetAudience">target audience of header</param>
        /// <param name="previousNode">Previous node after which new header will be added</param>
        void AddQuickLaunchItem(SPWeb oweb, string header, string headerURL, string targetAudience, string previousNode)
        {
            try
            {
                SPNavigationNodeCollection quickLaunch = oweb.Navigation.QuickLaunch;
                SPNavigationNode nodeHeader = quickLaunch.Cast<SPNavigationNode>().Where(n => n.Title == header).FirstOrDefault();
                SPNavigationNode PreviousNodeHeader = null;
                if (!string.IsNullOrEmpty(previousNode))
                    PreviousNodeHeader = quickLaunch.Cast<SPNavigationNode>().Where(n => n.Title == previousNode).FirstOrDefault();
                if (nodeHeader == null && (!string.IsNullOrEmpty(previousNode)))
                {
                    nodeHeader = quickLaunch.Add(new SPNavigationNode(header, headerURL, true), PreviousNodeHeader);
                    if (!string.IsNullOrEmpty(targetAudience))
                    {
                        if (nodeHeader.Properties.Contains("Audience"))
                        {
                            nodeHeader.Properties["Audience"] = targetAudience;
                        }
                        else
                        {
                            nodeHeader.Properties.Add("Audience", targetAudience);
                        }
                    }
                }
                else
                {
                    nodeHeader = quickLaunch.AddAsFirst(new SPNavigationNode(header, headerURL, true));
                    if (!string.IsNullOrEmpty(targetAudience))
                    {
                        if (nodeHeader.Properties.Contains("Audience"))
                        {
                            nodeHeader.Properties["Audience"] = targetAudience;
                        }
                        else
                        {
                            nodeHeader.Properties.Add("Audience", targetAudience);
                        }
                    }
                }
                nodeHeader.Update();
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Ad Space3 SiteNavigation-AddQuickLaunchItem", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            }
        }

        /// <summary>
        /// Function which will call on feature deactivated event
        /// </summary>
        /// <param name="properties"></param>
        public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        {

        }

        /// <summary>
        /// This method gets the value based on key from Config list
        /// </summary>
        /// <param name="currentWeb"></param>
        /// <param name="listKey"></param>
        /// <returns></returns>
        private static string GetValueFromKey(SPWeb currentWeb, string Key)
        {
            string value = string.Empty;
            SPWeb rootWeb = currentWeb.Site.RootWeb;

            // Get config list
            SPList configList = rootWeb.Lists["FieldsConfigList"];
            SPQuery configQuery = new SPQuery();
            configQuery.Query = "<Where><Eq><FieldRef Name='Key'/><Value Type='Text'>" + Key + "</Value></Eq></Where>";
            SPListItemCollection configColl = configList.GetItems(configQuery);
            if (configColl != null && configColl.Count > 0)
            {
                // Get value based on Key
                value = Convert.ToString(configColl[0]["Value1"]);
            }
            else
            {
                throw new System.ArgumentException("The record with key : " + Key + "not found.", "Config Key");
            }

            return value;
        }
    }
}