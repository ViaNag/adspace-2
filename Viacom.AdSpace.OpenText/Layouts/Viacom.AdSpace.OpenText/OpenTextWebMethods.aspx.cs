﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System;
using System.Text;
using System.Web.Services;
using System.Linq;

namespace Viacom.AdSpace.OpenText.Layouts.Viacom.AdSpace.OpenText
{
    public partial class OpenTextWebMethods : LayoutsPageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        [WebMethod]
        public static string UpdateAssetType(string siteURL, string documentListID, string FileName, string AssetId, string AssetType, string siteCollectionURL)
        {
            string response = null;
            StringBuilder sb = new StringBuilder();
            try
            {
                sb.Append("siteURL=" + siteURL + Constants.CONSTANT_SEPARATOR);
                sb.Append("documentListID=" + documentListID + Constants.CONSTANT_SEPARATOR);
                sb.Append("FileName=" + FileName + Constants.CONSTANT_SEPARATOR);
                sb.Append("AssetId=" + AssetId + Constants.CONSTANT_SEPARATOR);
                sb.Append("AssetType=" + AssetType + Constants.CONSTANT_SEPARATOR);
                sb.Append("siteCollectionURL=" + siteCollectionURL + Constants.CONSTANT_SEPARATOR);
                using (SPSite site = new SPSite(siteURL))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        string loginName = web.CurrentUser.Email;
                        bool webAllowUnsafeUpdates = web.AllowUnsafeUpdates;
                        web.AllowUnsafeUpdates = true;
                        Utility.Log("MediaSiloUpdateAssetType Parameters", sb.ToString(), "");

                        Guid guid = new Guid(documentListID);
                        SPList library = web.Lists[guid];
                        var file = library.RootFolder.Files.Cast<SPFile>().Where(x => x.Item["Name"].Equals(FileName)).Select(x => x).FirstOrDefault();
                        if (file == null)
                        {
                            response = loginName + "No File Found with the asset id";
                        }
                        else
                        {
                            SPListItem item = file.GetListItem();
                            if (item.File.Level == SPFileLevel.Checkout)
                            {
                                item.File.UndoCheckOut();
                            }
                            if (item.File.Level != SPFileLevel.Checkout)
                            {
                                item.File.CheckOut();
                            }
                            item[Constants.ASSET_TYPE] = AssetType;
                            item.SystemUpdate(false);
                            item.File.CheckIn("SystemCheckedin");
                            response = loginName + " | Asset Type update was successful.";

                        }
                        web.AllowUnsafeUpdates = webAllowUnsafeUpdates;
                        site.Dispose();
                        web.Dispose();
                        Utility.Log("MediaSiloUpdateAssetType Parameters", sb.ToString(), "");
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.Log("MediaSilouplaod.cs-UpdateAssetType", ex.Message, ex.StackTrace);
                Utility.Log("MediaSilouplaod Parameters", sb.ToString(), "");
            }
            return response;
        }
    }
}
