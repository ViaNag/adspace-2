﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;

namespace Viacom.AdSpace.OpenText
{
    class Utility
    {
        public static void Log(string exceptionLocation, string Message, string StackTrace)
        {
            //log to ULS
            SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory(exceptionLocation, TraceSeverity.High, EventSeverity.ErrorCritical), TraceSeverity.Unexpected, Message, StackTrace);
        }
    }
}
