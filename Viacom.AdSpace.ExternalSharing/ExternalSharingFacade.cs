﻿using Microsoft.SharePoint.Administration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.ExternalSharing.Entities;
using Viacom.AdSpace.ExternalSharing.Operations;


namespace Viacom.AdSpace.ExternalSharing
{
    /// <summary>
    /// This class contains the facade for all the external sharing related operations
    /// </summary>
    public class ExternalSharingFacade
    {
        /// <summary>
        /// This is the facade for performing the delete expired items operation
        /// </summary>
        /// 
        public void DeleteExpiredContent(IOperationParam srcParam)
        {
            // Call the delete operation
            new DeleteExpiredContentOperation().Execute(srcParam);
        }

        public void ShareContent(IOperationParam srcParam)
        {
            // Call the shared operation
            new ShareContentOperation().Execute(srcParam);
        }

        public void ShareConfigurationRequests(IOperationParam srcParam)
        {
            // Call the shared operation
            new ShareConfigurationRequestsOperation().Execute(srcParam);
        }

        public void BulkArchival(IOperationParam srcParam)
        {
            // Call the BulkArchival operation
            new BulkArchivalOperation().Execute(srcParam);
        }
    }
}
