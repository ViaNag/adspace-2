﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.ExternalSharing.Entities;
using Viacom.AdSpace.ExternalSharing.Entities.Entities;
using Microsoft.Office.DocumentManagement;
using Microsoft.SharePoint.Utilities;
using Viacom.AdSpace.ExternalSharing.Actions;

namespace Viacom.AdSpace.ExternalSharing.Operations
{
    class ShareConfigurationRequestsOperation : AbstractExternalSharingOperations
    {
        Dictionary<string, FolderSettings> folderSettingDict = new Dictionary<string, FolderSettings>();
        // Initialize the Action Outcome object
        ActionOutcome operationOutcome = new ActionOutcome();
        string siteUrl = "";
        List<ExtShareConfigRequest> extShareConfigRequestObject = new List<ExtShareConfigRequest>();
        public override IOutcome Execute(IOperationParam param)
        {
            siteUrl = param.Siteurl;
            param.Query = string.Format(Constants.ExtShareConfigListQuery, Constants.JobStatus, Constants.ChoiceType, Constants.NewJobStatus, Constants.InProgressJobStatus, Constants.CreatedField);
            param.ListTitle = Constants.ExtShareConfigurationListName;
            ExternalSharingItemCollection esItemCollection = this.GetItemCollectionOnStatus(param) as ExternalSharingItemCollection;
           // Check if collection in the read outcome is not null and item count is great than 0
            if (esItemCollection != null && esItemCollection.IsSuccess && esItemCollection.ItemCollection.Count > 0)
            {
                LoadListItemIntoCollection(esItemCollection.ItemCollection);
            }
            return null;
        }

        private void LoadListItemIntoCollection(SPListItemCollection configListItems)
        {
            try
            {
                foreach (SPListItem configListItem in configListItems)
                {
                    ExtShareConfigRequest extShareConfigItem = new ExtShareConfigRequest();
                    extShareConfigItem.SharedItemType = configListItem[Constants.SharedItemType].ToString();
                    extShareConfigItem.SharingEnabled = Convert.ToBoolean(configListItem[Constants.SharingEnabled]);
                    extShareConfigItem.ListTitle = configListItem[Constants.LibraryName].ToString();
                    extShareConfigItem.SharingDuration = Convert.ToInt32(configListItem[Constants.SharingDuration]);
                    extShareConfigItem.Siteurl = configListItem[Constants.SiteURL].ToString();
                    extShareConfigItem.ItemURL = configListItem[Constants.ItemURL].ToString();
                    extShareConfigItem.JobMessage = (configListItem[Constants.JobMessage] == null) ? string.Empty : configListItem[Constants.JobMessage].ToString();
                    extShareConfigItem.JobStatus = configListItem[Constants.JobStatus].ToString();
                    extShareConfigItem.Created = Convert.ToDateTime(configListItem[Constants.CreatedField]);
                    extShareConfigItem.ID = Convert.ToInt32(configListItem[Constants.IDField]);
                    extShareConfigRequestObject.Add(extShareConfigItem);
                }
                IExternalSharingAction markFolderAction = new MarkFolders();  
                ActionOutcome consolidatedOutcome = new ActionOutcome();
                consolidatedOutcome.folderSettingDict = new Dictionary<string, IOperationParam>();
                IOperationParam dictValue = new FolderSettings();
                foreach (ExtShareConfigRequest extShareConfigItem in extShareConfigRequestObject)
                {
                    FolderSettings folderSetting = new FolderSettings();
                    folderSetting.FolderURL = extShareConfigItem.ItemURL;
                    folderSetting.LibraryName = extShareConfigItem.ListTitle;
                    folderSetting.SiteURL = extShareConfigItem.Siteurl;
                    folderSetting.SettingDateTime = extShareConfigItem.Created;
                    folderSetting.ParentRequest = extShareConfigItem;
                    ActionOutcome outcome = markFolderAction.PerformAction(folderSetting) as ActionOutcome;                  
                    if (outcome != null && outcome.IsSuccess)
                    {
                        foreach (KeyValuePair<string, IOperationParam> folders in outcome.folderSettingDict)
                        {
                            if (consolidatedOutcome.folderSettingDict.TryGetValue(folders.Key, out dictValue))
                            {
                                FolderSettings consolidatedOutcomeValue = dictValue as FolderSettings;
                                FolderSettings outcomeValue = folders.Value as FolderSettings;
                                int result = DateTime.Compare(outcomeValue.SettingDateTime, consolidatedOutcomeValue.SettingDateTime);
                                if (result > 0)
                                {
                                    consolidatedOutcome.folderSettingDict.Remove(folders.Key);
                                    consolidatedOutcome.folderSettingDict.Add(folders.Key, folders.Value);
                                }

                            }
                            else
                            {
                                //key not found in dictionary, add folderSetting to dictionary with key as folderurl
                                consolidatedOutcome.folderSettingDict.Add(folders.Key, folders.Value);
                            }
                        }
                    }
                }
                if (consolidatedOutcome != null && consolidatedOutcome.folderSettingDict.Count>0)
                {
                    GetFoldersFromDictionary(consolidatedOutcome);
                }
            }
            catch (Exception ex)
            {
                Logging.LogException(Constants.LoadListItemIntoCollectionJobLocation, ex);
            }

        }

       
        private void GetFoldersFromDictionary(ActionOutcome outcome)
        {
            try
            {
                ReadItemCollectionParams param = new ReadItemCollectionParams();
                param.Query = String.Format(Constants.Query, Constants.ConfigKeyField, Constants.TextTypeField, Constants.DocumentFetchingLimit);
                param.Siteurl = siteUrl;
                param.ListTitle = Constants.ConfigurationList;
                ExternalSharingItemCollection configListItems = this.GetItemCollectionOnStatus(param) as ExternalSharingItemCollection;
                // Check if collection in the read outcome is not null and item count is great than 0
                if (configListItems != null && configListItems.IsSuccess && configListItems.ItemCollection.Count > 0)
                {
                    SPListItem item = configListItems.ItemCollection[0];
                    uint DocumentFetchingLimit = item[Constants.Value1] != null ? Convert.ToUInt32(item[Constants.Value1]) : 0;
                    if (DocumentFetchingLimit > 0)
                    {
                        foreach (KeyValuePair<string, IOperationParam> folders in outcome.folderSettingDict)
                        {
                            UpdateExternalSharingMetadata(folders.Value as FolderSettings, DocumentFetchingLimit);
                        }
                    }
                }
                UpdateExtConfigListItem();
            }
            catch (Exception ex)
            {
                Logging.LogException(Constants.GetFoldersFromDictionaryLocation, ex);
            }
        }

        private void UpdateExternalSharingMetadata(FolderSettings folders , uint DocumentFetchingLimit)
        {
            int count = 0;
            ExtShareConfigRequest parentRequest = (folders.ParentRequest) as ExtShareConfigRequest;
            // do something with entry.Value or entry.Key
            bool sharingEnabled = parentRequest.SharingEnabled;
            int sharingDuration = parentRequest.SharingDuration;
            DateTime sharingUpdateDate = folders.SettingDateTime;
            string listTitle = folders.LibraryName;
            string subSiteUrl = folders.SiteURL;
            string isSharingEnabled;
            if(sharingEnabled)
            {
                isSharingEnabled = "1";
            }
            else
            {
                isSharingEnabled = "0";
            }
            string updateTime = SPUtility.CreateISO8601DateTimeFromSystemDateTime(sharingUpdateDate);
            string query = String.Format(Constants.ExtShareMetadataQuery, updateTime, isSharingEnabled, sharingDuration);
            string itemType = parentRequest.SharedItemType;
            string itemURL = parentRequest.ItemURL;
            try
            {
                using (SPSite site = new SPSite(subSiteUrl))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        SPFolder folder = null;
                        SPList oList = web.Lists.TryGetList(listTitle);
                        if (oList != null)
                        {
                            SPQuery listQuery = new SPQuery();
                           listQuery.Query = query;

                           //  if (itemType == Constants.FolderItemType)         //Condition commented with respect to a bug TFS#19137
                          //  {
                                folder = web.GetFolder(folders.FolderURL);
                                listQuery.Folder = folder;  // This should restrict the query to the subfolder
                         //   }
                            listQuery.RowLimit = (DocumentFetchingLimit + 1);
                            SPListItemCollection filesCollection = oList.GetItems(listQuery);
                            if (filesCollection != null && filesCollection.Count > 0)
                            {    
                                foreach (SPListItem file in filesCollection)
                                {
                                    count++;
                                    if (count < DocumentFetchingLimit)
                                    {
                                        if(file.Folder != null)
                                        {
                                            MetadataDefaults columnDefaults = new MetadataDefaults(oList);
                                            SetLocationBasedDefaults(web, columnDefaults, file.Folder, sharingEnabled, sharingDuration);
                                        }
                                        else
                                        {
                                            UpdateColumn(web, file, sharingEnabled, sharingDuration, sharingUpdateDate);
                                        }
                                        
                                    }
                                    else
                                    {
                                        parentRequest.JobStatus = Constants.InProgressJobStatus;
                                        parentRequest.JobMessage = Constants.InProgressJobMessage;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                parentRequest.JobMessage = ex.Message;
                operationOutcome.Message = ex.Message;
                operationOutcome.IsSuccess = false;
                Logging.LogException(Constants.UpdateExternalSharingMetadataLocation, ex);
            }
        }

        /// <summary>
        /// updates list item column value
        /// </summary>
        /// <param name="web"></param>
        /// <param name="item"></param>
        /// <param name="action"></param>
        /// <param name="expiryDate"></param>
        private void UpdateColumn(SPWeb web, SPListItem item, bool action, int expiryDate, DateTime sharingUpdateDate)
        {
            try
            {
                if (action)
                {
                    item[Constants.ShareableField] = 1;
                }
                else
                {
                    item[Constants.ShareableField] = 0;
                }
                if (Convert.ToString(expiryDate) != "-")
                {
                    item[Constants.MaxShareDurationField] = expiryDate;
                }
                item[Constants.ExternalSharingupdateDateTime] = sharingUpdateDate;
                web.AllowUnsafeUpdates = true;
                item.SystemUpdate();
                web.AllowUnsafeUpdates = false;
            }
            catch (Exception ex)
            {
                Logging.LogException(Constants.UpdateColumnLocation, ex);
            }
        }

        /// <summary>
        /// Set location based defaults for library folders
        /// </summary>
        /// <param name="web"></param>
        /// <param name="columnDefaults"></param>
        /// <param name="folder"></param>
        /// <param name="action"></param>
        /// <param name="expiryDate"></param>
        private void SetLocationBasedDefaults(SPWeb web, MetadataDefaults columnDefaults, SPFolder folder, bool action, int expiryDate)
        {
            if (action)
            {
                columnDefaults.SetFieldDefault(folder, Constants.ShareableField, "1");
            }
            else
            {
                columnDefaults.SetFieldDefault(folder, Constants.ShareableField, "0");
            }
            if (Convert.ToString(expiryDate) != "-")
            {
                columnDefaults.SetFieldDefault(folder, Constants.MaxShareDurationField, Convert.ToString(expiryDate));
            }
            web.AllowUnsafeUpdates = true;
            columnDefaults.Update();
            web.AllowUnsafeUpdates = false;
        }

        /// <summary>
        /// Set location based defaults for library folders
        /// </summary>
        /// <param name="web"></param>
        /// <param name="columnDefaults"></param>
        /// <param name="folder"></param>
        /// <param name="action"></param>
        /// <param name="expiryDate"></param>
        private void UpdateExtConfigListItem()
        {
            try
            {
                using (SPSite osite = new SPSite(siteUrl))
                {
                    using (SPWeb oWeb = osite.RootWeb)
                    {
                        oWeb.AllowUnsafeUpdates = true;
                        SPList oList = oWeb.Lists.TryGetList(Constants.ExtShareConfigurationListName);
                        if (oList != null)
                        {
                            foreach (ExtShareConfigRequest extShareConfigItem in extShareConfigRequestObject)
                            {
                                SPListItem oListItem = oList.GetItemById(extShareConfigItem.ID);
                                if (extShareConfigItem.JobStatus != Constants.InProgressStatus)
                                {
                                    oListItem[Constants.JobStatus] = Constants.CompletedStatus;
                                    oListItem[Constants.JobMessage] = Constants.JobMessageText;
                                }
                                else
                                {
                                    oListItem[Constants.JobStatus] = extShareConfigItem.JobStatus;
                                    oListItem[Constants.JobMessage] = extShareConfigItem.JobMessage;
                                }
                                oListItem.SystemUpdate();
                            }
                        }
                        oWeb.AllowUnsafeUpdates = false;
                    }
                }
            }
            catch (Exception ex)
            {
                operationOutcome.Message = ex.Message;
                operationOutcome.IsSuccess = false;
                Logging.LogException(Constants.UpdateExtConfigListItemLocation, ex);
            }
        }
    }
}