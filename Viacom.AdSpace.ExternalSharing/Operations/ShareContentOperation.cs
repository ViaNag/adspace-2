﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using Viacom.AdSpace.ExternalSharing.Entities;
using Viacom.FBA.UserAccount;

namespace Viacom.AdSpace.ExternalSharing
{
    /// <summary>
    /// This class will outline the actions to be performed for sharing content operation
    /// </summary>
    class ShareContentOperation : AbstractExternalSharingOperations
    {
        /// <summary>
        /// This method will perform the share content operation
        /// </summary>
        /// <returns>Outcome of the operations will be returned</returns>
        public override IOutcome Execute(IOperationParam param)
        {
            // Initialize the Action Outcome object
            ActionOutcome operationOutcome = new ActionOutcome();

            param.Query = string.Format(Constants.Query, Constants.ExternallySharedContentStatusField, Constants.ChoiceType, Constants.PendingStatus);
            param.ListTitle = Constants.ExternalSharedTitle;

            // Get the items for which have New items and perform the share operation on them
            ExternalSharingItemCollection esItemCollection = this.GetItemCollectionOnStatus(param) as ExternalSharingItemCollection;

            // TODO: Check if collection in the read outcome is not null and item count is great than 0
            if (esItemCollection != null && esItemCollection.IsSuccess && esItemCollection.ItemCollection.Count > 0)
            {
                // Initialize param object for getting mail template
                param.ListTitle = Constants.EmailTemplateListTitle;
                param.Query = string.Format(Constants.Query, Constants.EmailTitleField, Constants.TextTypeField, Constants.ExistingUserMailKey);

                // Get mail templates
                MailDetails mailItemDetails = new GetMailTemplates().PerformAction(param) as MailDetails;
                if (mailItemDetails.IsSuccess)
                {
                    esItemCollection.TemplateWebUrl = (param as ReadItemCollectionParams).TemplateWebUrl;
                    esItemCollection.Status = Constants.InProgressStatus;
                    esItemCollection.ColumnName = Constants.ExternallySharedContentStatusField;
                    ActionOutcome outcome = new MarkItemStatusAction().PerformAction(esItemCollection) as ActionOutcome;
                    if (outcome != null && outcome.IsSuccess)
                    {
                        List<string> distinctContentGroup = new List<string>();
                        distinctContentGroup = (esItemCollection.ItemCollection.Cast<SPListItem>().Select(p => "" + p[Constants.ContentGroupField]).Distinct()).ToList();
                        esItemCollection.DistinctContentGroupCollection = distinctContentGroup;
                        foreach (string contentgroup in esItemCollection.DistinctContentGroupCollection)
                        {
                            esItemCollection.CurrentContentGroup = contentgroup;
                            esItemCollection.ExternalSiteUrl = (param as ReadItemCollectionParams).ExternalSiteUrl;
                            esItemCollection.Siteurl = (param as ReadItemCollectionParams).Siteurl;
                            esItemCollection.ContentGroupItems = (from SPListItem contentGroupitem in esItemCollection.ItemCollection
                                                                  where Convert.ToString(contentGroupitem[Constants.ContentGroupField]) == contentgroup
                                                                  orderby contentGroupitem.ID descending
                                                                  select contentGroupitem).ToList();
                            foreach (SPListItem contentGroupitem in esItemCollection.ContentGroupItems)
                            {
                                var folderCreatedForDownloadableContent = false;
                                var folderCreatedForSharedContent = false;
                                if (Convert.ToBoolean(contentGroupitem[Constants.AllowDownloadField]) == true && folderCreatedForDownloadableContent == false)
                                {
                                    esItemCollection.ListTitle = Constants.DownloadableContentListTitle;
                                    outcome = new CreateDocSetAction().PerformAction(esItemCollection) as ActionOutcome;
                                    esItemCollection.permissionLevel = Constants.Read;
                                    folderCreatedForDownloadableContent = true;
                                }
                                else if (Convert.ToBoolean(contentGroupitem[Constants.AllowDownloadField]) == false && folderCreatedForSharedContent == false)
                                {
                                    esItemCollection.ListTitle = Constants.SharedContentListTitle;
                                    outcome = new CreateDocSetAction().PerformAction(esItemCollection) as ActionOutcome;
                                    esItemCollection.permissionLevel = Constants.ViewOnly;
                                    folderCreatedForSharedContent = true;
                                }

                                if (outcome != null && outcome.IsSuccess)
                                {
                                    //Read all the user names with whom the file is to be shared 
                                    string userNames = contentGroupitem[Constants.SharedWith].ToString();
                                    string[] uniqueUsers = (userNames.Split(',')).Distinct().ToArray();
                                    esItemCollection.Query = string.Format(Constants.Query, Constants.FileTitleField, Constants.TitleField, esItemCollection.CurrentContentGroup);
                                    Viacom.FBA.Entities.ActionOutcome userOutcome = new Viacom.FBA.Entities.ActionOutcome();
                                    Viacom.FBA.Entities.Account oAccount = new Viacom.FBA.Entities.Account();
                                    foreach (string user in uniqueUsers)
                                    {
                                        oAccount.UserName = user;
                                        oAccount.TemplateWebUrl = esItemCollection.TemplateWebUrl;
                                        UserAccountActionsFacade oUserAccActionFacade = new UserAccountActionsFacade();
                                        userOutcome = oUserAccActionFacade.RequestMembership(oAccount) as Viacom.FBA.Entities.ActionOutcome;

                                        if (userOutcome != null && (userOutcome.IsSuccess || userOutcome.Message == Constants.UserExistsMsg)) //User Creation Facade return false in case of existing user
                                        {
                                            esItemCollection.User = user;
                                            esItemCollection.FbaUserId = (param as ReadItemCollectionParams).FbaUserId;
                                            outcome = new AddUserToPermissionGroupAction().PerformAction(esItemCollection) as ActionOutcome;
                                        }
                                        else
                                        {
                                            operationOutcome.IsSuccess = false;
                                            // Log error for failure in creating user account
                                            Logging.Log(Constants.ExceptionLocation, Constants.FailedToCreateUser, null);
                                        }
                                    }
                                }
                                else
                                {
                                    operationOutcome.IsSuccess = false;
                                    // Log error for failure in creating Folder
                                    Logging.Log(Constants.ExceptionLocation, Constants.FailedToCreateFolder, null);
                                }
                                if (outcome != null && outcome.IsSuccess)
                                {
                                    esItemCollection.CurentItem = contentGroupitem;
                                    outcome = new CopyFilesAction().PerformAction(esItemCollection) as ActionOutcome;
                                }
                                else
                                {
                                    operationOutcome.IsSuccess = false;
                                    // Log error for giving permissions to user
                                    Logging.Log(Constants.ExceptionLocation, Constants.FailedToGivePermissions, null);
                                }


                            }
                            if (outcome != null && outcome.IsSuccess)
                            {

                                mailItemDetails.ContentGroupItems = esItemCollection.ContentGroupItems;
                                mailItemDetails.Siteurl = esItemCollection.ExternalSiteUrl;
                                mailItemDetails.ContentGroup = contentgroup;
                                mailItemDetails.ResetPasswordUrl = (param as ReadItemCollectionParams).ResetPasswordUrl;
                                mailItemDetails = new GetMailContent().PerformAction(mailItemDetails) as MailDetails;
                                if (mailItemDetails.IsSuccess)
                                {

                                    outcome = new SendMail().PerformAction(mailItemDetails) as ActionOutcome;
                                    if (outcome.IsSuccess)
                                    {
                                        operationOutcome.IsSuccess = true;
                                    }
                                    else
                                    {
                                        operationOutcome.IsSuccess = false;
                                        // Log error for sending mail
                                        Logging.Log(Constants.ExceptionLocation, Constants.FailedToSendMail, null);
                                    }
                                }
                                else
                                {
                                    operationOutcome.IsSuccess = false;
                                    // Log error for creating mail structure
                                    Logging.Log(Constants.ExceptionLocation, Constants.FailedToCreateMailStrcture, null);
                                }

                            }
                            else
                            {
                                operationOutcome.IsSuccess = false;
                                // Log error for sending mail
                                Logging.Log(Constants.ExceptionLocation, Constants.FailedToCopyFiles, null);
                            }
                        }

                        // Update the status of items
                        if (operationOutcome.IsSuccess)
                        {
                            esItemCollection.Status = Constants.SharedStatus;
                            outcome = new MarkItemStatusAction().PerformAction(esItemCollection) as ActionOutcome;
                        }
                    }
                    else
                    {
                        operationOutcome.IsSuccess = false;
                        // Log error for failure in updating item status
                        Logging.Log(Constants.ExceptionLocation, Constants.FailedToUpdateItemStatus, null);
                    }
                }
                else
                {
                    operationOutcome.IsSuccess = false;
                    // Log error for getting mail template
                    Logging.Log(Constants.ExceptionLocation, Constants.FailedToGetMailTemplate, null);
                }
            }
            else
            {
                operationOutcome.IsSuccess = false;
                //Log Error for failure in getting item collection
                Logging.Log(Constants.ExceptionLocation, Constants.ExternalSharingItemCollectionFailed, null);
            }
            return operationOutcome;
        }
    }
}
