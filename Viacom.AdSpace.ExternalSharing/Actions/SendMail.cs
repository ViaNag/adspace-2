﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.ExternalSharing.Entities;
using Viacom.FBA.UserAccount;

namespace Viacom.AdSpace.ExternalSharing
{
    /// <summary>
    /// This is the action for sending an email to the user with the list of items shared
    /// </summary>
    class SendMail : IExternalSharingAction
    {
        public IOutcome PerformAction(IOperationParam param)
        {

            // Initialize the outcome of this action
            ActionOutcome result = new ActionOutcome();
            result.IsSuccess = true;
            MailDetails mailIemDetailsParam = param as MailDetails;

            string extSiteUrl = string.Format(Constants.MailHtmlExtSiteurl, mailIemDetailsParam.Siteurl);
            string resetPassword = string.Format(Constants.MailHtmlResetPassword, mailIemDetailsParam.ResetPasswordUrl);
            string messageBody = string.Empty;

            foreach (string emailId in mailIemDetailsParam.MailToUsers)
            {
                try
                {
                    messageBody = mailIemDetailsParam.MailTemplate;
                    messageBody = messageBody.Replace(Constants.ChangePasswordMailHtml, resetPassword);
                    messageBody = messageBody.Replace(Constants.UserNameMailHtml, emailId);
                    messageBody = messageBody.Replace(Constants.LibraryNameMailHtml, mailIemDetailsParam.MailBody);
                    messageBody = messageBody.Replace(Constants.UserComment, mailIemDetailsParam.MailMessage);
                    messageBody = messageBody.Replace(Constants.PortalUrlMailHtml, extSiteUrl);

                    // Send mail using fba send mail method
                    // Initiate the account object
                    Viacom.FBA.Entities.AccountEmail mailAcc = new Viacom.FBA.Entities.AccountEmail();
                    mailAcc.UserName = emailId;
                    mailAcc.EmailBody = messageBody;
                    mailAcc.EmailSubject = mailIemDetailsParam.MailSubject;

                    UserAccountActionsFacade oUserAccActionFacade = new UserAccountActionsFacade();
                    Viacom.FBA.Entities.ActionOutcome mailoutcome = oUserAccActionFacade.SendEmailAction(mailAcc) as Viacom.FBA.Entities.ActionOutcome;
                    result.IsSuccess = mailoutcome.IsSuccess;
                }
                catch (Exception ex)
                {
                    result.IsSuccess = false;
                    Logging.LogException(Constants.ExceptionLocation, ex);
                }

            }
            return result;
        }
    }
}