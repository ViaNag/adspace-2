﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.ExternalSharing.Entities;

namespace Viacom.AdSpace.ExternalSharing
{
    /// <summary>
    /// This is the action to add permission to user on external site.
    /// </summary>
    class AddUserToPermissionGroupAction : IExternalSharingAction
    {
        public IOutcome PerformAction(IOperationParam outcome)
        {
            // Initialize the outcome of this action
            ActionOutcome result = new ActionOutcome();

            // Type casting ioutcome type outcome to  ExternalSharingItemCollection
            ExternalSharingItemCollection input = outcome as ExternalSharingItemCollection;
            try
            {
                using (SPSite oSiteCollection = new SPSite(input.ExternalSiteUrl))
                {
                    using (SPWeb web = oSiteCollection.RootWeb)
                    {
                        // Add user to external site visitors group
                        string snamecon = input.FbaUserId + input.User;
                        SPUser user = web.EnsureUser(snamecon);
                        SPGroup associatedMemberGroup = web.AssociatedVisitorGroup;
                        associatedMemberGroup.AddUser(user);
                        associatedMemberGroup.Update();

                        SPList list = web.Lists.TryGetList(input.ListTitle);
                        if (list != null)
                        {
                            SPQuery query = new SPQuery();
                            query.Query = input.Query;
                            SPListItemCollection items = list.GetItems(query);
                            if (items != null && items.Count == 1)
                            {
                                if (!(items[0].HasUniqueRoleAssignments))
                                {
                                    items[0].BreakRoleInheritance(true, false);
                                    SPRoleAssignmentCollection roleassigncoll = items[0].RoleAssignments;
                                    foreach (SPRoleAssignment roleassign in roleassigncoll)
                                    {
                                        roleassign.RoleDefinitionBindings.RemoveAll();
                                        roleassign.Update();
                                    }
                                }


                                SPRoleDefinition roleDef = null;
                                foreach (SPRoleDefinition oSPRoleDefinition in web.RoleDefinitions)
                                {
                                    if (oSPRoleDefinition.Name == input.permissionLevel)
                                    {
                                        roleDef = oSPRoleDefinition;
                                        break;
                                    }
                                }
                                SPRoleAssignment oSPRoleAssignment = new SPRoleAssignment(user);
                                oSPRoleAssignment.RoleDefinitionBindings.Add(roleDef);
                                items[0].RoleAssignments.Add(oSPRoleAssignment);
                                items[0].Update();
                                result.IsSuccess = true;

                            }
                            else
                            {
                                result.IsSuccess = false;
                            }
                        }
                        else
                        {
                            result.IsSuccess = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.Message;
            }
            return result;
        }
    }

}
