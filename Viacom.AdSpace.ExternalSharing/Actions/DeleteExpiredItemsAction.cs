﻿using Microsoft.SharePoint;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.ExternalSharing.Entities;

namespace Viacom.AdSpace.ExternalSharing
{
    /// <summary>
    /// This is the Deletion expired items action
    /// </summary>
    class DeleteExpiredItemsAction : IExternalSharingAction
    {
        /// <summary>
        /// This method performs the underlying action
        /// </summary>
        /// <param name="outcome">Outcome of the read operations is expected</param>
        /// <returns>Outcome of the action</returns>
        public IOutcome PerformAction(IOperationParam param)
        {
            // Initialize the outcome of this action
            ActionOutcome result = new ActionOutcome();
            result.IsSuccess = true;
            try
            {
                // Initialize the items on which action has to be performed
                ExternalSharingItemCollection externalSharedItems = param as ExternalSharingItemCollection;
                string contentGroup = string.Empty;
                string fileName = string.Empty;
                bool isDownloadable = false;
                SPList list = null;
                // Open the external web.
                using (SPSite site = new SPSite(externalSharedItems.Siteurl))
                {
                    using (SPWeb web = site.RootWeb)
                    {
                        SPList sharedList = web.Lists.TryGetList(Constants.SharedContentListTitle);
                        SPList downlodableList = web.Lists.TryGetList(Constants.DownloadableContentListTitle);
                        // Delete files from external web
                        foreach (SPListItem item in externalSharedItems.ItemCollection)
                        {
                            try
                            {
                                fileName = Convert.ToString(item[Constants.FileNameField]);
                                contentGroup = Convert.ToString(item[Constants.ContentGroupField]);
                                isDownloadable = Convert.ToBoolean(item[Constants.AllowDownloadField]);
                                list = isDownloadable ? downlodableList : sharedList;

                                // Get file from external web and delete it
                                SPFile file = web.GetFile(string.Format(Constants.FileRelativeUrl, list.RootFolder.ServerRelativeUrl, contentGroup, fileName));
                                if (file.Exists)
                                {
                                    file.Delete();
                                }
                            }
                            catch (Exception ex)
                            {
                                result.IsSuccess = false;
                                Logging.Log(Constants.ExceptionLocation, string.Format(Constants.FileDeleteErrorMsg, fileName, list.Title), ex.StackTrace);
                            }
                        }
                        // Update list and web
                        sharedList.Update();
                        downlodableList.Update();

                        // Delete content group folder from external web if it is empty
                        foreach (SPListItem item in externalSharedItems.ItemCollection)
                        {
                            try
                            {
                                contentGroup = Convert.ToString(item[Constants.ContentGroupField]);
                                isDownloadable = Convert.ToBoolean(item[Constants.AllowDownloadField]);
                                list = isDownloadable ? downlodableList : sharedList;

                                // Get content group folder from external web and delete it if empty
                                SPFolder folder = web.GetFolder(string.Format(Constants.FolderRelativeUrl, list.RootFolder.ServerRelativeUrl, contentGroup));
                                if (folder.Exists && folder.Files.Count == 0)
                                {
                                    folder.Delete();
                                }
                            }
                            catch (Exception ex)
                            {
                                result.IsSuccess = false;
                                Logging.Log(Constants.ExceptionLocation, string.Format(Constants.FolderDeleteErrorMsg, contentGroup, list.Title), ex.StackTrace);
                            }
                        }
                        // Update list and web
                        sharedList.Update();
                        downlodableList.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                Logging.LogException(Constants.ExceptionLocation, ex);
            }
            return result;
        }
    }
}