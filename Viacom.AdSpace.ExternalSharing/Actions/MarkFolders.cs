﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.ExternalSharing.Entities;
using Viacom.AdSpace.ExternalSharing.Entities.Entities;

namespace Viacom.AdSpace.ExternalSharing.Actions
{
    class MarkFolders : IExternalSharingAction
    {
        // Initialize the outcome of this action
        ActionOutcome outcome = new ActionOutcome();
        Dictionary<string, IOperationParam> foldersSettingDict = new Dictionary<string, IOperationParam>();
        public IOutcome PerformAction(IOperationParam param)
        {
            // Type casting ioutcome type outcome to  ExternalSharingItemCollection
            FolderSettings folderSetting = param as FolderSettings;
            IOperationParam dictValue=new FolderSettings();
            bool isAddedToDict = false;
            try
            {
                   if (foldersSettingDict.TryGetValue(folderSetting.FolderURL, out dictValue))
                    {
                        FolderSettings dictValue1 = dictValue as FolderSettings;
                        int result = DateTime.Compare(folderSetting.SettingDateTime, dictValue1.SettingDateTime);
                        if (result > 0)
                        {
                            isAddedToDict = true;
                            foldersSettingDict.Remove(folderSetting.FolderURL);
                            foldersSettingDict.Add(folderSetting.FolderURL, param);
                        }

                    }
                    else
                    {
                        //key not found in dictionary, add folderSetting to dictionary with key as folderurl
                        foldersSettingDict.Add(folderSetting.FolderURL, param);
                        isAddedToDict = true;
                    }
                //if folder has added to dictionary , fetch all folders inside it
                if (isAddedToDict)
                {
                    using (SPSite osite = new SPSite(folderSetting.SiteURL))
                    {
                        using (SPWeb oWeb = osite.OpenWeb())
                        {
                            SPFolder currentFolder = oWeb.GetFolder(folderSetting.FolderURL);
                            SPFolderCollection folders = currentFolder.SubFolders;
                            foreach (SPFolder folder in folders)
                            {
                                FolderSettings subfolderSetting = new FolderSettings();
                                subfolderSetting.FolderURL = folderSetting.SiteURL + "/" + folder.Url;
                                subfolderSetting.LibraryName = folderSetting.LibraryName;
                                subfolderSetting.SiteURL = folderSetting.SiteURL;
                                subfolderSetting.SettingDateTime = folderSetting.SettingDateTime;
                                subfolderSetting.ParentRequest = folderSetting.ParentRequest;
                                PerformAction(subfolderSetting);
                            }
                            outcome.IsSuccess = true;
                        }
                    }
                }
                outcome.folderSettingDict = new Dictionary<string, IOperationParam>(foldersSettingDict);
            }
            catch (Exception ex)
            {
                outcome.IsSuccess = false;
                outcome.Message = ex.Message;
                Logging.LogException("MarkFolders", ex);
            }
            return outcome;
        }
    }
}
