﻿// Create a namespace for our functions so we don't collide with anything else
var Viacom = Viacom || {};

// Create a function for customizing the Field Rendering of our fields
Viacom.CustomizeFieldRendering = function () {
    var fieldJsLinkOverride = {};
    fieldJsLinkOverride.Templates = {};
    fieldJsLinkOverride.Templates.Fields =
    {
        // Make sure the Priority field view gets hooked up to the GetPriorityFieldIcon method defined below
        'DownloadProxy': { 'View': Viacom.GetPriorityFieldIcon }
    };
    // Register the rendering template
    SPClientTemplates.TemplateManager.RegisterTemplateOverrides(fieldJsLinkOverride);
};

// Create a function for getting the Priority Field Icon value (called from the first method)
Viacom.GetPriorityFieldIcon = function (ctx) {
    var value = ctx.CurrentItem.DownloadProxy;
    // In the following section we simply determine what the rendered html output should be. In my case I'm setting an icon.
    if (value != null && value != "") {
        var url = _spPageContextInfo.siteAbsoluteUrl + ctx.CurrentItem.FileRef;
        return "<a title=\"Download video from proxy server\" href=\"javascript:void(0);\" onclick=\"Viacom.DownloadVideo('" + value + "','" + "DownloadProxy" + "','" + url + "');return false\"><img src='/_layouts/15/images/Viacom.AdSpace.MediaSilo/download.png' /></a>";
    }
};

// Call the function. 
// We could've used a self-executing function as well but I think this simplifies the example
Viacom.CustomizeFieldRendering();

//Download video
Viacom.DownloadVideo = function (astID, downloadType, intVideoUrl) {
    var videoURL = '';
    try {
        $.ajax({
            async: true,
            type: "POST",
            url: _spPageContextInfo.siteAbsoluteUrl + "/_catalogs/masterpage/MediaSiloUploadVideo.aspx/" + "AssetDetails",
            data: JSON.stringify({ assetID: '"' + astID + '"', siteCollectionURL: '"' + _spPageContextInfo.siteAbsoluteUrl + '"' }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                if (data.d != null) {
                    var obj = jQuery.parseJSON(data.d);
                    var assetDetails = obj[0];
                    if (downloadType == "DownloadProxy") {
                        videoURL = assetDetails.derivatives[1].url;
                    }
                    if (downloadType == "DownloadServer") {
                        videoURL = assetDetails.derivatives[0].url;;
                    }
                    if (videoURL != "") {
                        var anchor = document.createElement('a');
                        anchor.download = "filename.txt";
                        anchor.href = videoURL;
                        anchor.click();
                    }
                    //Save video data
                    var IsDownload = "true";
                    Viacom.SaveVideoData(intVideoUrl, IsDownload);
                }
            }
            ,
            error: function (err) {
                console.log('Asset Details Process failed');
            }
        });
    }
    catch (err) {
        console.log(err.message + 'Something went wrong during creation of detailsAsset');
    }
}