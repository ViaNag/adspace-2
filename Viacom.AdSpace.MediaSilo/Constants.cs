﻿
namespace Viacom.AdSpace.MediaSilo
{
    class Constants
    {
        public const string PROXY_THUMBANIL_URL = "ProxyThumbnailURL";
        public const string PROXY_VIDEO_URL = "ProxyVideoURL";
        public const string PLAY = "Play";
        public const string ASSETID = "AssetID";
        public const string PROJECTID = "ProjectID";
        public const string PROXY_RTMP_STREAMER = "ProxyRTMP_Streamer";
        public const string PROXY_RTMP_URL = "ProxyRTMPURL";
        public const string PROXY_RTMPT_STREAMER = "ProxyRTMPTStreamer";
        public const string PROXY_RTMPT_URL = "ProxyRTMPTURL";
        public const string IS_VIDEO = "IsVideo";
        public const string SOURCE_VIDEO_URL = "SourceVideoURL";
        public const string MEDIASILO_CONFIGURATION_LIST = "MediaSiloConfiguration";
        public const string CONFIGURATION_LIST_QUERY = @"<where>
                                                   <Eq>
                                                       <FiledRef Name='KEY'/>
                                                       <Value Type='Text'>MediaSiloConfiguration</Value>
                                                   </Eq>
                                               </Where>";
        public const string CONFIG_FIELD_VALUE = "VALUE";
        public const string LIB_PREFIX = "DL_";
        public const string FOLDERURL_POSTFIX = "/folders";
        public const string SUBFOLDERURL_POSTFIX = "/subfolders";
        public const string METADATA_POSTFIX = "/metadata";
        public const string HTTP_POST_METHOD = "Post";
        public const string HTTP_DELETE_METHOD = "DELETE";
        public const int CACHE_DAY = 0;
        public const int CACHE_HOUR = 0;
        public const int CACHE_MIN = 30;
        public const int CACHE_SEC = 0;
        public const string BASE_ADDRESS = "BaseAddress";
        public const string AUTHORIZATION_KEY = "AurthorizationKey";
        public const string HOST_CONTEXTKEY = "HostContextKey";
        public const string HOST_CONTEXTVALUE = "HostContextValue";
        public const string API_KEY = "APIKEY";
        public const string APPLICATION_KEY = "ApplicationKey";
        public const string PROJECT_ID = "ProjectID";
        public const string UPLOAD_TICKET_URL = "UploadTicketURL";
        public const string ASSET_URL = "AssetURL";
        public const string ASSET_DETAILS_URL = "AssetDetailsURL";
        public const string API_URL = "APIURL";
        public const string METADATA_URL = "MetaDataURL";
        public const string AURTHORIZATION_VALUE = "AurthorizationValue";
        public const string API_VALUE = "APIValue";
        public const string APPLICATION_VALUE = "ApplicationValue";
        public const string ROOT_FOLDER = "RootFolder";
        public const string CONFIG_VALUE_FIELD = "VALUE";
        public const string CONTENT_TYPE = "0x0101001A3D171C261B422D8D55F76EFD45F286009BEB8AB200A24167ABEB12EF638983B6";
        public const string CONTENTTYPE_ID_TEXT = "ContentTypeId";
        public const string FOLDERID = "FolderID";
        public const string IMAGEICON = "/_layouts/15/images/Viacom.AdSpace.MediaSilo/VIDEOPREVIEW.PNG";
        public const string MS_CACHE_OBJECT_NAME = "MediaSiloConfiguration";
        public const string SITE_URL_KEY = "SiteURL";
        public const string LIST_ID_KEY = "ListID";
        public const string DOC_URL_KEY = "DocURL";
        public const string Container_ID = "Container ID";
        public const string CREATEDBY_KEY = "Created By";

        public const string Spliter = "##$MSilo$##";
        public const string HTTP = "http";
        public const string HTTP_Replace = "$@protocol@$";
        public const string MediaSilo_ThreadDelayTime = "MediaSiloThreadDelayTime";
    }
}
