﻿using Microsoft.SharePoint;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Configuration;

namespace Viacom.AdSpace.MediaSilo
{
    public class MediasiloProxy : Microsoft.SharePoint.Publishing.PublishingLayoutPage
    {
        public static string editFormURL = string.Empty;
        public const string CONSTANT_SEPARATOR = "@#@";

        /// <summary>
        /// Funcation to create Upload Ticket
        /// </summary>
        /// <param name="fileName">Name of file being uploaded</param>
        /// <param name="siteCollectionURL">Site collection URL</param>
        /// <returns>Return success or fail</returns>
        [WebMethod]
        public static string CreateUploadTicket(string fileName, string siteCollectionURL)
        {
            //Site Collection url being assign to static variable
            Configuration oConfiguration = Utility.GetConfigObject(siteCollectionURL);
            string response = null;
            var client = new WebClient();
            try
            {
                client = Utility.GetConfigurationHeader(client, oConfiguration);
                Dictionary<string, object> dictobj = new Dictionary<string, object>();
                dictobj.Add("fileName", fileName.Trim('"'));
                string requestJson = Utility.GetJson(dictobj);
                //string requestJson = @"{""fileName"": " + fileName + @"}";
                byte[] byteResponse = client.UploadData(oConfiguration.UploadTicketURL, Encoding.UTF8.GetBytes(requestJson));
                response = Encoding.UTF8.GetString(byteResponse);

            }
            catch (Exception ex)
            {
                Utility.Log("MediaSilouplaod.cs-CreateUploadTicket", ex.Message, ex.StackTrace);
            }
            finally
            {
                if (client != null) ((IDisposable)client).Dispose();
            }
            return response;
        }

        /// <summary>
        /// Funcation to create asset in MediaSilo
        /// </summary>
        /// <param name="assetURL">Asset URL</param>
        /// <param name="folderId">Folder ID where asset will be uploaded</param>
        /// <returns>>Return success or fail</returns>
        [WebMethod]
        public static string CreateAsset(string assetURL, string folderId, string siteCollectionURL)
        {
            folderId = folderId.Trim('"');
            string response = null;
            Configuration oConfiguration = Utility.GetConfigObject(siteCollectionURL);
            var client = new WebClient();
            try
            {
                client = Utility.GetConfigurationHeader(client, oConfiguration);
                StringBuilder sbJson = new StringBuilder();
                Dictionary<string, object> dictobj = new Dictionary<string, object>();

                if (!string.IsNullOrEmpty(folderId))
                {
                    dictobj.Add("sourceUrl", assetURL.Trim('"'));
                    dictobj.Add("projectId", oConfiguration.ProjectID.Trim('"'));
                    dictobj.Add("folderId", folderId.Trim('"'));
                }
                else
                {
                    dictobj.Add("sourceUrl", assetURL.Trim('"'));
                    dictobj.Add("projectId", oConfiguration.ProjectID.Trim('"'));
                }
                string requestJson = Utility.GetJson(dictobj);
                response = client.UploadString(oConfiguration.AssetURL, Constants.HTTP_POST_METHOD, requestJson);

            }
            catch (Exception ex)
            {
                Utility.Log("MediaSilouplaod.cs-CreateAsset", ex.Message, ex.StackTrace);
            }
            finally
            {
                if (client != null) ((IDisposable)client).Dispose();
            }
            return response;
        }

        /// <summary>
        /// Funcation to get created asset details from MediaSilo
        /// </summary>
        /// <param name="assetID">ID of Asset</param>
        /// <returns>Return success or fail</returns>
        [WebMethod]
        public static string AssetDetails(string assetID, string siteCollectionURL)
        {
            string response = null;
            Configuration oConfiguration = Utility.GetConfigObject(siteCollectionURL);
            var client = new WebClient();
            try
            {
                client = Utility.GetConfigurationHeader(client, oConfiguration);
                string sMilliSecond = oConfiguration.MediaSiloThreadDelayTime; //Convert.ToString(ConfigurationManager.AppSettings[Constants.MediaSilo_ThreadDelayTime]);
                int iMilliSecond = 1000;
                if (!string.IsNullOrEmpty(sMilliSecond))
                {
                    iMilliSecond = Convert.ToInt32(sMilliSecond);
                }
                System.Threading.Thread.Sleep(iMilliSecond);
                assetID = assetID.Split(new char[] { ':' }).Last();
                assetID = assetID.Replace("}", "").Replace("{", "").Replace("\"", "").Replace("\\", "").Replace(" ", "");
                string URL = string.Format("{0}{1}", oConfiguration.AssetDetailsURL, assetID);
                response = client.DownloadString(URL);

            }
            catch (Exception ex)
            {
                Utility.Log("MediaSilouplaod.cs-AssetDetails", ex.Message, ex.StackTrace);
            }
            finally
            {
                if (client != null) ((IDisposable)client).Dispose();
            }
            return response;
        }

        /// <summary>
        /// Funcation to upload document to sharepoint library
        /// </summary>
        /// <param name="siteURL">Current Site URL</param>
        /// <param name="documentListID">Document List ID</param>
        /// <param name="documentURL">Document URL</param>
        /// <param name="sourceURL">Source URL</param>
        /// <param name="vedioURL">Vedio URL</param>
        /// <param name="assetID">Asset ID</param>
        /// <param name="projectID">Project ID</param>
        /// <param name="folderId">Folder ID</param>
        /// <param name="imageURL">Image URL</param>
        /// <param name="rtmp_url">rtmp url</param>
        /// <param name="rtmp_streamer">rtmp streamer</param>
        /// <param name="rtmpt_url">rtmpt url</param>
        /// <param name="rtmpt_streamer">rtmpt streamer</param>
        /// <returns>Return success or fail</returns>
        [WebMethod]
        public static string UploadDocument(string siteURL, string documentListID, string documentURL, string sourceURL, string vedioURL, string assetID, string projectID, string folderId, string imageURL, string rtmp_url, string rtmp_streamer, string rtmpt_url, string rtmpt_streamer, string siteCollectionURL)
        {
            string response = null;
            Configuration oConfiguration = Utility.GetConfigObject(siteCollectionURL);
            StringBuilder sb = new StringBuilder();
            try
            {                
                sb.Append("siteURL=" + siteURL + CONSTANT_SEPARATOR);
                sb.Append("documentListID=" + documentListID + CONSTANT_SEPARATOR);
                sb.Append("documentURL=" + documentURL + CONSTANT_SEPARATOR);
                sb.Append("sourceURL=" + sourceURL + CONSTANT_SEPARATOR);
                sb.Append("vedioURL=" + vedioURL + CONSTANT_SEPARATOR);
                sb.Append("assetID=" + assetID + CONSTANT_SEPARATOR);
                sb.Append("projectID=" + projectID + CONSTANT_SEPARATOR);
                sb.Append("folderId=" + folderId + CONSTANT_SEPARATOR);
                sb.Append("imageURL=" + imageURL + CONSTANT_SEPARATOR);
                sb.Append("rtmp_url=" + rtmp_url + CONSTANT_SEPARATOR);
                sb.Append("rtmp_streamer=" + rtmp_streamer + CONSTANT_SEPARATOR);
                sb.Append("rtmpt_url=" + rtmpt_url + CONSTANT_SEPARATOR);
                sb.Append("rtmpt_streamer=" + rtmpt_streamer + CONSTANT_SEPARATOR);
                sb.Append("siteCollectionURL=" + siteCollectionURL + CONSTANT_SEPARATOR);

                using (SPSite site = new SPSite(siteURL))
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        string loginName = web.CurrentUser.Email;
                        bool webAllowUnsafeUpdates = web.AllowUnsafeUpdates;
                        web.AllowUnsafeUpdates = true;
                        Guid guid = new Guid(documentListID);
                        SPList library = web.Lists[guid];                        
                        Hashtable metaData = new Hashtable();
                        metaData.Add(Constants.PROXY_THUMBANIL_URL, imageURL);
                        //metaData.Add(Constants.PROXY_VIDEO_URL, vedioURL);
                        metaData.Add(Constants.ASSETID, assetID);
                        metaData.Add(Constants.PROJECTID, projectID);
                        //metaData.Add(Constants.PROXY_RTMP_STREAMER, rtmp_streamer);
                        //metaData.Add(Constants.PROXY_RTMP_URL, rtmp_url);
                        //metaData.Add(Constants.PROXY_RTMPT_STREAMER, rtmpt_streamer);
                        //metaData.Add(Constants.PROXY_RTMPT_URL, rtmpt_url);
                        metaData.Add(Constants.IS_VIDEO, true);
                        //metaData.Add(Constants.SOURCE_VIDEO_URL, sourceURL);
                        metaData.Add(Constants.FOLDERID, folderId);
                        metaData.Add(Constants.PLAY, assetID);
                        try
                        {
                            SPListItem item = web.GetListItem(documentURL);

                            if (item != null)
                            {
                                if (item.File.CheckOutStatus != SPFile.SPCheckOutStatus.None)
                                {
                                    item.File.UndoCheckOut();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Utility.Log("MediaSilouplaod Item", ex.Message, ex.StackTrace);
                            Utility.Log("MediaSilouplaod Parameters", sb.ToString(), "");
                        }
                        //byte[] imageBytes = null;
                        //using (var webclient = new WebClient())
                        //{
                        //    webclient.UseDefaultCredentials = true;
                        //    string imageIcon = string.Format("{0}{1}", web.Url, Constants.IMAGEICON);
                        //    imageBytes = webclient.DownloadData(imageIcon);
                        //}
                        string tempImageString = "iVBORw0KGgoAAAANSUhEUgAAAoAAAAFoCAYAAADHMkpRAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABtbSURBVHhe7d3hylzJkQRQPbrefAyGYTBYVGWqmmlFnIX9s51qKs+t70Y0hvWPH/6HAAECBAgQIECAAAECBAgQIECAAAECBAgQIECAAAECBAgQIECAAAECBAgQIECAAAECBAgQIECAAAECBAgQIECAAAECBAgQIECAAAECBAgQIECAAAECBAgQIECAAAECBAgQIECAAAECBAgQIECAAAECBAgQIECAAAECBAgQIECAAAECBAgQIECAAAECBAgQIECAAAECBAgQIECAAAECBAgQIECAAAECBAgQIECAAAECBAgQIEBgLfDz58+//C8Dd8AdcAf+/x1Yv1z9QwIECHyzgJe+4HcH3AF34Nd34Jvf385GgACBtYAXv/B3B9wBd0ABXIeIf0iAwJ8p4MUv/N0Bd8AdUAD/zARzagIE1gJe/MLfHXAH3AEFcB0i/iEBAn+mgBe/8HcH3AF3QAH8MxPMqQkQWAt48Qt/d8AdcAcUwHWI+IcECPyZAl78wt8dcAfcAQXwz0wwpyZAYC3gxS/83QF3wB1QANch4h8SIPBnCvzqxf/Xv/A/L0PoXzj+0/+H2s4/F3B//ikxc73f/xep/n/mm92pCRAgcBBQAH8/+P7+htQA3Oz1TvX+mzbndP/vfU+Tqf5ChAABApECAvAUa/efpwbgZq97tXeTm3O6//xPP+AiX/yWIkCAgAAUgKcA3BSrd6r337Q5p/t/73uaTPWXEgQIEIgUEICnWLv/PDUAN3vdq72b3JzT/ed/+gEU+eK3FAECBASgADwF4KZYvVO9/6bNOd3/e9/TZKq/lCBAgECkgAA8xdr956kBuNnrXu3d5Oac7j//0w+gyBe/pQgQICAABeApADfF6p3q/Tdtzun+3/ueJlP9pQQBAgQiBQTgKdbuP08NwM1e92rvJjfndP/5n34ARb74LUWAAAEBKABPAbgpVu9U779pc073/973NJnqLyUIECAQKSAAT7F2/3lqAG72uld7N7k5p/vP//QDKPLFbykCBAgIQAF4CsBNsXqnev9Nm3O6//e+p8lUfylBgACBSAEBeIq1+89TA3Cz173au8nNOd1//qcfQJEvfksRIEBAAArAUwBuitU71ftv2pzT/b/3PU2m+ksJAgQIRAoIwFOs3X+eGoCbve7V3k1uzun+8z/9AIp88VuKAAECAlAAngJwU6zeqd5/0+ac7v+972ky1V9KECBAIFJAAJ5i7f7z1ADc7HWv9m5yc073n//pB1Dki99SBAgQEIAC8BSAm2L1TvX+mzbndP/vfU+Tqf5SggABApECAvAUa/efpwbgZq97tXeTm3O6//xPP4AiX/yWIkCAgAAUgKcA3BSrd6r337Q5p/t/73uaTPWXEgQIEIgUEICnWLv/PDUAN3vdq72b3JzT/ed/+gEU+eK3FAECBASgADwF4KZYvVO9/6bNOd3/e9/TZKq/lCBAgECkgAA8xdr956kBuNnrXu3d5Oac7j//0w+gyBe/pQgQICAABeApADfF6p3q/Tdtzun+3/ueJlP9pQQBAgQiBQTgKdbuP08NwM1e92rvJjfndP/5n34ARb74LUWAAAEBKABPAbgpVu9U779pc073/973NJnqLyUIECAQKSAAT7F2/3lqAG72uld7N7k5p/vP//QDKPLFbykCBAgIQAF4CsBNsXqnev9Nm3O6//e+p8lUfylBgACBSAEBeIq1+89TA3Cz173au8nNOd1//qcfQJEvfksRIEDgZWj6rp9/MWDgDmTdASlBgACBSAFhlRVWnqfn6Q68vQORL35LESBAQFi8DQuePN2BrDsgJQgQIBApIKyywsrz9Dzdgbd3IPLFbykCBAgIi7dhwZOnO5B1B6QEAQIEIgWEVVZYeZ6epzvw9g5EvvgtRYAAAWHxNix48nQHsu6AlCBAgECkgLDKCivP0/N0B97egcgXv6UIECAgLN6GBU+e7kDWHZASBAgQiBQQVllh5Xl6nu7A2zsQ+eK3FAECBL7pvwrr3X8plW8iQIDATOBX70IpQYAAgUgBBXAWEqYJEMgUUAAjI85SBAj8SkABzAwzWxEgMBNQAOUkAQJVAgrgLCRMEyCQKaAAVkWfZQkQUAAzw8xWBAjMBBRAeUiAQJWAAjgLCdMECGQKKIBV0WdZAgQUwMwwsxUBAjMBBVAeEiBQJaAAzkLCNAECmQIKYFX0WZYAAQUwM8xsRYDATEABlIcECFQJKICzkDBNgECmgAJYFX2WJUBAAcwMM1sRIDATUADlIQECVQIK4CwkTBMgkCmgAFZFn2UJEFAAM8PMVgQIzAQUQHlIgECVgAI4CwnTBAhkCiiAVdFnWQIEFMDMMLMVAQIzAQVQHhIgUCWgAM5CwjQBApkCCmBV9FmWAAEFMDPMbEWAwExAAZSHBAhUCSiAs5AwTYBApoACWBV9liVAQAHMDDNbESAwE1AA5SEBAlUCCuAsJEwTIJApoABWRZ9lCRBQADPDzFYECMwEFEB5SIBAlYACOAsJ0wQIZAoogFXRZ1kCBBTAzDCzFQECMwEFUB4SIFAloADOQsI0AQKZAgpgVfRZlgABBTAzzGxFgMBMQAGUhwQIVAkogLOQME2AQKaAAlgVfZYlQEABzAwzWxEgMBNQAOUhAQJVAgrgLCRMEyCQKaAAVkWfZQkQUAAzw8xWBAjMBBRAeUiAQJWAAjgLCdMECGQKKIBV0WdZAgQUwMwwsxUBAjMBBVAeEiBQJaAAzkLCNAECmQIKYFX0WZYAAQUwM8xsRYDATEABlIcECFQJKICzkDBNgECmgAJYFX2WJUBAAcwMM1sRIDATUADlIQECVQIK4CwkTBMgkCmgAFZFn2UJEFAAM8PMVgQIzAQUQHlIgECVgAI4CwnTBAhkCiiAVdFnWQIEFMDMMLMVAQIzAQVQHhIgUCWgAM5CwjQBApkCCmBV9FmWAAEFMDPMbEWAwExAAZSHBAhUCSiAs5AwTYBApoACWBV9liVAQAHMDDNbESAwE1AA5SEBAlUCCuAsJEwTIJApoABWRZ9lCRBQADPDzFYECMwEFEB5SIBAlYACOAsJ0wQIZAoogFXRZ1kCBBTAzDCzFQECMwEFUB4SIFAloADOQsI0AQKZAgpgVfRZlgABBTAzzGxFgMBMQAGUhwQIVAkogLOQME2AQKaAAlgVfZYlQEABzAwzWxEgMBNQAOUhAQJVAgrgLCRMEyCQKaAAVkWfZQkQUAAzw8xWBAjMBBRAeUiAQJWAAjgLCdMECGQKKIBV0WdZAgQUwMwwsxUBAjMBBVAeEiBQJaAAzkLCNAECmQIKYFX0WZYAAQUwM8xsRYDATEABlIcECFQJKICzkDBNgECmgAJYFX2WJUBAAcwMM1sRIDATUADlIQECVQIK4CwkTBMgkCmgAFZFn2UJEFAAM8PMVgQIzAQUQHlIgECVgAI4CwnTBAhkCiiAVdFnWQIEFMDMMLMVAQIzAQVQHhIgUCWgAM5CwjQBApkCCmBV9FmWAAEFMDPMbEWAwExAAZSHBAhUCSiAs5AwTYBApoACWBV9liVAQAHMDDNbESAwE1AA5SEBAlUCCuAsJEwTIJApoABWRZ9lCRBQADPDzFYECMwEFEB5SIBAlYACOAsJ0wQIZAoogFXRZ1kCBBTAzDCzFQECMwEFUB4SIFAloADOQsI0AQKZAgpgVfRZlgABBTAzzGxFgMBMQAGUhwQIVAkogLOQME2AQKaAAlgVfZYlQEABzAwzWxEgMBNQAOUhAQJVAgrgLCRMEyCQKaAAVkWfZQkQUAAzw8xWBAjMBBRAeUiAQJWAAjgLCdMECGQKKIBV0WdZAgQUwMwwsxUBAjMBBVAeEiBQJaAAzkLCNAECmQIKYFX0WZYAAQUwM8xsRYDATEABlIcECFQJKICzkDBNgECmgAJYFX2WJUBAAcwMM1sRIDATUADlIQECVQIK4CwkTBMgkCmgAFZFn2UJEFAAM8PMVgQIzAQUQHlIgECVgAI4CwnTBAhkCiiAVdFnWQIEFMDMMLMVAQIzAQVQHhIgUCWgAM5CwjQBApkCCmBV9FmWAAEFMDPMbEWAwExAAZSHBAhUCSiAs5AwTYBApoACWBV9liVAQAHMDDNbESAwE1AA5SEBAlUCCuAsJEwTIJApoABWRZ9lCRBQADPDzFYECMwEFEB5SIBAlYACOAsJ0wQIZAoogFXRZ1kCBBTAzDCzFQECMwEFUB4SIFAloADOQsI0AQKZAgpgVfRZlgABBTAzzGxFgMBMQAGUhwQIVAkogLOQME2AQKaAAlgVfZYlQEABzAwzWxEgMBNQAOUhAQJVAgrgLCRMEyCQKaAAVkWfZQkQUAAzw8xWBAjMBBRAeUiAQJWAAjgLCdMECGQKKIBV0WdZAgQUwMwwsxUBAjMBBVAeEiBQJaAAzkLCNAECmQIKYFX0WZYAAQUwM8xsRYDATEABlIcECFQJKICzkDBNgECmgAJYFX2WJUBAAcwMM1sRIDATUADlIQECVQIK4CwkTBMgkCmgAFZFn2UJEFAAM8PMVgQIzAQUQHlIgECVgAI4CwnTBAhkCiiAVdFnWQIEFMDMMLMVAQIzAQVQHhIgUCWgAM5CwjQBApkCCmBV9FmWAAEFMDPMbEWAwExAAZSHBAhUCSiAs5AwTYBApoACWBV9liVAQAHMDDNbESAwE1AA5SEBAlUCCuAsJEwTIJApoABWRZ9lCRBQADPDzFYECMwEFEB5SIBAlYACOAsJ0wQIZAoogFXRZ1kCBBTAzDCzFQECMwEFUB4SIFAloADOQsI0AQKZAgpgVfRZlgABBTAzzGxFgMBMQAGUhwQIVAkogLOQME2AQKaAAlgVfZYlQEABzAwzWxEgMBNQAOUhAQJVAgrgLCRMEyCQKaAAVkWfZQkQUAAzw8xWBAjMBBRAeUiAQJWAAjgLCdMECGQKKIBV0WdZAgQUwMwwsxUBAjMBBVAeEiBQJaAAzkLCNAECmQIKYFX0WZYAAQUwM8xsRYDATEABlIcECFQJKICzkDBNgECmgAJYFX2WJUBAAcwMM1sRIDATUADlIQECVQIK4CwkTBMgkCmgAFZFn2UJEFAAM8PMVgQIzAQUQHlIgECVgAI4CwnTBAhkCiiAVdFnWQIEFMDMMLMVAQIzAQVQHhIgUCWgAM5CwjQBApkCCmBV9FmWAAEFMDPMbEWAwExAAZSHBAhUCSiAs5AwTYBApoACWBV9liVAQAHMDDNbESAwE1AA5SEBAlUCCuAsJEwTIJApoABWRZ9lCRBQADPDzFYECMwEFEB5SIBAlYACOAsJ0wQIZAoogFXRZ1kCBBTAzDCzFQECMwEFUB4SIFAloADOQsI0AQKZAgpgVfRZlgABBTAzzGxFgMBMQAGUhwQIVAkogLOQME2AQKaAAlgVfZYlQEABzAwzWxEgMBNQAOUhAQJVAgrgLCRMEyCQKaAAVkWfZQkQUAAzw8xWBAjMBBRAeUiAQJWAAjgLCdMECGQKKIBV0WdZAgQUwMwwsxUBAjMBBVAeEiBQJaAAzkLCNAECmQIKYFX0WZYAAQUwM8xsRYDATEABlIcECFQJKICzkDBNgECmgAJYFX2WJUBAAcwMM1sRIDATUADlIQECVQIK4CwkTBMgkCmgAFZFn2UJEFAAM8PMVgQIzAQUQHlIgECVgAI4CwnTBAhkCiiAVdFnWQIEFMDMMLMVAQIzAQVQHhIgUCWgAM5CwjQBApkCCmBV9FmWAAEFMDPMbEWAwExAAZSHBAhUCSiAs5AwTYBApoACWBV9liVAQAHMDDNbESAwE1AA5SEBAlUCCuAsJEwTIJApoABWRZ9lCRBQADPDzFYECMwEFEB5SIBAlYACOAsJ0wQIZAoogFXRZ1kCBBTAzDCzFQECMwEFUB4SIFAloADOQsI0AQKZAgpgVfRZlgABBTAzzGxFgMBMQAGUhwQIVAkogLOQME2AQKaAAlgVfZYlQEABzAwzWxEgMBNQAOUhAQJVAgrgLCRMEyCQKaAAVkWfZQkQUAAzw8xWBAjMBBRAeUiAQJWAAjgLCdMECGQKKIBV0WdZAgQUwMwwsxUBAjMBBVAeEiBQJaAAzkLCNAECmQIKYFX0WZYAAQUwM8xsRYDATEABlIcECFQJKICzkDBNgECmgAJYFX2WJUBAAcwMM1sRIDATUADlIQECVQIK4CwkTBMgkCmgAFZFn2UJEFAAM8PMVgQIzAQUQHlIgECVgAI4CwnTBAhkCiiAVdFnWQIEFMDMMLMVAQIzAQVQHhIgUCWgAM5CwjQBApkCCmBV9FmWAAEFMDPMbEWAwExAAZSHBAhUCSiAs5AwTYBApoACWBV9liVAQAHMDDNbESAwE1AA5SEBAlUCCuAsJEwTIJApoABWRZ9lCRBQADPDzFYECMwEFEB5SIBAlYACOAsJ0wQIZAoogFXRZ1kCBBTAzDCzFQECMwEFUB4SIFAloADOQsI0AQKZAgpgVfRZlgABBTAzzGxFgMBMQAGUhwQIVAkogLOQME2AQKaAAlgVfZYlQEABzAwzWxEgMBNQAOUhAQJVAgrgLCRMEyCQKaAAVkWfZQkQUAAzw8xWBAjMBBRAeUiAQJWAAjgLCdMECGQKKIBV0WdZAgQUwMwwsxUBAjMBBVAeEiBQJaAAzkLCNAECmQIKYFX0WZYAAQUwM8xsRYDATEABlIcECFQJKICzkDBNgECmgAJYFX2WJUBAAcwMM1sRIDATUADlIQECVQIK4CwkTBMgkCmgAFZFn2UJEFAAM8PMVgQIzAQUQHlIgECVgAI4CwnTBAhkCiiAVdFnWQIEFMDMMLMVAQIzAQVQHhIgUCWgAM5CwjQBApkCCmBV9FmWAAEFMDPMbEWAwExAAZSHBAhUCSiAs5AwTYBApoACWBV9liVAQAHMDDNbESAwE1AA5SEBAlUCCuAsJEwTIJApoABWRZ9lCRBQADPDzFYECMwEFEB5SIBAlYACOAsJ0wQIZAoogFXRZ1kCBBTAzDCzFQECMwEFUB4SIFAloADOQsI0AQKZAgpgVfRZlgABBTAzzGxFgMBMQAGUhwQIVAkogLOQME2AQKaAAlgVfZYlQEABzAwzWxEgMBNQAOUhAQJVAgrgLCRMEyCQKaAAVkWfZQkQUAAzw8xWBAjMBBRAeUiAQJWAAjgLCdMECGQKKIBV0WdZAgQUwMwwsxUBAjMBBVAeEiBQJaAAzkLCNAECmQIKYFX0WZYAAQUwM8xsRYDATEABlIcECFQJKICzkDBNgECmgAJYFX2WJUBAAcwMM1sRIDATUADlIQECVQIK4CwkTBMgkCmgAFZFn2UJEFAAM8PMVgQIzAQUQHlIgECVgAI4CwnTBAhkCiiAVdFnWQIEFMDMMLMVAQIzAQVQHhIgUCWgAM5CwjQBApkCCmBV9FmWAAEFMDPMbEWAwExAAZSHBAhUCfzqpef//vMvBgzcAXegKhAsS4BAj4CAE3DugDvgDvz6DvSkgU0JEKgS8OIX/u6AO+AOKIBVwWdZAgR+/PDiF/7ugDvgDiiA8pAAgTIBL37h7w64A+6AAlgWfdYlQMCLX/i7A+6AO6AASkMCBMoEvPiFvzvgDrgDCmBZ9FmXAAEvfuHvDrgD7oACKA0JECgT8OIX/u6AO+AOKIBl0WddAgS8+IW/O+AOuAMKoDQkQKBM4Fcv/tl/idKb6Zch9OZEs29x/n9CdCb3Zpo//9+5Sb+6P2WRYF0CBFoEFMDfiYz//bcKiALyO7fJ/fnO+9OSBfYkQKBMQAH8nchWAN0f9+dvgdQCWxYJ1iVAoEVAgAvw9ADfFJN3t+L+mzbn9Pd773ua9B8Bt6SePQkQ+K+AADnFwv3nAvw7/yO8zXO5f+rvJjfn9Pf7eX9RQYAAgUgBAfL5ANkE+7tT3X/T5pzuz73vaZL/d/6AiHzxW4oAAQIC/BTL958L8O8M8M1zuX/q7yY35/T3+3l/KUGAAIFIAQHy+QDZBPu7U91/0+ac7s+972mS/3f+gIh88VuKAAECAvwUy/efC/DvDPDNc7l/6u8mN+f09/t5fylBgACBSAEB8vkA2QT7u1Pdf9PmnO7Pve9pkv93/oCIfPFbigABAgL8FMv3nwvw7wzwzXO5f+rvJjfn9Pf7eX8pQYAAgUgBAfL5ANkE+7tT3X/T5pzuz73vaZL/d/6AiHzxW4oAAQIC/BTL958L8O8M8M1zuX/q7yY35/T3+3l/KUGAAIFIAQHy+QDZBPu7U91/0+ac7s+972mS/3f+gIh88VuKAAECAvwUy/efC/DvDPDNc7l/6u8mN+f09/t5fylBgACBSAEB8vkA2QT7u1Pdf9PmnO7Pve9pkv93/oCIfPFbigABAgL8FMv3nwvw7wzwzXO5f+rvJjfn9Pf7eX8pQYAAgUgBAfL5ANkE+7tT3X/T5pzuz73vaZL/d/6AiHzxW4oAAQIC/BTL958L8O8M8M1zuX/q7yY35/T3+3l/KUGAAIFIAQHy+QDZBPu7U91/0+ac7s+972mS/3f+gIh88VuKAAECAvwUy/efC/DvDPDNc7l/6u8mN+f09/t5fylBgACBSAEB8vkA2QT7u1Pdf9PmnO7Pve9pkv93/oCIfPFbigABAgL8FMv3nwvw7wzwzXO5f+rvJjfn9Pf7eX8pQYAAgUgBAfL5ANkE+7tT3X/T5pzuz73vaZL/d/6AiHzxW4oAAQIC/BTL958L8O8M8M1zuX/q7yY35/T3+3l/KUGAAIFIAQHy+QDZBPu7U91/0+ac7s+972mS/3f+gIh88VuKAAECAvwUy/efC/DvDPDNc7l/6u8mN+f09/t5fylBgACBSAEB8vkA2QT7u1Pdf9PmnO7Pve9pkv93/oCIfPFbigABAgL8FMv3nwvw7wzwzXO5f+rvJjfn9Pf7eX8pQYAAgUiBl6Hju/4pQCxYuAMZdyDyxW8pAgQICKmMkPIcPUd34DN3QEoQIEAgUkBofCY0uHJ1BzLuQOSL31IECBAQUhkh5Tl6ju7AZ+6AlCBAgECkgND4TGhw5eoOZNyByBe/pQgQICCkMkLKc/Qc3YHP3AEpQYAAgUgBofGZ0ODK1R3IuAORL35LESBAQEhlhJTn6Dm6A5+5A1KCAAECkQJC4zOhwZWrO5BxByJf/JYiQIAAAQIECBAgQIAAAQIECBAgQIAAAQIECBAgQIAAAQIECBAgQIAAAQIECBAgQIAAAQIECBAgQIAAAQIECBAgQIAAAQIECBAgQIAAAQIECBAgQIAAAQIECBAgQIAAAQIECBAgQIAAAQIECBAgQIAAAQIECBAgQIAAAQIECBAgQIAAAQIECBAgQIAAAQIECBAgQIAAAQIECBAgQIAAAQIECBAgQIAAAQL/rsB/AKIRl6ZwKHM1AAAAAElFTkSuQmCC";
                        var files = library.RootFolder.Files.Add(documentURL, Convert.FromBase64String(tempImageString), metaData, true);
                        files.Item[Constants.CONTENTTYPE_ID_TEXT] = Constants.CONTENT_TYPE;
                        files.Item.SystemUpdate(false);
                        var title = files.Item.Title;
                        var ctid = files.Item.ParentList.ContentTypes[0].Id;
                        var itemID = files.Item.ID;
                        var editURL = files.Item.Web.Site.WebApplication.GetResponseUri(Microsoft.SharePoint.Administration.SPUrlZone.Default).AbsoluteUri + files.Item.ParentList.DefaultEditFormUrl;
                        
                        sb.Append("AbsoluteUri=" + files.Item.Web.Site.WebApplication.GetResponseUri(Microsoft.SharePoint.Administration.SPUrlZone.Default).AbsoluteUri + CONSTANT_SEPARATOR);
                        sb.Append("DefaultEditFormUrl=" + files.Item.ParentList.DefaultEditFormUrl + CONSTANT_SEPARATOR);
                        sb.Append("editURL=" + editURL + CONSTANT_SEPARATOR);

                        var defaultViewURL = files.Item.Web.Site.RootWeb.Url + files.Item.ParentList.DefaultViewUrl;                        
                        sb.Append("defaultViewURL=" + defaultViewURL + CONSTANT_SEPARATOR);

                        var rootFolder = files.Item.ParentList.RootFolder.Name;                        
                        sb.Append("rootFolder=" + rootFolder + CONSTANT_SEPARATOR);

                        editFormURL = string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}", editURL, "?ID=", itemID, "&Source=", defaultViewURL, "&ContentTypeId=", ctid, "&RootFolder=", rootFolder);
                        sb.Append("editFormURL=" + editFormURL + CONSTANT_SEPARATOR);

                        // Log different parameters in sharepoint log
                        Utility.Log("MediaSilouplaod Parameters", sb.ToString(), "");

                        web.AllowUnsafeUpdates = webAllowUnsafeUpdates;
                        response = loginName + "|" + editFormURL;
                        site.Dispose();
                        web.Dispose();


                    }
                }

            }
            catch (Exception ex)
            {
                Utility.Log("MediaSilouplaod.cs-UploadDocument", ex.Message, ex.StackTrace);
                Utility.Log("MediaSilouplaod Parameters", sb.ToString(), "");
            }
            return response;
        }

        /// <summary>
        /// Funcation to add metadata to MediaSilo
        /// </summary>
        /// <param name="assetID">Asset ID</param>
        /// <param name="documentURL">Document URL in SP Library</param>
        /// <param name="loginName">Login Name</param>
        /// <param name="ListID">List ID</param>
        /// <param name="SiteURL">Site URL</param>
        /// <returns>Return success or fail</returns>
        [WebMethod]
        public static string AddMetadata(string assetID, string containerID, string documentURL, string MetaDataInfo, string ListID, string SiteURL, string siteCollectionURL)
        {
            string response = null;
            string loginName = MetaDataInfo.Split('|').First().Trim('"');
            Configuration oConfiguration = Utility.GetConfigObject(siteCollectionURL);
            var client = new WebClient();
            try
            {
                client = Utility.GetConfigurationHeader(client, oConfiguration);
                assetID = assetID.Split(new char[] { ':' }).Last();
                assetID = assetID.Replace("}", "").Replace("{", "").Replace("\"", "").Replace("\\", "").Replace(" ", "");
                StringBuilder sbValue = new StringBuilder();
                SiteURL = SiteURL.Trim('"');
                ListID = ListID.Trim('"');
                documentURL = documentURL.Trim('"');
                string SiteURLkey = Constants.SITE_URL_KEY;
                string SiteURLvalue = SiteURL;

                string ListIDkey = Constants.LIST_ID_KEY;
                string ListIDvalue = ListID;

                string DocURLkey = Constants.DOC_URL_KEY;
                string DocURLValue = documentURL;

                string CreatedBykey = Constants.CREATEDBY_KEY;
                string CreatedByValue = string.Empty;
                if (!string.IsNullOrEmpty(loginName))
                {
                    CreatedByValue = loginName.Trim('"');
                }
                else
                {
                    CreatedByValue = "NA";
                }

                string containerIDKey = Constants.Container_ID;
                string containerIDValue = containerID;
                StringBuilder sbJson = new StringBuilder();
                sbJson.AppendFormat(@"{0}{1}""key"": ""{2}"",""value"": ""{3}""{4},{5}""key"": ""{6}"",""value"": ""{7}""{8},{9}""key"": ""{10}"",""value"": ""{11}""{12},{13}""key"": ""{14}"",""value"": ""{15}""{16},{17}""key"": ""{18}"",""value"": ""{19}""{20}{21}", "[", "{", SiteURLkey, SiteURLvalue, "}", "{", CreatedBykey, CreatedByValue, "}", "{", ListIDkey, ListIDvalue, "}", "{", DocURLkey, DocURLValue, "}", "{", containerIDKey, containerIDValue, "}", "]");
                string requestJson = sbJson.ToString();

                string URL = string.Format("{0}{1}{2}", oConfiguration.MetaDataURL, assetID, Constants.METADATA_POSTFIX);
                response = client.UploadString(URL, Constants.HTTP_POST_METHOD, requestJson);
                response = MetaDataInfo.Split('|').Last().Trim('"');

            }
            catch (Exception ex)
            {
                Utility.Log("MediaSilouplaod.cs-AddMetadata", ex.Message, ex.StackTrace);
            }
            finally
            {
                if (client != null) ((IDisposable)client).Dispose();
            }
            return response;
        }

        /// <summary>
        /// funcation to Delete Asset from MediaSilo
        /// </summary>
        /// <param name="assetID">Asset ID</param>
        /// <returns>Return success or fail</returns>
        [WebMethod]
        public static string DeleteAsset(string assetID, string siteCollectionURL)
        {
            string response = null;
            Configuration oConfiguration = Utility.GetConfigObject(siteCollectionURL);
            var client = new WebClient();
            try
            {
                client = Utility.GetConfigurationHeader(client, oConfiguration);
                assetID = assetID.Split(new char[] { ':' }).Last();
                assetID = assetID.Replace("}", "").Replace("{", "").Replace("\"", "").Replace("\\", "").Replace(" ", "");
                string URL = string.Format("{0}{1}", oConfiguration.AssetURL + "/", assetID);
                response = client.UploadString(URL, Constants.HTTP_DELETE_METHOD, "");
            }
            catch (Exception ex)
            {
                Utility.Log("MediaSilouplaod.cs-DeleteAsset", ex.Message, ex.StackTrace);
            }
            finally
            {
                if (client != null) ((IDisposable)client).Dispose();
            }
            return response;
        }

        /// <summary>
        /// Funcation to Get FolderSturcture for MediaSilo
        /// </summary>
        /// <param name="webURL">Web URL</param>
        /// <param name="documentListID">List ID</param>
        /// <returns>Key value pair</returns>
        private static Dictionary<int, string> GetSubSiteProp(string webURL, string documentListID, string siteCollectionURL)
        {
            Configuration oConfiguration = Utility.GetConfigObject(siteCollectionURL);
            Dictionary<int, string> webProp = new Dictionary<int, string>();
            webURL = webURL.Trim('"');
            documentListID = documentListID.Trim('"');
            Guid guid = new Guid(documentListID);
            string listTitle = string.Empty;
            string webTitle = string.Empty;
            int iCount = 0;
            try
            {
                Microsoft.SharePoint.SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    using (SPSite oSite = new SPSite(webURL))
                    {
                        using (SPWeb oWeb = oSite.OpenWeb())
                        {
                            SPList pics = oWeb.Lists[guid];
                            listTitle = Constants.LIB_PREFIX + pics.Title;
                            //Adding List Title to Dictionary
                            webProp.Add(iCount, listTitle);

                            if (oWeb.ParentWeb != null)
                            {
                                iCount = iCount + 1;
                                //Adding current web Title to Dictionary
                                webProp.Add(iCount, oWeb.Title);
                                SPWeb countParent = oWeb.ParentWeb;
                                while (countParent != null)
                                {
                                    if (!countParent.IsRootWeb)
                                    {
                                        iCount = iCount + 1;
                                        //Adding sub web Title to Dictionary
                                        webProp.Add(iCount, countParent.Title);

                                    }
                                    countParent = countParent.ParentWeb;
                                }
                            }
                            if (!string.IsNullOrEmpty(oConfiguration.RootFolder))
                            {
                                iCount = iCount + 1;
                                //Adding Title from config list to Dictionary
                                webProp.Add(iCount, oConfiguration.RootFolder);
                            }
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                Utility.Log("MediaSilouplaod.cs-GetSubSiteProp", ex.Message, ex.StackTrace);
            }
            return webProp;
        }

        /// <summary>
        /// Funcation to MediaSilo Folder ID
        /// </summary>
        /// <param name="webURL">Current Web URL</param>
        /// <param name="listID">List ID</param>
        /// <returns>Return Folder ID</returns>
        [WebMethod]
        public static string GetMediaSiloFolderID(string webURL, string listID, string siteCollectionURL)
        {
            Dictionary<int, string> webProp = GetSubSiteProp(webURL, listID, siteCollectionURL);
            string folderID = string.Empty;
            try
            {
                if (webProp.Count > 0 && webProp != null)
                {
                    int noOfSubFolder = 1;
                    for (int count = webProp.Count - 1; count >= 0; count--)
                    {
                        folderID = CheckMediaSiloFolder(webProp[count], folderID, ref noOfSubFolder, siteCollectionURL);
                    }
                }
            }
            catch (Exception ex)
            {
                Utility.Log("MediaSilouplaod.cs-GetMediaSiloFolderID", ex.Message, ex.StackTrace);
            }
            return folderID;
        }

        /// <summary>
        /// Funcation to Check Folder exist or not in MediaSilo
        /// </summary>
        /// <param name="folderName">Folder Name</param>
        /// <param name="parentFolder">Parent Folder ID</param>
        /// <param name="noOfSubFolder">Number of subfolder</param>
        /// <returns>Return success or fail</returns>
        private static string CheckMediaSiloFolder(string folderName, string parentFolder, ref int noOfSubFolder, string siteCollectionURL)
        {
            Configuration oConfiguration = Utility.GetConfigObject(siteCollectionURL);
            string response = null;
            Boolean folderExist = false;

            var client = new WebClient();
            try
            {
                if (noOfSubFolder > 0)
                {
                    client = Utility.GetConfigurationHeader(client, oConfiguration);
                    string URL = string.Empty;
                    if (!string.IsNullOrEmpty(parentFolder))
                    {
                        URL = string.Format("{0}{1}{2}", oConfiguration.SubFolderURL, parentFolder, Constants.SUBFOLDERURL_POSTFIX);
                    }
                    else
                    {
                        URL = string.Format("{0}{1}{2}", oConfiguration.APIURL, oConfiguration.ProjectID, Constants.FOLDERURL_POSTFIX);
                    }
                    response = client.DownloadString(URL);
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    FolderName[] msfolderName = serializer.Deserialize<FolderName[]>(response);
                    foreach (FolderName fname in msfolderName)
                    {
                        if (fname.name.Equals(folderName, StringComparison.InvariantCultureIgnoreCase))
                        {
                            parentFolder = fname.id;
                            noOfSubFolder = fname.folderCount;
                            folderExist = true;
                            break;
                        }
                    }
                }
                if (!folderExist)
                {
                    parentFolder = CreateFolder(folderName, oConfiguration.ProjectID, parentFolder, siteCollectionURL);
                    noOfSubFolder = 0;
                }

            }
            catch (Exception ex)
            {
                Utility.LogException("MediasiloUpload.cs-CheckMediaSiloFolder", ex);
            }
            finally
            {
                if (client != null) ((IDisposable)client).Dispose();
            }
            return parentFolder;

        }

        /// <summary>
        /// Funcation to create new folder in MediaSilo API
        /// </summary>
        /// <param name="folderName">Folder Name</param>
        /// <param name="projectID">Project ID</param>
        /// <param name="parentID">Parent ID</param>
        /// <returns>Folder ID of created folder</returns>
        public static string CreateFolder(string folderName, string projectID, string parentID, string siteCollectionURL)
        {
            string response = null;
            Configuration oConfiguration = Utility.GetConfigObject(siteCollectionURL);
            var client = new WebClient();
            try
            {
                client = Utility.GetConfigurationHeader(client, oConfiguration);
                StringBuilder sbJson = new StringBuilder();
                Dictionary<string, object> dictobj = new Dictionary<string, object>();
                if (!string.IsNullOrEmpty(parentID) && !string.IsNullOrEmpty(projectID))
                {
                    dictobj.Add("name", folderName.Trim('"'));
                    dictobj.Add("projectId", projectID.Trim('"'));
                    dictobj.Add("parentId", parentID.Trim('"'));
                }
                else if (!string.IsNullOrEmpty(projectID))
                {
                    dictobj.Add("name", folderName.Trim('"'));
                    dictobj.Add("projectId", projectID.Trim('"'));
                }
                else
                {
                    dictobj.Add("name", folderName.Trim('"'));
                }
                string requestJson = Utility.GetJson(dictobj);
                string createFolderURL = oConfiguration.SubFolderURL.TrimEnd('/');
                response = client.UploadString(createFolderURL, Constants.HTTP_POST_METHOD, requestJson);
                if (!string.IsNullOrEmpty(response))
                {
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    FolderName msfolderName = serializer.Deserialize<FolderName>(response);
                    response = msfolderName.id.Replace("\"", "");
                }
            }
            catch (Exception ex)
            {
                Utility.LogException("MediasiloUpload.cs-CreateFolder", ex);
            }
            finally
            {
                if (client != null) ((IDisposable)client).Dispose();
            }
            return response;
        }

        [WebMethod]
        public static string CheckPermission(string itemID, string listID, string siteCollectionURL, string siteURL, string actionType, string objectType)
        {
            string returnValue = "Fail";
            try
            {
                Configuration oConfiguration = Utility.GetConfigObject(siteCollectionURL);
                siteURL = siteURL.Trim('"');
                objectType = objectType.Trim('"');
                actionType = actionType.Trim('"');
                itemID = itemID.Trim('"');
                SPSecurity.RunWithElevatedPrivileges(delegate()
                        {
                            SPPermissionInfo oInfo = null;
                            using (SPSite oSite = new SPSite(siteURL))
                            {
                                using (SPWeb oWeb = oSite.OpenWeb())
                                {
                                    listID = listID.Trim('"');
                                    Guid guid = new Guid(listID);
                                    SPList oList = oWeb.Lists[guid];

                                    if (objectType.Equals("List", StringComparison.InvariantCultureIgnoreCase) && actionType.Equals("UploadVideoPermission", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        oInfo = oList.GetUserEffectivePermissionInfo(oWeb.CurrentUser.LoginName);
                                        returnValue = CheckCustomPermissionLevel(oInfo, oConfiguration.UploadVideoPermission);
                                    }
                                    else if (objectType.Equals("Item", StringComparison.InvariantCultureIgnoreCase))
                                    {
                                        SPItem oItem = oList.GetItemById(Convert.ToInt32(itemID));
                                        oInfo = oItem.GetUserEffectivePermissionInfo(oWeb.CurrentUser.LoginName);
                                        if (actionType.Equals("DownloadVideoPermission", StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            returnValue = CheckCustomPermissionLevel(oInfo, oConfiguration.DownloadVideoPermission);
                                        }
                                        else if (actionType.Equals("DeleteVideoPermission", StringComparison.InvariantCultureIgnoreCase))
                                        {
                                            returnValue = CheckCustomPermissionLevel(oInfo, oConfiguration.DeleteVideoPermission);
                                        }
                                    }

                                }
                            }
                        });
            }
            catch (Exception ex)
            {
                Utility.LogException("MediasiloUpload.cs-CheckPermission", ex);
            }
            return returnValue;
        }

        /// <summary>
        /// Funcation to Check custom level permission
        /// </summary>
        /// <param name="oInfo">Information of current user permission</param>
        /// <param name="permissionLevel">PermissionLevel</param>
        /// <returns>Does user has permission or not</returns>
        private static string CheckCustomPermissionLevel(SPPermissionInfo oInfo, string permissionLevel)
        {
            if (!string.IsNullOrEmpty(permissionLevel))
            {
                foreach (SPRoleAssignment assgn in oInfo.RoleAssignments)
                {
                    foreach (SPRoleDefinition spdef in assgn.RoleDefinitionBindings)
                    {
                        if (Convert.ToString(spdef.Name).Equals(permissionLevel, StringComparison.InvariantCultureIgnoreCase))
                        {
                            return "Success";
                        }
                    }
                }
            }
            else
            {
                return "Success";
            }
            return "Fail";
        }
    }



    /// <summary>
    /// Clase to store name,id and folder count
    /// </summary>
    public class FolderName
    {
        public string name { get; set; }
        public string id { get; set; }
        public int folderCount { get; set; }
    }
}
