﻿using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Configuration;
using System.Web.Hosting;
using Viacom.AdSpace.Shared;


namespace Viacom.Adspace.HttpModule
{
    class RestrictedIP : IHttpModule
    {
        private HttpContext context = null;


        public RestrictedIP() { }

        #region [ HTTP Handler]
        public void Init(HttpApplication context)
        {

            context.PreSendRequestContent += new EventHandler(context_HandlePreSendRequestContent);
        }


        /// <summary>
        /// End Request handler
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">event argument</param>
        void context_HandlePreSendRequestContent(object sender, EventArgs e)
        {
            context = ((HttpApplication)sender).Context;
            string ipAddress = GetUserIP(context);

            string IP4Address = String.Empty;

            foreach (IPAddress IPA in Dns.GetHostAddresses(GetUserIP(context)))
            {
                if (IPA.AddressFamily.ToString() == HttpModuleConstants.INTER_NETWORK)
                {
                    IP4Address = IPA.ToString();
                    break;
                }
            }

            foreach (IPAddress IPA in Dns.GetHostAddresses(Dns.GetHostName()))
            {
                if (IPA.AddressFamily.ToString() == HttpModuleConstants.INTER_NETWORK)
                {
                    IP4Address = IPA.ToString();
                    break;
                }
            }
            string SiteUrl = GetAppKeyValue(HttpModuleConstants.SITEURL);

            if (!string.IsNullOrEmpty(SiteUrl))
            {
                RestrictTheUser(SiteUrl, ipAddress);
            }
        }
        #endregion

        #region [Private Functions]
        /// <summary>
        /// Gets the app key value pair
        /// </summary>
        /// <param name="key">key</param>
        /// <returns>value</returns>
        private static string GetAppKeyValue(string key)
        {
            string returnValue = string.Empty;

            String _siteName = HostingEnvironment.ApplicationHost.GetSiteName();
            Configuration config = WebConfigurationManager.OpenWebConfiguration("/", _siteName);

            if (config.AppSettings.Settings.AllKeys.Contains(key))
            {
                returnValue = config.AppSettings.Settings[key].Value;
            }

            return returnValue;

        }

        /// <summary>
        /// Gets the Restricted urls which are active
        /// </summary>
        /// <param name="SiteUrl">SiteUrl</param>
        /// <param name="ipAddress">Ip Address</param>
        private void RestrictTheUser(string SiteUrl, string ipAddress)
        {
            try
            {
                string restrictedUrlCacheKey = HttpModuleConstants.RESTRICTED_URL;
                SPListItemCollection restrictedURLs = null;

                //If the ip which is hit does not fall in the range of IP's. It is an external IP.

                if (!CheckIpAddress(SiteUrl, ipAddress))
                {

                    if (!CacheManager.SingleInstance.KeyExists(restrictedUrlCacheKey))
                    {
                        SPSecurity.RunWithElevatedPrivileges(delegate
                        {
                            using (SPSite site = new SPSite(SiteUrl))
                            {
                                using (SPWeb web = site.OpenWeb())
                                {
                                    SPList restrictedURLsList = web.Lists.TryGetList(HttpModuleConstants.RESTRICTED_URLS_LIST);

                                    if (restrictedURLsList != null)
                                    {
                                        try
                                        {

                                            SPQuery query = new SPQuery();
                                            query.Query = HttpModuleConstants.QUERY_ONACTIVE;
                                            restrictedURLs = restrictedURLsList.GetItems(query);
                                            CacheManager.SingleInstance.Add(restrictedURLs, restrictedUrlCacheKey);
                                        }
                                        catch (Exception ex)
                                        {
                                            ExceptionHandling.LogToSPDiagnostic(ex);
                                        }
                                    }

                                }
                            }
                        });
                    }
                    else
                    {
                        restrictedURLs = (SPListItemCollection)CacheManager.SingleInstance.GetCache(restrictedUrlCacheKey);
                    }

                    string requestedPath = context.Request.Url.AbsolutePath.ToLower();
                    bool restrict = false;
                    foreach (SPListItem restrictedURL in restrictedURLs)
                    {
                        string restrictedUrlStr = Convert.ToString(restrictedURL[HttpModuleConstants.TITLE]);
                        string restrictedUrlPath = restrictedUrlStr.ToLower();
                        if (requestedPath.IndexOf(restrictedUrlPath) != -1)
                        {
                            restrict = true;                           
                           SPUtility.Redirect(SPUtility.AccessDeniedPage, SPRedirectFlags.RelativeToLayoutsPage, context);
                           

                        }
                        if (restrict)
                            break;
                    }

                }
            }
            catch (Exception ex)
            {
                ExceptionHandling.LogToSPDiagnostic(ex);
            }
        }

        /// <summary>
        /// Function to check if an IP is in the given list
        /// </summary>
        /// <param name="siteUrl">Site Url</param>
        /// <param name="ipAddress">Ip Address</param>
        /// <returns>If a given IP is in the list</returns>
        private bool CheckIpAddress(string siteUrl, string ipAddress)
        {
            string restrictedIPCacheKey = HttpModuleConstants.RESTRICTED_IP;
            var rangeList = new List<IpRange>();
            if (!CacheManager.SingleInstance.KeyExists(restrictedIPCacheKey))
            {
                SPSecurity.RunWithElevatedPrivileges(delegate
                      {
                          using (SPSite site = new SPSite(siteUrl))
                          {
                              using (SPWeb web = site.OpenWeb())
                              {
                                  SPList IpAddresses = web.Lists.TryGetList(HttpModuleConstants.IPCONFIGURATION_LIST);
                                  if (IpAddresses != null)
                                  {
                                      SPQuery query = new SPQuery();
                                      query.Query = HttpModuleConstants.QUERY_ONACTIVE;
                                      SPListItemCollection collection = IpAddresses.GetItems(query);
                                      if (collection.Count > 0)
                                      {
                                          foreach (SPItem item in collection)
                                          {
                                              if ((item[HttpModuleConstants.IPADDRESS_STARTRANGE] != null) && (item[HttpModuleConstants.IPADDRESS_ENDRANGE] != null))
                                                  rangeList.Add(new IpRange(IPAddress.Parse(item[HttpModuleConstants.IPADDRESS_STARTRANGE].ToString()), IPAddress.Parse(item[HttpModuleConstants.IPADDRESS_ENDRANGE].ToString())));

                                          }
                                          CacheManager.SingleInstance.Add(rangeList, restrictedIPCacheKey);
                                      }
                                  }
                              }
                          }
                      });

            }
            else
            {
                rangeList = CacheManager.SingleInstance.GetCache(restrictedIPCacheKey) as List<IpRange>;
            }
            bool result = CheckIsIpPublic(ipAddress, rangeList);
            return result;
        }

        #endregion


        #region [Public Functions]
        public void Dispose() { /* clean up */ }

        /// <summary>
        /// CHecks if the given IP is in the given Range
        /// </summary>
        /// <param name="adress">IP Adress</param>
        /// <param name="rangeList">Range List</param>
        /// <returns>If an IP is in given Range or Not</returns>
        public bool CheckIsIpPublic(string adress, List<IpRange> rangeList)
        {
            foreach (var range in rangeList)
            {
                List<int> adressInt = adress.Split('.').Select(str => int.Parse(str)).ToList();
                List<int> lowerInt = range.LowerIP.ToString().Split('.').Select(str => int.Parse(str)).ToList();
                List<int> upperInt = range.UpperIP.ToString().Split('.').Select(str => int.Parse(str)).ToList();

                if (adressInt[0] >= lowerInt[0] && adressInt[0] < upperInt[0])
                {
                    return true;
                }
                else if (adressInt[0] >= lowerInt[0] && adressInt[0] == upperInt[0])
                {
                    if (adressInt[1] >= lowerInt[1] && adressInt[1] < upperInt[1])
                    {
                        return true;
                    }
                    else if (adressInt[1] >= lowerInt[1] && adressInt[1] == upperInt[1])
                    {
                        if (adressInt[2] >= lowerInt[2] && adressInt[2] < upperInt[2])
                        {
                            return true;
                        }
                        else if (adressInt[2] >= lowerInt[2] && adressInt[2] == upperInt[2])
                        {
                            if (adressInt[3] >= lowerInt[3] && adressInt[3] <= upperInt[3])
                            {
                                return true;
                            }
                        }

                    }

                }
            }
            return false;
        }

        public string GetUserIP(HttpContext context)
        {
            var ip = (context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null
            && context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != "")
            ? context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"]
            : context.Request.ServerVariables["REMOTE_ADDR"];
            if (ip.Contains(","))
                ip = ip.Split(',').First().Trim();
            return ip;
        }

        #endregion

    }

    public struct IpRange
    {
        public IPAddress LowerIP;
        public IPAddress UpperIP;
        public IpRange(IPAddress lowerIP, IPAddress upperIP)
        {
            LowerIP = lowerIP;
            UpperIP = upperIP;
        }
    }


}




