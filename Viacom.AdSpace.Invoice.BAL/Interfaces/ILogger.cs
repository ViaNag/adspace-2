﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.Invoice.BAL
{
    public interface ILogger
    {
        void LogInfo(string Infotext);
        void LogError(string error);
        void LogError(string error,Exception ex);
    }
}
