﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.Invoice.BAL.Interfaces
{
    public interface IBrand
    {
        string BrandValue { get; set; }

    }
    public interface ICurrentStatus
    {
        string ItemID { get; set; }
        string CurrentFormStatus { get; set; }
        string NextFormStatus { get; set; }
        string NextRequestChangeFormStatus { get; set; }
        string CurrentStateGroup { get; set; }
        string NextStateGroup { get; set; }
        bool IsUserMemberofNextApproverGroup { get; set; }
        string IARequestCurrentStateListCol { get; set; }
        string IARequestCurrentStateListDateCol { get; set; }
        string IARequestNextStateListCol { get; set; }
        string IARequestNextStateListDateCol { get; set; }
        string IARequestNextChangeStateListCol { get; set; }
        string IARequestNextChangeStateListDateCol { get; set; }
        double ThresholdAmount { get; set; }
        bool IsBillingVisibleForNextStage { get; set; }
        bool IsNextStageLastStage { get; set; }
        string CurrentStatusMessage { get; set; }
    }
    public interface IInvoiceDetails
    {
        string Brand_Year { get; set; }
        string Network_Call_Sign { get; set; }
        string Agency_Current_Invoicing { get; set; }
        string Advertiser_Current { get; set; }
        string Traffic_Deal_Code { get; set; }
        string Invoice_ID { get; set; }
        string Invoice_Rev_No { get; set; }
        string Brand { get; set; }
        string Traffic_Deal_Start_Date { get; set; }
        string Traffic_Deal_End_Date { get; set; }
    }

    public interface IInvoiceID
    {
        string InvoiceIdValue { get; set; }
    }

    public interface INetwork
    {
        string ID { get; set; }
        string NetworkValue { get; set; }
    }
    public interface ITrafficDeal
    {
        string TrafficDealValue { get; set; }
    }

    public interface ITrafficDealDetails 
    {
        string Agency { get; set; }
        string Advertiser { get; set; }
    }
}
