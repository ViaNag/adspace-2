﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Viacom.AdSpace.Invoice.BAL
{
    interface IBillingConfiguration
    {
        string SPSMTPserver { get; }
        string MailFromEmailAddress { get; }
    }
}
