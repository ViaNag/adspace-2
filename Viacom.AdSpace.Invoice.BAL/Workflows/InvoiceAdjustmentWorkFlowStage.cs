﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvoiceBase = Viacom.AdSpace.Invoice.BAL;

namespace Viacom.AdSpace.Invoice.BAL.Workflows
{
   public class InvoiceAdjustmentWorkflowStage:IWorkflowStage
   {
       public string ApprovalStage { get; set; }
       public int ApprovalStageOrder { get; set; }
       public string NextApprovalStage { get; set; }
       public string StageGroupColumn { get; set; }
       public string IARequestListCol { get; set; }
       public string IARequestListDateCol { get; set; }
       public string ChangeRequestNextStage { get; set; }
       public string ChangeRequestIARListCol { get; set; }
       public string ChangeRequestIARListDateCol { get; set; }
       public double ThresholdAmount { get; set; }
       public string ThresholdBreachNextStage { get; set; }
       public bool IsBillingVisible { get; set; }
       public bool IsFinalStage { get; set; }
       public string CurrentStatusMessage { get; set; }
   }
}
