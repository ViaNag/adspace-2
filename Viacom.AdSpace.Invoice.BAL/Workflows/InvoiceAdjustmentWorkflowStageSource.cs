﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.Invoice.BAL;
using Microsoft.SharePoint;

namespace Viacom.AdSpace.Invoice.BAL.Workflows
{
    public class InvoiceAdjustmentInvoiceAdjustmentWorkflowStageSource : IWorkflowStageSource<InvoiceAdjustmentWorkflowStage>
    {
        public virtual Logging.ULSLogger InvoiceLogger
        {
            get
            {
                return new Logging.ULSLogger();
            }
        }

        public List<InvoiceAdjustmentWorkflowStage> Stages
        {
            get 
            {
                
                InvoiceAdjustmentWorkflowStage stage = null;
                List<InvoiceAdjustmentWorkflowStage> stages = new List<InvoiceAdjustmentWorkflowStage>();
                try
                {
                    SPWeb web = SPContext.Current.Web;
                    SPList list = web.Lists[Constants.IAAPPROVALSTAGECONFIGLIST];
                    SPListItemCollection items = list.GetItems();
                    foreach (SPListItem item in items)
                    {
                        if (item[Constants.WFCURRENTSTAGEKEYFIELD] != null)
                        {
                            stage = new InvoiceAdjustmentWorkflowStage();
                            stage.ApprovalStage = item[Constants.WFCURRENTSTAGEKEYFIELD].ToString();
                            if (item[Constants.ORDERNOFIELD] != null)
                            {
                                if (item[Constants.ORDERNOFIELD].ToString().IndexOf('.') > -1)
                                {
                                    double orderNo = Double.Parse(item[Constants.ORDERNOFIELD].ToString());
                                    stage.ApprovalStageOrder = Convert.ToInt32(orderNo);
                                }
                                else
                                {
                                    stage.ApprovalStageOrder = Int32.Parse(item[Constants.ORDERNOFIELD].ToString());
                                }
                            }
                            if (item[Constants.NEXTSTAGEKEYFIELD] != null)
                            {
                                stage.NextApprovalStage = item[Constants.NEXTSTAGEKEYFIELD].ToString();
                            }
                            if (item[Constants.APPROVALCOLUMNNAMEFIELD] != null)
                            {
                                stage.StageGroupColumn = item[Constants.APPROVALCOLUMNNAMEFIELD].ToString();
                            }
                            if (item[Constants.CURRENTSTAGEPERSONGROUPFIELD] != null)
                            {
                                stage.IARequestListCol = item[Constants.CURRENTSTAGEPERSONGROUPFIELD].ToString();
                            }
                            if (item[Constants.CURRENTSTAGEDATETIMEFIELD] != null)
                            {
                                stage.IARequestListDateCol = item[Constants.CURRENTSTAGEDATETIMEFIELD].ToString();
                            }
                            if (item[Constants.CHANGEREQUESTSTAGEKEYFIELD] != null)
                            {
                                SPFieldCalculated CRStageField = list.Fields.GetFieldByInternalName(Constants.CHANGEREQUESTSTAGEKEYFIELD) as SPFieldCalculated;
                                stage.ChangeRequestNextStage = CRStageField.GetFieldValueAsText(item[Constants.CHANGEREQUESTSTAGEKEYFIELD]);
                            }
                            if (item[Constants.CHANGEREQSTAGEPERSONGROUPFIELD] != null)
                            {
                                SPFieldCalculated CRPersonField = list.Fields.GetFieldByInternalName(Constants.CHANGEREQSTAGEPERSONGROUPFIELD) as SPFieldCalculated;
                                stage.ChangeRequestIARListCol = CRPersonField.GetFieldValueAsText(item[Constants.CHANGEREQSTAGEPERSONGROUPFIELD]);
                            }
                            if (item[Constants.CHANGEREQSTAGEDATETIMEFIELD] != null)
                            {
                                SPFieldCalculated CRDateField = list.Fields.GetFieldByInternalName(Constants.CHANGEREQSTAGEDATETIMEFIELD) as SPFieldCalculated;
                                stage.ChangeRequestIARListDateCol = CRDateField.GetFieldValueAsText(item[Constants.CHANGEREQSTAGEDATETIMEFIELD]);
                            }
                            if (item[Constants.THRESHOLDVALUEFIELD] != null)
                            {
                                if (item[Constants.THRESHOLDVALUEFIELD].ToString().IndexOf('.') > -1)
                                {
                                    stage.ThresholdAmount = Double.Parse(item[Constants.THRESHOLDVALUEFIELD].ToString());
                                }
                                else
                                {
                                    stage.ThresholdAmount = Int32.Parse(item[Constants.THRESHOLDVALUEFIELD].ToString());
                                }
                            }
                            if (item[Constants.THRESHOLDSTAGEKEYFIELD] != null)
                            {
                                stage.ThresholdBreachNextStage = item[Constants.THRESHOLDSTAGEKEYFIELD].ToString();
                            }
                            if (list.Fields.ContainsField(Constants.ISBILLINGVISIBLEFIELD))
                            {
                                if (item[Constants.ISBILLINGVISIBLEFIELD] != null)
                                {
                                    stage.IsBillingVisible = Boolean.Parse(item[Constants.ISBILLINGVISIBLEFIELD].ToString());
                                }
                            }
                            else {
                                stage.IsBillingVisible = false;
                            }
                            if (item[Constants.CURRENTSTATUSMESSAGEFIELD] != null)
                            {
                                stage.CurrentStatusMessage = item[Constants.CURRENTSTATUSMESSAGEFIELD].ToString();
                            }
                            if (item[Constants.NEXTSTAGEKEYFIELD] == null && item[Constants.THRESHOLDSTAGEKEYFIELD] == null)
                            {
                                stage.IsFinalStage = true;
                            }
                            else
                            {
                                stage.IsFinalStage = false;
                            }
                            stages.Add(stage);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }                

                return stages;
            }
        }

        public InvoiceMethodResponse<string> getCurrentStatusMessage(string stageName, string adjAmount)
        {
            InvoiceMethodResponse<string> actionresponse = new InvoiceMethodResponse<string>();
            actionresponse.Data = null;
            Double adjAmt = Math.Abs(Convert.ToDouble(adjAmount));
            try
            {
                string currentStatusMessageStage = null;
                List<InvoiceAdjustmentWorkflowStage> wfStages = new InvoiceAdjustmentInvoiceAdjustmentWorkflowStageSource().Stages;
                foreach (InvoiceAdjustmentWorkflowStage wfstage in wfStages)
                {
                    if (wfstage.ApprovalStage.Equals(stageName, StringComparison.CurrentCultureIgnoreCase))
                    {
                        if (wfstage.ThresholdAmount != 0 && adjAmt >= wfstage.ThresholdAmount)
                        {
                            currentStatusMessageStage = wfstage.ThresholdBreachNextStage;
                        }
                        else
                        {
                            currentStatusMessageStage = wfstage.NextApprovalStage;                            
                        }
                        break;
                    }
                }
                if (currentStatusMessageStage != null)
                {
                    foreach (InvoiceAdjustmentWorkflowStage wfstage in wfStages)
                    {
                        if (wfstage.ApprovalStage.Equals(currentStatusMessageStage, StringComparison.CurrentCultureIgnoreCase))
                        {
                            actionresponse.Data = wfstage.CurrentStatusMessage;
                            actionresponse.ActionSuccessful = true;
                            break;
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                InvoiceLogger.LogError("getCurrentStatusMessage: Internal Error", ex);
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;
        }
    }

}
