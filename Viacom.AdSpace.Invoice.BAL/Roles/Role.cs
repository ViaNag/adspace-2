﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvoiceBase = Viacom.AdSpace.Invoice.BAL;

namespace Viacom.AdSpace.Invoice.BAL.Roles
{
    public class Role:InvoiceBase.IRole
    {
        string _title;
        public string Title
        {
            get { return _title; }            
        }

        string _id;
        public string ID
        {
            get { return _id; }            
        }

        string _assoclistcolumn;
        public string AssociatedListColumn
        {
            get { return _assoclistcolumn; }
        }

        public Role(string title,string id,string asscolistcolumn)
        {
            _title = title;
            _id = id;
            _assoclistcolumn = asscolistcolumn;
        }

        
    }

    public class RoleSource : InvoiceBase.IRoleSource
    {
        public List<InvoiceBase.IRole> Roles
        {
            get
            {
                List<InvoiceBase.IRole> _roles=new List<InvoiceBase.IRole>();
                
                Workflows.InvoiceAdjustmentInvoiceAdjustmentWorkflowStageSource wfsrc = new Workflows.InvoiceAdjustmentInvoiceAdjustmentWorkflowStageSource();
                foreach(Workflows.InvoiceAdjustmentWorkflowStage wfstg in wfsrc.Stages )
                {
                    _roles.Add(new Role(wfstg.StageGroupColumn, wfstg.StageGroupColumn, wfstg.StageGroupColumn));
                }

                return _roles; 
            }
        }

        public Role GetRoleByName(string name)
        {
            return Roles.First(x => x.Title.ToLower() == name.ToLower()) as Role;
        }

        public Role GetRoleByListCol(string listcol)
        {
            return Roles.First(x => ((Role)x).AssociatedListColumn.ToLower() == listcol.ToLower()) as Role;
        }
    }

}
