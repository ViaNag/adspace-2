﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;

namespace Viacom.AdSpace.Invoice.BAL.Email
{
    class BillingConfiguration : IBillingConfiguration
    {
        string _SPSMTPserver = null;
        string _MailFromEmailAddress = null;

        public virtual Logging.ULSLogger InvoiceLogger
        {
            get
            {
                return new Logging.ULSLogger();
            }
        }

        public string SPSMTPserver {
            get 
            {
                try
                {
                    SPSite site = SPContext.Current.Site;
                    _SPSMTPserver = site.WebApplication.OutboundMailServiceInstance.Parent.Name;
                }
                catch (Exception e) {
                    InvoiceLogger.LogError("BillingConfiguration - Could not get SMTP Server details", e);
                    throw e;
                }
                return _SPSMTPserver;
            }
        }
        public string MailFromEmailAddress
        {
            get
            {
                try
                {
                    SPSite site = SPContext.Current.Site;
                    _MailFromEmailAddress = site.WebApplication.OutboundMailSenderAddress;
                }
                catch (Exception e)
                {
                    InvoiceLogger.LogError("BillingConfiguration - Could not get Out Bound Email Address", e);
                    throw e;
                }
                return _MailFromEmailAddress;
            }
        }
    }
}
