﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Import Namespace="Microsoft.SharePoint.ApplicationPages" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HttpHeader.aspx.cs" Inherits="Viacom.AdSpace.HTTPHeader.POC.Layouts.Viacom.AdSpace.HTTPHeader.POC.HttpHeader" DynamicMasterPageFile="~masterurl/default.master" %>

<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">

</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <div><h2>Context Details</h2>
    </br>
    <div>
    <asp:Label ID="LabelUser" runat="server" Text="Label"><b>UserName:</b></asp:Label>
    <asp:Label ID="labelUser1" runat="server" Text="Label"></asp:Label>
    </div></br>
    <div>
    <asp:Label ID="LabelUrl" runat="server" Text="Label"><b>URL:</b></asp:Label>
    <asp:Label ID="LabelUrl1" runat="server" Text="Label"></asp:Label>
    </div></br>
    <div>
    <asp:Label ID="LabelHA" runat="server" Text="Label"><b>Host Address:</b></asp:Label>
    <asp:Label ID="LabelHA1" runat="server" Text="Label"></asp:Label>
    </div></br>
    <div>
    <asp:Label ID="LabelBrowser" runat="server" Text="Label"><b>Browser:</b></asp:Label>
    <asp:Label ID="LabelBrowser1" runat="server" Text="Label"></asp:Label>
    </div>
    </div>
    </br>
    <div><h2>Restricted IP</h2>
    </br>
    <div>
    <asp:Label ID="Label1" runat="server" Text="Label"><b>IPv4 Address Using User Host Address:</b></asp:Label>
    <asp:Label ID="label2" runat="server" Text="Label"></asp:Label>
    </div></br>
    <div>
    <asp:Label ID="Label3" runat="server" Text="Label"><b>IPv4 Address Using Host Name:</b></asp:Label>
    <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label>
    </div></br>
    <div><h2>Context Header Variables</h2>
    </br>
    <div>
    <asp:Label ID="Label5" runat="server" Text="Label"><b>REMOTE_ADDR:</b></asp:Label>
    <asp:Label ID="labelRA" runat="server" Text="Label"></asp:Label>
    </div></br>
    <div>
    <asp:Label ID="Label7" runat="server" Text="Label"><b>HTTP_VIA:</b></asp:Label>
    <asp:Label ID="LabelHV" runat="server" Text="Label"></asp:Label>
    </div></br>
        <div>
    <asp:Label ID="Label9" runat="server" Text="Label"><b>HTTP_X_FORWARDED_FOR:</b></asp:Label>
    <asp:Label ID="LabelHFF" runat="server" Text="Label"></asp:Label>
    </div></br>
    </div>
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
Application Page
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server" >
</asp:Content>
