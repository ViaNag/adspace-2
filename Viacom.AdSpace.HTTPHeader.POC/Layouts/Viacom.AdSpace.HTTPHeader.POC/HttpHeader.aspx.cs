﻿using System;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using System.Web;
using System.Text;
using System.Configuration;
using System.Net;

namespace Viacom.AdSpace.HTTPHeader.POC.Layouts.Viacom.AdSpace.HTTPHeader.POC
{
    public partial class HttpHeader : LayoutsPageBase
    {
        private HttpContext context = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            context = System.Web.HttpContext.Current;
            GetContextDetails();
            GetIPAddress();
            GetHeaderVariables(context);
        }

        private void GetContextDetails()
        {
            string userName = context.User.Identity.Name;
            string rawURL = context.Request.RawUrl.ToLower();
            string OrgURL = context.Request.Url.ToString().ToLower();
            string url = string.Empty;
            if (this.ValidateURL(OrgURL))
            {
                url = this.ProcessURL(rawURL).ToLower();
                if (!string.IsNullOrEmpty(url))
                {
                    url = HttpContext.Current.Server.UrlDecode(url);
                }
            }
            labelUser1.Text = userName;
            LabelUrl1.Text = url;
            LabelHA1.Text = context.Request.UserHostAddress;
            LabelBrowser1.Text = context.Request.Browser.Browser;
        }


        private void GetIPAddress()
        {
            string ipAddress = context.Request.UserHostAddress;
            string IP4Address = string.Empty;
            foreach (IPAddress IPA in Dns.GetHostAddresses(context.Request.UserHostAddress))
            {
                if (IPA.AddressFamily.ToString() == "InterNetwork")
                {
                    IP4Address = IPA.ToString();
                    label2.Text = IP4Address;
                    break;
                }
            }

            foreach (IPAddress IPA in Dns.GetHostAddresses(Dns.GetHostName()))
            {
                if (IPA.AddressFamily.ToString() == "InterNetwork")
                {
                    IP4Address = IPA.ToString();
                    Label4.Text = IP4Address;
                    break;
                }
            }
        }

        /// <summary>
        /// Get valid url from request
        /// </summary>
        /// <param name="URL">Web requested URL</param>
        /// <returns>formated URL</returns>
        private string ProcessURL(string url)
        {
            string validURL = string.Empty;
            StringBuilder sb = new StringBuilder();
            string[] arrSplitByQs = url.Split('?');
            if (url.Contains("wopiframe"))
            {
                string[] arrSplitAmp = arrSplitByQs[1].Split('&');

                if (arrSplitAmp != null && arrSplitAmp.Length > 0)
                {
                    for (int i = 0; i < arrSplitAmp.Length; i++)
                    {
                        string[] arrSplitEq = arrSplitAmp[i].Split('=');
                        if (arrSplitEq[0].Equals("sourcedoc", StringComparison.OrdinalIgnoreCase))
                        {
                            string modifyURL = SPContext.Current.Web.Site.WebApplication.GetResponseUri(Microsoft.SharePoint.Administration.SPUrlZone.Default).AbsoluteUri;
                            if (modifyURL.EndsWith("/"))
                            {
                                modifyURL = modifyURL.Substring(0, modifyURL.LastIndexOf('/'));
                            }
                            validURL = modifyURL + arrSplitEq[1];

                        }
                    }
                }
            }
            else if (arrSplitByQs[0].EndsWith(".aspx"))
            {
                var uri = new UriBuilder(HttpContext.Current.Request.Url);
                uri.Path = HttpContext.Current.Request.RawUrl;
                validURL = HttpContext.Current.Server.UrlDecode(uri.Uri.AbsoluteUri).Split(new[] { '?' })[0];

            }
            else
            {
                // get url of MS office files
                string filesTypeAllowed = Convert.ToString(ConfigurationManager.AppSettings["AdSpaceAnalyticsAllowFileExtension"]);
                string[] arrAllowFiles = filesTypeAllowed.Split(';');
                if (arrAllowFiles != null && arrAllowFiles.Length > 0)
                {
                    string tempURL = context.Request.Url.GetLeftPart(UriPartial.Path);
                    string reqFileExt = tempURL.Substring(tempURL.LastIndexOf('.') + 1);
                    int fileExistCount = Array.IndexOf(arrAllowFiles, reqFileExt);
                    if (fileExistCount > -1)
                    {
                        var uri = new UriBuilder(HttpContext.Current.Request.Url);
                        uri.Path = HttpContext.Current.Request.RawUrl;
                        validURL = HttpContext.Current.Server.UrlDecode(uri.Uri.AbsoluteUri).Split(new[] { '?' })[0];
                    }
                }
            }
            return validURL;
        }

        /// <summary>
        /// Skip requests except .aspx & office files
        /// </summary>
        /// <param name="URL">Requested URL</param>
        /// <returns>Formated URL</returns>
        private bool ValidateURL(string url)
        {
            bool isValid = true;
            string filesTypeNotAllowed = Convert.ToString(ConfigurationManager.AppSettings["AdSpaceAnalyticsDoNotAllowFileExtension"]);
            if (url.Contains("jquery"))
            {
                isValid = false;
            }
            else
            {
                string[] arrFilesNotAllowed = filesTypeNotAllowed.Split(';');
                if (arrFilesNotAllowed != null && arrFilesNotAllowed.Length > 0)
                {
                    for (int iCount = 0; iCount < arrFilesNotAllowed.Length; iCount++)
                    {
                        if (url.Contains("." + arrFilesNotAllowed[iCount]))
                        {
                            isValid = false;
                            break;
                        }
                    }

                }
            }
            return isValid;
        }

        private void GetHeaderVariables(HttpContext context) 
        {
            if (context.Request.ServerVariables["REMOTE_ADDR"] != null)
            {
                labelRA.Text = context.Request.ServerVariables["REMOTE_ADDR"];
            }
            else
                labelRA.Text = "No value present in the header";
            if (context.Request.ServerVariables["HTTP_VIA"] != null)
            {
                LabelHV.Text = context.Request.ServerVariables["HTTP_VIA"];
            }
            else
                LabelHV.Text = "No value present in the header";
            if (context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                LabelHFF.Text = context.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            }
            else
                LabelHFF.Text = "No value present in the header";
        }
    }
}
