﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json.Linq;
using Viacom.AdSpace.Invoice.InvoiceAdjustment;
using Viacom.AdSpace.Invoice.InvoiceAdjustment.Workflows;
using Viacom.AdSpace.Invoice.InvoiceAdjustment.Actions;
using Viacom.AdSpace.Invoice.InvoiceAdjustment.Entities;
using Viacom.AdSpace.Invoice.InvoiceAdjustment.Data;

namespace Viacom.AdSpace.Invoice.InvoiceAdjustment
{
    public class InvoiceAdjustmentRequest : IInvoiceAdjustmentRequest
    {
        InvoiceAdjustmentActions InvAdjActions
        {
            get 
            {
                return new InvoiceAdjustmentActions();
            }
        }

        public Actions.ActionResponse<List<Network>> GetNetworksList()
        {
            ActionResponse<List<Network>> actionresponse = new ActionResponse<List<Network>>();
            try
            {
                List<Network> networks = null;
                SPView view = null;
                SPList list = null;
                using (SPSite site = SPContext.Current.Site)
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        list = web.Lists[Constants.USERSMAPPINGLIST];
                        if (list != null)
                        {
                            view = list.Views[Constants.USERNETWORKVIEW];
                            if (view != null)
                            {
                                SPListItemCollection items = list.GetItems(view);

                                networks = new List<Network>();
                                foreach (SPListItem item in items)
                                {
                                    networks.Add(
                                        new Network
                                        {
                                            ID = item[Constants.IDFIELD].ToString(),
                                            NetworkValue = item[Constants.NETWORKNAMEFIELD].ToString()
                                        });
                                }
                            }
                        }
                    }
                }
                actionresponse.ActionSuccessful = true;
                actionresponse.Data = networks;
            }
            catch (Exception ex)
            {
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }

            return actionresponse;
        }

        public Actions.ActionResponse<bool> IsCurrentUserInRole(string Role, string NetWork)
        {
            ActionResponse<bool> actionresponse = new ActionResponse<bool>();
            try
            {
                Roles.RoleSource rolesrc = new Roles.RoleSource();
                Roles.Role rl = rolesrc.GetRoleByName(Role);
                if(rl!=null)
                {
                    using (SPSite site = SPContext.Current.Site)
                    {
                        using (SPWeb web = site.OpenWeb())
                        {
                            string querystring = string.Concat(
                                                "<Where><Eq>",
                                                   "<FieldRef Name='", Constants.NETWORKNAMEFIELD, "'/>",
                                                   "<Value Type='Text'>", NetWork, "</Value>",
                                                "</Eq></Where>");
                            string viewstring = string.Concat(
                                                "<FieldRef Name='", rl.AssociatedListColumn, "' />");
                            SPListItemCollection items = InvAdjActions.getListItems(web, Constants.USERSMAPPINGLIST, querystring, viewstring);
                            if (items.Count > 0)
                            {
                                SPListItem item = items[0];
                                string grpName = "";
                                SPGroup grp = null;
                                grpName = InvAdjActions.getStringDataPart(item[Constants.SAFIELD].ToString(), '#', 1);
                                grp = web.SiteGroups[grpName];
                                if (grp != null)
                                {
                                    actionresponse.Data = web.IsCurrentUserMemberOfGroup(grp.ID);
                                }
                            }
                        }
                    }
                }

                actionresponse.ActionSuccessful = true;
            }
            catch (Exception ex)
            {
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;
        }

        public Actions.ActionResponse<bool> IsRequestor(string network)
        {

            return IsCurrentUserInRole("SA", network);
        }


        public Actions.ActionResponse<bool> IsApprover(string itemID)
        {
            ActionResponse<bool> actionresponse = new ActionResponse<bool>();
            actionresponse.Data = false;
            
            try
            {
                using (SPSite site = SPContext.Current.Site)
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        SPListItemCollection requestItems = InvAdjActions.getRequestItemByID(web, itemID);
                        if (requestItems.Count > 0)
                        {
                            SPListItem requestItem = requestItems[0];
                            string networkName = null;
                            networkName = InvAdjActions.getStringDataPart(requestItem[Constants.NETWORKFIELD].ToString(), '#', 1);
                            string formStatus = requestItem[Constants.FORMSTATUSFIELD].ToString();
                            int adjAmt = Int32.Parse(requestItem[Constants.ADJUSTMENTAMTFIELD].ToString());
                            string nextStageGroupColumn = InvAdjActions.getNextStageGroupColumn(formStatus, adjAmt);
                            SPListItemCollection usermappingItems = InvAdjActions.GetUserMappingByNetworknStageGrp(web, networkName, nextStageGroupColumn);
                            if (usermappingItems.Count > 0)
                            {
                                SPListItem item = usermappingItems[0];
                                string grpName = "";
                                SPGroup grp = null;
                                grpName = InvAdjActions.getStringDataPart(item[nextStageGroupColumn].ToString(), '#', 1);
                                grp = web.SiteGroups[grpName];
                                if (grp != null)
                                {
                                    actionresponse.Data = web.IsCurrentUserMemberOfGroup(grp.ID);
                                }
                            }
                        }
                    }
                }

                actionresponse.ActionSuccessful = true;

            }
            catch (Exception ex)
            {
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;
        }

        public Actions.ActionResponse<bool> IsFormEditable(string itemID)
        {
            ActionResponse<bool> actionresponse = new ActionResponse<bool>();
            actionresponse.Data = false;
            try
            {
                using (SPSite site = SPContext.Current.Site)
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        SPListItemCollection requestItems = InvAdjActions.getRequestItemByID(web, itemID);
                        if (requestItems.Count > 0)
                        {
                            SPListItem requestItem = requestItems[0];
                            string networkName = null;
                            string previousApprovalResponse = null;
                            networkName = InvAdjActions.getStringDataPart(requestItem[Constants.NETWORKFIELD].ToString(), '#', 1);
                            previousApprovalResponse = requestItem[Constants.PREVAPPROVALRESPONSEFIELD].ToString();
                            if (IsRequestor(networkName).Data)
                            {
                                if (!String.IsNullOrEmpty(previousApprovalResponse))
                                {
                                    if (previousApprovalResponse.Equals(Constants.REQCHANGESTATUS))
                                    {
                                        actionresponse.Data = true;
                                    }
                                }
                            }
                        }
                    }
                }
                actionresponse.ActionSuccessful = true;
                
            }
            catch (Exception ex)
            {
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
                //throw new Exception(ex.Message, ex);
            }

            return actionresponse;
        }

        public Actions.ActionResponse<Entities.CurrentStatus> GetCurrentRequestStatus(string itemID)
        {
            ActionResponse<CurrentStatus> actionresponse = new ActionResponse<CurrentStatus>();
            try
            {
                CurrentStatus state;
                using (SPSite site = SPContext.Current.Site)
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        state = new CurrentStatus();
                        state.ItemID = itemID;
                        SPListItemCollection requestItems = InvAdjActions.getRequestItemByID(web, itemID);
                        if (requestItems.Count > 0)
                        {
                            SPListItem requestItem = requestItems[0];
                            string networkName = null;
                            networkName = InvAdjActions.getStringDataPart(requestItem[Constants.NETWORKFIELD].ToString(), '#', 1);
                            string formStatus = requestItem[Constants.FORMSTATUSFIELD].ToString();
                            int adjustmentAmt = Int32.Parse(requestItem[Constants.ADJUSTMENTAMTFIELD].ToString());
                            string prevApproverResponse = null;
                            var prevApproverResponseObj = requestItem[Constants.PREVAPPROVALRESPONSEFIELD];
                            if (prevApproverResponseObj != null)
                            {
                                prevApproverResponse = prevApproverResponseObj.ToString();
                            }
                            state.CurrentFormStatus = formStatus;
                            List<InvoiceAdjustmentWorkflowStage> wfStages = new InvoiceAdjustmentInvoiceAdjustmentWorkflowStageSource().Stages;//new InvoiceAdjustmentWorkflowStage().getWorkflowStages();
                            string nextStageGroupColumn = null;
                            string currentStageGroupColumn = null;
                            string grpName = null;

                            foreach (InvoiceAdjustmentWorkflowStage wfstage in wfStages)
                            {
                                if (wfstage.ApprovalStage.Equals(formStatus))
                                {

                                    currentStageGroupColumn = wfstage.StageGroupColumn;
                                    state.IARequestCurrentStateListCol = wfstage.IARequestListCol;
                                    state.IARequestCurrentStateListDateCol = wfstage.IARequestListDateCol;
                                    state.ThresholdAmount = wfstage.ThresholdAmount;
                                    if (state.ThresholdAmount > 0 && state.ThresholdAmount <= adjustmentAmt)
                                    {
                                        state.NextFormStatus = wfstage.ThresholdBreachNextStage;
                                    }
                                    else
                                    {
                                        state.NextFormStatus = wfstage.NextApprovalStage;
                                    }
                                    break;
                                }
                            }
                            foreach (InvoiceAdjustmentWorkflowStage wfstage in wfStages)
                            {
                                if (wfstage.ApprovalStage.Equals(state.NextFormStatus))
                                {
                                    nextStageGroupColumn = wfstage.StageGroupColumn;
                                    state.IARequestNextStateListCol = wfstage.IARequestListCol;
                                    state.IARequestNextStateListDateCol = wfstage.IARequestListDateCol;
                                    state.NextRequestChangeFormStatus = wfstage.ChangeRequestNextStage;
                                    state.IsBillingVisibleForNextStage = wfstage.IsBillingVisible;
                                    state.IsNextStageLastStage = wfstage.IsFinalStage;
                                    break;
                                }
                            }
                            foreach (InvoiceAdjustmentWorkflowStage wfstage in wfStages)
                            {
                                if (wfstage.ApprovalStage.Equals(state.NextRequestChangeFormStatus))
                                {
                                    state.IARequestNextChangeStateListCol = wfstage.IARequestListCol;
                                    state.IARequestNextChangeStateListDateCol = wfstage.IARequestListDateCol;
                                    break;
                                }
                            }
                            SPListItem item = null;
                            SPGroup grp = null;
                            SPListItemCollection usermappingItems = null;
                            string querystring = string.Concat(
                                                   "<Where><Eq>",
                                                      "<FieldRef Name='", Constants.NETWORKNAMEFIELD, "'/>",
                                                      "<Value Type='Text'>", networkName, "</Value>",
                                                   "</Eq></Where>");
                            string viewstring = string.Concat(
                                                                 "<FieldRef Name='", currentStageGroupColumn, "' />",
                                                                 "<FieldRef Name='", nextStageGroupColumn, "' />");

                            usermappingItems = InvAdjActions.getListItems(web, Constants.USERSMAPPINGLIST, querystring, viewstring);
                            if (usermappingItems.Count > 0)
                            {
                                item = usermappingItems[0];
                                grpName = InvAdjActions.getStringDataPart(item[currentStageGroupColumn].ToString(), '#', 1);
                                state.CurrentStateGroup = grpName;
                                grpName = InvAdjActions.getStringDataPart(item[nextStageGroupColumn].ToString(), '#', 1);
                                state.NextStateGroup = grpName;
                                grp = web.SiteGroups[grpName];
                                if (grp != null)
                                {
                                    if (prevApproverResponse != null && prevApproverResponse.Equals(Constants.REQCHANGESTATUS))
                                    {
                                        state.IsUserMemberofNextApproverGroup = false;
                                    }
                                    else
                                    {
                                        state.IsUserMemberofNextApproverGroup = web.IsCurrentUserMemberOfGroup(grp.ID);
                                    }
                                }
                            }
                        }
                    }
                }
                actionresponse.ActionSuccessful = true;
                actionresponse.Data = state;
            }
            catch (Exception ex)
            {
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;            
        }

        public Actions.ActionResponse<bool> IsBillingVisible(string itemID)
        {
            ActionResponse<bool> actionresponse = new ActionResponse<bool>();
            actionresponse.Data = false;
            try
            {
                CurrentStatus currentStatus = GetCurrentRequestStatus(itemID).Data;
                if (currentStatus.IsBillingVisibleForNextStage)
                {
                    actionresponse.Data = true;
                }
                actionresponse.ActionSuccessful = true;
            }
            catch (Exception ex)
            {
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;       
        }

        public Actions.ActionResponse<bool> SaveApproverComment(string itemID, string approvalStatus, string comments)
        {
            ActionResponse<bool> actionresponse = new ActionResponse<bool>();
            actionresponse.Data = false;
            try
            {
                if (approvalStatus.Equals(Constants.APPROVEDSTATUS) ||
                    approvalStatus.Equals(Constants.REJECTSTATUS) ||
                    approvalStatus.Equals(Constants.REQCHANGESTATUS))
                {
                    bool isApprover = IsApprover(itemID).Data;
                    if (isApprover)
                    {
                        CurrentStatus currentStatus = GetCurrentRequestStatus(itemID).Data;
                        using (SPSite site = SPContext.Current.Site)
                        {
                            using (SPWeb web = site.OpenWeb())
                            {


                                SPList list = web.Lists[Constants.IAREQUESTLIST];
                                // Update the List item by ID  
                                SPListItem itemToUpdate = list.GetItemById(Int32.Parse(itemID));
                                if (approvalStatus.Equals(Constants.APPROVEDSTATUS))
                                {
                                    itemToUpdate[Constants.FORMSTATUSFIELD] = currentStatus.NextFormStatus;
                                    itemToUpdate[currentStatus.IARequestNextStateListCol] = web.CurrentUser;
                                    itemToUpdate[currentStatus.IARequestNextStateListDateCol] = DateTime.Now;
                                }
                                if (approvalStatus.Equals(Constants.REJECTSTATUS))
                                {
                                    itemToUpdate[Constants.FORMSTATUSFIELD] = Constants.REJECTSTATUS;
                                    itemToUpdate[currentStatus.IARequestNextStateListCol] = web.CurrentUser;
                                    itemToUpdate[currentStatus.IARequestNextStateListDateCol] = DateTime.Now;
                                }
                                if (approvalStatus.Equals(Constants.REQCHANGESTATUS))
                                {
                                    itemToUpdate[Constants.FORMSTATUSFIELD] = currentStatus.NextRequestChangeFormStatus;
                                    itemToUpdate[currentStatus.IARequestNextChangeStateListCol] = web.CurrentUser;
                                    itemToUpdate[currentStatus.IARequestNextChangeStateListDateCol] = DateTime.Now;
                                }
                                SPFieldUserValueCollection approverValues = (SPFieldUserValueCollection)itemToUpdate[Constants.APPROVERSFIELD];
                                if (approverValues != null)
                                {
                                    approverValues.Add(new SPFieldUserValue(web, web.CurrentUser.ID, web.CurrentUser.Name));
                                    itemToUpdate[Constants.APPROVERSFIELD] = approverValues;
                                }
                                else
                                {
                                    itemToUpdate[Constants.APPROVERSFIELD] = web.CurrentUser;
                                }
                                itemToUpdate[Constants.PREVAPPROVALRESPONSEFIELD] = approvalStatus;
                                itemToUpdate[Constants.FORMPREVSTATUSFIELD] = currentStatus.CurrentFormStatus;
                                itemToUpdate[Constants.COMMENTSFIELD] = comments;
                                itemToUpdate[Constants.MODIFYBYFIELD] = web.CurrentUser;
                                itemToUpdate[Constants.ISRESUBMITTEDFIELD] = false;

                                SPSecurity.RunWithElevatedPrivileges(delegate()
                                {
                                    bool unsafeUpdateState = web.AllowUnsafeUpdates;
                                    web.AllowUnsafeUpdates = true;
                                    itemToUpdate.Update();
                                    web.AllowUnsafeUpdates = unsafeUpdateState;
                                    actionresponse.Data = true;
                                });
                            }
                        }
                    }
                }

                actionresponse.ActionSuccessful = true;

            }
            catch (Exception ex)
            {
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;
        }

        public Actions.ActionResponse<bool> SaveInvoiceChanges(string itemID, List<KeyValuePair<string, string>> kvpList)
        {
            ActionResponse<bool> actionresponse = new ActionResponse<bool>();
            actionresponse.Data = false;
            try
            {
                using (SPSite site = SPContext.Current.Site)
                {
                    using (SPWeb web = site.OpenWeb())
                    {
                        SPListItemCollection requestItems =InvAdjActions.getRequestItemByID(web, itemID);
                        if (requestItems.Count > 0)
                        {
                            SPListItem requestItem = null;
                            requestItem = requestItems[0];
                            string network = InvAdjActions.getStringDataPart(requestItem[Constants.NETWORKFIELD].ToString(), '#', 1);
                            string prevFormStatus = requestItem[Constants.FORMPREVSTATUSFIELD].ToString();
                            string comment = "";
                            string newcomment = "";
                            CurrentStatus currentStatus = GetCurrentRequestStatus(itemID).Data;
                            comment = requestItem[Constants.COMMENTSFIELD].ToString();
                            comment = comment.Substring(0, comment.IndexOf(']'));
                            newcomment = ",{" +
                                "\"UserName\":\"" + web.CurrentUser.Name + "\"," +
                                "\"Group\":\"" + currentStatus.CurrentStateGroup + "\"," +
                                "\"Comments\":\"Updated By SA\"," +
                                "\"Date\":\"" + DateTime.Now.ToString("yyyy-MM-ddThh:mm:ss.fffZ") + "\"}]";
                            comment += newcomment;


                            if (IsFormEditable(itemID).Data)
                            {

                                requestItem = web.Lists[Constants.IAREQUESTLIST].GetItemById(Int32.Parse(itemID));
                                foreach (KeyValuePair<string, string> kvp in kvpList)
                                {
                                    requestItem[kvp.Key] = kvp.Value;
                                }
                                requestItem[Constants.FORMPREVSTATUSFIELD] = currentStatus.CurrentFormStatus;
                                requestItem[Constants.FORMSTATUSFIELD] = prevFormStatus;
                                requestItem[Constants.PREVAPPROVALRESPONSEFIELD] = Constants.UPDATEDSTATUS;
                                requestItem[Constants.IASACRFIELD] = web.CurrentUser;
                                requestItem[Constants.IASACRDATETIMEFIELD] = DateTime.Now;
                                requestItem[Constants.MODIFYBYFIELD] = web.CurrentUser;
                                requestItem[Constants.COMMENTSFIELD] = comment;
                                requestItem[Constants.ISRESUBMITTEDFIELD] = true;
                                SPSecurity.RunWithElevatedPrivileges(delegate()
                                {
                                    bool unsafeUpdateState = web.AllowUnsafeUpdates;
                                    web.AllowUnsafeUpdates = true;
                                    requestItem.Update();
                                    web.AllowUnsafeUpdates = unsafeUpdateState;
                                    actionresponse.Data = true;
                                });
                            }
                        }
                    }
                }
                actionresponse.ActionSuccessful = true;
            }
            catch (Exception ex)
            {
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;
        }

        public Actions.ActionResponse<List<Entities.Brand>> GetBrandsForNetwork(string Network)
        {
            ActionResponse<List<Entities.Brand>> actionresponse = new ActionResponse<List<Entities.Brand>>();
            try
            {
                SQLDataSource sqldata = new SQLDataSource(Constants.CONNECTIONSTRINGKEY,"");
                SQLDataOperationParam dataparam = new SQLDataOperationParam();
                dataparam.SQLCommand = Constants.SP_GETBRANDS;
                dataparam.SQLCommandType = CommandType.StoredProcedure;
                dataparam.CommandParameters = new List<SqlParameter>();
                SqlParameter sqlparam1 = new SqlParameter(Constants.SP_PARA_NETWORKCALLSIGN, SqlDbType.VarChar);
                sqlparam1.Value = Network;
                dataparam.CommandParameters.Add(sqlparam1);
                List<DataRow> allrows = sqldata.GetData(dataparam);
                actionresponse.Data = new List<Brand>();
                foreach(DataRow rw in allrows)
                {
                    actionresponse.Data.Add(new Brand{BrandValue=rw["Brand"].ToString()});
                }
                actionresponse.ActionSuccessful = true;
            }
            catch (Exception ex)
            {
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;
        }

        public Actions.ActionResponse<List<Entities.InvoiceID>> GetInvoiceID(string network, string brand)
        {
            ActionResponse<List<Entities.InvoiceID>> actionresponse = new ActionResponse<List<Entities.InvoiceID>>();
            try
            {
                SQLDataSource sqldata = new SQLDataSource(Constants.CONNECTIONSTRINGKEY,string.Empty);
                SQLDataOperationParam dataparam = new SQLDataOperationParam();
                dataparam.SQLCommand = Constants.SP_GetINVOICEID;
                dataparam.SQLCommandType = CommandType.StoredProcedure;
                dataparam.CommandParameters = new List<SqlParameter>();
                SqlParameter sqlparam1 = new SqlParameter(Constants.SP_PARA_NETWORKCALLSIGN, SqlDbType.VarChar);
                sqlparam1.Value = network;
                SqlParameter sqlparam2 = new SqlParameter(Constants.SP_PARA_BRAND, SqlDbType.VarChar);
                sqlparam2.Value = brand;
                dataparam.CommandParameters.Add(sqlparam1);
                dataparam.CommandParameters.Add(sqlparam2);
                List<DataRow> allrows = sqldata.GetData(dataparam);
                actionresponse.Data = new List<InvoiceID>();
                foreach (DataRow rw in allrows)
                {
                    actionresponse.Data.Add(new InvoiceID { InvoiceIdValue = rw["Invoice_ID"].ToString() });
                }
                actionresponse.ActionSuccessful = true;
            }
            catch (Exception ex)
            {
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;
        }

        public Actions.ActionResponse<Entities.InvoiceDetails> GetInvoiceDetails(string invoiceID)
        {
            ActionResponse<Entities.InvoiceDetails> actionresponse = new ActionResponse<Entities.InvoiceDetails>();
            try
            {
                SQLDataSource sqldata = new SQLDataSource(Constants.CONNECTIONSTRINGKEY);
                SQLDataOperationParam dataparam = new SQLDataOperationParam();
                dataparam.SQLCommand = Constants.SP_GETINVOICEDETAILS;
                dataparam.SQLCommandType = CommandType.StoredProcedure;
                dataparam.CommandParameters = new List<SqlParameter>();
                SqlParameter sqlparam1 = new SqlParameter(Constants.SP_PARA_INVOICEID, SqlDbType.VarChar);
                sqlparam1.Value = invoiceID;                
                dataparam.CommandParameters.Add(sqlparam1);
                List<DataRow> allrows = sqldata.GetData(dataparam);
                if (allrows.Count>0)
                {
                    DataRow dr = allrows[0];
                    InvoiceDetails invoiceDetails = new InvoiceDetails();
                    invoiceDetails.Brand_Year = dr["Brand_Year"].ToString();
                    invoiceDetails.Network_Call_Sign = dr["Network_Call_Sign"].ToString();
                    invoiceDetails.Agency_Current_Invoicing = dr["Agency_Current_Invoicing"].ToString();
                    invoiceDetails.Advertiser_Current = dr["Advertiser_Current"].ToString();
                    invoiceDetails.Traffic_Deal_Code = dr["Traffic_Deal_Code"].ToString();
                    invoiceDetails.Invoice_ID = dr["Invoice_ID"].ToString();
                    invoiceDetails.Invoice_Rev_No = dr["Invoice_Rev_No"].ToString();
                    invoiceDetails.Brand = dr["Brand"].ToString();
                    invoiceDetails.Traffic_Deal_Start_Date = dr["Traffic_Deal_Start_Date"].ToString();
                    invoiceDetails.Traffic_Deal_End_Date = dr["Traffic_Deal_End_Date"].ToString();
                    actionresponse.Data = invoiceDetails;
                }
                actionresponse.ActionSuccessful = true;
            }
            catch (Exception ex)
            {
                actionresponse.ActionSuccessful = false;
                actionresponse.ExceptionIfAny = ex;
            }
            return actionresponse;
        }
    }
}
