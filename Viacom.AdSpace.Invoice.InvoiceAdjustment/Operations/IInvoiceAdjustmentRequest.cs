﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Viacom.AdSpace.Invoice.InvoiceAdjustment;
using Viacom.AdSpace.Invoice.InvoiceAdjustment.Actions;
using Viacom.AdSpace.Invoice.InvoiceAdjustment.Entities;
using Viacom.AdSpace.Invoice.InvoiceAdjustment.Workflows;


namespace Viacom.AdSpace.Invoice.InvoiceAdjustment
{
    public interface IInvoiceAdjustmentRequest
    {
        ActionResponse<List<Network>> GetNetworksList();
        ActionResponse<bool> IsCurrentUserInRole(string Role,string NetWork);
        ActionResponse<bool> IsFormEditable(string itemID);
        ActionResponse<CurrentStatus> GetCurrentRequestStatus(string itemID);
        ActionResponse<bool> IsBillingVisible(string itemID);
        ActionResponse<bool> SaveApproverComment(string itemID, string approvalStatus, string comments);
        ActionResponse<bool> SaveInvoiceChanges(string itemID, List<KeyValuePair<string, string>> kvpList);
        ActionResponse<List<Brand>> GetBrandsForNetwork(string Network);
        ActionResponse<List<InvoiceID>> GetInvoiceID(string network, string brand);
        ActionResponse<InvoiceDetails> GetInvoiceDetails(string invoiceID);
    }
}
