﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

using InvoiceBase = Viacom.AdSpace.Invoice.Base;

namespace Viacom.AdSpace.Invoice.InvoiceAdjustment.Data
{
    public class SQLDataSource : InvoiceBase.Data.IDataSource
    {
        string _connectionstring;

        public SQLDataSource(string webconfigconnectionkey)
        {
           // _connectionstring = connectionstring;
            _connectionstring = Convert.ToString(ConfigurationManager.ConnectionStrings[webconfigconnectionkey]);

            
        }

        public SQLDataSource(string webconfigconnectionkey, string temp)
        {
            //_connectionstring = ConfigurationManager.ConnectionStrings[webconfigconnectionkey].ConnectionString;
            _connectionstring = Convert.ToString(ConfigurationManager.ConnectionStrings[webconfigconnectionkey]);
        }

        SqlConnection _dataconnectiont;
        SqlConnection _dataconnection
        {
            get 
            {
                if (_dataconnectiont == null) _dataconnectiont = new SqlConnection(_connectionstring);
                return _dataconnectiont;
            }
        }

     
        public List<DataRow> GetData(SQLDataOperationParam param)
        {

            try
            {
                _dataconnection.Open();               
                SqlDataAdapter adp = new SqlDataAdapter(param.GetCommand(_dataconnection));
                DataSet ds = new DataSet();
                adp.Fill(ds);
                DataTable dt = ds.Tables[0];
                return dt.AsEnumerable().ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
            finally { _dataconnection.Close(); }
        }
   
  
    }

   
   
}
