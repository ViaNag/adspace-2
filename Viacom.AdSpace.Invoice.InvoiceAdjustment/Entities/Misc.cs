﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using InvoiceBase = Viacom.AdSpace.Invoice.Base;

namespace Viacom.AdSpace.Invoice.InvoiceAdjustment.Entities
{
    public class Brand
    {
        public string BrandValue { get; set; }        
    }

    public class CurrentStatus
    {
        public string ItemID { get; set; }
        public string CurrentFormStatus { get; set; }
        public string NextFormStatus { get; set; }
        public string NextRequestChangeFormStatus { get; set; }
        public string CurrentStateGroup { get; set; }
        public string NextStateGroup { get; set; }
        public bool IsUserMemberofNextApproverGroup { get; set; }
        public string IARequestCurrentStateListCol { get; set; }
        public string IARequestCurrentStateListDateCol { get; set; }
        public string IARequestNextStateListCol { get; set; }
        public string IARequestNextStateListDateCol { get; set; }
        public string IARequestNextChangeStateListCol { get; set; }
        public string IARequestNextChangeStateListDateCol { get; set; }
        public int ThresholdAmount { get; set; }
        public bool IsBillingVisibleForNextStage { get; set; }
        public bool IsNextStageLastStage { get; set; }
    }

    public class InvoiceDetails
    {
        public string Brand_Year { get; set; }
        public string Network_Call_Sign { get; set; }
        public string Agency_Current_Invoicing { get; set; }
        public string Advertiser_Current { get; set; }
        public string Traffic_Deal_Code { get; set; }
        public string Invoice_ID { get; set; }
        public string Invoice_Rev_No { get; set; }
        public string Brand { get; set; }
        public string Traffic_Deal_Start_Date { get; set; }
        public string Traffic_Deal_End_Date { get; set; }
    }

    public class InvoiceID
    {
        public string InvoiceIdValue { get; set; }
    }

    public class Network
    {
        public string ID { get; set; }
        public string NetworkValue { get; set; }
    }


}
