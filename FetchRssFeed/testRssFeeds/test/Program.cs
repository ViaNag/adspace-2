﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace test
{
    class Program
    {
        static void Main(string[] args)
        {

            FileStream ostrm;
            StreamWriter writer;
            TextWriter oldOut = Console.Out;
            try
            {
                ostrm = new FileStream(@"D:\Adspace\test\log.txt", FileMode.OpenOrCreate, FileAccess.Write);
                writer = new StreamWriter(ostrm);
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot open log.txt for writing");
                Console.WriteLine(e.Message);
                return;
            }
            Console.SetOut(writer);
            string csv_file_path = @"D:\Adspace\test\links.csv";
            bool isRowOneHeader = true;
            DataTable DT = new DataTable();
            String[] liData = File.ReadAllLines(csv_file_path);
            if (liData .Length == 0)
            {
                throw new Exception("liData File Appears to be Empty");
            }
            String[] headings = liData [0].Split('\t');
            int index = 0;
            if (isRowOneHeader)
            {
                index = 0;
                for (int i = 0; i < headings.Length; i++)
                {
                    headings[i] = headings[i].Replace(" ", "_");
                    DT.Columns.Add(headings[i], typeof(string));
                }
            }
            else
            {
                for (int i = 0; i < headings.Length; i++)
                {
                    DT.Columns.Add("col" + (i + 1).ToString(), typeof(string));
                }
            }
            char[] delimiterChars = { '"' };
            for (int i = index; i < liData.Length; i++)
            {
                if (liData[i] == "'")
                    continue;
                else
                {
                    DataRow row = DT.NewRow();
                    for (int j = 0; j < headings.Length; j++)
                    {
                        string[] rowData = liData[i].Split('\t');
                        if (rowData.Length <= j)
                            continue;
                        row[j] = rowData[j].Trim(delimiterChars);
                        if (liData[i] != "'" && liData[i] != "")
                        {
                            string s = row[j].ToString();
                            s = s.Replace("'", "");
                            row[j] = s.Replace(",", "");
                        }
                    }
                    DT.Rows.Add(row);
                }
            }


            Console.WriteLine("Feed Urls in CSV "+(DT.Rows.Count-1));
            for (int k = 1; k < DT.Rows.Count; k++)
            {
                try
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                    string url = DT.Rows[k].ItemArray[0].ToString();
                    Console.WriteLine();
                    Console.WriteLine("\n"+url);
                    //string url = "http://www.ft.com/rss/companies/media";
                    int pageCount = 0;
                    int pageSize = 5;
                    string feed_outerName = "content";
                    string feed_attribute = "url";

                    XmlReader reader = XmlReader.Create(url);

                    IList<SyndicationItem> items = null;
                    List<NewsFeedItem> newsFeedItems = new List<NewsFeedItem>();

                    SyndicationFeed feed = SyndicationFeed.Load(reader);

                    if (Convert.ToInt32(pageCount) == 0)
                    {
                        items = feed.Items.Take(Convert.ToInt32(pageSize)).ToList();
                    }


                    RSSFeedObject obj = new RSSFeedObject();

                    // get news feed item from syndication item
                    foreach (SyndicationItem synItem in items)
                    {
                        NewsFeedItem feedItem = new NewsFeedItem(synItem, feed_outerName, feed_attribute);
                        newsFeedItems.Add(feedItem);
                    }


                    obj.Items = newsFeedItems;
                    obj.Source = feed.Title.Text;

                    if (obj != null)
                    {
                        Console.WriteLine("its working fine");

                        Console.WriteLine("total newsfeed returned:" + newsFeedItems.Count + "\n");
                        for (int i = 0; i < newsFeedItems.Count; i++)
                        {
                            Console.WriteLine(newsFeedItems[i].Item.Title.Text);
                        }

                        //Console.Read();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
            Console.SetOut(oldOut);
            writer.Close();
            ostrm.Close();
            Console.WriteLine("Done");
            Console.Read();
        }
    }

    public class RSSFeedObject
    {
        /// <summary>
        /// Source of the site
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// Source of the site
        /// </summary>
        public string ImageUrl { get; set; }

        /// <summary>
        /// Items
        /// </summary>
        public IList<NewsFeedItem> Items { get; set; }
    }

    /// <summary>
    /// News item details
    /// </summary>
    public class NewsFeedItem
    {

        public SyndicationItem Item { get; set; }

        public string ImageUrl { get; set; }

        //Constructer
        public NewsFeedItem(SyndicationItem synItem, string outerName, string attribute)
        {
            this.Item = synItem;
            this.SetThumbnailImageSource(outerName, attribute);
        }

        //Set the image url property
        private void SetThumbnailImageSource(string outerName, string attribute)
        {

            var outerNameArray = outerName.Split(',');
            var attributeArray = attribute.Split(',');
            string img1 = string.Empty;
            string img2 = string.Empty;

            if (Item != null)
            {
                foreach (SyndicationElementExtension synEx in Item.ElementExtensions)
                {

                    var index = Array.IndexOf(outerNameArray, synEx.OuterName);
                    if (index >= 0)
                    {
                        XmlReader r = synEx.GetReader();
                        img1 = r.GetAttribute(attributeArray[index]);
                        while (r.Read())
                        {
                            if (r.NodeType == XmlNodeType.Element)
                            {
                                img2 = r.GetAttribute(attributeArray[index]);
                                break;
                            }

                        }

                    }
                }
            }

            //Use img1 if img2 not available.
            this.ImageUrl = img1;
            if (!string.IsNullOrEmpty(img2))
            {
                this.ImageUrl = img2;
            }


        }



    }
}
