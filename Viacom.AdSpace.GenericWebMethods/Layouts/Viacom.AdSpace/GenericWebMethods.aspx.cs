﻿using System;
using System.Web.Services;
using Microsoft.SharePoint;
using Microsoft.SharePoint.WebControls;
using Microsoft.SharePoint.Administration;
using Newtonsoft.Json.Linq;
using Viacom.AdSpace.GenericWebMethods.Classes;

namespace Viacom.AdSpace.Layouts.Viacom.AdSpace.GenericWebMethods
{
    public partial class GenericWebMethods : LayoutsPageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #region WebMethods

        /// <summary>
        /// Promote link from parent link list
        /// </summary>
        /// <param name="requestSubmitted">Json: {siteUrl:"<Source Site Url>",listName:"<Source Links list>",itemId:"<source item id>"}</param>
        [WebMethod]
        public static string PromoteToRootSite(string requestSubmitted)
        {
            string response = string.Empty;
            string siteURL = string.Empty;
            string listName = string.Empty;
            string itemId = string.Empty;
            string parentUrl = string.Empty;


            try
            {
                JObject results = JObject.Parse(requestSubmitted);
                siteURL = Convert.ToString(results[Constants.SiteUrl]);
                listName = Convert.ToString(results[Constants.ListName]);
                using (SPSite sourceSite = new SPSite(siteURL))
                {
                    using (SPWeb webSource = sourceSite.OpenWeb())
                    {
                        if (sourceSite != null)
                        {
                            itemId = Convert.ToString(results[Constants.ItemId]);
                            SPList listSource = webSource.Lists[listName];
                            SPListItem itemSource = listSource.GetItemById(Convert.ToInt32(itemId));

                            parentUrl = sourceSite.WebApplication.GetResponseUri(SPUrlZone.Default).ToString();
                            if (parentUrl.Length > 0)
                            {
                                using (SPSite siteDest = new SPSite(parentUrl))
                                {
                                    using (SPWeb webDest = siteDest.OpenWeb())
                                    {
                                        SPList listDest = webDest.Lists[listName];
                                        SPQuery query = new SPQuery();
                                        query.Query = "<Where><And><Eq><FieldRef Name='Title' /><Value Type='Text'>" + Convert.ToString(itemSource["Title"])
                                            + "</Value></Eq><Eq><FieldRef Name='LinkSource' /><Value Type='Text'>" + webSource.Title + "</Value></Eq></And></Where>";
                                        SPListItemCollection items = listDest.GetItems(query);
                                        if (items != null && items.Count > 0)
                                        {
                                            foreach (SPListItem updateItem in items)
                                            {
                                                updateItem["Title"] = itemSource["Title"];
                                                updateItem["LinkURL"] = itemSource["LinkURL"];
                                                updateItem["LinkAccessTo"] = itemSource["LinkAccessTo"];
                                                updateItem["IsFeaturedLink"] = itemSource["IsFeaturedLink"];
                                                updateItem["LinkSource"] = webSource.Title;
                                                updateItem["IsPromoted"] = itemSource["IsPromoted"];
                                                updateItem["IsHeaderLink"] = itemSource["IsHeaderLink"];
                                                updateItem["LinksCategory"] = itemSource["LinksCategory"];
                                                updateItem.SystemUpdate();
                                                response = "Item Updated at destination site";
                                            }
                                        }
                                        else
                                        {
                                            SPListItem newItem = listDest.AddItem();
                                            newItem["Title"] = itemSource["Title"];
                                            newItem["LinkURL"] = itemSource["LinkURL"];
                                            newItem["LinkAccessTo"] = itemSource["LinkAccessTo"];
                                            newItem["IsFeaturedLink"] = itemSource["IsFeaturedLink"];
                                            newItem["LinkSource"] = webSource.Title;
                                            newItem["IsPromoted"] = itemSource["IsPromoted"];
                                            newItem["IsHeaderLink"] = itemSource["IsHeaderLink"];
                                            newItem["LinksCategory"] = itemSource["LinksCategory"];
                                            newItem.SystemUpdate();
                                            response = "Item added at destination site";
                                        }
                                    }
                                }
                            }
                            else
                            {
                                response = "Could not open Destination site";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response = ex.Message;
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("AddItemToList", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                });
            }
            return response;
        }

        /// <summary>
        /// Unpromote link from parent link list
        /// </summary>
        /// <param name="requestSubmitted">Json: {siteUrl:"<Source Site Url>",listName:"<Source Links list>",itemId:"<source item id>"}</param>
        [WebMethod]
        public static string RemovePromotedFromRootSite(string requestSubmitted)
        {
            string response = string.Empty;
            string siteURL = string.Empty;
            string listName = string.Empty;
            string itemId = string.Empty;
            string parentUrl = string.Empty;


            try
            {
                JObject results = JObject.Parse(requestSubmitted);
                siteURL = Convert.ToString(results[Constants.SiteUrl]);
                listName = Convert.ToString(results[Constants.ListName]);
                using (SPSite sourceSite = new SPSite(siteURL))
                {
                    using (SPWeb webSource = sourceSite.OpenWeb())
                    {
                        if (sourceSite != null)
                        {
                            itemId = Convert.ToString(results[Constants.ItemId]);
                            SPList listSource = webSource.Lists[listName];
                            SPListItem itemSource = listSource.GetItemById(Convert.ToInt32(itemId));

                            parentUrl = sourceSite.WebApplication.GetResponseUri(SPUrlZone.Default).ToString();
                            if (parentUrl.Length > 0)
                            {
                                using (SPSite siteDest = new SPSite(parentUrl))
                                {
                                    using (SPWeb webDest = siteDest.OpenWeb())
                                    {
                                        SPList listDest = webDest.Lists[listName];
                                        SPQuery query = new SPQuery();
                                        query.Query = "<Where><And><Eq><FieldRef Name='Title' /><Value Type='Text'>" + Convert.ToString(itemSource["Title"])
                                            + "</Value></Eq><Eq><FieldRef Name='LinkSource' /><Value Type='Text'>" + webSource.Title + "</Value></Eq></And></Where>";
                                        SPListItemCollection items = listDest.GetItems(query);
                                        
                                        if (items != null && items.Count > 0)
                                        {
                                            int itemCount = items.Count;
                                            for (int i = 0; i < itemCount; i++)
                                            {
                                                items.Delete(i);
                                                response = "Item deleted at destination site.";
                                            }
                                        }
                                        else
                                        {
                                            response = "Could not find item at destination site.";
                                        }
                                    }
                                }
                            }
                            else
                            {
                                response = "Could not open destination site.";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response = ex.Message;
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("AddItemToList", TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
                });
            }
            return response;
        }

        #endregion
    }
}
